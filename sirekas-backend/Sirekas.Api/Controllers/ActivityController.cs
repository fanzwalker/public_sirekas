﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/programs/{programId}/activities")]
  [Authorize]
  public class ActivityController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public ActivityController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int programId, [FromQuery]CommonFiltersDto filters)
    {
      try
      {
        if (!await _unitOfWork.Program.Find(p => p.Id == programId)) return NotFound();

        var models = await _unitOfWork.Activity
          .GetAllActivity(programId, filters.UnitId, filters.ExcludeType,
            filters.ActivityType, filters.Stages);

        return Ok(_mapper.Map<IEnumerable<ActivityDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetActivity")]
    public async Task<IActionResult> Get(int programId, int id)
    {
      try
      {
        var model = await _unitOfWork.Activity.Get(id);

        if (model == null) return NotFound();

        return Ok(_mapper.Map<ActivityDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [Authorize(Policy = "Administrator")]
    [HttpPost]
    public async Task<IActionResult> Post(int programId, [FromBody]ActivityCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (!await _unitOfWork.Program.Find(p => p.Id == programId)) return NotFound();

        if (await _unitOfWork.Activity.Find(a => a.ProgramId == programId && a.Code == dto.Code))
          throw new InvalidOperationException("Kode kegiatan sudah dipakai di program yang sama");

        var model = _mapper.Map<Activity>(dto);
        model.ProgramId = programId;

        _unitOfWork.Activity.Add(model);

        if (!await _unitOfWork.Complete()) return BadRequest();

        return CreatedAtRoute("GetActivity", new { model.Id }, _mapper.Map<ActivityDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [Authorize(Policy = "Administrator")]
    [HttpPut("{id}")]
    public async Task<IActionResult> Post(int programId, int id, [FromBody]ActivityCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.Activity.Get(id);

        if (model == null) return NotFound();

        if (await _unitOfWork.Activity.Find(a => a.Id != id && a.Code == dto.Code && a.ProgramId == programId))
          throw new InvalidOperationException("Kode kegiatan sudah terpakai");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<ActivityDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [Authorize(Policy = "Administrator")]
    [HttpDelete]
    public async Task<IActionResult> Post([FromBody]NumbersDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        foreach (var id in dto.Ids)
        {
          var activity = await _unitOfWork.Activity.Get(id);

          if (activity == null) continue;

          if (await _unitOfWork.UnitActivity.Find(u => u.ActivityId == activity.Id))
            throw new InvalidOperationException($"Kegiatan \"{activity.Code} {activity.Name}\" sudah dipakai di RKA.");

          _unitOfWork.Activity.Remove(activity);
        }

        if (!await _unitOfWork.Complete()) return BadRequest();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
