﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/student-datas")]
  [Authorize]
  public class StudentDataController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public StudentDataController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId)
    {
      try
      {
        var models = await _unitOfWork.StudentData.GetAll(s => s.UnitId == unitId);

        return Ok(_mapper.Map<IEnumerable<StudentDataDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetStudentData")]
    public async Task<IActionResult> Get(int unitId, int id)
    {
      try
      {
        var model = await _unitOfWork.StudentData.Get(s => s.UnitId == unitId && s.Id == id);

        return Ok(_mapper.Map<StudentDataDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    public async Task<IActionResult> Post(int unitId, [FromBody]StudentDataCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId))
          return NotFound("Unit tidak ditemukan");

        var model = _mapper.Map<StudentData>(dto);

        model.UnitId = unitId;

        _unitOfWork.StudentData.Add(model);

        await _unitOfWork.Complete();

        return CreatedAtRoute("GetStudentData", new { model.Id }, _mapper.Map<StudentDataDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut]
    public async Task<IActionResult> Put(int unitId, int id, [FromBody]StudentDataCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId))
          return NotFound("Unit tidak ditemukan");

        var model = await _unitOfWork.StudentData.Get(id);

        if (model == null) return NotFound("Data tidak ditemukan");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<StudentDataDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete]
    public async Task<IActionResult> Delete(int unitId, int id)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId))
          return NotFound("Unit tidak ditemukan");

        var model = await _unitOfWork.StudentData.Get(id);

        if (model == null) return NotFound("Data tidak ditemukan");

        _unitOfWork.StudentData.Remove(model);

        await _unitOfWork.Complete();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
