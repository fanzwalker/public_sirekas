﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/last-year-balance")]
  [Authorize]
  public class LastYearBalanceController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public LastYearBalanceController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId)
    {
      try
      {
        var model = await _unitOfWork.LastYearBalance.GetLastYearBalance(l => l.UnitId == unitId);

        return Ok(_mapper.Map<LastYearBalanceDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut]
    public async Task<IActionResult> Get(int unitId, [FromBody] LastYearBalanceCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId))
          return NotFound("Unit tidak ditemukan");

        var model = await _unitOfWork.LastYearBalance.GetLastYearBalance(l => l.UnitId == unitId);

        var newBalance = _mapper.Map<LastYearBalance>(dto);

        if (model == null)
        {
          newBalance.UnitId = unitId;

          _unitOfWork.LastYearBalance.Add(newBalance);

          await _unitOfWork.Complete();

          var returnModel = await _unitOfWork.LastYearBalance.GetLastYearBalance(l => l.Id == newBalance.Id);

          return Ok(_mapper.Map<LastYearBalanceDto>(returnModel));
        }

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<LastYearBalanceDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
