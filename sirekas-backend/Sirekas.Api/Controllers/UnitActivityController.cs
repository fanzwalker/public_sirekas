﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/stages/{stagesId}/programs/{programId}/activities")]
  [Authorize]
  public class UnitActivityController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UnitActivityController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, Stages stagesId,
      int programId, [FromQuery] ActivityType activityType = ActivityType.Both)
    {
      var models = activityType == ActivityType.Both
        ? await _unitOfWork.UnitActivity
          .GetAllUnitActivity(u => u.UnitId == unitId && u.Stages == stagesId && u.Activity.ProgramId == programId)
        : await _unitOfWork.UnitActivity.GetAllUnitActivity(u =>
          u.UnitId == unitId && u.Stages == stagesId && u.Activity.ProgramId == programId &&
          u.Activity.ActivityType == activityType);

      return Ok(_mapper.Map<IEnumerable<UnitActivityDto>>(models));
    }

    [HttpGet("{activityId}", Name = "GetUnitActivity")]
    public async Task<IActionResult> Get(int unitId, Stages stagesId, int programId, int activityId)
    {
      var model = await _unitOfWork.UnitActivity
        .GetUnitActivity(u =>
          u.UnitId == unitId && u.Stages == stagesId && u.Activity.ProgramId == programId &&
          u.ActivityId == activityId);

      if (model == null) return NotFound();

      return Ok(_mapper.Map<UnitActivityDto>(model));
    }

    //[HttpPost]
    //public async Task<IActionResult> Post(int unitId, Stages stagesId, int programId,
    //  [FromBody]IEnumerable<UnitActivityCreateDto> dtos)
    //{
    //  if (!ModelState.IsValid) return BadRequest(ModelState);

    //  if (!await _unitOfWork.Unit.Find(u => u.Id == unitId)) return NotFound();

    //  if (!await _unitOfWork.Program.Find(p => p.Id == programId)) return NotFound();

    //  var models = _mapper.Map<IEnumerable<UnitActivity>>(dtos);

    //  var unitActivities = models.ToList();

    //  unitActivities.ForEach(u =>
    //  {
    //    u.UnitId = unitId;
    //    u.Stages = stagesId;
    //  });

    //  _unitOfWork.UnitActivity.AddRange(unitActivities);

    //  if (!await _unitOfWork.Complete()) return BadRequest();

    //  var returnModels = await _unitOfWork.UnitActivity.GetAllUnitActivity(u =>
    //    u.UnitId == unitId && u.Stages == stagesId && u.Activity.ProgramId == programId);

    //  return Ok(_mapper.Map<IEnumerable<UnitActivityDto>>(returnModels));
    //}

    //[HttpPut]
    //public async Task<IActionResult> Put(int unitId, Stages stagesId,
    //  int programId, int activityId, [FromBody]UnitActivityUpdateDto dto)
    //{
    //  if (!ModelState.IsValid) return BadRequest(ModelState);

    //  var model = await _unitOfWork.UnitActivity
    //    .GetUnitActivity(u =>
    //      u.UnitId == unitId && u.Stages == stagesId && u.Activity.ProgramId == programId &&
    //      u.ActivityId == activityId);

    //  if (model == null) return NotFound();

    //  _mapper.Map(dto, model);

    //  if (!await _unitOfWork.Complete()) return BadRequest();

    //  return Ok(_mapper.Map<UnitActivityDto>(model));
    //}

    //[HttpDelete]
    //public async Task<IActionResult> Delete(int unitId, Stages stagesId, int programId,
    //  [FromBody]NumbersDto dtos)
    //{
    //  if (!ModelState.IsValid) return BadRequest(ModelState);

    //  if (!await _unitOfWork.Unit.Find(u => u.Id == unitId)) return NotFound();

    //  if (!await _unitOfWork.Program.Find(p => p.Id == programId)) return NotFound();

    //  foreach (var activityId in dtos.Ids)
    //  {
    //    var unitActivity = await _unitOfWork.UnitActivity.Get(u => u.ActivityId == activityId);

    //    if (unitActivity == null) continue;

    //    _unitOfWork.UnitActivity.Remove(unitActivity);
    //  }

    //  if (!await _unitOfWork.Complete()) return BadRequest();

    //  return Ok();
    //}
  }
}
