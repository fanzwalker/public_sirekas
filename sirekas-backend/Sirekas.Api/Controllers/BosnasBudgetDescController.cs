﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Enums;
using System;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/stages/{stageId}/units/{unitId}/bosnas-budget-desc")]
  [Authorize]
  public class BosnasBudgetDescController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public BosnasBudgetDescController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, Stages stageId)
    {
      try
      {
        var model = await _unitOfWork.BosnasBudgetDesc.GetBosnasBudgetDesc(unitId, stageId);

        return Ok(_mapper.Map<BosnasBudgetDescDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
