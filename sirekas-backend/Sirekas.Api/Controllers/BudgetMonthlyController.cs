﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/activities/{activityId}/stages/{stageId}/budget-monthlies")]
  [Authorize]
  public class BudgetMonthlyController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public BudgetMonthlyController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, int activityId, Stages stageId)
    {
      var models = await _unitOfWork.BudgetMonthly
        .GetBudgetMonthly(unitId, activityId, stageId);

      return Ok(_mapper.Map<IEnumerable<BudgetMonthlyDto>>(models));
    }

    [HttpPut("{budgetMonthlyId}")]
    public async Task<IActionResult> Put(int unitId, int activityId, Stages stageId, int budgetMonthlyId,
      [FromBody] BudgetMonthlyUpdateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.BudgetMonthly
          .GetBudgetMonthly(budgetMonthlyId);

        if (model == null) return NotFound();

        var totalQ = dto.Q1 + dto.Q2 + dto.Q3 + dto.Q4;

        if (totalQ != model.BudgetDet.Total)
          throw new InvalidOperationException("Total pengurangan/penambahan anggaran kas tidak " +
                                              $"sama dengan pagu rekening yang tersedia Rp. {model.BudgetDet.Total:C}");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<BudgetMonthlyDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
