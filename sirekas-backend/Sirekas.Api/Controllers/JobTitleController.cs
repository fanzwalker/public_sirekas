﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/job-titles")]
  [Authorize]
  public class JobTitleController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public JobTitleController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get([FromQuery]UnitStructure unitStructure = UnitStructure.SubDinasSubUnitBagian)
    {
      try
      {
        var models = await _unitOfWork.JobTitle.GetJobTitles(unitStructure);
        return Ok(_mapper.Map<IEnumerable<JobTitleDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetJobTitle")]
    public async Task<IActionResult> Get(int id)
    {
      try
      {
        var model = await _unitOfWork.JobTitle.Get(id);

        if (model == null) return NotFound();

        return Ok(_mapper.Map<JobTitleDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}/units/{unitId}/employees")]
    public async Task<IActionResult> Get(int id, int unitId)
    {
      try
      {
        var model = await _unitOfWork.JobTitle
          .GetJobTitleWithEmployees(j => j.Id == id && j.Employees.Any(e => e.UnitId == unitId));

        if (model == null) return NotFound();

        return Ok(_mapper.Map<JobTitleEmpDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
