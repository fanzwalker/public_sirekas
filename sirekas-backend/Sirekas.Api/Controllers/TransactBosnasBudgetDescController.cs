﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/transact/units/{unitId}/stages/{stageId}/bosnas-budget-desc")]
  [Authorize]
  public class TransactBosnasBudgetDescController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly SirekasDbContext _context;

    public TransactBosnasBudgetDescController(IUnitOfWork unitOfWork, IMapper mapper, SirekasDbContext context)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _context = context;
    }

    [HttpPut]
    public async Task<IActionResult> Put(int unitId, Stages stageId, [FromBody]TransactBosnasDescCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId))
          return NotFound();

        var act = await _unitOfWork.Activity.Get(a => a.ActivityType == ActivityType.Bosnas);

        if (act == null) return NotFound("Kegiatan tidak ditemukan");

        using (var transction = await _context.Database.BeginTransactionAsync())
        {
          try
          {
            var bosnasDesc = await _unitOfWork.BosnasBudgetDesc.GetBosnasBudgetDesc(unitId, stageId);

            var bosnasDescModel = _mapper.Map<BosnasBudgetDesc>(dto.BosnasBudgetDesc);

            if (bosnasDesc == null)
            {
              bosnasDescModel.UnitId = unitId;
              bosnasDescModel.Stages = stageId;

              _unitOfWork.BosnasBudgetDesc.Add(bosnasDescModel);

              await _unitOfWork.Complete();

              _unitOfWork.UnitActivity.Add(new UnitActivity
              {
                Stages = stageId,
                ActivityId = act.Id,
                UnitId = unitId,
                Total = bosnasDescModel.Total
              });

              await _unitOfWork.Complete();

              transction.Commit();

              return Ok();
            }

            var unitAct = await _unitOfWork.UnitActivity
              .Get(u => u.UnitId == unitId &&
                        u.Activity.ActivityType == ActivityType.Bosnas);

            unitAct.Total = dto.BosnasBudgetDesc.StandardPrice *
                            dto.BosnasBudgetDesc.TotalStudent;

            _mapper.Map(dto.BosnasBudgetDesc, bosnasDesc);

            await _unitOfWork.Complete();

            transction.Commit();

            return Ok();
          }
          catch (Exception e)
          {
            transction.Rollback();
            ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
            return BadRequest(ModelState);
          }
        }
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
