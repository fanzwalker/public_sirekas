﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/web-option")]
  [Authorize(Policy = "Administrator")]
  public class WebOptionController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public WebOptionController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get([FromQuery]string name)
    {
      try
      {
        var models = await _unitOfWork.WebOption.Get(name);

        return Ok(models);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetWebOption")]
    public async Task<IActionResult> Get(int id)
    {
      try
      {
        var model = await _unitOfWork.WebOption.Get(id);

        if (model == null) return NotFound();

        return Ok(model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    public async Task<IActionResult> Post([FromBody]WebOptionCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest();

        var model = _mapper.Map<WebOption>(dto);

        _unitOfWork.WebOption.Add(model);

        await _unitOfWork.Complete();

        return CreatedAtRoute("GetWebOption", new { model.Id }, model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int id, [FromBody] WebOptionCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest();

        var model = await _unitOfWork.WebOption.Get(id);

        if (model == null) return NotFound();

        if (await _unitOfWork.WebOption.Find(w => w.Id != id && w.Name == dto.Name))
          return BadRequest("Kode tidak boleh sama");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
      try
      {
        var model = await _unitOfWork.WebOption.Get(id);

        if (model == null) return NotFound();

        _unitOfWork.WebOption.Remove(model);

        await _unitOfWork.Complete();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
