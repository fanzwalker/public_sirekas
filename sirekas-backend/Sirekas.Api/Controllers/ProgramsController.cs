﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/programs")]
  [Authorize]
  public class ProgramsController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public ProgramsController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get([FromQuery]int? unitId)
    {
      try
      {
        var models = await _unitOfWork.Program
          .GetPrograms(unitId);

        return Ok(_mapper.Map<IEnumerable<ProgramDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetProgram")]
    public async Task<IActionResult> Get(int id)
    {
      try
      {
        var models = await _unitOfWork.Program
          .Get(id);

        return Ok(_mapper.Map<ProgramDto>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [Authorize(Policy = "Administrator")]
    [HttpPost]
    public async Task<IActionResult> Post([FromBody]ProgramCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = _mapper.Map<Domain.Entities.Program>(dto);

        if (await _unitOfWork.Program.Find(p => p.Code == dto.Code && p.UnitId == dto.UnitId))
          throw new InvalidOperationException("Kode sudah dipakai di daftar program");

        _unitOfWork.Program.Add(model);

        if (!await _unitOfWork.Complete()) return BadRequest();

        return CreatedAtRoute("GetProgram", new { model.Id },
          _mapper.Map<ProgramDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [Authorize(Policy = "Administrator")]
    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int id, [FromBody]ProgramUpdateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.Program.Get(id);

        if (model == null) return NotFound();

        if (await _unitOfWork.Program.Find(p => p.Code == dto.Code &&
                                                p.UnitId == model.UnitId &&
                                                p.Id != id))
          throw new InvalidOperationException("Kode sudah dipakai di daftar program");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<ProgramDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [Authorize(Policy = "Administrator")]
    [HttpDelete]
    public async Task<IActionResult> Delete([FromBody]NumbersDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        foreach (var id in dto.Ids)
        {
          var program = await _unitOfWork.Program.Get(id);

          if (program == null) continue;

          if (await _unitOfWork.Activity.Find(a => a.ProgramId == program.Id))
            throw new InvalidOperationException($"Program \"{program.Code} {program.Name}.\" memiliki kegiatan.");

          _unitOfWork.Program.Remove(program);
        }

        if (!await _unitOfWork.Complete()) return BadRequest();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }

    }
  }
}
