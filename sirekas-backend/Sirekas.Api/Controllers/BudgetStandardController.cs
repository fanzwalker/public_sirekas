﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Enums;
using System;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/standard-types/{standardTypeId}/budget-standards")]
  [Authorize]
  public class BudgetStandardController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public BudgetStandardController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(StandardType standardTypeId,
      [FromQuery]EducationLvl educationLvl = 0)
    {
      try
      {
        var models = await _unitOfWork.BudgetStandard.GetAll(standardTypeId, educationLvl);

        return Ok(models);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    [Authorize(Policy = "Administrator")]
    public async Task<IActionResult> Put(int id, [FromBody]BudgetStandardCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.BudgetStandard.Get(id);

        if (model == null) return NotFound();

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
