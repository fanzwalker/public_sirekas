﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Data.Repository;
using Sirekas.Domain.Enums;
using System;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/trans-type/{transTypeId}/coa-apbs")]
  [Authorize]
  public class CoaApbsController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public CoaApbsController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(TransType transTypeId, [FromQuery]string parentCode)
    {
      try
      {
        var models = await _unitOfWork.CoaApbs.GetCoaApbs(transTypeId, parentCode);

        return Ok(models);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
