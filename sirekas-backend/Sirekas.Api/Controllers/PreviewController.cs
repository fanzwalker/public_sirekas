﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Domain.Enums;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/previews")]
  [Authorize]
  public class PreviewController : Controller
  {
    private readonly DbConnection _connection;

    public PreviewController(DbConnection connection)
    {
      _connection = connection;
    }

    [HttpGet("apbs/expenses-type/{expensesType}/stages/{stageId}")]
    public async Task<IActionResult> Get(int expensesType, int stageId, int? unitId, CoaLevel coaLevel)
    {
      using (IDbConnection dbConnection = _connection)
      {
        dbConnection.Open();

        if (expensesType == 1)
          return Ok(await dbConnection.QueryAsync<ApbsBtlDto>("ApbsBtl",
            new { unitId, stageId, coaLevel }, commandType: CommandType.StoredProcedure));


        return Ok(await dbConnection.QueryAsync("ApbsBl",
            new { unitId, stageId }, commandType: CommandType.StoredProcedure));
      }
    }

    [HttpGet("apbs-sbdana/stages/{stageId}")]
    public async Task<IActionResult> Get(int stageId, [FromQuery] int? unitId)
    {
      using (IDbConnection dbConnection = _connection)
      {
        dbConnection.Open();

        return Ok(await dbConnection.QueryAsync<ApbsSbDanaDto>("ApbsSbDana",
          new { unitId, stageId }, commandType: CommandType.StoredProcedure));
      }
    }

    [HttpGet("rka2/unit/{unitId}/stages/{stageId}/activities/{activityId}")]
    public async Task<IActionResult> Get(int unitId, Stages stageId, int activityId)
    {
      using (IDbConnection dbConnection = _connection)
      {
        dbConnection.Open();

        return Ok(await dbConnection.QueryAsync<Rka2Dto>("Rka2",
          new { unitId, stageId, activityId }, commandType: CommandType.StoredProcedure));
      }
    }

    [HttpGet("rka221-tw/unit/{unitId}/stages/{stageId}/activity-type/{activityTypeId}")]
    public async Task<IActionResult> Get(int unitId, Stages stageId, ActivityType activityTypeId)
    {
      using (IDbConnection dbConnection = _connection)
      {
        dbConnection.Open();

        return Ok(await dbConnection.QueryAsync("Rka221Tw",
          new { unitId, stageId, activityTypeId }, commandType: CommandType.StoredProcedure));
      }
    }

    [HttpGet("apbs-rincian/unit/{unitId}/stages/{stageId}")]
    public async Task<IActionResult> Get(int unitId, Stages stageId)
    {
      using (IDbConnection dbConnection = _connection)
      {
        dbConnection.Open();

        return Ok(await dbConnection.QueryAsync<ApbsRincianDto>("ApbsRincian",
          new { unitId, stageId }, commandType: CommandType.StoredProcedure));
      }
    }
  }
}
