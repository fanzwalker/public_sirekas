﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/stages/{stageId}/act-types/{activityTypeId}/units")]
  [Authorize]
  public class BudgetUnitController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public BudgetUnitController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(Stages stageId, ActivityType activityTypeId)
    {
      try
      {
        var program = _unitOfWork.Program.GetPrograms(null)
          .Result.FirstOrDefault();

        if (program == null) return NotFound("Program tidak ditemukan");

        var models = await _unitOfWork.UnitActivity
          .GetAllUnitActivity(u => u.Activity.ProgramId == program.Id &&
                                   u.Stages == stageId &&
                                   u.Activity.ActivityType == activityTypeId);

        return Ok(_mapper.Map<IEnumerable<UnitActivityDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    public async Task<IActionResult> Post(Stages stageId,
      ActivityType activityTypeId, [FromBody] IEnumerable<UnitActivityCreateDto> dtos)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var program = _unitOfWork.Program.GetPrograms(null)
          .Result.FirstOrDefault();

        if (program == null) return NotFound("Program tidak ditemukan");

        var act = await _unitOfWork.Activity
          .Get(a => a.ProgramId == program.Id && a.ActivityType == activityTypeId);

        if (act == null) return NotFound("Kegiatan tidak ditemukan");

        var models = _mapper.Map<IEnumerable<UnitActivity>>(dtos);

        var unitActivities = models.ToList();

        foreach (var model in unitActivities)
        {
          model.ActivityId = act.Id;
          model.Stages = stageId;
        }

        _unitOfWork.UnitActivity.AddRange(unitActivities);

        if (!await _unitOfWork.Complete()) return BadRequest();

        var returnModels = await _unitOfWork.UnitActivity
          .GetAllUnitActivity(u => u.Activity.ProgramId == program.Id &&
                                   u.Stages == stageId &&
                                   u.Activity.ActivityType == activityTypeId);

        return Ok(_mapper.Map<IEnumerable<UnitActivityDto>>(returnModels));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut]
    public async Task<IActionResult> Put(Stages stageId,
      ActivityType activityTypeId, [FromBody]UnitActivityCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var unit = await _unitOfWork.Unit.Get(dto.UnitId);

        if (unit == null)
          return NotFound("Unit tidak ditemukan");

        var program = _unitOfWork.Program.GetPrograms(null)
          .Result.FirstOrDefault();

        if (program == null) return NotFound("Program tidak ditemukan");

        var act = await _unitOfWork.Activity
          .Get(a => a.ProgramId == program.Id && a.ActivityType == activityTypeId);

        if (act == null) return NotFound("Kegiatan tidak ditemukan");

        var model = await _unitOfWork.UnitActivity
            .GetUnitActivity(u =>
              u.UnitId == dto.UnitId && u.Stages == stageId &&
              u.ActivityId == act.Id);

        var bosdaSiswa = await _unitOfWork.BudgetStandard.Get(b =>
          b.EducationLvl == unit.EducationLvl && b.StandardType == StandardType.BosdaPerSiswa);

        var bosdaRombel = await _unitOfWork.BudgetStandard.Get(b =>
          b.EducationLvl == unit.EducationLvl && b.StandardType == StandardType.BosdaPerRombel);

        var standarSekolah = await _unitOfWork.BudgetStandard.GetAll(b =>
          b.EducationLvl == unit.EducationLvl && b.StandardType == StandardType.BosdaPerSekolah);

        var bosdaSekolah = 0m;

        var standarByStudentNum = standarSekolah as BudgetStandard[] ?? standarSekolah.ToArray();

        if (unit.EducationLvl != EducationLvl.Tk)
        {
          if (standarByStudentNum.Length == 3)
          {
            if (dto.StudentNumber <= 168)
              bosdaSekolah = standarByStudentNum[0].Value;
            else if (dto.StudentNumber <= 337)
              bosdaSekolah = standarByStudentNum[1].Value;
            else if (dto.StudentNumber > 337)
              bosdaSekolah = standarByStudentNum[2].Value;
          }
        }
        else
        {
          if (standarByStudentNum.Length > 0)
          {
            bosdaSekolah = standarByStudentNum[0].Value;
          }
        }

        if (model == null)
        {
          var unitAct = _mapper.Map<UnitActivity>(dto);
          unitAct.ActivityId = act.Id;
          unitAct.Stages = stageId;
          unitAct.PerRombelPrice = bosdaRombel?.Value ?? 0;
          unitAct.PerStudentPrice = bosdaSiswa?.Value ?? 0;
          unitAct.PerUnitPrice = bosdaSekolah;
          unitAct.TotalPerRombelPrice = unitAct.Rombel * bosdaRombel?.Value ?? 0;
          unitAct.TotalPerStudentPrice = unitAct.StudentNumber * bosdaSiswa?.Value ?? 0;
          unitAct.PerUnitPrice = bosdaSekolah;
          unitAct.Total = unitAct.Rombel * bosdaRombel?.Value ?? 0 +
                          unitAct.StudentNumber * bosdaSiswa?.Value ?? 0 +
                          bosdaSekolah + unitAct.ExtraPrice;

          _unitOfWork.UnitActivity.Add(unitAct);

          await _unitOfWork.Complete();

          var returnUnitAct = await _unitOfWork.UnitActivity
            .GetUnitActivity(u => u.UnitId == unitAct.UnitId &&
                                  u.ActivityId == unitAct.ActivityId &&
                                  u.Stages == unitAct.Stages);

          return Ok(_mapper.Map<UnitActivityDto>(returnUnitAct));
        }

        _mapper.Map(dto, model);
        model.PerRombelPrice = bosdaRombel?.Value ?? 0;
        model.PerStudentPrice = bosdaSiswa?.Value ?? 0;
        model.PerUnitPrice = bosdaSekolah;
        model.TotalPerRombelPrice = model.Rombel * bosdaRombel?.Value ?? 0;
        model.TotalPerStudentPrice = model.StudentNumber * bosdaSiswa?.Value ?? 0;
        model.PerUnitPrice = bosdaSekolah;
        model.Total = (model.Rombel * bosdaRombel?.Value ?? 0) +
                      (model.StudentNumber * bosdaSiswa?.Value ?? 0) +
                      bosdaSekolah + model.ExtraPrice;

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<UnitActivityDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete]
    public async Task<IActionResult> Delete(Stages stageId,
      ActivityType activityTypeId, [FromBody]NumbersDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var program = _unitOfWork.Program.GetPrograms(null)
          .Result.FirstOrDefault();

        if (program == null) return NotFound("Program tidak ditemukan");

        var act = await _unitOfWork.Activity
          .Get(a => a.ProgramId == program.Id && a.ActivityType == activityTypeId);

        if (act == null) return NotFound("Kegiatan tidak ditemukan");

        foreach (var unitId in dto.Ids)
        {
          var model = await _unitOfWork.UnitActivity
            .GetUnitActivity(u =>
              u.UnitId == unitId && u.Stages == stageId &&
              u.ActivityId == act.Id);

          if (model == null) continue;

          _unitOfWork.UnitActivity.Remove(model);
        }

        await _unitOfWork.Complete();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
