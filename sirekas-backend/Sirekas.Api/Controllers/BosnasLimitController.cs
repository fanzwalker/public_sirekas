﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sirekas.Api.Dtos;
using Sirekas.Data;
using Sirekas.Domain.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/stages/{stageId}/bosnas-limit")]
  [Authorize]
  public class BosnasLimitController : Controller
  {
    private readonly SirekasDbContext _context;

    public BosnasLimitController(SirekasDbContext context)
    {
      _context = context;
    }

    [HttpGet]
    public async Task<IActionResult> Get(Stages stageId)
    {
      try
      {
        var act = await _context.Activities.FirstOrDefaultAsync(a => a.ActivityType == ActivityType.Bosnas);

        if (act == null) return NotFound("Kegiatan tidak ditemukan");

        var models = await _context.Units
          .Where(u => u.BudgetType == BudgetType.Bosnas ||
                      u.BudgetType == BudgetType.Both)
          .SelectMany(u => u.BosnasBudgetDescs.Where(b => b.Stages == stageId).DefaultIfEmpty(), (u, b) =>
            new BosnasBudgetDescDto
            {
              UnitId = u.Id,
              UnitCode = u.Code,
              UnitName = u.Name,
              UnitEducationLvl = (int)u.EducationLvl,
              StandardPrice = b == null ? 0 : b.StandardPrice,
              TotalStudent = b == null ? 0 : b.TotalStudent,
              Total = b == null ? 0 : b.Total
            })
          .ToListAsync();

        return Ok(models);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
