﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/chart-of-accounts")]
  [Authorize]
  public class CoaController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public CoaController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get([FromQuery] string keyword,
      [FromQuery]int? expensesType, [FromQuery]BudgetType? budgetTypeId)
    {
      var models = await _unitOfWork.ChartOfAccount
        .GetChartOfAccount(keyword, expensesType, budgetTypeId);

      return Ok(_mapper.Map<IEnumerable<ChartOfAccountDto>>(models));
    }

    [HttpPut]
    public async Task<IActionResult> Put([FromBody]CoaBosdaUpdateDto dto)
    {
      try
      {
        var models = await _unitOfWork.ChartOfAccount.GetAll(c => dto.Ids.Any(d => c.Id == d));

        foreach (var model in models)
          model.BudgetType = dto.BudgetType;

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<IEnumerable<ChartOfAccountDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
