﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/unit-descs")]
  [Authorize]
  public class UnitDescController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UnitDescController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
      try
      {
        var models = await _unitOfWork.UnitDesc.GetAll();

        return Ok(models);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetUnitDesc")]
    public async Task<IActionResult> Get(int id)
    {
      try
      {
        var model = await _unitOfWork.UnitDesc.Get(id);

        if (model == null) return NotFound();

        return Ok(model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }

    }

    [HttpPost]
    [Authorize(Policy = "Administrator")]
    public async Task<IActionResult> Post([FromBody] UnitDescCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (await _unitOfWork.UnitDesc
          .Find(u => string.Equals(u.Name, dto.Name, StringComparison.CurrentCultureIgnoreCase)))
          throw new InvalidOperationException("Nama satuan harus unik");

        var model = _mapper.Map<UnitDesc>(dto);

        _unitOfWork.UnitDesc.Add(model);

        await _unitOfWork.Complete();

        return CreatedAtRoute("GetUnitDesc", new { Id = model.Id }, model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    [Authorize(Policy = "Administrator")]
    public async Task<IActionResult> Put(int id, [FromBody] UnitDescCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.UnitDesc.Get(id);

        if (model == null) return NotFound();

        if (!await _unitOfWork.UnitDesc
          .Find(u => string.Equals(u.Name, dto.Name, StringComparison.CurrentCultureIgnoreCase) &&
                     u.Id != model.Id))
          throw new InvalidOperationException("Nama satuan harus unik");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete]
    [Authorize(Policy = "Administrator")]
    public async Task<IActionResult> Delete(int id)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.UnitDesc.Get(id);

        if (model == null) return NotFound();

        _unitOfWork.UnitDesc.Remove(model);

        await _unitOfWork.Complete();

        return Ok(model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
