﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Sirekas.Api.Dtos;
using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/charts")]
  [Authorize]
  public class ChartsController : Controller
  {
    private readonly DbConnection _connection;

    public ChartsController(DbConnection connection)
    {
      _connection = connection;
    }

    [HttpPost]
    public async Task<IActionResult> Post([FromBody] ChartParamsDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        using (IDbConnection dbConnection = _connection)
        {
          dbConnection.Open();

          var parameters = new DynamicParameters();

          if (dto.Parameters != null || dto.Parameters?.Count > 0)
          {
            foreach (var kvp in dto.Parameters)
            {
              parameters.Add(kvp.Key, kvp.Value);
            }
          }

          return Ok(await dbConnection.QueryAsync(dto.SpName,
            parameters, commandType: CommandType.StoredProcedure));
        }
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
