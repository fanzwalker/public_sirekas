﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Data.Repository;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/activities/{activityId}/chart-of-accounts")]
  [Authorize]
  public class UnitActivityCoaController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UnitActivityCoaController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }
  }
}
