﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/chart-of-accounts/{chartOfAccountId}/bosda-coa-headers")]
  [Authorize]
  public class BosdaCoaHeaderController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public BosdaCoaHeaderController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int chartOfAccountId)
    {
      try
      {
        var models = await _unitOfWork.BosdaCoaHeader
          .GetAll(c => c.ChartOfAccountId == chartOfAccountId);

        return Ok(models);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetBosdaCoaHeader")]
    public async Task<IActionResult> Get(int chartOfAccountId, int id)
    {
      try
      {
        var model = await _unitOfWork.BosdaCoaHeader
          .Get(c => c.ChartOfAccountId == chartOfAccountId && c.Id == id);

        if (model == null) return NotFound("Header tidak ditemukan");

        return Ok(model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    public async Task<IActionResult> Post(int chartOfAccountId, [FromBody] BosdaCoaHeaderCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (!await _unitOfWork.ChartOfAccount.Find(c => c.Id == chartOfAccountId))
          return NotFound("Rekening tidak ditemukan");

        var code = await _unitOfWork.BosdaCoaHeader.GenerateLastCode(chartOfAccountId);

        if (await _unitOfWork.BosdaCoaHeader
          .Find(b => b.ChartOfAccountId == chartOfAccountId &&
                     (b.Code == code ||
                      string.Equals(b.Name, dto.Name, StringComparison.CurrentCultureIgnoreCase))))
          throw new InvalidOperationException("Kode/nama header harus unik");

        var model = _mapper.Map<BosdaCoaHeader>(dto);

        model.ChartOfAccountId = chartOfAccountId;

        model.Code = code;

        _unitOfWork.BosdaCoaHeader.Add(model);

        await _unitOfWork.Complete();

        return CreatedAtRoute("GetBosdaCoaHeader",
          new { model.Id },
          _mapper.Map<BosdaCoaHeaderDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int chartOfAccountId, int id, [FromBody] BosdaCoaHeaderCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.BosdaCoaHeader.Get(id);

        if (model == null)
          return NotFound("Header Rekening tidak ditemukan");

        if (await _unitOfWork.BudgetDet
          .Find(c => c.ChartOfAccountId == chartOfAccountId &&
                     c.Type == LevelTypes.Header &&
                     c.Activity.ActivityType == ActivityType.Bosda &&
                     c.Code == model.Code))
          throw new InvalidOperationException("Data tidak dapat dirubah karena sudah digunakan di RKA");

        if (await _unitOfWork.BosdaCoaHeader
          .Find(b => (b.Code == model.Code ||
                     string.Equals(b.Name, dto.Name, StringComparison.CurrentCultureIgnoreCase)) &&
                     b.ChartOfAccountId == chartOfAccountId &&
                     b.Id != model.Id))
          throw new InvalidOperationException("Kode/nama header harus unik");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<BosdaCoaHeaderDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete]
    public async Task<IActionResult> Delete(int chartOfAccountId, [FromBody]NumbersDto dto)
    {
      try
      {
        if (await _unitOfWork.ChartOfAccount.Find(c => c.Id == chartOfAccountId))
          return NotFound("Rekening tidak ditemukan");

        foreach (var id in dto.Ids)
        {
          var model = await _unitOfWork.BosdaCoaHeader.Get(id);

          if (model == null) continue;

          if (await _unitOfWork.BudgetDet
            .Find(c => c.ChartOfAccountId == chartOfAccountId &&
                       c.Type == LevelTypes.Header &&
                       c.Activity.ActivityType == ActivityType.Bosda &&
                       c.Code == model.Code))
            throw new InvalidOperationException("Data tidak bisa dihapus karena sudah digunakan di RKA");

          _unitOfWork.BosdaCoaHeader.Remove(model);
        }

        await _unitOfWork.Complete();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
