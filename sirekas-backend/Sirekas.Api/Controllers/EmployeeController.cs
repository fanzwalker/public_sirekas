﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/job-titles/{jobTitleId}/employees")]
  [Authorize]
  public class EmployeeController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public EmployeeController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, int jobTitleId)
    {
      try
      {
        if (unitId == 0)
          unitId = _unitOfWork.Unit.Get(u => u.Level == UnitStructure.DinasUnitSatker).Result.Id;

        var models = await _unitOfWork.Employee
            .GetEmployees(e => e.UnitId == unitId && e.JobTitleId == jobTitleId);

        return Ok(_mapper.Map<IEnumerable<EmployeeDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{employeeId}", Name = "GetEmployee")]
    public async Task<IActionResult> Get(int unitId, int jobTitleId, int employeeId)
    {
      try
      {
        if (unitId == 0)
          unitId = _unitOfWork.Unit.Get(u => u.Level == UnitStructure.DinasUnitSatker).Result.Id;

        var model = await _unitOfWork.Employee
          .GetEmployee(e => e.UnitId == unitId && e.JobTitleId == jobTitleId && e.Id == employeeId);

        if (model == null) return NotFound();

        return Ok(_mapper.Map<EmployeeDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    public async Task<IActionResult> Post(int unitId, int jobTitleId, [FromBody] EmployeeCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest();

        if (unitId == 0)
          unitId = _unitOfWork.Unit.Get(u => u.Level == UnitStructure.DinasUnitSatker).Result.Id;

        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId)) return NotFound();

        if (!await _unitOfWork.JobTitle.Find(j => j.Id == jobTitleId)) return NotFound();

        var model = _mapper.Map<Employee>(dto);
        model.UnitId = unitId;
        model.JobTitleId = jobTitleId;

        _unitOfWork.Employee.Add(model);

        if (!await _unitOfWork.Complete()) return BadRequest();

        var returnModel = await _unitOfWork.Employee.GetEmployee(e => e.Id == model.Id);

        return CreatedAtRoute("GetEmployee",
          new { EmployeeId = model.Id },
          _mapper.Map<EmployeeDto>(returnModel));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{employeeId}")]
    public async Task<IActionResult> Put(int unitId, int jobTitleId, int employeeId,
      [FromBody]EmployeeCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (unitId == 0)
          unitId = _unitOfWork.Unit.Get(u => u.Level == UnitStructure.DinasUnitSatker).Result.Id;

        var model = await _unitOfWork.Employee
          .GetEmployee(e => e.UnitId == unitId && e.JobTitleId == jobTitleId && e.Id == employeeId);

        if (model == null) return NotFound();

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<EmployeeDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete("{employeeId}")]
    public async Task<IActionResult> Delete(int unitId, int jobTitleId, int employeeId)
    {
      try
      {
        if (unitId == 0)
          unitId = _unitOfWork.Unit.Get(u => u.Level == UnitStructure.DinasUnitSatker).Result.Id;

        var model = await _unitOfWork.Employee
          .GetEmployee(e => e.UnitId == unitId && e.JobTitleId == jobTitleId && e.Id == employeeId);

        if (model == null) return NotFound();

        _unitOfWork.Employee.Remove(model);

        await _unitOfWork.Complete();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
