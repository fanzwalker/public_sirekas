﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units")]
  [Authorize]
  public class UnitsController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UnitsController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get([FromQuery]CommonFiltersDto filters)
    {
      var models = filters.ExcludeType == ExcludeType.ExcludeIfExists
        ? await _unitOfWork.Unit.GetUnitNotExistsInUnitBudget(filters.ActivityType, filters.BudgetType, filters.Stages)
        : await _unitOfWork.Unit.GetUnits(filters.UnitId, filters.UnitStructure, filters.BudgetType);

      return Ok(_mapper.Map<IEnumerable<UnitDto>>(models));
    }

    [HttpGet("{id}", Name = "GetUnit")]
    public async Task<IActionResult> Get(int id)
    {
      var model = await _unitOfWork.Unit.GetUnit(id);

      if (model == null) return NotFound();

      return Ok(_mapper.Map<UnitDto>(model));
    }

    [HttpPost]
    [Authorize(Policy = "Administrator")]
    public async Task<IActionResult> Post([FromBody] UnitCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = _mapper.Map<Unit>(dto);

        if (await _unitOfWork.Unit.Find(u => u.Code.Contains(model.Code)))
          throw new InvalidOperationException("Kode sudah dipakai di daftar unit");

        _unitOfWork.Unit.Add(model);

        await _unitOfWork.Complete();

        return CreatedAtRoute("GetUnit", new { model.Id }, _mapper.Map<UnitDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    [Authorize(Policy = "Administrator")]
    public async Task<IActionResult> Put(int id, [FromBody] UnitUpdateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.Unit.Get(id);

        if (model == null) return NotFound();

        _mapper.Map(dto, model);

        if (await _unitOfWork.Unit.Find(u => u.Code.Contains(model.Code) && u.Id != id))
          throw new InvalidOperationException("Kode sudah dipakai di daftar unit");

        await _unitOfWork.Complete();

        return Ok(model);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut]
    [Authorize(Policy = "Administrator")]
    public async Task<IActionResult> Put([FromBody]UnitsLockDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var units = new List<Unit>();

        foreach (var id in dto.Ids)
        {
          var unit = await _unitOfWork.Unit.Get(id);

          if (unit == null) continue;

          units.Add(unit);

          unit.IsLocked = dto.IsLocked;

          await _unitOfWork.Complete();
        }

        return Ok(_mapper.Map<IEnumerable<UnitDto>>(units));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPatch("{id}")]
    [Authorize(Policy = "Administrator")]
    public async Task<IActionResult> Put(int id, [FromBody] JsonPatchDocument<UnitPacthDto> pacthDoc)
    {
      try
      {
        if (pacthDoc == null)
          throw new InvalidOperationException("Patch Document Cannot be Null");

        var model = await _unitOfWork.Unit.Get(u => u.Id == id);

        if (model == null) return NotFound($"Unit dengan ID: {id} tidak ditemukan");

        var modelToPacth = _mapper.Map<UnitPacthDto>(model);

        pacthDoc.ApplyTo(modelToPacth, ModelState);

        TryValidateModel(modelToPacth);

        if (!ModelState.IsValid) return BadRequest(ModelState);

        _mapper.Map(modelToPacth, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<UnitDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
