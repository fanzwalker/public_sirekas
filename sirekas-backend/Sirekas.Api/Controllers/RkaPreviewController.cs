﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Domain.Enums;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/activities/{activityId}/stages/{stageId}/rka221-previews")]
  [Authorize]
  public class RkaPreviewController : Controller
  {
    private readonly DbConnection _connection;

    public RkaPreviewController(DbConnection connection)
    {
      _connection = connection;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, int activityId, Stages stageId)
    {
      using (IDbConnection dbConnection = _connection)
      {
        dbConnection.Open();

        if (activityId == 0)
          return Ok(await dbConnection.QueryAsync<PreviewRka221Dto>("Rka21Preview",
            new { unitId, stageId }, commandType: CommandType.StoredProcedure));

        return Ok(await dbConnection.QueryAsync<PreviewRka221Dto>("Rka221Preview",
          new { unitId, activityId, stageId }, commandType: CommandType.StoredProcedure));
      }
    }
  }
}
