﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Sirekas.Api.Dtos;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/auth")]
  public class AuthController : Controller
  {
    private readonly UserManager<User> _userMgr;
    private readonly RoleManager<Role> _roleMgr;
    private readonly IPasswordHasher<User> _hasher;
    private readonly IConfiguration _config;

    public AuthController(UserManager<User> userMgr, RoleManager<Role> roleMgr,
      IPasswordHasher<User> hasher, IConfiguration config)
    {
      _userMgr = userMgr;
      _roleMgr = roleMgr;
      _hasher = hasher;
      _config = config;
    }

    [HttpPost("token")]
    public async Task<IActionResult> CreateToken([FromBody] CredentialDto dto)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      var user = await _userMgr.Users
        .Include(u => u.UserRoles)
        .Include(u => u.Unit)
        .ThenInclude(u => u.District)
        .FirstOrDefaultAsync(u => u.UserName == dto.UserName);

      if (user == null) return NotFound();

      if (_hasher.VerifyHashedPassword(user, user.PasswordHash, dto.Password) == PasswordVerificationResult.Failed)
        return Unauthorized();

      var roleClaims = new List<Claim>();

      foreach (var ur in user.UserRoles)
      {
        var role = await _roleMgr.Roles.FirstOrDefaultAsync(r => r.Id == ur.RoleId);
        roleClaims.AddRange(await _roleMgr.GetClaimsAsync(role));
      }

      var claims = new List<Claim>();

      if (user.Unit != null)
      {
        claims.AddRange(new List<Claim>
        {
          new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
          new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
          new Claim(JwtRegisteredClaimNames.GivenName, user.FullName),
          new Claim("stages", Convert.ToString((int)user.Stages)),
          new Claim("unitId", user.UnitId.GetValueOrDefault().ToString()),
          new Claim("unitName", user.Unit.Name),
          new Claim("unitCode", user.Unit.Code),
          new Claim("unitDistrict", user.Unit.District?.Name ?? ""),
          new Claim("budgetType", Convert.ToString((int)user.Unit.BudgetType)),
          new Claim("isLocked", user.Unit.IsLocked.ToString())
        }.Union(roleClaims));
      }
      else
      {
        claims.AddRange(new List<Claim>
        {
          new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
          new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
          new Claim(JwtRegisteredClaimNames.GivenName, user.FullName),
          new Claim("stages", Convert.ToString((int)user.Stages)),
          new Claim("unitId", ""),
          new Claim("unitName", ""),
          new Claim("unitCode", ""),
          new Claim("unitDistrict", ""),
          new Claim("budgetType", ""),
          new Claim("isLocked", "False")
        }.Union(roleClaims));
      }

      var token = new JwtSecurityToken(
        issuer: _config["TokenOptions:Issuer"],
        audience: _config["TokenOptions:Audience"],
        claims: claims,
        expires: DateTime.UtcNow.AddDays(30),
        signingCredentials: SigningCredentials()
        );

      return Ok(new JwtSecurityTokenHandler().WriteToken(token));
    }

    private SigningCredentials SigningCredentials()
    {
      var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["TokenOptions:Key"]));

      var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
      return creds;
    }
  }
}
