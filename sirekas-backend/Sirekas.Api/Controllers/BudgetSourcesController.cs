﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/budget-sources")]
  [Authorize]
  public class BudgetSourcesController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public BudgetSourcesController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int? levelType)
    {
      try
      {
        var models = await _unitOfWork.BudgetSource.GetBudgetSources(false, levelType);

        return Ok(_mapper.Map<IEnumerable<BudgetSourceDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetBudgetSource")]
    public async Task<IActionResult> Get(int id)
    {
      try
      {
        var model = await _unitOfWork.BudgetSource.Get(id);

        if (model == null) return NotFound();

        return Ok(_mapper.Map<BudgetSourceDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    public async Task<IActionResult> Post([FromBody]BudgetSourceCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (await _unitOfWork.BudgetSource.Find(b => b.Code == dto.Code || b.Name == dto.Name))
          throw new InvalidOperationException("Kode/Nama sudah ada di daftar Sumber Dana");

        var model = _mapper.Map<BudgetSource>(dto);

        _unitOfWork.BudgetSource.Add(model);

        await _unitOfWork.Complete();

        return CreatedAtRoute("GetBudgetSource", new { model.Id }, _mapper.Map<BudgetSourceDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int id, [FromBody]BudgetSourceUpdateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.BudgetSource.Get(id);

        if (model == null) return NotFound();

        if (await _unitOfWork.BudgetSource.Find(b => (b.Code == dto.Code || b.Name == dto.Name) && b.Id != id))
          throw new InvalidOperationException("Kode/Nama sudah ada di daftar Sumber Dana");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<BudgetSourceDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete]
    public async Task<IActionResult> Delete([FromBody] NumbersDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        foreach (var id in dto.Ids)
        {
          var model = await _unitOfWork.BudgetSource.GetBudgetSource(id, true);

          if (model == null) continue;

          if (model.Childrens.Any())
            throw new InvalidOperationException($"Data yang akan dihapus masih memiliki {model.Childrens.Count} child");

          _unitOfWork.BudgetSource.Remove(model);
        }

        await _unitOfWork.Complete();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
