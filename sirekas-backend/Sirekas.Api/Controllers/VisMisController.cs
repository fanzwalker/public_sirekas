﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/vismises")]
  [Authorize]
  public class VisMisController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public VisMisController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, [FromQuery] VisMisType? visMisType)
    {
      try
      {
        var models = await _unitOfWork.VisMis.GetVisMis(unitId, visMisType);

        return Ok(_mapper.Map<IEnumerable<VisMisDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetVisMis")]
    public async Task<IActionResult> Get(int unitId, int id)
    {
      try
      {
        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId))
          return NotFound("Unit tidak ditemukan");

        var model = await _unitOfWork.VisMis.GetVisMis(id);

        if (model == null) return NotFound("Data tidak ditemukan");

        return Ok(_mapper.Map<VisMisDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    public async Task<IActionResult> Post(int unitId, [FromBody] VisMisCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId))
          return NotFound("Unit tidak ditemukan");

        var model = _mapper.Map<VisMis>(dto);

        model.UnitId = unitId;

        _unitOfWork.VisMis.Add(model);

        await _unitOfWork.Complete();

        var returnModel = await _unitOfWork.VisMis.GetVisMis(model.Id);

        return CreatedAtRoute("GetVisMis", new { model.Id }, _mapper.Map<VisMisDto>(returnModel));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int unitId, int id, [FromBody] VisMisUpdateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.VisMis.GetVisMis(id);

        if (model == null) return NotFound("Data tidak ditemukan");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<VisMisDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int unitId, int id)
    {
      try
      {
        var model = await _unitOfWork.VisMis.GetVisMis(id);

        if (model == null) return NotFound("Data tidak ditemukan");

        _unitOfWork.VisMis.Remove(model);

        await _unitOfWork.Complete();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
