﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/stages/{stageId}/unit-budgets")]
  [Authorize]
  public class UnitBudgetsController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UnitBudgetsController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, Stages stageId)
    {
      var models = await _unitOfWork.UnitBudget
        .GetUnitBudget(u => u.Stages == stageId && u.UnitId == unitId, true);

      return Ok(_mapper.Map<IEnumerable<UnitBudgetDto>>(models));
    }

    [HttpGet("{unitBudgetId}", Name = "GetUnitBudget")]
    public async Task<IActionResult> Get(int unitId, Stages stageId, int unitBudgetId)
    {
      var model = await _unitOfWork.UnitBudget
        .GetUnitBudget(u => u.Id == unitBudgetId, true);

      return Ok(_mapper.Map<UnitBudgetDto>(model));
    }

    [HttpPost]
    public async Task<IActionResult> Post(int unitId, Stages stageId, [FromBody]UnitBudgetCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (await _unitOfWork.UnitBudget.Find(u => u.UnitId == unitId && u.Stages == stageId))
          throw new InvalidOperationException("Plafon anggaran belanja tidak langsung sudah dimasukkan");

        var model = _mapper.Map<UnitBudget>(dto);
        model.UnitId = unitId;
        model.Stages = stageId;

        _unitOfWork.UnitBudget.Add(model);

        await _unitOfWork.Complete();

        return CreatedAtRoute("GetUnitBudget", new { unitBudgetId = model.Id },
          _mapper.Map<IEnumerable<UnitBudgetDto>>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut]
    public async Task<IActionResult> Put(int unitId, Stages stageId, [FromBody]UnitBudgetUpdateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.UnitBudget.Get(u => u.UnitId == unitId && u.Stages == stageId);

        if (model == null)
        {
          model = _mapper.Map<UnitBudget>(dto);
          model.UnitId = unitId;
          model.Stages = stageId;

          _unitOfWork.UnitBudget.Add(model);

          await _unitOfWork.Complete();

          return CreatedAtRoute("GetUnitBudget", new { unitBudgetId = model.Id },
            _mapper.Map<UnitBudgetDto>(model));
        }

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<UnitBudgetDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete("{unitBudgetId}")]
    public async Task<IActionResult> Delete(int unitId, Stages stageId,
      int unitBudgetId, [FromBody]NumbersDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.UnitBudget.Get(u => u.Id == unitBudgetId);

        if (model == null) return NotFound();

        _unitOfWork.UnitBudget.Remove(model);

        await _unitOfWork.Complete();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
