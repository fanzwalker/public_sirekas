﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/messages")]
  [Authorize]
  public class MessagesController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public MessagesController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
      try
      {
        var models = await _unitOfWork.Message.GetAll();

        return Ok(_mapper.Map<IEnumerable<MessageDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetMessage")]
    public async Task<IActionResult> Get(int id)
    {
      try
      {
        var model = await _unitOfWork.Message.Get(m => m.Id == id);

        if (model == null) return NotFound($"Pesan dengan id: {id} tidak ditemukan");

        return Ok(_mapper.Map<MessageDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    public async Task<IActionResult> Post([FromBody] MessageCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = _mapper.Map<Message>(dto);

        _unitOfWork.Message.Add(model);

        await _unitOfWork.Complete();

        return CreatedAtRoute("GetMessage", new { model.Id }, _mapper.Map<MessageDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int id, [FromBody]MessageCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.Message.Get(m => m.Id == id);

        if (model == null) return NotFound($"Pesan dengan id: {id} tidak ditemukan");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<MessageDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(int id)
    {
      try
      {
        var model = await _unitOfWork.Message.Get(m => m.Id == id);

        if (model == null) return NotFound($"Pesan dengan id: {id} tidak ditemukan");

        _unitOfWork.Message.Remove(model);

        await _unitOfWork.Complete();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
