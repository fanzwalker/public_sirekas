﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/budget-types/{budgetTypeId}/perf-benchmark-fixed")]
  [Authorize]
  public class PerfBenchmarkFixedController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public PerfBenchmarkFixedController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(BudgetType budgetTypeId, [FromQuery]int unitId,
      [FromQuery]Stages stageId, [FromQuery]int activityId)
    {
      try
      {
        var unitAct = await _unitOfWork.UnitActivity
          .GetUnitActivity(u => u.UnitId == unitId &&
                                u.Stages == stageId &&
                                u.ActivityId == activityId);

        var models = await _unitOfWork.PerfBenchmarkFixed
          .GetAll(p => p.BudgetType == budgetTypeId);

        foreach (var m in models)
        {
          if (m.PerfBenchmarkType == PerfBenchmarkType.Masukan)
            m.Target = unitAct?.Total.ToString("C") ?? "Rp. 0,00";
        }

        return Ok(_mapper.Map<IEnumerable<PerfBenchmarkFixedDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(BudgetType budgetTypeId, int id,
      [FromBody]PerfBenchmarkFixedUpdateDto dto)
    {
      try
      {
        var model = await _unitOfWork.PerfBenchmarkFixed.Get(id);

        if (model == null) return NotFound("Kinerja kegiatan tidak ditemukan");

        _mapper.Map(dto, model);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<PerfBenchmarkFixedDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
