﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/stages/{stageId}/activities/{activityId}/perf-benchmarks")]
  [Authorize]
  public class PerfBenchmarkController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public PerfBenchmarkController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, Stages stageId, int activityId)
    {
      var models = await _unitOfWork.PerfBenchmark
        .GetAll(p => p.UnitId == unitId && p.Stages == stageId && p.ActivityId == activityId);

      if (models.Any()) return Ok(_mapper.Map<IEnumerable<PerfBenchmarkDto>>(models));

      var modelFixeds = await _unitOfWork.PerfBenchmarkFixed
        .GetAll();

      return Ok(_mapper.Map<IEnumerable<PerfBenchmarkFixedDto>>(modelFixeds));
    }

    [HttpPost]
    public async Task<IActionResult> Post(int unitId, Stages stageId, int activityId,
      [FromBody] IEnumerable<PerfBenchmarkCreateDto> dtos)
    {
      if (!ModelState.IsValid) return BadRequest(ModelState);

      if (!await _unitOfWork.Unit.Find(u => u.Id == unitId)) return NotFound();

      if (!await _unitOfWork.Activity.Find(a => a.Id == activityId)) return NotFound();

      var existingData = await _unitOfWork.PerfBenchmark
        .GetAll(p => p.UnitId == unitId && p.Stages == stageId && p.ActivityId == activityId);

      var benchmarks = existingData.ToList();

      var perfBenchmarkCreateDtos = dtos.ToList();

      if (benchmarks.Any())
      {
        foreach (var b in benchmarks)
        {

          b.Name = perfBenchmarkCreateDtos.FirstOrDefault(d => d.PerfBenchmarkType == b.PerfBenchmarkType)?.Name;
          b.Target = perfBenchmarkCreateDtos.FirstOrDefault(d => d.PerfBenchmarkType == b.PerfBenchmarkType)?.Target;
        }

        await _unitOfWork.Complete();

        return Ok();
      }

      var models = _mapper.Map<IEnumerable<PerfBenchmark>>(dtos);

      var perfBenchmarks = models.ToList();

      perfBenchmarks.ForEach(p =>
      {
        p.UnitId = unitId;
        p.Stages = stageId;
        p.ActivityId = activityId;
      });

      _unitOfWork.PerfBenchmark.AddRange(perfBenchmarks);

      await _unitOfWork.Complete();

      return Ok();
    }
  }
}
