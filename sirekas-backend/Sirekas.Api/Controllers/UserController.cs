﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/roles/{roleId}/users")]
  [Authorize]
  public class UserController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly UserManager<User> _usrMgr;
    private readonly IPasswordHasher<User> _hasher;
    private readonly IMapper _mapper;

    public UserController(IUnitOfWork unitOfWork,
      RoleManager<Role> roleMgr,
      UserManager<User> usrMgr,
      IPasswordHasher<User> hasher,
      IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _usrMgr = usrMgr;
      _hasher = hasher;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int roleId)
    {
      try
      {
        var role = await _unitOfWork.Role.Get(roleId);

        if (role == null) return NotFound();

        var models = await _unitOfWork.User.GetUsers(u => u.UserRoles.Any(r => r.RoleId == roleId));

        return Ok(_mapper.Map<IEnumerable<UserDto>>(models));

      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }

    }

    [HttpGet("{userId}", Name = "GetUser")]
    public async Task<IActionResult> Get(int roleId, int userId)
    {
      try
      {
        var model = await _unitOfWork.User.Get(userId);

        if (model == null) return NotFound();

        return Ok(_mapper.Map<UserDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    [Authorize(Policy = "Administrator")]
    public async Task<IActionResult> Post(int roleId, [FromBody]UserCreateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var role = await _unitOfWork.Role.Get(roleId);

        if (role == null) return NotFound();

        var existingUser = await _usrMgr.FindByNameAsync(dto.UserName);

        if (existingUser != null)
          throw new InvalidOperationException("Username sudah terdaftar");

        var model = _mapper.Map<User>(dto);

        var createUserResult = await _usrMgr.CreateAsync(model, dto.Password);

        var createRoleResult = await _usrMgr.AddToRoleAsync(model, role.Name);

        if (!createUserResult.Succeeded && !createRoleResult.Succeeded)
          throw new InvalidOperationException("Tambah user gagal");

        return CreatedAtRoute("GetUser", new { UserId = model.Id },
          _mapper.Map<UserDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{userName}")]
    public async Task<IActionResult> Put(int roleId, string userName, [FromBody]UserUpdateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        var user = await _unitOfWork.User.Get(u => u.UserName == userName);

        if (user == null) return NotFound($"Username: {userName} tidak ditemukan");

        var model = _mapper.Map(dto, user);

        model.PasswordHash = _hasher.HashPassword(model, dto.NewPassword);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<UserDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpDelete("{userId}")]
    public async Task<IActionResult> Delete(int roleId, int userId)
    {
      try
      {
        var role = await _unitOfWork.Role.Get(roleId);

        if (role == null) return NotFound($"User dengan id: {userId} tidak ditemukan");

        var user = await _unitOfWork.User.Get(userId);

        if (user == null) return NotFound();

        var deleteResult = await _usrMgr.DeleteAsync(user);

        if (!deleteResult.Succeeded) return BadRequest();

        return Ok();
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
