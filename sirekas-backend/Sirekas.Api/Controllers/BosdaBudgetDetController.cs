﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Api.Helpers;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/bosda/units/{unitId}/stages/{stageId}/budgetdets")]
  [Authorize]
  public class BosdaBudgetDetController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public BosdaBudgetDetController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, Stages stageId,
      int? coaId,
      [FromQuery]string code,
      [FromQuery]LevelTypes levelTypes)
    {
      try
      {
        var models = await _unitOfWork.BudgetDet
          .GetAllBudgetDet(b => b.UnitId == unitId &&
                                b.Stages == stageId &&
                                b.Activity.ActivityType == ActivityType.Bosda, coaId, code, levelTypes);

        return Ok(_mapper.Map<IEnumerable<BudgetDet>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetBosdaBudgetDet")]
    public async Task<IActionResult> Get(int unitId, Stages stageId, int id)
    {
      var model = await _unitOfWork.BudgetDet
        .GetBudgetDet(id);

      if (model == null) return NotFound();

      return Ok(_mapper.Map<BudgetDetDto>(model));
    }

    [HttpGet("{id}/childs")]
    public async Task<IActionResult> Get(int unitId, Stages stageId, int id, [FromQuery] LevelTypes type)
    {
      try
      {
        var act = await _unitOfWork.Activity.Get(a => a.ActivityType == ActivityType.Bosda);

        if (act == null) return NotFound("Kegiatan BOSDA tidak ditemukan");

        var models = await _unitOfWork.BudgetDet
          .GetBudgetChilds(unitId, stageId, act.Id, id, type);

        return Ok(_mapper.Map<IEnumerable<BudgetDetDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPost]
    public async Task<IActionResult> Post(int unitId, Stages stageId,
      [FromBody]BosdaBudgetDetCreateDto dto)
    {
      try
      {
        var act = await _unitOfWork.Activity.Get(a => a.ActivityType == ActivityType.Bosda);

        if (act == null) return NotFound("Kegiatan BOSDA tidak ditemukan");

        if (!await _unitOfWork.UnitActivity
          .Find(a => a.ActivityId == act.Id &&
                     a.Stages == stageId))
          throw new InvalidOperationException("Pagu kegiatan belum di set");

        var coaHeader = await _unitOfWork.BosdaCoaHeader
          .Get(b => b.Id == dto.HeaderId);

        if (coaHeader == null) return NotFound("Header tidak ditemukan");

        var budgetHeader = await _unitOfWork.BudgetDet
          .Get(b => b.ChartOfAccountId == dto.ChartOfAccountId &&
                    b.Stages == stageId &&
                    b.UnitId == unitId &&
                    b.ActivityId == act.Id &&
                    b.Code == coaHeader.Code &&
                    b.Description == coaHeader.Name &&
                    b.Type == LevelTypes.Header);

        if (budgetHeader == null)
        {
          budgetHeader = new BudgetDet
          {
            Code = coaHeader.Code,
            Description = coaHeader.Name,
            ChartOfAccountId = dto.ChartOfAccountId,
            UnitId = unitId,
            Stages = stageId,
            ActivityId = act.Id,
            Type = LevelTypes.Header,
            BudgetSourceId = BudgetType.Bosda
          };

          _unitOfWork.BudgetDet.Add(budgetHeader);

          await _unitOfWork.Complete();
        }

        var unitBudgetTotal = await _unitOfWork.UnitActivity
          .GetTotalPerActivity(u => u.UnitId == unitId &&
                                    u.Stages == stageId &&
                                    u.ActivityId == act.Id);

        var budgetDetTotal = await _unitOfWork.BudgetDet.GetTotalDetail(unitId, stageId, act.Id);

        var code = await _unitOfWork.BudgetDet
          .GetLastCode(unitId, stageId, act.Id, dto.ChartOfAccountId,
            LevelTypes.Detail, dto.SubHeaderId ?? budgetHeader.Id);

        var total = ExpressionConverter.Calculate(dto.Expression) * dto.CostPerUnit;

        var model = new BudgetDet
        {
          ActivityId = act.Id,
          ParentId = dto.SubHeaderId ?? budgetHeader.Id,
          UnitId = unitId,
          Stages = stageId,
          Code = code,
          Expression = dto.Expression,
          Amount = ExpressionConverter.Calculate(dto.Expression),
          CostPerUnit = dto.CostPerUnit,
          ChartOfAccountId = dto.ChartOfAccountId,
          Description = dto.Description,
          BudgetSourceId = BudgetType.Bosda,
          Type = LevelTypes.Detail,
          UnitDesc = dto.UnitDesc,
          Total = total
        };

        var curTotal = budgetDetTotal + total;

        if (curTotal > unitBudgetTotal)
        {
          ModelState.AddModelError("Total", $"Total nilai: {curTotal:C}, " +
                                            $"melebihi pagu yang tersedia: {unitBudgetTotal:C}");
          return BadRequest(ModelState);
        }

        _unitOfWork.BudgetDet.Add(model);

        await _unitOfWork.Complete();

        await _unitOfWork.BudgetDet.RecursiveUpdate(model);

        var quarterVal = model.Total / 4;

        _unitOfWork.BudgetMonthly.Add(new BudgetMonthly
        {
          ActivityId = model.ActivityId,
          BudgetDetId = model.Id,
          Stages = model.Stages,
          UnitId = model.UnitId,
          Q1 = quarterVal,
          Q2 = quarterVal,
          Q3 = quarterVal,
          Q4 = quarterVal
        });

        await _unitOfWork.Complete();

        var returnModel = await _unitOfWork.BudgetDet.GetBudgetDet(model.Id);

        return CreatedAtRoute("GetBosdaBudgetDet", new { id = model.Id }, _mapper.Map<BudgetDetDto>(returnModel));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
