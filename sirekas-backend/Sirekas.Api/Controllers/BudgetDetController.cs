﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Sirekas.Api.Dtos;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/stages/{stageId}/activities/{activityId}/budgetdets")]
  [Authorize]
  public class BudgetDetController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public BudgetDetController(IUnitOfWork unitOfWork, IMapper mapper)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, Stages stageId,
      int activityId, [FromQuery] int? coaId, [FromQuery] LevelTypes type)
    {
      var models = await _unitOfWork.BudgetDet
        .GetAllBudgetDet(b => b.UnitId == unitId &&
                              b.Stages == stageId &&
                              b.ActivityId == (activityId == 0 ? (int?)null : activityId), coaId, null, type);

      return Ok(_mapper.Map<IEnumerable<BudgetDetDto>>(models));
    }

    [HttpGet("{id}", Name = "GetBudgetDet")]
    public async Task<IActionResult> Get(int unitId, Stages stageId, int activityId, int id)
    {
      var model = await _unitOfWork.BudgetDet
        .GetBudgetDet(id);

      if (model == null) return NotFound();

      return Ok(_mapper.Map<BudgetDetDto>(model));
    }

    [HttpGet("{id}/childs")]
    public async Task<IActionResult> Get(int unitId, Stages stageId, int activityId, int id, [FromQuery] LevelTypes type)
    {
      var models = await _unitOfWork.BudgetDet
        .GetBudgetChilds(unitId, stageId, activityId == 0 ? (int?)null : activityId, id, type);

      return Ok(_mapper.Map<IEnumerable<BudgetDetDto>>(models));
    }

    [HttpPost]
    public async Task<IActionResult> Post(int unitId, Stages stageId, int activityId, [FromBody] BudgetDetCreateDto dto)
    {
      var actId = activityId == 0 ? (int?)null : activityId;

      if (!ModelState.IsValid) return BadRequest(ModelState);

      var activity = await _unitOfWork.Activity.Get(activityId);

      if (!await _unitOfWork.Unit.Find(u => u.Id == unitId)) return NotFound();

      if (actId.HasValue)
      {
        if (!await _unitOfWork.UnitActivity
          .Find(a => a.ActivityId == actId &&
                     a.Stages == stageId &&
                     a.UnitId == unitId))
          return NotFound();

        if (activity.ActivityType == ActivityType.Bosda &&
            dto.Type == LevelTypes.Header && dto.ParentId == null)
        {
          var header = await _unitOfWork.BudgetDet
            .GetBudgetDet(b => b.UnitId == unitId &&
                               b.Stages == stageId &&
                               b.ActivityId == actId &&
                               b.ChartOfAccountId == dto.ChartOfAccountId &&
                               b.Code == dto.Code &&
                               b.Description == dto.Description);

          if (header != null) return Ok(_mapper.Map<BudgetDetDto>(header));
        }
      }

      var lastYearBalance = _unitOfWork.LastYearBalance
        .GetLastYearBalance(l => l.UnitId == unitId)
        .Result?.Value ?? 0;

      var unitBudgetTotal = actId != null
        ? await _unitOfWork.UnitActivity
            .GetTotalPerActivity(u => u.UnitId == unitId &&
                                      u.Stages == stageId &&
                                      u.ActivityId == actId) + lastYearBalance
        : await _unitOfWork.UnitBudget.GetUnitBudgetValue(u => u.UnitId == unitId &&
                                                               u.Stages == stageId);

      var budgetDetTotal = await _unitOfWork.BudgetDet.GetTotalDetail(unitId, stageId, actId);

      var code = await _unitOfWork.BudgetDet
        .GetLastCode(unitId, stageId, actId, dto.ChartOfAccountId, dto.Type, dto.ParentId);

      var model = _mapper.Map<BudgetDet>(dto);

      model.Code = activity?.ActivityType == ActivityType.Bosda &&
                   dto.Type == LevelTypes.Header &&
                   dto.ParentId == null
        ? dto.Code
        : code;

      model.UnitId = unitId;

      model.Stages = stageId;

      model.ActivityId = actId;

      var curTotal = budgetDetTotal + model.Total;

      if (actId != null && (curTotal > unitBudgetTotal))
      {
        ModelState.AddModelError("Total", $"Total nilai: {curTotal:C}, melebihi pagu yang tersedia: {unitBudgetTotal:C}");
        return BadRequest(ModelState);
      }

      _unitOfWork.BudgetDet.Add(model);

      if (!await _unitOfWork.Complete()) return BadRequest();

      await _unitOfWork.BudgetDet.RecursiveUpdate(model);

      var quarterVal = model.Total / 4;

      _unitOfWork.BudgetMonthly.Add(new BudgetMonthly
      {
        ActivityId = model.ActivityId,
        BudgetDetId = model.Id,
        Stages = model.Stages,
        UnitId = model.UnitId,
        Q1 = quarterVal,
        Q2 = quarterVal,
        Q3 = quarterVal,
        Q4 = quarterVal
      });

      if (!await _unitOfWork.Complete()) return BadRequest();

      var returnModel = await _unitOfWork.BudgetDet.GetBudgetDet(model.Id);

      return CreatedAtRoute("GetBudgetDet", new { id = model.Id }, _mapper.Map<BudgetDetDto>(returnModel));
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int unitId, Stages stageId, int activityId, int id,
      [FromBody] BudgetDetUpdateDto dto)
    {
      try
      {
        var actId = activityId == 0 ? (int?)null : activityId;

        if (!ModelState.IsValid) return BadRequest(ModelState);

        var model = await _unitOfWork.BudgetDet.GetBudgetDet(id);

        if (model == null) return NotFound();

        var lastYearBalance = _unitOfWork.LastYearBalance
                                .GetLastYearBalance(l => l.UnitId == unitId)
                                .Result?.Value ?? 0;

        var totalUnitBudget = actId != null
          ? await _unitOfWork.UnitActivity
              .GetTotalPerActivity(u => u.UnitId == unitId &&
                                        u.Stages == stageId &&
                                        u.ActivityId == actId) + lastYearBalance
          : await _unitOfWork.UnitBudget.GetUnitBudgetValue(u => u.UnitId == unitId &&
                                                                 u.Stages == stageId);

        var totalBudgetDet = await _unitOfWork.BudgetDet.GetTotalDetail(unitId, stageId, actId);

        var curTotal = totalBudgetDet - model.Total;

        _mapper.Map(dto, model);

        if (actId != null && curTotal + model.Total > totalUnitBudget)
          throw new InvalidOperationException($"Total nilai: {curTotal + model.Total:C}, melebihi pagu yang tersedia: {totalUnitBudget:C}");

        await _unitOfWork.Complete();

        var budgetMonth = await _unitOfWork.BudgetMonthly
          .GetBudgetMonthly(b => b.UnitId == unitId &&
                                 b.ActivityId == actId &&
                                 b.BudgetDetId == model.Id);

        var quarterVal = model.Total / 4;
        budgetMonth.Q1 = quarterVal;
        budgetMonth.Q2 = quarterVal;
        budgetMonth.Q3 = quarterVal;
        budgetMonth.Q4 = quarterVal;

        await _unitOfWork.Complete();

        await _unitOfWork.BudgetDet.RecursiveUpdate(model);

        return Ok(_mapper.Map<BudgetDetDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }

    }

    [HttpDelete]
    public async Task<IActionResult> Delete(int unitId, Stages stageId, int activityId, [FromBody] NumbersDto dto)
    {
      var actId = activityId == 0 ? (int?)null : activityId;

      if (!ModelState.IsValid) return BadRequest(ModelState);

      foreach (var id in dto.Ids)
      {
        var budgetDet = await _unitOfWork.BudgetDet.Get(id);

        if (budgetDet == null) continue;

        budgetDet.Total = 0;

        await _unitOfWork.BudgetDet.RecursiveUpdate(budgetDet);

        _unitOfWork.BudgetDet.Remove(budgetDet);

        if (!await _unitOfWork.Complete()) return BadRequest();
      }

      return Ok();
    }
  }
}
