﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sirekas.Api.Dtos;
using Sirekas.Data;
using Sirekas.Domain.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/stages/{stageId}/bosda-limit")]
  [Authorize]
  public class BosdaLimitController : Controller
  {
    private readonly SirekasDbContext _context;

    public BosdaLimitController(SirekasDbContext context)
    {
      _context = context;
    }

    [HttpGet]
    public async Task<IActionResult> Get(Stages stageId)
    {
      try
      {
        var act = await _context.Activities.FirstOrDefaultAsync(a => a.ActivityType == ActivityType.Bosda);

        if (act == null) return NotFound("Kegiatan tidak ditemukan");

        var models = await _context.Units
          .Where(u => u.BudgetType == BudgetType.Bosda ||
                      u.BudgetType == BudgetType.Both)
          .SelectMany(u => u.UnitActivities.Where(ua => ua.Stages == stageId && ua.ActivityId == act.Id)
            .DefaultIfEmpty(), (unit, activity) => new UnitActivityDto
            {
              UnitId = unit.Id,
              UnitCode = unit.Code,
              UnitName = unit.Name,
              UnitEducationLvl = unit.EducationLvl,
              Stages = stageId,
              ActivityId = act.Id,
              ActivityCode = act.Code,
              ActivityName = act.Name,
              StudentNumber = activity == null ? 0 : activity.StudentNumber,
              PerStudentPrice = activity == null ? 0 : activity.PerStudentPrice,
              TotalPerStudentPrice = activity == null ? 0 : activity.TotalPerStudentPrice,
              Rombel = activity == null ? 0 : activity.Rombel,
              PerRombelPrice = activity == null ? 0 : activity.PerRombelPrice,
              TotalPerRombelPrice = activity == null ? 0 : activity.TotalPerRombelPrice,
              PerUnitPrice = activity == null ? 0 : activity.PerUnitPrice,
              ExtraPrice = activity == null ? 0 : activity.ExtraPrice,
              Total = activity == null ? 0 : activity.Total
            })
          .ToListAsync();

        return Ok(models);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
