﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sirekas.Api.Dtos;
using Sirekas.Data;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Api.Controllers
{
  [Route("api/units/{unitId}/stages/{stageId}/manual-entries")]
  [Authorize]
  public class ManualEntryController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly SirekasDbContext _context;

    public ManualEntryController(IUnitOfWork unitOfWork,
      IMapper mapper, SirekasDbContext context)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _context = context;
    }

    [HttpGet]
    public async Task<IActionResult> Get(int unitId, Stages stageId)
    {
      try
      {
        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId)) return NotFound("Unit tidak ditemukan");

        var models = await _context.CoaApbs
          .Where(c => c.LevelTypes == LevelTypes.Detail)
          .SelectMany(c => c.ManualEntries.Where(m => m.UnitId == unitId && m.Stages == stageId)
            .DefaultIfEmpty(), (coa, manual) => new
            {
              UnitId = unitId,
              Category = coa.TransType == TransType.Income ? "Penerimaan" : "Pengeluaran",
              Stages = stageId,
              CoaApbsCode = coa.Code,
              CoaApbsName = coa.Name,
              CoaApbsId = coa.Id,
              Id = manual == null ? 0 : manual.Id,
              Total = manual == null ? 0 : manual.Total
            })
          .OrderBy(c => c.CoaApbsCode)
          .ToListAsync();

        return Ok(models);
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpGet("{id}", Name = "GetManualEntry")]
    public async Task<IActionResult> Get(int unitId, Stages stageId, int id)
    {
      try
      {
        var model = await _unitOfWork.ManualEntry.GetManualEntry(id);

        return Ok(_mapper.Map<ManualEntryDto>(model));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Put(int unitId, Stages stageId, int id, [FromBody]ManualEntryUpdateDto dto)
    {
      try
      {
        if (!ModelState.IsValid) return BadRequest(ModelState);

        if (!await _unitOfWork.Unit.Find(u => u.Id == unitId)) return NotFound("Unit tidak ditemukan");

        if (id == 0)
        {
          var newModel = _mapper.Map<ManualEntry>(dto);

          newModel.UnitId = unitId;

          newModel.Stages = stageId;

          _unitOfWork.ManualEntry.Add(newModel);

          await _unitOfWork.Complete();

          var returnModel = await _unitOfWork.ManualEntry.GetManualEntry(newModel.Id);

          return CreatedAtRoute("GetManualEntry", new { newModel.Id }, _mapper.Map<ManualEntryDto>(returnModel));
        }

        var updateModel = await _unitOfWork.ManualEntry.GetManualEntry(id);

        _mapper.Map(dto, updateModel);

        await _unitOfWork.Complete();

        return Ok(_mapper.Map<ManualEntryDto>(updateModel));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.InnerException?.Message ?? e.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
