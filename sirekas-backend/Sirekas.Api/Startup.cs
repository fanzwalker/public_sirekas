﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Sirekas.Data;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using Swashbuckle.AspNetCore.Swagger;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;

namespace Sirekas.Api
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public DbConnection DbConnection => new SqlConnection(Configuration.GetConnectionString("SirekasConnection"));

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddSingleton(Configuration);

      services.AddScoped(conn => DbConnection);

      services.AddMvc()
        .AddJsonOptions(c =>
        {
          c.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
          c.SerializerSettings.Formatting = Formatting.Indented;
        });

      services.AddCors();

      services.AddDbContextPool<SirekasDbContext>(c =>
        c.UseSqlServer(Configuration.GetConnectionString("SirekasConnection")));

      services.AddIdentity<User, Role>(opt =>
        {
          opt.Password.RequireDigit = false;
          opt.Password.RequiredLength = 6;
          opt.Password.RequiredUniqueChars = 0;
          opt.Password.RequireLowercase = false;
          opt.Password.RequireNonAlphanumeric = false;
          opt.Password.RequireUppercase = false;
        })
        .AddEntityFrameworkStores<SirekasDbContext>()
        .AddDefaultTokenProviders();

      services.AddAuthentication(cfg =>
        {
          cfg.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
          cfg.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;

        })
        .AddJwtBearer(cfg =>
        {
          cfg.RequireHttpsMetadata = false;
          cfg.SaveToken = true;
          cfg.TokenValidationParameters = new TokenValidationParameters
          {
            ValidIssuer = Configuration["TokenOptions:Issuer"],
            ValidAudience = Configuration["TokenOptions:Audience"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["TokenOptions:Key"]))
          };
        });

      services.AddAuthorization(cfg =>
      {
        cfg.AddPolicy("Administrator", p => p.RequireClaim("IsAdmin", "True"));
      });

      services.AddTransient(typeof(IUnitOfWork<>), typeof(UnitOfWork<>));

      services.AddTransient<IUnitOfWork, UnitOfWork>();

      services.AddAutoMapper();

      services.AddTransient<SirekasDbInitializer>();

      services.AddTransient<IdentityInitializer>();

      services.AddScoped(typeof(JsonDataSeeder<>));

      services.AddSwaggerGen(c =>
      {
        c.SwaggerDoc("v1", new Info { Title = "Sirekas API", Version = "v1" });
        c.AddSecurityDefinition("Bearer",
          new ApiKeyScheme
          {
            In = "header",
            Description = "Please insert JWT Bearer into field",
            Name = "Authorization",
            Type = "apiKey"
          });
      });
    }

    public void Configure(IApplicationBuilder app,
      IHostingEnvironment env,
      SirekasDbInitializer seeder,
      IdentityInitializer identitySeeder,
      JsonDataSeeder<ChartOfAccount> coaSeeder)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseCors(opt =>
        opt.AllowAnyHeader()
          .AllowAnyMethod()
          .AllowAnyOrigin()
      );

      app.UseSwagger(c => c.RouteTemplate = "swagger/{documentName}/swagger.json");

      app.UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("v1/swagger.json", "Sirekas Api V1");
        c.DocExpansion("none");
      });

      app.UseAuthentication();

      app.UseMvc();

      //var coaJson = System.IO.File.ReadAllText(@"Assets/coa.json");
      //coaSeeder.Seed(coaJson).Wait();

      //seeder.Seed().Wait();
      //identitySeeder.Seed().Wait();
    }
  }
}
