﻿using AutoMapper;
using Sirekas.Api.Dtos;
using Sirekas.Api.Helpers;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;

namespace Sirekas.Api
{
  public class MappingProfile : Profile
  {
    public MappingProfile()
    {
      CreateMap<Unit, UnitDto>()
        .ForMember(d => d.DistrictName, opt => opt.MapFrom(s => s.District.Name));
      CreateMap<UnitCreateDto, Unit>()
        .ForMember(d => d.Type, opt => opt.UseValue("D"))
        //.ForMember(d => d.Code, opt => opt.ResolveUsing(s => string.Concat("1.01.01.01.", s.Code)))
        .ForMember(d => d.Level, opt => opt.UseValue(UnitStructure.SubDinasSubUnitBagian));
      CreateMap<UnitUpdateDto, Unit>();
      CreateMap<UnitPacthDto, Unit>();
      CreateMap<Unit, UnitPacthDto>();

      CreateMap<BudgetSource, BudgetSourceDto>();
      CreateMap<BudgetSourceCreateDto, BudgetSource>()
        .ForMember(d => d.Type, opt => opt.ResolveUsing(s => s.ParentId.HasValue ? LevelTypes.Detail : LevelTypes.Header));
      CreateMap<BudgetSourceUpdateDto, BudgetSource>();

      CreateMap<ChartOfAccount, ChartOfAccountDto>()
        .ForMember(d => d.FullName, opt => opt.MapFrom(s => s.Code + s.Name));

      CreateMap<Domain.Entities.Program, ProgramDto>();
      CreateMap<ProgramCreateDto, Domain.Entities.Program>();
      CreateMap<ProgramUpdateDto, Domain.Entities.Program>();

      CreateMap<Activity, ActivityDto>();
      CreateMap<ActivityCreateDto, Activity>();

      CreateMap<UnitActivity, UnitActivityDto>();
      CreateMap<UnitActivityCreateDto, UnitActivity>();
      CreateMap<UnitActivityUpdateDto, UnitActivity>();

      CreateMap<UnitBudget, UnitBudgetDto>();
      CreateMap<UnitBudgetCreateDto, UnitBudget>();
      CreateMap<UnitBudgetUpdateDto, UnitBudget>();

      CreateMap<BudgetDet, BudgetDetDto>()
        .ForMember(d => d.BudgetSourceName, opt =>
          opt.ResolveUsing(s => s.BudgetSourceId == BudgetType.Bosda ? "APBD" : "APBN"));
      CreateMap<BudgetDetCreateDto, BudgetDet>()
        .ForMember(d => d.Amount,
          opt => opt.ResolveUsing(s => s.Type == LevelTypes.Detail ? ExpressionConverter.Calculate(s.Expression) : 0m))
        .ForMember(d => d.Total,
          opt => opt.ResolveUsing(s =>
            s.Type == LevelTypes.Detail ? ExpressionConverter.Calculate(s.Expression) * s.CostPerUnit : 0m));

      CreateMap<BudgetDetUpdateDto, BudgetDet>()
        .ForMember(d => d.Amount, opt => opt.ResolveUsing(s => ExpressionConverter.Calculate(s.Expression)))
        .ForMember(d => d.Total, opt => opt.ResolveUsing(s => s.Type == LevelTypes.Detail ? ExpressionConverter.Calculate(s.Expression) * s.CostPerUnit : 0m));

      CreateMap<BudgetMonthly, BudgetMonthlyDto>()
        .ForMember(d => d.ChartOfAccountId, opt => opt.MapFrom(s => s.BudgetDet.ChartOfAccountId))
        .ForMember(d => d.ChartOfAccountCode, opt => opt.MapFrom(s => s.BudgetDet.ChartOfAccount.Code))
        .ForMember(d => d.ChartOfAccountName, opt => opt.MapFrom(s => s.BudgetDet.ChartOfAccount.Name))
        .ForMember(d => d.Code, opt => opt.MapFrom(s => s.BudgetDet.Code))
        .ForMember(d => d.Description, opt => opt.MapFrom(s => s.BudgetDet.Description))
        .ForMember(d => d.Total, opt => opt.MapFrom(s => s.BudgetDet.Total))
        .ForMember(d => d.Type, opt => opt.MapFrom(s => s.BudgetDet.Type));
      CreateMap<BudgetMonthlyUpdateDto, BudgetMonthly>();

      CreateMap<PerfBenchmark, PerfBenchmarkDto>();
      CreateMap<PerfBenchmarkCreateDto, PerfBenchmark>();

      CreateMap<PerfBenchmarkFixed, PerfBenchmarkFixedDto>();
      CreateMap<PerfBenchmarkFixedUpdateDto, PerfBenchmarkFixed>();

      CreateMap<JobTitle, JobTitleDto>();
      CreateMap<JobTitle, JobTitleEmpDto>();

      CreateMap<Employee, EmployeeDto>();
      CreateMap<EmployeeCreateDto, Employee>();

      CreateMap<User, UserDto>();
      CreateMap<UserCreateDto, User>();
      CreateMap<UserUpdateDto, User>();

      CreateMap<Role, KeyValuePairDto>();

      CreateMap<UnitDescCreateDto, UnitDesc>();

      CreateMap<WebOptionCreateDto, WebOption>();

      CreateMap<BudgetStandardCreateDto, BudgetStandard>();

      CreateMap<LastYearBalance, LastYearBalanceDto>();
      CreateMap<LastYearBalanceCreateDto, LastYearBalance>();

      CreateMap<BosnasBudgetDesc, BosnasBudgetDescDto>();
      CreateMap<BosnasBudgetDescCreateDto, BosnasBudgetDesc>()
        .ForMember(d => d.Total, opt => opt.MapFrom(s => s.StandardPrice * s.TotalStudent));

      CreateMap<BosdaCoaHeader, BosdaCoaHeaderDto>();
      CreateMap<BosdaCoaHeaderCreateDto, BosdaCoaHeader>();

      CreateMap<VisMis, VisMisDto>()
        .ForMember(d => d.TypeName, opt => opt.ResolveUsing(s =>
        {
          var typeName = "";

          switch (s.Type)
          {
            case VisMisType.Vision:
              typeName = "Visi";
              break;
            case VisMisType.Mision:
              typeName = "Misi";
              break;
            case VisMisType.Target:
              typeName = "Tujuan";
              break;
            default:
              typeName = "None";
              break;
          }

          return typeName;
        }));
      CreateMap<VisMisCreateDto, VisMis>();
      CreateMap<VisMisUpdateDto, VisMis>();

      CreateMap<StudentData, StudentDataDto>();
      CreateMap<StudentDataCreateDto, StudentData>()
        .ForMember(d => d.Total, opt => opt.ResolveUsing(s => s.Male + s.Female));

      CreateMap<EmployeeData, EmployeeDataDto>()
        .ForMember(d => d.EmployeeDataTypeName, opt => opt.ResolveUsing(s =>
        {
          var name = "";

          switch (s.EmployeeDataType)
          {
            case EmployeeDataType.GuruPns:
              name = "Guru PNS";
              break;
            case EmployeeDataType.GuruNonPns:
              name = "Guru Non PNS";
              break;
            case EmployeeDataType.PegawaiTuPns:
              name = "Pegawai/TU PNS";
              break;
            case EmployeeDataType.PegawaiTuNonPns:
              name = "Pegawai/TU Non PNS";
              break;
            default:
              name = "None";
              break;
          }

          return name;
        }))
        .ForMember(d => d.StatusName,
          opt => opt.ResolveUsing(s => s.Status == EmployeeStatus.Active ? "Aktif" : "Non Aktif"));
      CreateMap<EmployeeDataCreateDto, EmployeeData>()
        .ForMember(d => d.Total, opt => opt.ResolveUsing(s => s.Male + s.Female));

      CreateMap<ManualEntry, ManualEntryDto>();
      CreateMap<ManualEntryUpdateDto, ManualEntry>();

      CreateMap<Message, MessageDto>();
      CreateMap<MessageCreateDto, Message>();
    }
  }
}
