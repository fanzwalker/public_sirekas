﻿namespace Sirekas.Api.Dtos
{
  public class UnitBudgetCreateDto
  {
    public decimal Total { get; set; }
  }
}
