﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class BosdaCoaHeaderCreateDto
  {
    [MaxLength(255), Required]
    public string Name { get; set; }
  }
}