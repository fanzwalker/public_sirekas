﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class VisMisCreateDto
  {
    [Required]
    public VisMisType Type { get; set; }
    [Required, MaxLength(255)]
    public string Description { get; set; }
  }
}