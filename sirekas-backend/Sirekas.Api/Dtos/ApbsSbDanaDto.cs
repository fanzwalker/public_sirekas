﻿namespace Sirekas.Api.Dtos
{
  public class ApbsSbDanaDto
  {
    public string Code { get; set; }
    public string Name { get; set; }
    public decimal Total { get; set; }
  }
}
