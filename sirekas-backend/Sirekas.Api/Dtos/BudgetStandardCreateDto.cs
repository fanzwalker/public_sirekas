﻿namespace Sirekas.Api.Dtos
{
  public class BudgetStandardCreateDto
  {
    public decimal Value { get; set; }
  }
}
