﻿namespace Sirekas.Api.Dtos
{
  public class BudgetMonthlyUpdateDto
  {
    public decimal Q1 { get; set; }
    public decimal Q2 { get; set; }
    public decimal Q3 { get; set; }
    public decimal Q4 { get; set; }
  }
}
