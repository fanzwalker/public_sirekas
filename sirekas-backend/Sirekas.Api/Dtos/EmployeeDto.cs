﻿namespace Sirekas.Api.Dtos
{
  public class EmployeeDto
  {
    public int Id { get; set; }
    public string Nip { get; set; }
    public string Name { get; set; }
    public int UnitId { get; set; }
    public string UnitName { get; set; }
    public int JobTitleId { get; set; }
    public string JobTitleName { get; set; }
    public string JobDesc { get; set; }
    public int Signer { get; set; }
  }
}
