﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class CredentialDto
  {
    [Required]
    public string UserName { get; set; }
    [Required]
    public string Password { get; set; }
  }
}
