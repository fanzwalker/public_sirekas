﻿using Sirekas.Domain.Enums;
using System.Collections.Generic;

namespace Sirekas.Api.Dtos
{
  public class CoaBosdaUpdateDto
  {
    public BudgetType BudgetType { get; set; }
    public IEnumerable<int> Ids { get; set; }
  }
}
