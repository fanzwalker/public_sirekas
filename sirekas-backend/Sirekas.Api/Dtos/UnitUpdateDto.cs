﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class UnitUpdateDto
  {
    [Required]
    public string Code { get; set; }
    [Required]
    public string Name { get; set; }
    public BudgetType BudgetType { get; set; }
    public int DistrictId { get; set; }
    public EducationLvl EducationLvl { get; set; }
    public string Npsn { get; set; }
  }
}
