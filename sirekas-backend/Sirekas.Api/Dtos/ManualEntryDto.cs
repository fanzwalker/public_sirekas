﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class ManualEntryDto
  {
    public int Id { get; set; }
    public int UnitId { get; set; }
    public string UnitCode { get; set; }
    public string UnitName { get; set; }
    public Stages Stages { get; set; }
    public int CoaApbsId { get; set; }
    public string CoaApbsCode { get; set; }
    public string CoaApbsName { get; set; }
    public decimal Total { get; set; }
  }
}