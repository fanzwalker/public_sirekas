﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class NumbersDto
  {
    [Required]
    public IEnumerable<int> Ids { get; set; }
  }
}
