﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class EmployeeCreateDto
  {
    [Required]
    public string Nip { get; set; }
    [Required]
    public string Name { get; set; }
    public string JobDesc { get; set; }
    public int Signer { get; set; }
  }
}
