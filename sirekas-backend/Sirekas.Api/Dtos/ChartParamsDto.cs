﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class ChartParamsDto
  {
    [Required]
    public string SpName { get; set; }
    public Dictionary<string, object> Parameters { get; set; }
  }
}
