﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class BudgetUpdateDto
  {
    [Required]
    public int BudgetSourceId { get; set; }
    [Required]
    public decimal Value { get; set; }
  }
}
