﻿namespace Sirekas.Api.Dtos
{
  public class ManualEntryUpdateDto
  {
    public int CoaApbsId { get; set; }
    public decimal Total { get; set; }
  }
}
