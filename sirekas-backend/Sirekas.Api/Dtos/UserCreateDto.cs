﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class UserCreateDto
  {
    [MinLength(4), MaxLength(20), Required]
    public string UserName { get; set; }
    [MaxLength(64), Required]
    public string FullName { get; set; }
    [MinLength(6), Required]
    public string Password { get; set; }
    public Stages Stages { get; set; } = Stages.Raperda;
    public int? UnitId { get; set; }
  }
}
