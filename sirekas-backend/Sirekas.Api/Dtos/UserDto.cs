﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class UserDto
  {
    public int Id { get; set; }
    public string UserName { get; set; }
    public string FullName { get; set; }
    public Stages Stages { get; set; } = Stages.Raperda;
    public int? UnitId { get; set; }
  }
}
