﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class CoaFiltersDto
  {
    public Stages Stages { get; set; } = Stages.Raperda;
    public ExpensesType ExpensesType { get; set; } = ExpensesType.BelanjaLangsung;
  }
}
