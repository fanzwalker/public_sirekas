﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class BudgetCreateDto
  {
    [Required]
    public Stages Stages { get; set; } = Stages.Raperda;
    [Required]
    public int ChartOfAccountId { get; set; }
    [Required]
    public int BudgetSourceId { get; set; }
    public decimal Value { get; set; }
  }
}
