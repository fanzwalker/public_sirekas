﻿namespace Sirekas.Api.Dtos
{
  public class BudgetDto
  {
    public int Id { get; set; }
    public int ChartOfAccountId { get; set; }
    public string ChartOfAccountCode { get; set; }
    public string ChartOfAccountName { get; set; }
    public int BudgetSourceId { get; set; }
    public string BudgetSourceName { get; set; }
    public decimal Value { get; set; }
  }
}
