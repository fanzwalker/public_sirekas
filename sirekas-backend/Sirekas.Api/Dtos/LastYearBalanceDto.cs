﻿namespace Sirekas.Api.Dtos
{
  public class LastYearBalanceDto
  {
    public int Id { get; set; }
    public int UnitId { get; set; }
    public string UnitCode { get; set; }
    public string UnitName { get; set; }
    public decimal Value { get; set; }
  }
}
