﻿using Sirekas.Domain.Enums;
using System;

namespace Sirekas.Api.Dtos
{
  public class MessageDto
  {
    public int Id { get; set; }
    public MessageType Type { get; set; }
    public string Title { get; set; }
    public DateTime DateCreate { get; set; }
    public string Description { get; set; }
  }
}
