﻿using Sirekas.Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class MessageCreateDto
  {
    public MessageType Type { get; set; }
    [Required, MaxLength(255)]
    public string Title { get; set; }
    public DateTime DateCreate { get; set; }
    public string Description { get; set; }
  }
}