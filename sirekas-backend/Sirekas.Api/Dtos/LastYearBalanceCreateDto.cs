﻿namespace Sirekas.Api.Dtos
{
  public class LastYearBalanceCreateDto
  {
    public decimal Value { get; set; }
  }
}