﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class CommonFiltersDto
  {
    public int? UnitId { get; set; }
    public Stages Stages { get; set; } = Stages.Raperda;
    public ExcludeType ExcludeType { get; set; } = ExcludeType.NoExclude;
    public UnitStructure UnitStructure { get; set; } = UnitStructure.SubDinasSubUnitBagian;
    public ActivityType ActivityType { get; set; } = ActivityType.Both;
    public BudgetType BudgetType { get; set; } = BudgetType.Both;
  }
}
