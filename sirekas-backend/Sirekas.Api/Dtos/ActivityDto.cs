﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class ActivityDto
  {
    public int Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public int ProgramId { get; set; }
    public ActivityType ActivityType { get; set; }
  }
}
