﻿namespace Sirekas.Api.Dtos
{
  public class UnitActivityCreateDto
  {
    //[Required]
    //public int ActivityId { get; set; }
    public int UnitId { get; set; }
    public int StudentNumber { get; set; }
    //public decimal PerStudentPrice { get; set; }
    public int Rombel { get; set; }
    //public decimal PerRombelPrice { get; set; }
    //public decimal PerUnitPrice { get; set; }
    public decimal ExtraPrice { get; set; }
  }
}
