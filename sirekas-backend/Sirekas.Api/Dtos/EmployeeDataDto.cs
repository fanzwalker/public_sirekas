﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class EmployeeDataDto
  {
    public int Id { get; set; }
    public int UnitId { get; set; }
    public EmployeeDataType EmployeeDataType { get; set; }
    public string EmployeeDataTypeName { get; set; }
    public int Male { get; set; }
    public int Female { get; set; }
    public int Total { get; set; }
    public EmployeeStatus Status { get; set; }
    public string StatusName { get; set; }
  }
}