﻿namespace Sirekas.Api.Dtos
{
  public class StudentDataDto
  {
    public int Id { get; set; }
    public int UnitId { get; set; }
    public string Class { get; set; }
    public int Male { get; set; }
    public int Female { get; set; }
    public int Total { get; set; }
    public int Rombel { get; set; }
  }
}
