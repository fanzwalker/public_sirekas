﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class BudgetSourceDto
  {
    public int Id { get; set; }
    public int? ParentId { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public LevelTypes Type { get; set; }
  }
}
