﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class VisMisUpdateDto
  {
    [Required, MaxLength(255)]
    public string Description { get; set; }
  }
}