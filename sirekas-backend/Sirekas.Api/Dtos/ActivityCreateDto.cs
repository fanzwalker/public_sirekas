﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class ActivityCreateDto
  {
    [Required, MinLength(4)]
    public string Code { get; set; }
    [Required, MaxLength(255)]
    public string Name { get; set; }
    public ActivityType ActivityType { get; set; }
  }
}
