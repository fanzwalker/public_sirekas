﻿namespace Sirekas.Api.Dtos
{
  public class BosnasBudgetDescCreateDto
  {
    public int TotalStudent { get; set; }
    public decimal StandardPrice { get; set; }
  }
}
