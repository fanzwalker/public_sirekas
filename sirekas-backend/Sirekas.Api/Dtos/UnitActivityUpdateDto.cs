﻿namespace Sirekas.Api.Dtos
{
  public class UnitActivityUpdateDto
  {
    public decimal Total { get; set; }
  }
}
