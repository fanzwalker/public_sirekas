﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class BudgetMonthlyDto
  {
    public int Id { get; set; }
    public int BudgetDetId { get; set; }
    public string Code { get; set; }
    public string Description { get; set; }
    public int? ActivityId { get; set; }
    public int ChartOfAccountId { get; set; }
    public string ChartOfAccountCode { get; set; }
    public string ChartOfAccountName { get; set; }
    public int UnitId { get; set; }
    public decimal Total { get; set; }
    public decimal Q1 { get; set; }
    public decimal Q2 { get; set; }
    public decimal Q3 { get; set; }
    public decimal Q4 { get; set; }
    public LevelTypes Type { get; set; }
  }
}
