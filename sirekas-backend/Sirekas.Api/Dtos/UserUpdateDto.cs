﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class UserUpdateDto
  {
    [MaxLength(64), Required]
    public string FullName { get; set; }
    [MinLength(6), Required]
    public string NewPassword { get; set; }
    public Stages Stages { get; set; } = Stages.Raperda;
  }
}
