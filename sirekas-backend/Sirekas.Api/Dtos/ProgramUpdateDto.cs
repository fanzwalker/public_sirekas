﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class ProgramUpdateDto
  {
    [Required, MinLength(3)]
    public string Code { get; set; }
    [Required, MaxLength(255)]
    public string Name { get; set; }
  }
}