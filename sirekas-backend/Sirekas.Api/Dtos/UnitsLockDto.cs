﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class UnitsLockDto
  {
    [Required]
    public IEnumerable<int> Ids { get; set; }
    public bool IsLocked { get; set; }
  }
}
