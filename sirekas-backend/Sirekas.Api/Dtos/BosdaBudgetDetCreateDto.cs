﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class BosdaBudgetDetCreateDto
  {
    [Required]
    public int HeaderId { get; set; }
    public int? SubHeaderId { get; set; }
    [Required]
    public int ChartOfAccountId { get; set; }
    public string Description { get; set; }
    public string Expression { get; set; }
    public string UnitDesc { get; set; }
    public decimal CostPerUnit { get; set; }
  }
}