﻿using System.Collections.Generic;

namespace Sirekas.Api.Dtos
{
  public class JobTitleEmpDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public List<EmployeeDto> Employees { get; set; }
  }
}
