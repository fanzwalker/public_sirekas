﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class BudgetDetDto
  {
    public int Id { get; set; }
    public int? ParentId { get; set; }
    public int? ActivityId { get; set; }
    public int ChartOfAccountId { get; set; }
    public string ChartOfAccountCode { get; set; }
    public string ChartOfAccountName { get; set; }
    public Stages Stages { get; set; }
    public int UnitId { get; set; }
    public BudgetType BudgetSourceId { get; set; }
    public string BudgetSourceName { get; set; }
    public string Code { get; set; }
    public string Description { get; set; }
    public string Expression { get; set; }
    public decimal Amount { get; set; }
    public string UnitDesc { get; set; }
    public decimal CostPerUnit { get; set; }
    public decimal Total { get; set; }
    public LevelTypes Type { get; set; }
  }
}
