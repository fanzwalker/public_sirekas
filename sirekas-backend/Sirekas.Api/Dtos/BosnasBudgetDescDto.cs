﻿namespace Sirekas.Api.Dtos
{
  public class BosnasBudgetDescDto
  {
    public int UnitId { get; set; }
    public string UnitCode { get; set; }
    public string UnitName { get; set; }
    public int UnitEducationLvl { get; set; }
    public int TotalStudent { get; set; }
    public decimal StandardPrice { get; set; }
    public decimal Total { get; set; }
  }
}
