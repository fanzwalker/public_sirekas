﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class ChartOfAccountDto
  {
    public int Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public CoaLevel CoaLevel { get; set; }
    public string Type { get; set; }
    public BudgetType BudgetType { get; set; }
    public string FullName { get; set; }
  }
}
