﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class PerfBenchmarkCreateDto
  {
    public PerfBenchmarkType PerfBenchmarkType { get; set; }
    [MaxLength(255)]
    public string Name { get; set; }
    [MaxLength(255)]
    public string Target { get; set; }
  }
}
