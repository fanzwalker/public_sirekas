﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class EmployeeDataCreateDto
  {
    public EmployeeDataType EmployeeDataType { get; set; }
    public int Male { get; set; }
    public int Female { get; set; }
    public EmployeeStatus Status { get; set; }
  }
}