﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class BudgetSourceUpdateDto
  {
    [Required]
    public string Code { get; set; }
    [Required]
    public string Name { get; set; }
  }
}
