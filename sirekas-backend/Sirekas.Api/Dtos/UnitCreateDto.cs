﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class UnitCreateDto
  {
    [Required, MinLength(4)]
    public string Code { get; set; }
    [Required]
    public string Name { get; set; }
    public BudgetType BudgetType { get; set; }
    public int DistrictId { get; set; }
    public EducationLvl EducationLvl { get; set; }
    [MaxLength(255), Required]
    public string Npsn { get; set; }
  }
}
