﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class BudgetSourceCreateDto
  {
    public int? ParentId { get; set; }
    [Required]
    public string Code { get; set; }
    [Required]
    public string Name { get; set; }
  }
}
