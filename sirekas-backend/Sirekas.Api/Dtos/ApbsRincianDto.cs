﻿namespace Sirekas.Api.Dtos
{
  public class ApbsRincianDto
  {
    public int CoaLevel { get; set; }
    public int Type { get; set; }
    public string CoaCode { get; set; }
    public string Description { get; set; }
    public decimal Amount { get; set; }
    public string UnitDesc { get; set; }
    public decimal CostPerUnit { get; set; }
    public decimal Total { get; set; }
    public decimal BosPusat { get; set; }
    public decimal Bosda { get; set; }
  }
}
