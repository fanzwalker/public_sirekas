﻿namespace Sirekas.Api.Dtos
{
  public class PreviewRka221Dto
  {
    public int Type { get; set; }
    public string CoaCode { get; set; }
    public string Description { get; set; }
    public string Expression { get; set; }
    public decimal? Amount { get; set; }
    public string UnitDesc { get; set; }
    public decimal? CostPerUnit { get; set; }
    public decimal? Q1 { get; set; }
    public decimal? Q2 { get; set; }
    public decimal? Q3 { get; set; }
    public decimal? Q4 { get; set; }
    public decimal? Total { get; set; }
  }
}
