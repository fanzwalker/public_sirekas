﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class PerfBenchmarkDto
  {
    public int Id { get; set; }
    public PerfBenchmarkType PerfBenchmarkType { get; set; }
    public string Name { get; set; }
    public string Target { get; set; }
  }
}
