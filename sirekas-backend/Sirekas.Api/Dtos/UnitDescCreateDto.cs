﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class UnitDescCreateDto
  {
    [Required, MaxLength(32)]
    public string Name { get; set; }
  }
}
