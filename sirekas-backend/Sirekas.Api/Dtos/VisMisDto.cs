﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class VisMisDto
  {
    public int Id { get; set; }
    public VisMisType Type { get; set; }
    public string TypeName { get; set; }
    public int UnitId { get; set; }
    public string UnitCode { get; set; }
    public string UnitName { get; set; }
    public string UnitNpsn { get; set; }
    public string Description { get; set; }
  }
}