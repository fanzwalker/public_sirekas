﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class UnitBudgetDto
  {
    public int Id { get; set; }
    public int UnitId { get; set; }
    public string UnitName { get; set; }
    public Stages Stages { get; set; }
    public decimal Total { get; set; }
  }
}
