﻿namespace Sirekas.Api.Dtos
{
  public class ApbsBtlDto
  {
    public string CoaLevel { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public decimal Total { get; set; }
  }
}
