﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class UnitActivityDto
  {
    public int UnitId { get; set; }
    public string UnitCode { get; set; }
    public string UnitName { get; set; }
    public EducationLvl UnitEducationLvl { get; set; }
    public int ActivityId { get; set; }
    public string ActivityCode { get; set; }
    public string ActivityName { get; set; }
    public Stages Stages { get; set; }
    public int StudentNumber { get; set; }
    public decimal PerStudentPrice { get; set; }
    public decimal TotalPerStudentPrice { get; set; }
    public int Rombel { get; set; }
    public decimal PerRombelPrice { get; set; }
    public decimal TotalPerRombelPrice { get; set; }
    public decimal PerUnitPrice { get; set; }
    public decimal ExtraPrice { get; set; }
    public decimal Total { get; set; }
  }
}
