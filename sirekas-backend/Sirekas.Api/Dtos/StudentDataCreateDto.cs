﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class StudentDataCreateDto
  {
    [Required, MaxLength(5)]
    public string Class { get; set; }
    public int Male { get; set; }
    public int Female { get; set; }
    public int Rombel { get; set; }
  }
}