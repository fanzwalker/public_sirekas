﻿namespace Sirekas.Api.Dtos
{
  public class PerfBenchmarkFixedUpdateDto
  {
    public string Name { get; set; }
    public string Target { get; set; }
  }
}
