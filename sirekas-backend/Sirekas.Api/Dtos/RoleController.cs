﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Sirekas.Data.Repository;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sirekas.Api.Dtos
{
  [Route("api/roles")]
  [Authorize(Policy = "Administrator")]
  public class RoleController : Controller
  {
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly RoleManager<Role> _roleMgr;

    public RoleController(IUnitOfWork unitOfWork, IMapper mapper,
      RoleManager<Role> roleMgr)
    {
      _unitOfWork = unitOfWork;
      _mapper = mapper;
      _roleMgr = roleMgr;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
      try
      {
        var models = await _roleMgr.Roles.ToListAsync();

        return Ok(_mapper.Map<IEnumerable<KeyValuePairDto>>(models));
      }
      catch (Exception e)
      {
        ModelState.AddModelError("error", e.Message ?? e.InnerException.Message);
        return BadRequest(ModelState);
      }
    }
  }
}
