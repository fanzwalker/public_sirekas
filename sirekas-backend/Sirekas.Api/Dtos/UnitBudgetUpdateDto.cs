﻿namespace Sirekas.Api.Dtos
{
  public class UnitBudgetUpdateDto
  {
    public decimal Total { get; set; }
  }
}
