﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class WebOptionCreateDto
  {
    [MaxLength(255), Required]
    public string Name { get; set; }
    [MaxLength(255), Required]
    public string Value { get; set; }
    [MaxLength(255)]
    public string Description { get; set; }
  }
}
