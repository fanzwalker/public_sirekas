﻿namespace Sirekas.Api.Dtos
{
  public class JobTitleDto
  {
    public int Id { get; set; }
    public string Name { get; set; }
  }
}
