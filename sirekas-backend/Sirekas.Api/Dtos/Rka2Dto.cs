﻿namespace Sirekas.Api.Dtos
{
  public class Rka2Dto
  {
    public string Code { get; set; }
    public string Name { get; set; }
    public decimal Total { get; set; }
  }
}
