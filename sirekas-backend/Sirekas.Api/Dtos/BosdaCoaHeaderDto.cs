﻿namespace Sirekas.Api.Dtos
{
  public class BosdaCoaHeaderDto
  {
    public int Id { get; set; }
    public int ChartOfAccountId { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
  }
}
