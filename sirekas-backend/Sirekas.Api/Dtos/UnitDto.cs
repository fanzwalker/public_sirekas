﻿using Sirekas.Domain.Enums;

namespace Sirekas.Api.Dtos
{
  public class UnitDto
  {
    public int Id { get; set; }
    public string Code { get; set; }
    public string Name { get; set; }
    public UnitStructure Level { get; set; }
    public string Type { get; set; }
    public BudgetType BudgetType { get; set; }
    public int DistrictId { get; set; }
    public string DistrictName { get; set; }
    public EducationLvl EducationLvl { get; set; }
    public string Npsn { get; set; }
    public bool IsLocked { get; set; }
  }
}
