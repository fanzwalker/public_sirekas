﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Api.Dtos
{
  public class BudgetDetCreateDto
  {
    public int? ParentId { get; set; }
    [Required]
    public int ChartOfAccountId { get; set; }
    public BudgetType BudgetSourceId { get; set; }
    [MaxLength(255)]
    public string Code { get; set; }
    [MaxLength(512)]
    public string Description { get; set; }
    public string Expression { get; set; }
    public string UnitDesc { get; set; }
    public decimal CostPerUnit { get; set; }
    public LevelTypes Type { get; set; }
  }
}
