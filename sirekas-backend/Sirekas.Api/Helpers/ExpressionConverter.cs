﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Sirekas.Api.Helpers
{
  public static class ExpressionConverter
  {
    public static int Calculate(string expr)
    {
      var operators = new[] {"+", "-", "x", "/"};

      var num = Regex.Split(expr, @"\-|\+|x|\/|\s+")
        .Where(s => !string.IsNullOrEmpty(s) && Regex.IsMatch(s, @"[0-9]")).ToArray();

      var op = Regex.Split(expr, @"\d{1,3}|\s+")
        .Where(s => !string.IsNullOrEmpty(s) && operators.Contains(s)).ToArray();

      var numCtr = 0;
      var lastVal = 0;
      var lastOp = "";

      foreach (var n in num)
      {
        numCtr++;
        if (numCtr == 1)
        {
          lastVal = int.Parse(n);
        }
        else
        {
          if (!string.IsNullOrEmpty(lastOp))
          {
            switch (lastOp)
            {
              case "+":
                lastVal += int.Parse(n);
                break;
              case "-":
                lastVal -= int.Parse(n);
                break;
              case "x":
                lastVal *= int.Parse(n);
                break;
              case "/":
                lastVal /= int.Parse(n);
                break;
            }
          }
        }

        var opCtr = 0;

        foreach (var o in op)
        {
          opCtr++;

          if (opCtr != numCtr) continue;
          lastOp = o;
          break;
        }
      }

      return lastVal;
    }
  }
}
