﻿namespace Sirekas.Domain.Enums
{
  public enum EmployeeDataType
  {
    GuruPns = 1,
    GuruNonPns = 2,
    PegawaiTuPns = 3,
    PegawaiTuNonPns = 4
  }
}