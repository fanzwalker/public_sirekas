﻿namespace Sirekas.Domain.Enums
{
  public enum StandardType
  {
    Bosnas = 1,
    BosdaPerSiswa = 2,
    BosdaPerRombel = 3,
    BosdaPerSekolah = 4
  }
}
