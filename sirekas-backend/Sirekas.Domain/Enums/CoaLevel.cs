﻿namespace Sirekas.Domain.Enums
{
  public enum CoaLevel
  {
    Bab = 1,
    Kelompok,
    Jenis,
    Objek,
    Rincian,
    SubRincian
  }
}
