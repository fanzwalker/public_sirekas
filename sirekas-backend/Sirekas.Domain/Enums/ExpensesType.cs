﻿namespace Sirekas.Domain.Enums
{
  public enum ExpensesType
  {
    BelanjaTidakLangsung = 1,
    BelanjaLangsung
  }
}
