﻿namespace Sirekas.Domain.Enums
{
  public enum PerfBenchmarkType
  {
    Capaian = 1,
    Masukan,
    Keluaran,
    Hasil,
    Kelompok,
    LatarBelakang
  }
}
