﻿namespace Sirekas.Domain.Enums
{
  public enum UnitStructure
  {
    JenisUrusan,
    Urusan,
    SubUrusan,
    DinasUnitSatker,
    SubDinasSubUnitBagian
  }
}
