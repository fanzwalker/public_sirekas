﻿namespace Sirekas.Domain.Enums
{
  public enum Stages
  {
    Raperda = 1,
    Perda,
    RaperdaPerubahan,
    PerdaPerubahan
  }
}
