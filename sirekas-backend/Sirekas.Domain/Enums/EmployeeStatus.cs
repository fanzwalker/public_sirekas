﻿namespace Sirekas.Domain.Enums
{
  public enum EmployeeStatus
  {
    Active = 1,
    NonActive = 2
  }
}
