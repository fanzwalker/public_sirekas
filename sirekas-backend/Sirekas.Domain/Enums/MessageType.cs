﻿namespace Sirekas.Domain.Enums
{
  public enum MessageType
  {
    Information = 1,
    Warning
  }
}
