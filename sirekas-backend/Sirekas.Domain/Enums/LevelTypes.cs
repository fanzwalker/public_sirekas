﻿namespace Sirekas.Domain.Enums
{
  public enum LevelTypes
  {
    None,
    Header,
    Detail
  }
}
