﻿namespace Sirekas.Domain.Enums
{
  public enum TransType
  {
    Income = 1,
    Expense = 2
  }
}
