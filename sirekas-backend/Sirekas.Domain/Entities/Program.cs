﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class Program : KeyValuePair
  {
    [MaxLength(16)]
    public string Code { get; set; }
    public int? UnitId { get; set; }
    public Unit Unit { get; set; }
  }
}