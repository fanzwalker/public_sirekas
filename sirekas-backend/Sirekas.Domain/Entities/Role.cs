﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Sirekas.Domain.Entities
{
  public class Role : IdentityRole<int>
  {
    public ICollection<IdentityRoleClaim<int>> Claims { get; set; }

    public Role()
    {
      Claims = new List<IdentityRoleClaim<int>>();
    }
  }
}