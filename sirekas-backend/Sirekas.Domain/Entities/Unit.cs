﻿using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class Unit : KeyValuePair
  {
    public UnitStructure Level { get; set; }
    [MaxLength(32), Required]
    public string Code { get; set; }
    [MaxLength(2)]
    public string Type { get; set; }
    [MaxLength(255), Required]
    public string Npsn { get; set; }
    public EducationLvl EducationLvl { get; set; }
    public District District { get; set; }
    public int? DistrictId { get; set; }
    public BudgetType BudgetType { get; set; }
    public ICollection<BudgetDet> BudgetDets { get; set; }
    public ICollection<BudgetMonthly> BudgetMonthlies { get; set; }
    public ICollection<UnitActivity> UnitActivities { get; set; }
    public ICollection<BosnasBudgetDesc> BosnasBudgetDescs { get; set; }
    public bool IsLocked { get; set; }

    public Unit()
    {
      BudgetDets = new Collection<BudgetDet>();
      BudgetMonthlies = new Collection<BudgetMonthly>();
      UnitActivities = new Collection<UnitActivity>();
      BosnasBudgetDescs = new Collection<BosnasBudgetDesc>();
    }
  }
}