﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class BosdaCoaHeader
  {
    public int Id { get; set; }
    public ChartOfAccount ChartOfAccount { get; set; }
    public int ChartOfAccountId { get; set; }
    [MaxLength(10), Required]
    public string Code { get; set; }
    [MaxLength(255), Required]
    public string Name { get; set; }
  }
}
