﻿using Sirekas.Domain.Enums;

namespace Sirekas.Domain.Entities
{
  public class PerfBenchmark : KeyValuePair
  {
    public PerfBenchmarkType PerfBenchmarkType { get; set; }
    public int ActivityId { get; set; }
    public Activity Activity { get; set; }
    public Stages Stages { get; set; }
    public int UnitId { get; set; }
    public Unit Unit { get; set; }
    public string Target { get; set; }
  }
}
