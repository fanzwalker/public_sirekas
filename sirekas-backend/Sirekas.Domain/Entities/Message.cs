﻿using Sirekas.Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class Message
  {
    public int Id { get; set; }
    public MessageType Type { get; set; }
    [Required, MaxLength(255)]
    public string Title { get; set; }
    public DateTime DateCreate { get; set; }
    public string Description { get; set; }
  }
}
