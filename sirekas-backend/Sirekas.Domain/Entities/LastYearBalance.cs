﻿namespace Sirekas.Domain.Entities
{
  public class LastYearBalance
  {
    public int Id { get; set; }
    public Unit Unit { get; set; }
    public int UnitId { get; set; }
    public decimal Value { get; set; }
  }
}
