﻿using Microsoft.AspNetCore.Identity;
using Sirekas.Domain.Enums;
using System.Collections.Generic;

namespace Sirekas.Domain.Entities
{
  public class User : IdentityUser<int>
  {
    public Stages Stages { get; set; } = Stages.Raperda;
    public int? UnitId { get; set; }
    public Unit Unit { get; set; }
    public string FullName { get; set; }
    public ICollection<IdentityUserRole<int>> UserRoles { get; set; }

    public User()
    {
      UserRoles = new List<IdentityUserRole<int>>();
    }
  }
}
