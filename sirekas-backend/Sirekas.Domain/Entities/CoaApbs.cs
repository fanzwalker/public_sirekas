﻿using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class CoaApbs
  {
    public int Id { get; set; }
    public CoaApbs Parent { get; set; }
    public int? ParentId { get; set; }
    [Required, MaxLength(255)]
    public string Code { get; set; }
    [Required, MaxLength(255)]
    public string Name { get; set; }
    public TransType TransType { get; set; }
    public LevelTypes LevelTypes { get; set; }
    public ICollection<CoaApbs> Childs { get; set; }
    public ICollection<ManualEntry> ManualEntries { get; set; }

    public CoaApbs()
    {
      Childs = new Collection<CoaApbs>();
      ManualEntries = new Collection<ManualEntry>();
    }
  }
}
