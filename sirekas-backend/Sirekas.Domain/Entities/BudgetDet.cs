﻿using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class BudgetDet
  {
    public int Id { get; set; }
    public int? ParentId { get; set; }
    public BudgetDet Parent { get; set; }
    public int? ActivityId { get; set; }
    public Activity Activity { get; set; }
    public int ChartOfAccountId { get; set; }
    public ChartOfAccount ChartOfAccount { get; set; }
    public Stages Stages { get; set; }
    public int UnitId { get; set; }
    public Unit Unit { get; set; }
    public BudgetType BudgetSourceId { get; set; }
    [MaxLength(255)]
    public string Code { get; set; }
    [MaxLength(512)]
    public string Description { get; set; }
    [MaxLength(255)]
    public string Expression { get; set; }
    public decimal Amount { get; set; }
    [MaxLength(128)]
    public string UnitDesc { get; set; }
    public decimal CostPerUnit { get; set; }
    public decimal Total { get; set; }
    public LevelTypes Type { get; set; }
    public List<BudgetDet> Childs { get; set; }
    public List<BudgetMonthly> BudgetMonthlies { get; set; }

    public BudgetDet()
    {
      Childs = new List<BudgetDet>();
      BudgetMonthlies = new List<BudgetMonthly>();
    }
  }
}