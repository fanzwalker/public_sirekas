﻿namespace Sirekas.Domain.Entities
{
  public class District
  {
    public int Id { get; set; }
    public string Name { get; set; }
  }
}
