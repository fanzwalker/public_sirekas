﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class StudentData
  {
    public int Id { get; set; }
    public Unit Unit { get; set; }
    public int UnitId { get; set; }
    [Required, MaxLength(5)]
    public string Class { get; set; }
    public int Male { get; set; }
    public int Female { get; set; }
    public int Total { get; set; }
    public int Rombel { get; set; }
  }
}
