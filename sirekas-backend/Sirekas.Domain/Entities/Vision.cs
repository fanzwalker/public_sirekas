﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class VisMis
  {
    public int Id { get; set; }
    public Unit Unit { get; set; }
    public int UnitId { get; set; }
    public VisMisType Type { get; set; }
    [Required, MaxLength(255)]
    public string Description { get; set; }
  }
}
