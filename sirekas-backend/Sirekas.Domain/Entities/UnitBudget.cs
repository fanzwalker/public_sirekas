﻿using Sirekas.Domain.Enums;

namespace Sirekas.Domain.Entities
{
  public class UnitBudget
  {
    public int Id { get; set; }
    public int UnitId { get; set; }
    public Unit Unit { get; set; }
    public Stages Stages { get; set; }
    public decimal Total { get; set; }
  }
}
