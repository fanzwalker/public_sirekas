﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class WebOption : KeyValuePair
  {
    [MaxLength(255)]
    public string Value { get; set; }
    [MaxLength(255)]
    public string Description { get; set; }
  }
}