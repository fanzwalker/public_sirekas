﻿using Sirekas.Domain.Enums;

namespace Sirekas.Domain.Entities
{
  public class BudgetMonthly
  {
    public int Id { get; set; }
    public int BudgetDetId { get; set; }
    public BudgetDet BudgetDet { get; set; }
    public int UnitId { get; set; }
    public Unit Unit { get; set; }
    public int? ActivityId { get; set; }
    public Activity Activity { get; set; }
    public Stages Stages { get; set; }
    public decimal Q1 { get; set; }
    public decimal Q2 { get; set; }
    public decimal Q3 { get; set; }
    public decimal Q4 { get; set; }
  }
}