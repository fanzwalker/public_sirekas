﻿using Sirekas.Domain.Enums;

namespace Sirekas.Domain.Entities
{
  public class UnitActivity
  {
    public int UnitId { get; set; }
    public Unit Unit { get; set; }
    public int ActivityId { get; set; }
    public Activity Activity { get; set; }
    public Stages Stages { get; set; }
    public int StudentNumber { get; set; }
    public int Rombel { get; set; }
    public decimal PerStudentPrice { get; set; }
    public decimal TotalPerStudentPrice { get; set; }
    public decimal PerRombelPrice { get; set; }
    public decimal TotalPerRombelPrice { get; set; }
    public decimal PerUnitPrice { get; set; }
    public decimal ExtraPrice { get; set; }
    public decimal Total { get; set; }
  }
}
