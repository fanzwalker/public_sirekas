﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sirekas.Domain.Enums;

namespace Sirekas.Domain.Entities
{
  public class JobTitle : KeyValuePair
  {
    public UnitStructure UnitStructure { get; set; }
    public ICollection<Employee> Employees { get; set; }

    public JobTitle()
    {
      Employees = new Collection<Employee>();
    }
  }
}
