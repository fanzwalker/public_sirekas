﻿using Sirekas.Domain.Enums;

namespace Sirekas.Domain.Entities
{
  public class ManualEntry
  {
    public int Id { get; set; }
    public Unit Unit { get; set; }
    public int UnitId { get; set; }
    public Stages Stages { get; set; }
    public CoaApbs CoaApbs { get; set; }
    public int CoaApbsId { get; set; }
    public decimal Total { get; set; }
  }
}
