﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class BudgetStandard
  {
    public int Id { get; set; }
    [MaxLength(255)]
    public string Name { get; set; }
    public EducationLvl EducationLvl { get; set; }
    public StandardType StandardType { get; set; }
    public decimal Value { get; set; }
  }
}
