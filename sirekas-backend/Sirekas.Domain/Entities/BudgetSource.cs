﻿using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class BudgetSource : KeyValuePair
  {
    public BudgetSource Parent { get; set; }
    public int? ParentId { get; set; }
    [MaxLength(32)]
    public string Code { get; set; }
    public LevelTypes Type { get; set; }
    public ICollection<BudgetSource> Childrens { get; set; }

    public BudgetSource()
    {
      Childrens = new Collection<BudgetSource>();
    }
  }
}
