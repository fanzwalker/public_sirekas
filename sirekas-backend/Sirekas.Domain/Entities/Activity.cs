﻿using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Sirekas.Domain.Entities
{
  public class Activity : KeyValuePair
  {
    public int ProgramId { get; set; }
    public Program Program { get; set; }
    public string Code { get; set; }
    public ActivityType ActivityType { get; set; }
    public ICollection<UnitActivity> UnitActivities { get; set; }

    public Activity()
    {
      UnitActivities = new Collection<UnitActivity>();
    }
  }
}