﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class Employee : KeyValuePair
  {
    [MaxLength(255)]
    public string Nip { get; set; }
    public int UnitId { get; set; }
    public Unit Unit { get; set; }
    public JobTitle JobTitle { get; set; }
    public int JobTitleId { get; set; }
    [MaxLength(255)]
    public string JobDesc { get; set; }
    public int Signer { get; set; }
  }
}