﻿using Sirekas.Domain.Enums;

namespace Sirekas.Domain.Entities
{
  public class BosnasBudgetDesc
  {
    public int Id { get; set; }
    public Unit Unit { get; set; }
    public int UnitId { get; set; }
    public int TotalStudent { get; set; }
    public decimal StandardPrice { get; set; }
    public decimal Total { get; set; }
    public Stages Stages { get; set; }
  }
}
