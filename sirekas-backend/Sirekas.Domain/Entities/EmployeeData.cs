﻿using Sirekas.Domain.Enums;

namespace Sirekas.Domain.Entities
{
  public class EmployeeData
  {
    public int Id { get; set; }
    public Unit Unit { get; set; }
    public int UnitId { get; set; }
    public EmployeeDataType EmployeeDataType { get; set; }
    public int Male { get; set; }
    public int Female { get; set; }
    public int Total { get; set; }
    public EmployeeStatus Status { get; set; }
  }
}