﻿using Sirekas.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class ChartOfAccount : KeyValuePair
  {
    [MaxLength(32)]
    public string Code { get; set; }
    public CoaLevel CoaLevel { get; set; }
    [MaxLength(2)]
    public string Type { get; set; }
    public BudgetType BudgetType { get; set; }
  }
}