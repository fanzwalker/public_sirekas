﻿using System.ComponentModel.DataAnnotations;

namespace Sirekas.Domain.Entities
{
  public class KeyValuePair
  {
    public int Id { get; set; }
    [MaxLength(255)]
    public string Name { get; set; }
  }
}
