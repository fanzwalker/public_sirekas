﻿using Sirekas.Domain.Enums;

namespace Sirekas.Domain.Entities
{
  public class PerfBenchmarkFixed : KeyValuePair
  {
    public PerfBenchmarkType PerfBenchmarkType { get; set; }
    public string Target { get; set; }
    public BudgetType BudgetType { get; set; }
  }
}