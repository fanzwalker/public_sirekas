﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data
{
  public class SirekasDbInitializer
  {
    private readonly SirekasDbContext _context;

    private readonly List<Unit> _units = new List<Unit>
    {
      new Unit {Code = "1.", Name = "URUSAN WAJIB", Level = UnitStructure.JenisUrusan, Type = "H", Npsn = "1"},
      new Unit {Code = "1.01.", Name = "URUSAN WAJIB PELAYANAN DASAR", Level = UnitStructure.Urusan, Type = "H", Npsn = "101"},
      new Unit {Code = "1.01.01.", Name = "PENDIDIKAN", Level = UnitStructure.SubUrusan, Type = "H", Npsn = "10101"},
      new Unit {Code = "1.01.01.01.", Name = "DINAS PENDIDIKAN, PEMUDA DAN OLAHRAGA", Level = UnitStructure.DinasUnitSatker, Type = "H", Npsn = "1010101"}
    };

    //private readonly List<BudgetSource> _budgetSources = new List<BudgetSource>
    //{
    //  new BudgetSource {Code = "01", Name = "APBN"},
    //  new BudgetSource {Code = "02", Name = "APBD Provinsi"},
    //  new BudgetSource {Code = "03", Name = "APBD"},
    //  new BudgetSource {Code = "04", Name = "Partisipasi Masyarakat"},
    //  new BudgetSource {Code = "05", Name = "Bantuan Pihak Ketiga"}
    //};

    private readonly List<Program> _programs = new List<Program>
    {
      new Program {UnitId = 3, Code = "23.", Name = "Program Wajib Belajar Pendidikan Dasar Dua Belas Tahun"}
    };

    private readonly List<UnitDesc> _unitDescs = new List<UnitDesc>
    {
      new UnitDesc { Name = "Orang" },
      new UnitDesc { Name = "Bulan"},
      new UnitDesc { Name = "Kali"}
    };

    private readonly List<JobTitle> _jobTitles = new List<JobTitle>
    {
      new JobTitle {Name = "Kepala Sekolah", UnitStructure = UnitStructure.SubDinasSubUnitBagian},
      new JobTitle {Name = "Komite Sekolah", UnitStructure = UnitStructure.SubDinasSubUnitBagian},
      new JobTitle {Name = "Bendahara BOSNAS", UnitStructure = UnitStructure.SubDinasSubUnitBagian},
      new JobTitle {Name = "Bendahara BOSDA", UnitStructure = UnitStructure.SubDinasSubUnitBagian},
      new JobTitle {Name = "Guru", UnitStructure = UnitStructure.SubDinasSubUnitBagian},
      new JobTitle {Name = "Sekretaris Dinas", UnitStructure = UnitStructure.DinasUnitSatker},
      new JobTitle {Name = "Kepala Bidang", UnitStructure = UnitStructure.DinasUnitSatker},
      new JobTitle {Name = "Kasubbag. Perencanaan Program", UnitStructure = UnitStructure.DinasUnitSatker},
      new JobTitle {Name = "Kasubbag. Keuangan", UnitStructure = UnitStructure.DinasUnitSatker},
      new JobTitle {Name = "PPTK", UnitStructure = UnitStructure.DinasUnitSatker}
    };

    private readonly List<WebOption> _webOptions = new List<WebOption>
    {
      new WebOption {Name = "NmPemda", Description = "Nama Pemda", Value = "Kabupaten Penajam Paser Utara"},
      new WebOption {Name = "NmKaDinas", Description = "Nama Kepala Dinas Pendidikan", Value = "Marjani, S.Sos, M.Si"},
      new WebOption {Name = "NipKaDinas", Description = "NIP Kepala Dinas Pendidikan", Value = "197109231994031008"},
      new WebOption {Name = "NmDinas", Description = "Nama Dinas Induk", Value = "Dinas Pendidikan, Pemuda dan Olahraga"},
      new WebOption {Name = "CurTahun", Description = "Tahun Anggaran", Value = "2018"},
    };

    private readonly List<BudgetStandard> _budgetStandards = new List<BudgetStandard>
    {
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSNAS SD",
        EducationLvl = EducationLvl.Sd,
        Value = 800000m,
        StandardType = StandardType.Bosnas
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSNAS SMP",
        EducationLvl = EducationLvl.Smp,
        Value = 1000000m,
        StandardType = StandardType.Bosnas
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA Per Siswa (TK)",
        EducationLvl = EducationLvl.Tk,
        Value = 350000m,
        StandardType = StandardType.BosdaPerSiswa
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA Per Siswa (SD)",
        EducationLvl = EducationLvl.Sd,
        Value = 375000m,
        StandardType = StandardType.BosdaPerSiswa
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA Per Siswa (SMP)",
        EducationLvl = EducationLvl.Smp,
        Value = 520000m,
        StandardType = StandardType.BosdaPerSiswa
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA Per Rombel (TK)",
        EducationLvl = EducationLvl.Tk,
        Value = 4000000m,
        StandardType = StandardType.BosdaPerRombel
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA Per Rombel (SD)",
        EducationLvl = EducationLvl.Sd,
        Value = 4500000m,
        StandardType = StandardType.BosdaPerRombel
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA Per Rombel (SMP)",
        EducationLvl = EducationLvl.Smp,
        Value = 4500000m,
        StandardType = StandardType.BosdaPerRombel
      },
      new BudgetStandard()
      {
        Name = "Standarisasi Anggaran BOSDA Per Sekolah (TK)",
        EducationLvl = EducationLvl.Tk,
        Value = 10000000m,
        StandardType = StandardType.BosdaPerSekolah
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA SD Jumlah Siswa <= 168 Orang",
        EducationLvl = EducationLvl.Sd,
        Value = 18000000m,
        StandardType = StandardType.BosdaPerSekolah
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA SD Jumlah Siswa <= 337 Orang",
        EducationLvl = EducationLvl.Sd,
        Value = 15000000m,
        StandardType = StandardType.BosdaPerSekolah
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA SD Jumlah Siswa > 337 Orang",
        EducationLvl = EducationLvl.Sd,
        Value = 8000000m,
        StandardType = StandardType.BosdaPerSekolah
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA SMP Jumlah Siswa <= 168 Orang",
        EducationLvl = EducationLvl.Smp,
        Value = 18000000m,
        StandardType = StandardType.BosdaPerSekolah
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA SMP Jumlah Siswa <= 337 Orang",
        EducationLvl = EducationLvl.Smp,
        Value = 15000000m,
        StandardType = StandardType.BosdaPerSekolah
      },
      new BudgetStandard
      {
        Name = "Standarisasi Anggaran BOSDA SMP Jumlah Siswa > 337 Orang",
        EducationLvl = EducationLvl.Smp,
        Value = 8000000m,
        StandardType = StandardType.BosdaPerSekolah
      }
    };

    private readonly List<District> _districts = new List<District>
    {
      new District {Name = "Kecamatan Penajam"},
      new District {Name = "Kecamatan Waru"},
      new District {Name = "Kecamatan Babulu"},
      new District {Name = "Kecamatan Sepaku"}
    };

    private readonly List<CoaApbs> _coaApbsHeaders = new List<CoaApbs>
    {
      new CoaApbs {Code = "1.", Name = "Penerimaan", LevelTypes = LevelTypes.Header, TransType = TransType.Income},
      new CoaApbs {Code = "2.", Name = "Pengeluaran", LevelTypes = LevelTypes.Header, TransType = TransType.Expense}
    };

    private readonly List<CoaApbs> _coaApbsSubHeaders = new List<CoaApbs>
    {
      new CoaApbs {Code = "1.1.", Name = "Saldo Tahun Lalu", LevelTypes = LevelTypes.Header, TransType = TransType.Income},
      new CoaApbs {Code = "1.2.", Name = "Pendapatan Rutin", LevelTypes = LevelTypes.Header, TransType = TransType.Income},
      new CoaApbs {Code = "1.3.", Name = "Bantuan Operasional Sekolah", LevelTypes = LevelTypes.Header, TransType = TransType.Income},
      new CoaApbs {Code = "1.4.", Name = "Bantuan", LevelTypes = LevelTypes.Header, TransType = TransType.Income},
      new CoaApbs {Code = "2.1.", Name = "Pengeluaran Rutin", LevelTypes = LevelTypes.Header, TransType = TransType.Income},
      new CoaApbs {Code = "2.2.", Name = "Operasional Sekolah", LevelTypes = LevelTypes.Header, TransType = TransType.Income},
      new CoaApbs {Code = "2.3.", Name = "Bantuan", LevelTypes = LevelTypes.Header, TransType = TransType.Expense}
    };

    private readonly List<CoaApbs> _coaApbsSecSubHeaders = new List<CoaApbs>
    {
      new CoaApbs {Code = "2.2.1.", Name = "BOS Pusat", LevelTypes = LevelTypes.Header, TransType = TransType.Expense},
      new CoaApbs {Code = "2.2.2.", Name = "BOSDA Kabupaten", LevelTypes = LevelTypes.Header, TransType = TransType.Expense},
    };

    private readonly List<CoaApbs> _coaApbsDetails = new List<CoaApbs>
    {
      new CoaApbs {Code = "1.4.1.", Name = "Dana Dekonsentrasi", LevelTypes = LevelTypes.Detail, TransType = TransType.Income},
      new CoaApbs {Code = "1.4.2.", Name = "Dana Alokasi Khusus (DAK)", LevelTypes = LevelTypes.Detail, TransType = TransType.Income},
      new CoaApbs {Code = "1.4.3.", Name = "Lain-lain (Bantuan Luar Negeri/Hibah)", LevelTypes = LevelTypes.Detail, TransType = TransType.Income},
      new CoaApbs {Code = "2.2.1.1.", Name = "Pengembangan Kompetensi Lulusan", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense},
      new CoaApbs {Code = "2.2.1.2.", Name = "Pengembangan Standar Isi", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense},
      new CoaApbs {Code = "2.2.1.3.", Name = "Pengembangan Pendidik dan Tenaga Kependidikan", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense},
      new CoaApbs {Code = "2.2.1.4.", Name = "Pengembangan Standar Proses", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense},
      new CoaApbs {Code = "2.2.1.5.", Name = "Pengembangan Sarana dan Prasaranan Sekolah", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense},
      new CoaApbs {Code = "2.2.1.6.", Name = "Pengembangan Standar Pengelolaan", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense},
      new CoaApbs {Code = "2.2.1.7.", Name = "Pengembangan Standar Pembiayaan", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense},
      new CoaApbs {Code = "2.2.1.8.", Name = "Pengembangan dan Impelementasi Sistem Penilaian", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense},
      new CoaApbs {Code = "2.3.1.", Name = "Dana Alokasi Khusus (DAK)", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense},
      new CoaApbs {Code = "2.3.2.", Name = "Lain-lain (Bantuan Luar Negeri/Hibah)", LevelTypes = LevelTypes.Detail, TransType = TransType.Expense}
    };

    public SirekasDbInitializer(SirekasDbContext context)
    {
      _context = context;
    }

    public async Task Seed()
    {
      if (!_context.Units.Any())
      {
        _context.AddRange(_units);
        await _context.SaveChangesAsync();
      }

      if (!_context.Programs.Any())
      {
        _context.AddRange(_programs);
        await _context.SaveChangesAsync();
      }

      if (!_context.Activities.Any())
      {
        var program = await _context.Programs.SingleOrDefaultAsync(p => p.Code == "23.");

        if (program != null)
        {
          _context.Activities.Add(new Activity
          {
            Code = "001.",
            Name = "Penyediaan Biaya Operasional Sekolah Daerah TK, SD dan SMP Negeri",
            ProgramId = program.Id
          });

          await _context.SaveChangesAsync();
        }
      }

      if (!_context.UnitDescs.Any())
      {
        _context.UnitDescs.AddRange(_unitDescs);

        await _context.SaveChangesAsync();
      }

      if (!_context.PerfBenchmarkFixeds.Any())
      {
        var perfBenchs = new List<PerfBenchmarkFixed>
        {
          new PerfBenchmarkFixed
          {
            PerfBenchmarkType = PerfBenchmarkType.Capaian,
            Name = "Terlaksananya operasional sekolah",
            Target = "100%",
          },
          new PerfBenchmarkFixed
          {
            PerfBenchmarkType = PerfBenchmarkType.Masukan,
            Name = "Jumlah dana yang dibutuhkan",
            Target = ""
          },
          new PerfBenchmarkFixed
          {
            PerfBenchmarkType = PerfBenchmarkType.Keluaran,
            Name = "Dapat memenuhi kebutuhan operasional sekolah",
            Target = "12 bulan"
          },
          new PerfBenchmarkFixed
          {
            PerfBenchmarkType = PerfBenchmarkType.Hasil,
            Name = "Terlaksananya operasional sekolah",
            Target = "100%"
          }
        };

        await _context.PerfBenchmarkFixeds.AddRangeAsync(perfBenchs);
        await _context.SaveChangesAsync();
      }

      if (!_context.PerfBenchmarkFixeds.Any(p => p.BudgetType == BudgetType.Bosnas))
      {
        var perfBenchs = new List<PerfBenchmarkFixed>
        {
          new PerfBenchmarkFixed
          {
            PerfBenchmarkType = PerfBenchmarkType.Capaian,
            Name = "Terlaksananya operasional sekolah",
            Target = "100%",
            BudgetType = BudgetType.Bosnas
          },
          new PerfBenchmarkFixed
          {
            PerfBenchmarkType = PerfBenchmarkType.Masukan,
            Name = "Jumlah dana yang dibutuhkan",
            Target = "",
            BudgetType = BudgetType.Bosnas
          },
          new PerfBenchmarkFixed
          {
            PerfBenchmarkType = PerfBenchmarkType.Keluaran,
            Name = "Dapat memenuhi kebutuhan operasional sekolah",
            Target = "12 bulan",
            BudgetType = BudgetType.Bosnas
          },
          new PerfBenchmarkFixed
          {
            PerfBenchmarkType = PerfBenchmarkType.Hasil,
            Name = "Terlaksananya operasional sekolah",
            Target = "100%",
            BudgetType = BudgetType.Bosnas
          }
        };

        await _context.PerfBenchmarkFixeds.AddRangeAsync(perfBenchs);
        await _context.SaveChangesAsync();
      }

      if (!_context.JobTitles.Any())
      {
        await _context.AddRangeAsync(_jobTitles);
        await _context.SaveChangesAsync();
      }

      foreach (var jobTitle in _jobTitles)
      {
        if (_context.JobTitles.Any(j => j.Name == jobTitle.Name)) continue;

        _context.JobTitles.Add(jobTitle);

        await _context.SaveChangesAsync();
      }

      foreach (var option in _webOptions)
      {
        if (_context.WebOptions.Any(w => w.Name == option.Name)) continue;

        _context.WebOptions.Add(option);

        await _context.SaveChangesAsync();
      }

      foreach (var bs in _budgetStandards)
      {
        if (await _context.BudgetStandards.AnyAsync(b => b.Name == bs.Name)) continue;

        await _context.BudgetStandards.AddAsync(bs);

        await _context.SaveChangesAsync();
      }

      if (!await _context.Districts.AnyAsync())
      {
        await _context.Districts.AddRangeAsync(_districts);

        await _context.SaveChangesAsync();
      }

      if (!await _context.CoaApbs.AnyAsync())
      {
        await _context.CoaApbs.AddRangeAsync(_coaApbsHeaders);

        await _context.SaveChangesAsync();

        foreach (var sub in _coaApbsSubHeaders)
        {
          var headerId = await _context.CoaApbs
            .Where(c => sub.Code.StartsWith(c.Code))
            .Select(c => c.Id)
            .FirstOrDefaultAsync();

          sub.ParentId = headerId;

          await _context.AddAsync(sub);

          await _context.SaveChangesAsync();
        }

        foreach (var secSub in _coaApbsSecSubHeaders)
        {
          var headerId = await _context.CoaApbs
            .Where(c => secSub.Code.StartsWith(c.Code) && c.ParentId != null)
            .Select(c => c.Id)
            .FirstOrDefaultAsync();

          secSub.ParentId = headerId;

          await _context.AddAsync(secSub);

          await _context.SaveChangesAsync();
        }

        foreach (var det in _coaApbsDetails)
        {
          var headerId = await _context.CoaApbs
            .Where(c => det.Code.StartsWith(c.Code) && c.ParentId != null)
            .Select(c => c.Id)
            .FirstOrDefaultAsync();

          det.ParentId = headerId;

          await _context.AddAsync(det);

          await _context.SaveChangesAsync();
        }
      }
    }
  }
}
