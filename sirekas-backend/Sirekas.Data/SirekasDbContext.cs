﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data
{
  public class SirekasDbContext : IdentityDbContext<User, Role, int>
  {
    public SirekasDbContext(DbContextOptions<SirekasDbContext> options)
      : base(options)
    {
    }

    public DbSet<Unit> Units { get; set; }
    public DbSet<Activity> Activities { get; set; }
    public DbSet<ChartOfAccount> ChartOfAccounts { get; set; }
    public DbSet<Employee> Employees { get; set; }
    public DbSet<Program> Programs { get; set; }
    public DbSet<Year> Years { get; set; }
    public DbSet<BudgetDet> BudgetDets { get; set; }
    public DbSet<BudgetMonthly> BudgetMonthlies { get; set; }
    public DbSet<PerfBenchmark> PerfBenchmarks { get; set; }
    public DbSet<PerfBenchmarkFixed> PerfBenchmarkFixeds { get; set; }
    public DbSet<WebOption> WebOptions { get; set; }
    public DbSet<BudgetSource> BudgetSources { get; set; }
    public DbSet<UnitBudget> UnitBudgets { get; set; }
    public DbSet<UnitActivity> UnitActivities { get; set; }
    public DbSet<UnitDesc> UnitDescs { get; set; }
    public DbSet<JobTitle> JobTitles { get; set; }
    public DbSet<BudgetStandard> BudgetStandards { get; set; }
    public DbSet<LastYearBalance> LastYearBalances { get; set; }
    public DbSet<BosnasBudgetDesc> BosnasBudgetDescs { get; set; }
    public DbSet<District> Districts { get; set; }
    public DbSet<BosdaCoaHeader> BosdaCoaHeaders { get; set; }
    public DbSet<VisMis> VisMises { get; set; }
    public DbSet<EmployeeData> EmployeeDatas { get; set; }
    public DbSet<StudentData> StudentDatas { get; set; }
    public DbSet<CoaApbs> CoaApbs { get; set; }
    public DbSet<ManualEntry> ManualEntries { get; set; }
    public DbSet<Message> Messages { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      modelBuilder.Entity<Unit>(entity =>
      {
        entity.HasMany(e => e.BudgetDets)
          .WithOne(e => e.Unit)
          .OnDelete(DeleteBehavior.Restrict);

        entity.HasIndex(e => new { e.Code, e.Npsn })
          .IsUnique();
      });

      modelBuilder.Entity<BudgetDet>()
        .HasIndex(e => new { e.ActivityId, e.Code, e.Stages, e.UnitId, e.ChartOfAccountId })
        .IsUnique();

      modelBuilder.Entity<Unit>(entity =>
      {
        entity.HasMany(e => e.BudgetMonthlies)
          .WithOne(e => e.Unit)
          .OnDelete(DeleteBehavior.Restrict);

        entity.HasIndex(e => e.Code)
          .IsUnique();
      });

      modelBuilder.Entity<BudgetMonthly>(entity =>
      {
        entity.HasIndex(e => new { e.UnitId, e.BudgetDetId, e.ActivityId, e.Stages })
          .IsUnique();
      });

      modelBuilder.Entity<BosnasBudgetDesc>()
        .HasIndex(e => e.UnitId)
        .IsUnique();

      modelBuilder.Entity<ChartOfAccount>()
        .HasIndex(e => e.Code)
        .IsUnique();

      modelBuilder.Entity<BudgetSource>()
        .HasIndex(e => e.Code)
        .IsUnique();

      modelBuilder.Entity<UnitActivity>(entity =>
      {
        entity.HasKey(e => new { e.ActivityId, e.UnitId, e.Stages });

        entity.HasOne(e => e.Unit)
          .WithMany(e => e.UnitActivities);

        entity.HasOne(e => e.Activity)
          .WithMany(e => e.UnitActivities);
      });

      modelBuilder.Entity<District>()
        .HasIndex(e => e.Name)
        .IsUnique();

      modelBuilder.Entity<Role>()
        .HasMany(e => e.Claims)
        .WithOne()
        .HasForeignKey(e => e.RoleId)
        .IsRequired();

      modelBuilder.Entity<User>()
        .HasMany(e => e.UserRoles)
        .WithOne()
        .HasForeignKey(e => e.UserId)
        .IsRequired();

      modelBuilder.Entity<LastYearBalance>()
        .HasIndex(e => e.UnitId)
        .IsUnique();

      modelBuilder.Entity<UnitDesc>()
        .HasIndex(e => e.Name)
        .IsUnique();

      modelBuilder.Entity<BosdaCoaHeader>()
        .HasIndex(e => new { e.ChartOfAccountId, e.Code, e.Name })
        .IsUnique();

      modelBuilder.Entity<CoaApbs>(entity =>
      {
        entity.HasIndex(e => new { e.Code })
          .IsUnique();

        entity.HasOne(e => e.Parent)
          .WithMany(e => e.Childs)
          .OnDelete(DeleteBehavior.Restrict);
      });

      modelBuilder.Entity<ManualEntry>()
        .HasIndex(e => new { e.UnitId, e.CoaApbsId })
        .IsUnique();

      modelBuilder.Entity<User>().ToTable("User");
      modelBuilder.Entity<Role>().ToTable("Role");
      modelBuilder.Entity<IdentityUserClaim<int>>().ToTable("UserClaim");
      modelBuilder.Entity<IdentityUserRole<int>>().ToTable("UserRole");
      modelBuilder.Entity<IdentityUserLogin<int>>().ToTable("UserLogin");
      modelBuilder.Entity<IdentityRoleClaim<int>>().ToTable("RoleClaim");
      modelBuilder.Entity<IdentityUserToken<int>>().ToTable("UserToken");
    }
  }
}
