﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data
{
  public class JsonDataSeeder<T> where T : class
  {
    private readonly SirekasDbContext _context;

    public JsonDataSeeder(SirekasDbContext context)
    {
      _context = context;
    }

    public async Task Seed(string json)
    {
      var settings = new JsonSerializerSettings
      {
        ContractResolver = new CamelCasePropertyNamesContractResolver()
      };

      var jsonObj = JsonConvert.DeserializeObject<List<T>>(json, settings);

      if (_context.ChartOfAccounts.Any()) return;
      _context.AddRange(jsonObj);
      await _context.SaveChangesAsync();
    }
  }
}
