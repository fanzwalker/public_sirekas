﻿using Microsoft.AspNetCore.Identity;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Enums;

namespace Sirekas.Data
{
  public class IdentityInitializer
  {
    private readonly UserManager<User> _userMgr;
    private readonly RoleManager<Role> _roleMgr;
    private readonly SirekasDbContext _context;

    public IdentityInitializer(UserManager<User> userMgr,
      RoleManager<Role> roleMgr, SirekasDbContext context)
    {
      _userMgr = userMgr;
      _roleMgr = roleMgr;
      _context = context;
    }

    public async Task Seed()
    {
      var roles = new List<Role>
      {
        new Role {Name = "administrator"},
        new Role {Name = "operator"}
      };

      foreach (var role in roles)
      {
        if (await _roleMgr.RoleExistsAsync(role.Name)) continue;

        if (role.Name == "administrator")
          role.Claims.Add(new IdentityRoleClaim<int> { ClaimType = "IsAdmin", ClaimValue = "True" });

        var createdRoleResult = await _roleMgr.CreateAsync(role);

        if (!createdRoleResult.Succeeded)
          throw new InvalidOperationException("Failed to build roles!");
      }

      var users = new List<User>
      {
        new User { UserName = "admin", FullName = "Administrator"}
      };

      var userRoleMaps = new Dictionary<string, string>
      {
        ["admin"] = "administrator"
      };

      //var users = new List<User>();

      //var units = await _context.Units.Where(u => u.Level == UnitStructure.SubDinasSubUnitBagian)
      //  .ToListAsync();

      //foreach (var unit in units)
      //{
      //  users.Add(new User { UserName = unit.Npsn, FullName = unit.Name, UnitId = unit.Id});
      //  userRoleMaps.Add(unit.Npsn, "operator");
      //}

      foreach (var user in users)
      {
        if (await _userMgr.FindByNameAsync(user.UserName) != null) continue;

        var createdUserResult = await _userMgr.CreateAsync(user, "123456");

        var createdUserRoleResult = await _userMgr.AddToRoleAsync(user, userRoleMaps[user.UserName]);

        if (!createdUserResult.Succeeded | !createdUserRoleResult.Succeeded)
          throw new InvalidOperationException("Failed to build user and user roles!");
      }
    }
  }
}
