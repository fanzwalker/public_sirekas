﻿using System.Text.RegularExpressions;

namespace Sirekas.Data.Helpers
{
  public static class LastCodeGenerator
  {
    public static string Generate(string lastCode)
    {
      if (string.IsNullOrWhiteSpace(lastCode)) return "001.";

      var codeNum = int.Parse(Regex.Match(lastCode, @"\d+").Value);

      return string.Format("{0:000}.", codeNum + 1);
    }
  }
}
