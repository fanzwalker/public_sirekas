﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface ICoaApbsRepository : IRepository<CoaApbs>
  {
    Task<IEnumerable<CoaApbs>> GetCoaApbs(TransType transType, string parentCode);
  }

  public class CoaApbsRepository : Repository<CoaApbs>, ICoaApbsRepository
  {
    public CoaApbsRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<CoaApbs>> GetCoaApbs(TransType transType, string parentCode)
    {
      var query = AppContext.CoaApbs.Where(c => c.TransType == transType).AsQueryable();

      if (!string.IsNullOrWhiteSpace(parentCode))
      {
        query = query.Where(q => q.Code.StartsWith(parentCode));
      }

      return await query.ToListAsync();
    }
  }
}
