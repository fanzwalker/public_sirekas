﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IProgramRepository : IRepository<Program>
  {
    Task<IEnumerable<Program>> GetPrograms(int? unitId);
  }

  public class ProgramRepository : Repository<Program>, IProgramRepository
  {
    public ProgramRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<Program>> GetPrograms(int? unitId)
    {
      var query = AppContext.Programs.AsQueryable();

      if (unitId.HasValue)
      {
        query = unitId == 0 ? query.Where(q => q.UnitId == null) : query.Where(q => q.UnitId == unitId.Value);
      }

      return await query.ToListAsync();
    }
  }
}
