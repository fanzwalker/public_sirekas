﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data.Repository
{
  public interface IPerfBenchmarkFixedRepository : IRepository<PerfBenchmarkFixed>
  {

  }

  public class PerfBenchmarkFixedRepository : Repository<PerfBenchmarkFixed>, IPerfBenchmarkFixedRepository
  {
    public PerfBenchmarkFixedRepository(DbContext context) : base(context)
    {
    }
  }
}
