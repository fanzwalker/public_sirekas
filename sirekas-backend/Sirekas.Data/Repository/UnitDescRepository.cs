﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data.Repository
{
  public interface IUnitDescRepository : IRepository<UnitDesc>
  {

  }

  public class UnitDescRepository : Repository<UnitDesc>, IUnitDescRepository
  {
    public UnitDescRepository(DbContext context) : base(context)
    {
    }
  }
}
