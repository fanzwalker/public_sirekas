﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IManualEntryRepository : IRepository<ManualEntry>
  {
    Task<ManualEntry> GetManualEntry(int id);
  }

  public class ManualEntryRepository : Repository<ManualEntry>, IManualEntryRepository
  {
    public ManualEntryRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<ManualEntry> GetManualEntry(int id)
    {
      var query = await AppContext.ManualEntries
        .Include(m => m.Unit)
        .Include(m => m.CoaApbs)
        .Where(m => m.Id == id)
        .SingleOrDefaultAsync();

      return query;
    }
  }
}
