﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IBudgetStandardRepository : IRepository<BudgetStandard>
  {
    Task<IEnumerable<BudgetStandard>> GetAll(StandardType standardTypeId, EducationLvl educationLvl);
  }
  public class BudgetStandardRepository : Repository<BudgetStandard>, IBudgetStandardRepository
  {
    public BudgetStandardRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;


    public async Task<IEnumerable<BudgetStandard>> GetAll(StandardType standardTypeId, EducationLvl educationLvl)
    {
      var query = AppContext.BudgetStandards
        .Where(b => b.StandardType == standardTypeId)
        .AsQueryable();

      if (educationLvl != 0)
        query = query.Where(q => q.EducationLvl == educationLvl);

      return await query.ToListAsync();
    }
  }
}
