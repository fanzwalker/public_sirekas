﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data.Repository
{
  public interface IMessageRepository : IRepository<Message>
  {

  }

  public class MessageRepository : Repository<Message>, IMessageRepository
  {
    public MessageRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;
  }
}
