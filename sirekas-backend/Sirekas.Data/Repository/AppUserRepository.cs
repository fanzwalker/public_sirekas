﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data.Repository
{
  public interface IAppUserRepository : IRepository<User>
  {
  }

  public class AppUserRepository : Repository<User>, IAppUserRepository
  {
    public AppUserRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;
  }
}
