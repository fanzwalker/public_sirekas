﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IEmployeeRepository : IRepository<Employee>
  {
    Task<IEnumerable<Employee>> GetEmployees(Expression<Func<Employee, bool>> predicate);
    Task<Employee> GetEmployee(Expression<Func<Employee, bool>> predicate);
  }

  public class EmployeeRepository : Repository<Employee>, IEmployeeRepository
  {
    public EmployeeRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<Employee>> GetEmployees(Expression<Func<Employee, bool>> predicate)
    {
      var query = await AppContext.Employees
        .Include(e => e.Unit)
        .Include(e => e.JobTitle)
        .Where(predicate)
        .ToListAsync();

      return query;
    }

    public async Task<Employee> GetEmployee(Expression<Func<Employee, bool>> predicate)
    {
      var query = await AppContext.Employees
        .Include(e => e.Unit)
        .Include(e => e.JobTitle)
        .Where(predicate)
        .FirstOrDefaultAsync();

      return query;
    }
  }
}
