﻿using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IUnitOfWork
  {
    IAppUserRepository AppUser { get; }
    IUnitRepository Unit { get; }
    IBudgetSourceRepository BudgetSource { get; }
    IChartOfAccountRepository ChartOfAccount { get; }
    IProgramRepository Program { get; }
    IActivityRepository Activity { get; }
    IUnitBudgetRepository UnitBudget { get; }
    IBudgetDetRepository BudgetDet { get; }
    IUnitActivityRepository UnitActivity { get; }
    IUnitDescRepository UnitDesc { get; }
    IBudgetMonthlyRepository BudgetMonthly { get; }
    IPerfBenchmarkRepository PerfBenchmark { get; }
    IPerfBenchmarkFixedRepository PerfBenchmarkFixed { get; }
    IJobTitleRepository JobTitle { get; }
    IEmployeeRepository Employee { get; }
    IUserRepository User { get; }
    IRoleRepository Role { get; }
    IWebOptionRepository WebOption { get; }
    IBudgetStandardRepository BudgetStandard { get; }
    ILastYearBalanceRepository LastYearBalance { get; }
    IBosnasBudgetDescRepository BosnasBudgetDesc { get; }
    IDistrictRepository District { get; }
    IBosdaCoaHeaderRepository BosdaCoaHeader { get; }
    IVisMisRepository VisMis { get; }
    IEmployeeDataRepository EmployeeData { get; }
    IStudentDataRepository StudentData { get; }
    ICoaApbsRepository CoaApbs { get; }
    IManualEntryRepository ManualEntry { get; }
    IMessageRepository Message { get; }
    Task<bool> Complete();
  }

  public class UnitOfWork : IUnitOfWork
  {
    private readonly SirekasDbContext _context;

    public UnitOfWork(SirekasDbContext context)
    {
      _context = context;
      AppUser = new AppUserRepository(_context);
      Unit = new UnitRepository(_context);
      BudgetSource = new BudgetSourceRepository(_context);
      ChartOfAccount = new ChartOfAccountRepository(_context);
      Program = new ProgramRepository(_context);
      Activity = new ActivityRepository(_context);
      UnitBudget = new UnitBudgetRepository(_context);
      BudgetDet = new BudgetDetRepository(_context);
      UnitActivity = new UnitActivityRepository(_context);
      UnitDesc = new UnitDescRepository(_context);
      BudgetMonthly = new BudgetMonthlyRepository(_context);
      PerfBenchmark = new PerfBenchmarkRepository(_context);
      PerfBenchmarkFixed = new PerfBenchmarkFixedRepository(_context);
      JobTitle = new JobTitleRepository(_context);
      Employee = new EmployeeRepository(_context);
      User = new UserRepository(_context);
      Role = new RoleRepository(_context);
      WebOption = new WebOptionRepository(_context);
      BudgetStandard = new BudgetStandardRepository(_context);
      LastYearBalance = new LastYearBalanceRepository(_context);
      BosnasBudgetDesc = new BosnasBudgetDescRepository(_context);
      District = new DistrictRepository(_context);
      BosdaCoaHeader = new BosdaCoaHeaderRepository(_context);
      VisMis = new VisMisRepository(_context);
      EmployeeData = new EmployeeDataRepository(_context);
      StudentData = new StudentDataRepository(_context);
      CoaApbs = new CoaApbsRepository(_context);
      ManualEntry = new ManualEntryRepository(_context);
      Message = new MessageRepository(_context);
    }

    public IChartOfAccountRepository ChartOfAccount { get; }
    public IProgramRepository Program { get; }
    public IActivityRepository Activity { get; }
    public IUnitBudgetRepository UnitBudget { get; }
    public IBudgetDetRepository BudgetDet { get; }
    public IUnitActivityRepository UnitActivity { get; }
    public IUnitDescRepository UnitDesc { get; }
    public IBudgetMonthlyRepository BudgetMonthly { get; }
    public IPerfBenchmarkRepository PerfBenchmark { get; }
    public IPerfBenchmarkFixedRepository PerfBenchmarkFixed { get; }
    public IJobTitleRepository JobTitle { get; }
    public IEmployeeRepository Employee { get; }
    public IUserRepository User { get; }
    public IRoleRepository Role { get; }
    public IWebOptionRepository WebOption { get; }
    public IBudgetStandardRepository BudgetStandard { get; }
    public ILastYearBalanceRepository LastYearBalance { get; }
    public IBosnasBudgetDescRepository BosnasBudgetDesc { get; }
    public IDistrictRepository District { get; }
    public IBosdaCoaHeaderRepository BosdaCoaHeader { get; }
    public IVisMisRepository VisMis { get; }
    public IEmployeeDataRepository EmployeeData { get; }
    public IStudentDataRepository StudentData { get; }
    public ICoaApbsRepository CoaApbs { get; }
    public IManualEntryRepository ManualEntry { get; }
    public IMessageRepository Message { get; }
    public IAppUserRepository AppUser { get; }
    public IUnitRepository Unit { get; }
    public IBudgetSourceRepository BudgetSource { get; }

    public async Task<bool> Complete()
    {
      return await _context.SaveChangesAsync() > 0;
    }
  }

  public interface IUnitOfWork<TEntity> where TEntity : class
  {
    IRepository<TEntity> CommonRepository { get; }
    Task<bool> Complete();
  }

  public class UnitOfWork<TEntity> : IUnitOfWork<TEntity> where TEntity : class
  {
    private readonly SirekasDbContext _context;

    public UnitOfWork(SirekasDbContext context)
    {
      _context = context;
      CommonRepository = new Repository<TEntity>(_context);
    }

    public IRepository<TEntity> CommonRepository { get; }

    public async Task<bool> Complete()
    {
      return await _context.SaveChangesAsync() > 0;
    }
  }
}
