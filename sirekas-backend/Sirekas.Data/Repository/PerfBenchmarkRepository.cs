﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data.Repository
{
  public interface IPerfBenchmarkRepository : IRepository<PerfBenchmark>
  {
  }

  public class PerfBenchmarkRepository : Repository<PerfBenchmark>, IPerfBenchmarkRepository
  {
    public PerfBenchmarkRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;
  }
}
