﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IJobTitleRepository : IRepository<JobTitle>
  {
    Task<IEnumerable<JobTitle>> GetJobTitles(UnitStructure unitStructure);
    Task<JobTitle> GetJobTitleWithEmployees(Expression<Func<JobTitle, bool>> predicate);
  }

  public class JobTitleRepository : Repository<JobTitle>, IJobTitleRepository
  {
    public JobTitleRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<JobTitle>> GetJobTitles(UnitStructure unitStructure)
    {
      var query = await AppContext.JobTitles
        .Where(j => j.UnitStructure == unitStructure)
        .ToListAsync();

      return query;
    }

    public async Task<JobTitle> GetJobTitleWithEmployees(Expression<Func<JobTitle, bool>> predicate)
    {
      var query = AppContext.JobTitles
        .Where(predicate)
        .AsQueryable();

      return await query.Include(q => q.Employees).FirstOrDefaultAsync();
    }
  }
}
