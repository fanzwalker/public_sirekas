﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Data.Helpers;
using Sirekas.Domain.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IBosdaCoaHeaderRepository : IRepository<BosdaCoaHeader>
  {
    Task<string> GenerateLastCode(int chartOfAccountId);
  }

  public class BosdaCoaHeaderRepository : Repository<BosdaCoaHeader>, IBosdaCoaHeaderRepository
  {
    public BosdaCoaHeaderRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<string> GenerateLastCode(int chartOfAccountId)
    {
      var lastCode = await AppContext.BosdaCoaHeaders
        .Where(b => b.ChartOfAccountId == chartOfAccountId)
        .OrderByDescending(b => b.Code)
        .Select(b => b.Code)
        .FirstOrDefaultAsync();

      return LastCodeGenerator.Generate(lastCode);
    }
  }
}
