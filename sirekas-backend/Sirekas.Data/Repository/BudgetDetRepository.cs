﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Data.Helpers;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IBudgetDetRepository : IRepository<BudgetDet>
  {
    Task<IEnumerable<BudgetDet>> GetAllBudgetDet(Expression<Func<BudgetDet, bool>> predicate,
      int? coaId, string code, LevelTypes type = LevelTypes.None);
    Task<BudgetDet> GetBudgetDet(int id);
    Task<BudgetDet> GetBudgetDet(Expression<Func<BudgetDet, bool>> predicate);
    Task<decimal> GetTotalDetail(int unitId, Stages stageId, int? activityId);
    Task<IEnumerable<BudgetDet>> GetBudgetChilds(int unitId, Stages stageId, int? activityId,
      int id, LevelTypes type = LevelTypes.Header);
    Task<string> GetLastCode(int unitId, Stages stageId, int? activityId, int coaId, LevelTypes type, int? parentId);
    Task RecursiveUpdate(BudgetDet child);
  }

  public class BudgetDetRepository : Repository<BudgetDet>, IBudgetDetRepository
  {
    public BudgetDetRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<BudgetDet>> GetAllBudgetDet(Expression<Func<BudgetDet, bool>> predicate,
      int? coaId, string code, LevelTypes type = LevelTypes.None)
    {
      var query = AppContext.BudgetDets
        .Include(b => b.ChartOfAccount)
        .Where(predicate)
        .AsQueryable();

      if (coaId.HasValue) query = query.Where(q => q.ChartOfAccountId == coaId);

      switch (type)
      {
        case LevelTypes.Header:
          query = query.Where(q => q.Type == type && q.ParentId == null);
          break;
        case LevelTypes.Detail:
          query = query.Where(q => q.Type == type);
          break;
      }

      if (!string.IsNullOrWhiteSpace(code))
        query = query.Where(q => q.Parent.Code == code);

      return await query
        .OrderBy(b => b.ChartOfAccount.Code)
        .ThenBy(b => b.Code).ToListAsync();
    }

    public async Task<BudgetDet> GetBudgetDet(int id)
    {
      return await AppContext.BudgetDets
        .Include(b => b.ChartOfAccount)
        .Where(b => b.Id == id)
        .FirstOrDefaultAsync();
    }

    public async Task<BudgetDet> GetBudgetDet(Expression<Func<BudgetDet, bool>> predicate)
    {
      return await AppContext.BudgetDets
        .Include(b => b.ChartOfAccount)
        .Where(predicate)
        .FirstOrDefaultAsync();
    }

    public async Task<decimal> GetTotalDetail(int unitId, Stages stageId, int? activityId)
    {
      return await AppContext.BudgetDets
        .Where(b => b.UnitId == unitId &&
                    b.Stages == stageId &&
                    b.ActivityId == activityId &&
                    b.Type == LevelTypes.Detail)
        .SumAsync(b => b.Total);
    }

    public async Task<IEnumerable<BudgetDet>> GetBudgetChilds(int unitId, Stages stageId,
      int? activityId, int id, LevelTypes type = LevelTypes.Header)
    {
      var query = AppContext.BudgetDets
           .Include(b => b.ChartOfAccount)
           .Where(b => b.UnitId == unitId &&
                       b.Stages == stageId &&
                       b.ActivityId == activityId &&
                       b.ParentId.Value == id &&
                       b.Type == type)
           .AsQueryable();

      return await query.OrderBy(q => q.Code).ToListAsync();
    }

    public async Task<string> GetLastCode(int unitId, Stages stageId, int? activityId, int coaId, LevelTypes type, int? parentId)
    {
      var lastCode = "";

      var baseQuery = AppContext.BudgetDets
        .Where(b => b.UnitId == unitId &&
                    b.Stages == stageId &&
                    b.ActivityId == activityId &&
                    b.ChartOfAccountId == coaId
        ).AsQueryable();

      if (parentId == null)
      {
        lastCode = await baseQuery
          .Where(b => b.ParentId == null)
          .OrderByDescending(b => b.Code)
          .Select(b => b.Code)
          .FirstOrDefaultAsync() ?? "";

        lastCode = LastCodeGenerator.Generate(lastCode);
      }
      else
      {
        var parentCode = await baseQuery.Where(q => q.Id == parentId)
          .Select(q => q.Code)
          .FirstOrDefaultAsync();

        baseQuery = baseQuery.Where(q => q.ParentId == parentId.Value);

        lastCode = await baseQuery
                     .Where(q => q.ParentId == parentId)
                     .OrderByDescending(q => q.Code)
                     .Select(b => b.Code)
                     .FirstOrDefaultAsync() ?? "";

        lastCode = parentCode +
                   LastCodeGenerator.Generate(
                     string.IsNullOrWhiteSpace(lastCode) ? ""
                       : lastCode.Substring(parentCode.Length));

        //switch (type)
        //{
        //  case LevelTypes.Header:
        //    lastCode = await baseQuery
        //      .Where(q => q.Type == LevelTypes.Header)
        //      .OrderByDescending(q => q.Code)
        //      .Select(b => b.Code)
        //      .FirstOrDefaultAsync() ?? "";

        //    //lastCode = parentCode + GenerateLastCode(lastCode.Substring(parentCode.Length));
        //    lastCode = parentCode +
        //               LastCodeGenerator.Generate(
        //                 string.IsNullOrWhiteSpace(lastCode) ? ""
        //                   : lastCode.Substring(parentCode.Length));
        //    break;
        //  case LevelTypes.Detail:
        //    lastCode = await baseQuery
        //      .Where(q => q.Type == LevelTypes.Detail)
        //      .OrderByDescending(q => q.Code)
        //      .Select(b => b.Code)
        //      .FirstOrDefaultAsync() ?? "";

        //    lastCode = parentCode + LastCodeGenerator.Generate(
        //                 string.IsNullOrWhiteSpace(lastCode) ? ""
        //                   : lastCode.Substring(parentCode.Length));
        //    break;
        //}
      }

      return lastCode;
    }

    public async Task RecursiveUpdate(BudgetDet child)
    {
      var parent = await AppContext.BudgetDets
        .Include(b => b.Childs)
        .Where(b => b.Id == child.ParentId)
        .FirstOrDefaultAsync();

      if (parent == null) return;

      parent.Total = parent.Childs.Sum(c => c.Total);

      await AppContext.SaveChangesAsync();

      var budgetMonthlies = await AppContext.BudgetMonthlies
        .Where(b => b.BudgetDetId == parent.Id)
        .SingleOrDefaultAsync();

      if (budgetMonthlies != null)
      {
        var quarter = parent.Total / 4;
        budgetMonthlies.Q1 = quarter;
        budgetMonthlies.Q2 = quarter;
        budgetMonthlies.Q3 = quarter;
        budgetMonthlies.Q4 = quarter;
      }

      await AppContext.SaveChangesAsync();

      await RecursiveUpdate(parent);
    }
  }
}
