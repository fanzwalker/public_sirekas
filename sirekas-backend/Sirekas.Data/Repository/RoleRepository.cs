﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data.Repository
{
  public interface IRoleRepository : IRepository<Role>
  {

  }

  public class RoleRepository : Repository<Role>, IRoleRepository
  {
    public RoleRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;
  }
}
