﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IBudgetMonthlyRepository : IRepository<BudgetMonthly>
  {
    Task<IEnumerable<BudgetMonthly>> GetBudgetMonthly(int unitId, int activityId, Stages stages);
    Task<BudgetMonthly> GetBudgetMonthly(int budgetMonthlyId);
    Task<BudgetMonthly> GetBudgetMonthly(Expression<Func<BudgetMonthly, bool>> predicate);
  }

  public class BudgetMonthlyRepository : Repository<BudgetMonthly>, IBudgetMonthlyRepository
  {
    public BudgetMonthlyRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<BudgetMonthly>> GetBudgetMonthly(int unitId, int activityId, Stages stages)
    {
      return await AppContext.BudgetMonthlies
        .Include(b => b.BudgetDet)
        .ThenInclude(bd => bd.ChartOfAccount)
        .Where(b => b.UnitId == unitId &&
                    b.ActivityId == (activityId == 0 ? (int?)null : activityId) &&
                    b.Stages == stages)
        .OrderBy(b => b.BudgetDet.ChartOfAccount.Code)
        .ThenBy(b => b.BudgetDet.Code)
        .ToListAsync();
    }

    public async Task<BudgetMonthly> GetBudgetMonthly(int budgetMonthlyId)
    {
      return await AppContext.BudgetMonthlies
        .Include(b => b.BudgetDet)
        .ThenInclude(bd => bd.ChartOfAccount)
        .Where(b => b.Id == budgetMonthlyId)
        .FirstOrDefaultAsync();
    }

    public async Task<BudgetMonthly> GetBudgetMonthly(Expression<Func<BudgetMonthly, bool>> predicate)
    {
      return await AppContext.BudgetMonthlies
        .Include(b => b.BudgetDet)
        .ThenInclude(bd => bd.ChartOfAccount)
        .Where(predicate)
        .FirstOrDefaultAsync();
    }
  }
}
