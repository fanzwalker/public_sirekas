﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IBudgetSourceRepository : IRepository<BudgetSource>
  {
    Task<IEnumerable<BudgetSource>> GetBudgetSources(bool includeRelated, int? levelType);
    Task<BudgetSource> GetBudgetSource(int id, bool includeRelated);
  }

  public class BudgetSourceRepository : Repository<BudgetSource>, IBudgetSourceRepository
  {
    public BudgetSourceRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<BudgetSource>> GetBudgetSources(bool includeRelated, int? levelType)
    {
      var query = AppContext.BudgetSources
        .OrderBy(b => b.Code)
        .AsQueryable();

      if (levelType.HasValue)
        query = query.Where(b => b.Type == (LevelTypes)levelType);

      return includeRelated ?
        await query.Include(q => q.Childrens).ToListAsync() :
        await query.ToListAsync();
    }

    public async Task<BudgetSource> GetBudgetSource(int id, bool includeRelated)
    {
      var query = AppContext.BudgetSources
        .OrderBy(b => b.Code)
        .Where(b => b.Id == id)
        .AsQueryable();

      return includeRelated ?
        await query.Include(q => q.Childrens).FirstOrDefaultAsync() :
        await query.FirstOrDefaultAsync();
    }
  }
}
