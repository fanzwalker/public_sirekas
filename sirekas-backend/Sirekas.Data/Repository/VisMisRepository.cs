﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IVisMisRepository : IRepository<VisMis>
  {
    Task<IEnumerable<VisMis>> GetVisMis(int unitId, VisMisType? visMisType);
    Task<VisMis> GetVisMis(int id);
  }

  public class VisMisRepository : Repository<VisMis>, IVisMisRepository
  {
    public VisMisRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<VisMis>> GetVisMis(int unitId, VisMisType? visMisType)
    {
      var query = AppContext.VisMises
        .Where(v => v.UnitId == unitId)
        .Include(v => v.Unit)
        .AsQueryable();

      if (visMisType.HasValue)
        query = query.Where(q => q.Type == visMisType.Value);

      return await query.ToListAsync();
    }

    public async Task<VisMis> GetVisMis(int id)
    {
      var query = AppContext.VisMises
        .Where(v => v.Id == id)
        .Include(v => v.Unit)
        .AsQueryable();

      return await query.SingleOrDefaultAsync();
    }
  }
}
