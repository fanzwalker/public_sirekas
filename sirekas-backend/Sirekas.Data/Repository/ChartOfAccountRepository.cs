﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace Sirekas.Data.Repository
{
  public interface IChartOfAccountRepository : IRepository<ChartOfAccount>
  {
    Task<IEnumerable<ChartOfAccount>> GetChartOfAccount(string keyword, int? expensesType,
      BudgetType? budgetType);
  }

  public class ChartOfAccountRepository : Repository<ChartOfAccount>, IChartOfAccountRepository
  {
    public ChartOfAccountRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<ChartOfAccount>> GetChartOfAccount(string keyword, int? expensesType,
      BudgetType? budgetType)
    {
      var query = AppContext.ChartOfAccounts
        .Where(c => c.CoaLevel == CoaLevel.Rincian)
        .OrderBy(c => c.Code)
        .AsQueryable();

      var codeFilter = new Dictionary<int, Expression<Func<ChartOfAccount, bool>>>
      {
        [1] = c => c.Code.StartsWith("5.1."),
        [2] = c => c.Code.StartsWith("5.2.")
      };

      if (expensesType.HasValue)
        query = query.Where(codeFilter[expensesType.Value]);

      if (budgetType.HasValue)
        query = query.Where(q => q.BudgetType == budgetType);

      if (!string.IsNullOrWhiteSpace(keyword))
        query = query.Where(q => q.Code.Contains(keyword) || q.Name.ToLower().Contains(keyword.ToLower()));

      return await query.ToListAsync();
    }
  }
}
