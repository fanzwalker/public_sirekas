﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data.Repository
{
  public interface IEmployeeDataRepository : IRepository<EmployeeData>
  {

  }

  public class EmployeeDataRepository : Repository<EmployeeData>, IEmployeeDataRepository
  {
    public EmployeeDataRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;
  }
}
