﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IWebOptionRepository : IRepository<WebOption>
  {
    Task<IEnumerable<WebOption>> Get(string name);
  }

  public class WebOptionRepository : Repository<WebOption>, IWebOptionRepository
  {
    public WebOptionRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<WebOption>> Get(string name)
    {
      var query = AppContext.WebOptions
        .AsQueryable();

      if (!string.IsNullOrWhiteSpace(name))
        query = query.Where(q => q.Name.ToLower().Equals(name.ToLower()));

      return await query.ToListAsync();
    }
  }
}
