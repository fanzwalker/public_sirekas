﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IUnitActivityRepository : IRepository<UnitActivity>
  {
    Task<IEnumerable<UnitActivity>> GetAllUnitActivity(Expression<Func<UnitActivity, bool>> predicate);
    Task<UnitActivity> GetUnitActivity(Expression<Func<UnitActivity, bool>> predicate);
    Task<Decimal> GetTotalPerActivity(Expression<Func<UnitActivity, bool>> predicate);
  }

  public class UnitActivityRepository : Repository<UnitActivity>, IUnitActivityRepository
  {
    public UnitActivityRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<UnitActivity>> GetAllUnitActivity(Expression<Func<UnitActivity, bool>> predicate)
    {
      return await AppContext.UnitActivities
        .Include(u => u.Unit)
        .Include(u => u.Activity)
        .Where(predicate).OrderBy(u => u.Activity.Code)
        .ToListAsync();
    }

    public async Task<UnitActivity> GetUnitActivity(Expression<Func<UnitActivity, bool>> predicate)
    {
      return await AppContext.UnitActivities
        .Include(u => u.Unit)
        .Include(u => u.Activity)
        .Where(predicate)
        .SingleOrDefaultAsync();
    }

    public async Task<decimal> GetTotalPerActivity(Expression<Func<UnitActivity, bool>> predicate)
    {
      var query = await AppContext.UnitActivities
        .Where(predicate)
        .FirstOrDefaultAsync();

      return query?.Total ?? 0;
    }
  }
}
