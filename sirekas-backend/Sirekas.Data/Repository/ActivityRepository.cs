﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IActivityRepository : IRepository<Activity>
  {
    Task<IEnumerable<Activity>> GetAllActivity(int programId, int? unitId,
      ExcludeType excludeType, ActivityType activityType, Stages stages = Stages.Raperda);
  }

  public class ActivityRepository : Repository<Activity>, IActivityRepository
  {
    public ActivityRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<Activity>> GetAllActivity(int programId, int? unitId,
      ExcludeType excludeType, ActivityType activityType, Stages stages = Stages.Raperda)
    {
      var query = AppContext.Activities.Where(a => a.ProgramId == programId).AsQueryable();

      if (excludeType == ExcludeType.ExcludeIfExists && unitId.HasValue)
        query = query.Where(q => q.UnitActivities.Where(u => u.UnitId == unitId.Value && u.Stages == Stages.Raperda)
          .All(c => c.ActivityId != q.Id));

      if (activityType != ActivityType.Both)
        query = query.Where(q => q.ActivityType == activityType);

      return await query.ToListAsync();
    }
  }
}
