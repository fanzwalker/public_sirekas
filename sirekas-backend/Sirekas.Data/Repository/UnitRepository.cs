﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IUnitRepository : IRepository<Unit>
  {
    Task<IEnumerable<Unit>> GetUnits(int? unitId, UnitStructure unitStructure, BudgetType budgetType);
    Task<IEnumerable<Unit>> GetUnitNotExistsInUnitBudget(ActivityType activityType,
      BudgetType budgetType,
      Stages stages = Stages.Raperda);
    Task<Unit> GetUnit(int id);
  }

  public class UnitRepository : Repository<Unit>, IUnitRepository
  {
    public UnitRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<Unit>> GetUnits(int? unitId, UnitStructure unitStructure, BudgetType budgetType)
    {
      var query = AppContext.Units
        .Include(u => u.District)
        .Where(u => u.Level == unitStructure)
        .OrderBy(u => u.Code).AsQueryable();

      if (unitId.HasValue)
        query = query.Where(q => q.Id == unitId);

      if (budgetType != BudgetType.Both)
        query = query.Where(q => q.BudgetType == budgetType || q.BudgetType == BudgetType.Both);

      return await query.ToListAsync();
    }

    public async Task<IEnumerable<Unit>> GetUnitNotExistsInUnitBudget(ActivityType activityType,
      BudgetType budgetType,
      Stages stages = Stages.Raperda)
    {
      var unitInUnitBudgets = await AppContext.UnitActivities
        .Where(u => u.Stages == stages && u.Activity.ActivityType == activityType).ToListAsync();

      var query = AppContext.Units
        .Where(u => u.Level == UnitStructure.SubDinasSubUnitBagian &&
                    unitInUnitBudgets.All(ub => ub.UnitId != u.Id))
        .OrderBy(u => u.Code)
        .AsQueryable();

      if (budgetType != BudgetType.Both)
        query = query.Where(q => q.BudgetType == budgetType || q.BudgetType == BudgetType.Both);

      return await query.ToListAsync();
    }

    public Task<Unit> GetUnit(int id)
    {
      var model = AppContext.Units
        .Include(u => u.District)
        .SingleOrDefaultAsync(u => u.Id == id);

      return model;
    }
  }
}
