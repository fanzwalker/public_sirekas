﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IUserRepository : IRepository<User>
  {
    Task<IEnumerable<User>> GetUsers(Expression<Func<User, bool>> predicate);
  }

  public class UserRepository : Repository<User>, IUserRepository
  {
    public UserRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<User>> GetUsers(Expression<Func<User, bool>> predicate)
    {
      var query = await AppContext.Users
        .Include(u => u.Unit)
        .Include(u => u.UserRoles)
        .Where(predicate)
        .ToListAsync();

      return query;
    }
  }
}
