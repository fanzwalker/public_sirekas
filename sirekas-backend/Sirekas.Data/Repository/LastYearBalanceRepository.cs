﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface ILastYearBalanceRepository : IRepository<LastYearBalance>
  {
    Task<IEnumerable<LastYearBalance>> GetLastYearBalances(Expression<Func<LastYearBalance, bool>> predicate);
    Task<LastYearBalance> GetLastYearBalance(Expression<Func<LastYearBalance, bool>> predicate);
  }

  public class LastYearBalanceRepository : Repository<LastYearBalance>, ILastYearBalanceRepository
  {
    public LastYearBalanceRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<LastYearBalance>> GetLastYearBalances(Expression<Func<LastYearBalance, bool>> predicate)
    {
      var models = await AppContext.LastYearBalances
        .Include(l => l.Unit)
        .Where(predicate)
        .ToListAsync();

      return models;
    }

    public async Task<LastYearBalance> GetLastYearBalance(Expression<Func<LastYearBalance, bool>> predicate)
    {
      var model = await AppContext.LastYearBalances
        .Include(l => l.Unit)
        .Where(predicate)
        .FirstOrDefaultAsync();

      return model;
    }
  }
}
