﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data.Repository
{
  public interface IStudentDataRepository : IRepository<StudentData>
  {

  }

  public class StudentDataRepository : Repository<StudentData>, IStudentDataRepository
  {
    public StudentDataRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;
  }
}
