﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using Sirekas.Domain.Enums;
using System.Linq;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IBosnasBudgetDescRepository : IRepository<BosnasBudgetDesc>
  {
    Task<BosnasBudgetDesc> GetBosnasBudgetDesc(int unitId, Stages stageId);
  }

  public class BosnasBudgetDescRepository : Repository<BosnasBudgetDesc>, IBosnasBudgetDescRepository
  {
    public BosnasBudgetDescRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<BosnasBudgetDesc> GetBosnasBudgetDesc(int unitId, Stages stageId)
    {
      var model = await AppContext.BosnasBudgetDescs
        .Include(b => b.Unit)
        .Where(b => b.UnitId == unitId && b.Stages == stageId)
        .FirstOrDefaultAsync();

      return model;
    }
  }
}
