﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Sirekas.Data.Repository
{
  public interface IUnitBudgetRepository : IRepository<UnitBudget>
  {
    Task<IEnumerable<UnitBudget>> GetUnitBudget(Expression<Func<UnitBudget, bool>> predicate, bool includeRelated = false);
    Task<decimal> GetUnitBudgetValue(Expression<Func<UnitBudget, bool>> predicate);
  }

  public class UnitBudgetRepository : Repository<UnitBudget>, IUnitBudgetRepository
  {
    public UnitBudgetRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;

    public async Task<IEnumerable<UnitBudget>> GetUnitBudget(Expression<Func<UnitBudget, bool>> predicate, bool includeRelated = false)
    {
      return includeRelated
        ? await AppContext.UnitBudgets.Include(u => u.Unit).Where(predicate).OrderBy(u => u.Unit.Code).ToListAsync()
        : await AppContext.UnitBudgets.Where(predicate).ToListAsync();
    }

    public async Task<decimal> GetUnitBudgetValue(Expression<Func<UnitBudget, bool>> predicate)
    {
      var query = await AppContext.UnitBudgets
        .FirstOrDefaultAsync(predicate);

      return query?.Total ?? 0;
    }
  }
}
