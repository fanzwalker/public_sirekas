﻿using Microsoft.EntityFrameworkCore;
using Sirekas.Domain.Entities;

namespace Sirekas.Data.Repository
{
  public interface IDistrictRepository : IRepository<District>
  {

  }

  public class DistrictRepository : Repository<District>, IDistrictRepository
  {
    public DistrictRepository(DbContext context) : base(context)
    {
    }

    public SirekasDbContext AppContext => Context as SirekasDbContext;
  }
}
