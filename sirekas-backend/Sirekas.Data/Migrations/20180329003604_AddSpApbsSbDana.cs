﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpApbsSbDana : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE ApbsSbDana
(@unitId INT, @stageId INT)
AS

BEGIN
DECLARE @nmPemda AS VARCHAR(255), @curTahun VARCHAR(10)

SET @nmPemda = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda' ORDER BY wo.Id)
SET @curTahun = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun' ORDER BY wo.Id)

;WITH GroupChildSum AS (
SELECT bs.ParentId, SUM(bd.Total) AS Total FROM dbo.BudgetDets AS bd
LEFT JOIN dbo.BudgetSources AS bs
ON bs.Id = bd.BudgetSourceId
WHERE bd.Stages = @stageId AND (@unitId IS NULL OR bd.UnitId = @unitId)
GROUP BY bs.ParentId
)

SELECT bs.Code, bs.Name, bs.Type, 
CASE WHEN bs.Type = 1 THEN MAX(ISNULL(g.Total, 0)) ELSE MAX(ISNULL(bd.Total, 0)) END AS Total,
@nmPemda AS NmPemda, @curTahun AS CurTahun
FROM dbo.BudgetSources AS bs
LEFT JOIN 
(
	SELECT bd.ActivityId, bd.BudgetSourceId, SUM(bd.Total) AS Total FROM dbo.BudgetDets AS bd WHERE bd.Type = 2
	AND bd.Stages = @stageId AND (@unitId IS NULL OR bd.UnitId = @unitId)
	GROUP BY bd.ActivityId, bd.BudgetSourceId
) AS bd
ON bd.BudgetSourceId = bs.Id
LEFT JOIN GroupChildSum AS g
ON bs.Id = g.ParentId
GROUP BY bs.Code, bs.Name, bs.Type
ORDER BY bs.Code
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"IF EXISTS(SELECT 1
          FROM   INFORMATION_SCHEMA.ROUTINES
          WHERE  ROUTINE_NAME = 'ApbsSbDana'
                 AND SPECIFIC_SCHEMA = 'dbo')
BEGIN
	DROP PROCEDURE ApbsSbDana
END
");

    }
  }
}
