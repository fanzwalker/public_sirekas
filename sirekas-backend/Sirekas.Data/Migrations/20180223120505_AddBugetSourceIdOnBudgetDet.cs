﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddBugetSourceIdOnBudgetDet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BudgetSourceId",
                table: "BudgetDets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDets_BudgetSourceId",
                table: "BudgetDets",
                column: "BudgetSourceId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDets_BudgetSources_BudgetSourceId",
                table: "BudgetDets",
                column: "BudgetSourceId",
                principalTable: "BudgetSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDets_BudgetSources_BudgetSourceId",
                table: "BudgetDets");

            migrationBuilder.DropIndex(
                name: "IX_BudgetDets_BudgetSourceId",
                table: "BudgetDets");

            migrationBuilder.DropColumn(
                name: "BudgetSourceId",
                table: "BudgetDets");
        }
    }
}
