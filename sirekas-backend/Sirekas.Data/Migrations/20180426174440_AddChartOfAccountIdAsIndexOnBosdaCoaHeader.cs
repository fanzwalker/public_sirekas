﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddChartOfAccountIdAsIndexOnBosdaCoaHeader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BosdaCoaHeaders_ChartOfAccountId",
                table: "BosdaCoaHeaders");

            migrationBuilder.DropIndex(
                name: "IX_BosdaCoaHeaders_Code_Name",
                table: "BosdaCoaHeaders");

            migrationBuilder.CreateIndex(
                name: "IX_BosdaCoaHeaders_ChartOfAccountId_Code_Name",
                table: "BosdaCoaHeaders",
                columns: new[] { "ChartOfAccountId", "Code", "Name" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BosdaCoaHeaders_ChartOfAccountId_Code_Name",
                table: "BosdaCoaHeaders");

            migrationBuilder.CreateIndex(
                name: "IX_BosdaCoaHeaders_ChartOfAccountId",
                table: "BosdaCoaHeaders",
                column: "ChartOfAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_BosdaCoaHeaders_Code_Name",
                table: "BosdaCoaHeaders",
                columns: new[] { "Code", "Name" },
                unique: true);
        }
    }
}
