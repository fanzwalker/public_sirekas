﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class RemoveCodeAsUniqueIndexOnCoaApbs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_CoaApbs_Code_Name",
                table: "CoaApbs");

            migrationBuilder.CreateIndex(
                name: "IX_CoaApbs_Code",
                table: "CoaApbs",
                column: "Code",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_CoaApbs_Code",
                table: "CoaApbs");

            migrationBuilder.CreateIndex(
                name: "IX_CoaApbs_Code_Name",
                table: "CoaApbs",
                columns: new[] { "Code", "Name" },
                unique: true);
        }
    }
}
