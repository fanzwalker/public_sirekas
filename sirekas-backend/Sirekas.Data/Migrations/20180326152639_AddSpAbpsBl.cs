﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpAbpsBl : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[ApbsBl]
@unitId INT, @stageId INT
AS
BEGIN

DECLARE @unitCode VARCHAR(255)
SELECT @unitCode = u.Code FROM dbo.Units AS u WHERE u.Id = @unitId

DECLARE @BaseRkas TABLE (
	Id INT,
	ActivityId INT,
	Amount MONEY,
	Code VARCHAR(255),
	CostPerUnit MONEY,
	Description VARCHAR(255),
	Stages INT,
	Total MONEY,
	UnitDesc VARCHAR(128),
	UnitId INT,
	Expression VARCHAR(255),
	Type INT,
	ChartOfAccountId INT,
	BudgetSourceId INT,
	ProgramCode VARCHAR(16),
	ProgramName VARCHAR(255),
	ActivityCode VARCHAR(255),
	ActivityName VARCHAR(255),
	CoaCode VARCHAR(32),
	CoaName VARCHAR(255),
	BudgetSrcCode VARCHAR(16),
	BudgetSrcName VARCHAR(255),
	UnitCode VARCHAR(255)
)

INSERT INTO @BaseRkas
SELECT bd.Id, bd.ActivityId, bd.Amount, bd.Code, bd.CostPerUnit, bd.Description,
bd.Stages, bd.Total, bd.UnitDesc, bd.UnitId, bd.Expression, bd.Type, bd.ChartOfAccountId,
bd.BudgetSourceId, p.Code AS ProgramCode, p.Name AS ProgramName, a.Code AS ActivityCode, a.Name AS ActivityName, coa.Code AS CoaCode,
coa.Name AS CoaName, bs.Code AS BudgetSrcCode, bs.Name AS BudgetSrcName, u.Code AS UnitCode
FROM dbo.BudgetDets AS bd
INNER JOIN dbo.Activities AS a
ON a.Id = bd.ActivityId
INNER JOIN dbo.Programs AS p
ON p.Id = a.ProgramId
INNER JOIN dbo.Units AS u
ON u.Id = p.UnitId
INNER JOIN dbo.ChartOfAccounts AS coa
ON coa.Id = bd.ChartOfAccountId
LEFT JOIN dbo.BudgetSources AS bs
ON bs.Id = bd.BudgetSourceId
WHERE bd.Type = 2 AND bd.UnitId = @unitId AND bd.Stages = @stageId


DECLARE @ApbsDirect TABLE (
	[Type] CHAR(2),
	Code VARCHAR(255),
	[Desc] VARCHAR(512),
	BudgetSrc VARCHAR(255),
	Total MONEY,
	DetailDesc VARCHAR(512),
	Amount MONEY,
	CostPerUnit MONEY
)

-- Calculate Direct Expense
INSERT INTO @ApbsDirect
SELECT 'H' AS [Type], ISNULL(br.UnitCode, SUBSTRING(@unitCode, 1, 5)) + @unitCode + '00.00.' AS Code, 'BELANJA LANGSUNG' AS [Desc], 
'' AS BudgetSrc, SUM(br.Total), '' AS DetailDesc, NULL AS Amount, NULL AS CostPerunit
FROM @BaseRkas AS br
GROUP BY br.UnitCode

-- Calculate By Program
INSERT INTO @ApbsDirect
SELECT 'H' AS [Type], ISNULL(br.UnitCode, SUBSTRING(@unitCode, 1, 5)) + @unitCode + br.ProgramCode AS Code, 
UPPER(br.ProgramName) AS [Desc], '' AS BudgetSrc, SUM(br.Total) AS Total, '' AS DetailDesc, NULL AS Amount, NULL AS CostPerunit 
FROM @BaseRkas AS br
GROUP BY br.UnitCode, br.ProgramCode, br.ProgramName

-- Calculate By Activity
INSERT INTO @ApbsDirect
SELECT 'H' AS [Type], ISNULL(br.UnitCode, SUBSTRING(@unitCode, 1, 5)) + @unitCode + br.ProgramCode + br.ActivityCode AS Code, UPPER(br.ActivityName) AS [Desc], 
'' AS BudgetSrc, SUM(br.Total) AS Total, '' AS DetailDesc, NULL AS Amount, NULL AS CostPerunit
FROM @BaseRkas AS br
GROUP BY br.UnitCode, br.ProgramCode, br.ActivityCode, br.ActivityName

-- Calculate By Level Header Coa
INSERT INTO @ApbsDirect
SELECT 'H' AS [Type], ISNULL(br.UnitCode, SUBSTRING(@unitCode, 1, 5)) + @unitCode + br.ProgramCode + br.ActivityCode + coa.Code AS Code, 
coa.Name AS [Desc], '' AS BudgetSrc, SUM(br.Total) AS Total, '' AS DetailDesc, NULL AS Amount, NULL AS CostPerunit FROM
(
SELECT * FROM dbo.ChartOfAccounts AS coa WHERE coa.CoaLevel >= 3 AND coa.Type = 'H'
AND LEFT(coa.Code, 4) = '5.2.'
) AS coa CROSS JOIN
@BaseRkas AS br
WHERE br.CoaCode LIKE (coa.Code + '%')
GROUP BY br.UnitCode, br.ProgramCode, br.ActivityCode, coa.Code, coa.Name

-- Calculate By Level Detail Coa 
INSERT INTO @ApbsDirect
SELECT 'D' AS [Type], ISNULL(br.UnitCode, SUBSTRING(@unitCode, 1, 5)) + @unitCode + br.ProgramCode + br.ActivityCode + br.CoaCode AS Code, br.CoaName AS [Desc], 
'' AS BudgetSrc, SUM(br.Total) AS Total, '' AS DetailDesc, NULL AS Amount, NULL AS CostPerunit
FROM @BaseRkas AS br
GROUP BY br.UnitCode, br.ProgramCode, br.ActivityCode, br.ActivityName, br.CoaCode, br.CoaName

-- Calculate By Detail
INSERT INTO @ApbsDirect
SELECT 'D1' AS [Type], ISNULL(br.UnitCode, SUBSTRING(@unitCode, 1, 5)) + @unitCode + br.ProgramCode + br.ActivityCode + br.CoaCode + br.Code AS Code, '' AS [Desc], 
br.BudgetSrcName AS BudgetSrc, SUM(br.Total) AS Total, br.Description AS DetailDesc, br.Amount AS Amount, br.CostPerUnit AS CostPerunit
FROM @BaseRkas AS br
GROUP BY br.UnitCode, br.ProgramCode, br.ActivityCode, br.ActivityName, br.CoaCode, br.CoaName, 
br.Code, br.Description, br.BudgetSrcName, br.Amount, br.CostPerUnit

SELECT * FROM @ApbsDirect AS ad ORDER BY ad.Code
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"IF EXISTS(SELECT 1
          FROM   INFORMATION_SCHEMA.ROUTINES
          WHERE  ROUTINE_NAME = 'ApbsBl'
                 AND SPECIFIC_SCHEMA = 'dbo')
BEGIN
	DROP PROCEDURE ApbsBl
END
");
    }
  }
}
