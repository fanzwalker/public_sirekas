﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddNpsnEducationLvlAndDistrictIdOnUnits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Units_Code",
                table: "Units");

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "Units",
                maxLength: 32,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 32,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DistrictId",
                table: "Units",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EducationLvl",
                table: "Units",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Npsn",
                table: "Units",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Units_Code",
                table: "Units",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Units_DistrictId",
                table: "Units",
                column: "DistrictId");

            migrationBuilder.AddForeignKey(
                name: "FK_Units_Districts_DistrictId",
                table: "Units",
                column: "DistrictId",
                principalTable: "Districts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Units_Districts_DistrictId",
                table: "Units");

            migrationBuilder.DropIndex(
                name: "IX_Units_Code",
                table: "Units");

            migrationBuilder.DropIndex(
                name: "IX_Units_DistrictId",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "DistrictId",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "EducationLvl",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "Npsn",
                table: "Units");

            migrationBuilder.AlterColumn<string>(
                name: "Code",
                table: "Units",
                maxLength: 32,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 32);

            migrationBuilder.CreateIndex(
                name: "IX_Units_Code",
                table: "Units",
                column: "Code",
                unique: true,
                filter: "[Code] IS NOT NULL");
        }
    }
}
