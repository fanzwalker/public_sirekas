﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddParentIdAndTypeOnBudgetSource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentId",
                table: "BudgetSources",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "BudgetSources",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_BudgetSources_ParentId",
                table: "BudgetSources",
                column: "ParentId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetSources_BudgetSources_ParentId",
                table: "BudgetSources",
                column: "ParentId",
                principalTable: "BudgetSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetSources_BudgetSources_ParentId",
                table: "BudgetSources");

            migrationBuilder.DropIndex(
                name: "IX_BudgetSources_ParentId",
                table: "BudgetSources");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "BudgetSources");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "BudgetSources");
        }
    }
}
