﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AlterSpApbsBtl : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"ALTER PROCEDURE [dbo].[ApbsBtl]
(
    @unitId INT,
    @stageId INT,
	@coaLevel INT
)
AS
BEGIN
    DECLARE @nmUnit VARCHAR(255),
            @nmPemda VARCHAR(255);

    SET @nmPemda =
    (
        SELECT wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda'
    );
    SET @nmUnit =
    (
        SELECT u.Name FROM dbo.Units AS u WHERE u.Id = @unitId
    );

    DECLARE @ApbsBtl TABLE
    (
        CoaLevel INT,
        Code VARCHAR(255),
        [Name] VARCHAR(255),
        Total MONEY
    );

    INSERT INTO @ApbsBtl
    (
        CoaLevel,
        Code,
        Name,
        Total
    )
    SELECT coa.CoaLevel,
           coa.Code,
           coa.Name,
           bd.Total
    FROM dbo.ChartOfAccounts AS coa
        LEFT JOIN
        (
            SELECT bd.ChartOfAccountId,
                   SUM(bd.Total) AS Total
            FROM dbo.BudgetDets AS bd
            WHERE bd.Type = 2
                  AND bd.ActivityId IS NULL
                  AND
                  (
                      ISNULL(@unitId, '') = ''
                      OR bd.UnitId = @unitId
                  )
                  AND bd.Stages = @stageId
            GROUP BY bd.ChartOfAccountId
        ) AS bd
            ON bd.ChartOfAccountId = coa.Id;

    SELECT *,
           @nmPemda AS PemdaName,
           @nmUnit AS UnitName
    FROM
    (
        SELECT ab.CoaLevel,
               ab.Code,
               ab.Name,
               ISNULL(   (CASE
                              WHEN ab.CoaLevel = 5 THEN
                                  ab.Total
                              ELSE
                          (
                              SELECT SUM(ab2.Total)
                              FROM @ApbsBtl AS ab2
                              WHERE ab2.Code LIKE ab.Code + '%'
                                    AND ab2.CoaLevel = 5
                          )
                          END
                         ),
                         0
                     ) AS Total
        FROM @ApbsBtl AS ab
    ) a
    WHERE a.Total <> 0 AND a.CoaLevel <= @coaLevel
    ORDER BY a.Code;
END;");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"ALTER PROCEDURE [dbo].[ApbsBtl]
(
    @unitId INT,
    @stageId INT,
	@coaLevel INT
)
AS
BEGIN
    DECLARE @nmUnit VARCHAR(255),
            @nmPemda VARCHAR(255);

    SET @nmPemda =
    (
        SELECT wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda'
    );
    SET @nmUnit =
    (
        SELECT u.Name FROM dbo.Units AS u WHERE u.Id = @unitId
    );

    DECLARE @ApbsBtl TABLE
    (
        CoaLevel INT,
        Code VARCHAR(255),
        [Name] VARCHAR(255),
        Total MONEY
    );

    INSERT INTO @ApbsBtl
    (
        CoaLevel,
        Code,
        Name,
        Total
    )
    SELECT coa.CoaLevel,
           coa.Code,
           coa.Name,
           bd.Total
    FROM dbo.ChartOfAccounts AS coa
        LEFT JOIN
        (
            SELECT bd.ChartOfAccountId,
                   SUM(bd.Total) AS Total
            FROM dbo.BudgetDets AS bd
            WHERE bd.Type = 2
                  AND bd.ActivityId IS NULL
                  AND
                  (
                      ISNULL(@unitId, '') = ''
                      OR bd.UnitId = @unitId
                  )
                  AND bd.Stages = @stageId
            GROUP BY bd.ChartOfAccountId
        ) AS bd
            ON bd.ChartOfAccountId = coa.Id;

    SELECT *,
           @nmPemda AS PemdaName,
           @nmUnit AS UnitName
    FROM
    (
        SELECT ab.CoaLevel,
               ab.Code,
               ab.Name,
               ISNULL(   (CASE
                              WHEN ab.CoaLevel = 5 THEN
                                  ab.Total
                              ELSE
                          (
                              SELECT SUM(ab2.Total)
                              FROM @ApbsBtl AS ab2
                              WHERE ab2.Code LIKE ab.Code + '%'
                                    AND ab2.CoaLevel = 5
                          )
                          END
                         ),
                         0
                     ) AS Total
        FROM @ApbsBtl AS ab
    ) a
    WHERE a.Total <> 0 AND a.CoaLevel <= @coaLevel
    ORDER BY a.Code;
END;");
    }
  }
}
