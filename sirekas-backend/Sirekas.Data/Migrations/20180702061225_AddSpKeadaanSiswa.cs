﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpKeadaanSiswa : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[KeadaanSiswa]
(@unitId AS INT)
AS
BEGIN

--DECLARE @unitId INT

--SET @unitId = 1573

SELECT sd.Class, sd.Male, sd.Female, sd.Rombel, sd.Total 
FROM StudentDatas AS sd WHERE sd.UnitId = @unitId
ORDER BY sd.Class
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
