﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddCoaApbs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CoaApbs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(maxLength: 255, nullable: false),
                    LevelTypes = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    ParentId = table.Column<int>(nullable: false),
                    TransType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoaApbs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CoaApbs_CoaApbs_ParentId",
                        column: x => x.ParentId,
                        principalTable: "CoaApbs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CoaApbs_ParentId",
                table: "CoaApbs",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_CoaApbs_Code_Name",
                table: "CoaApbs",
                columns: new[] { "Code", "Name" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoaApbs");
        }
    }
}
