﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class RemodellingPerfBenchmarkFixed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PerfBenchmarkFixeds_Activities_ActivityId",
                table: "PerfBenchmarkFixeds");

            migrationBuilder.DropForeignKey(
                name: "FK_PerfBenchmarkFixeds_Units_UnitId",
                table: "PerfBenchmarkFixeds");

            migrationBuilder.DropIndex(
                name: "IX_PerfBenchmarkFixeds_ActivityId",
                table: "PerfBenchmarkFixeds");

            migrationBuilder.DropIndex(
                name: "IX_PerfBenchmarkFixeds_UnitId",
                table: "PerfBenchmarkFixeds");

            migrationBuilder.DropColumn(
                name: "ActivityId",
                table: "PerfBenchmarkFixeds");

            migrationBuilder.DropColumn(
                name: "Stages",
                table: "PerfBenchmarkFixeds");

            migrationBuilder.DropColumn(
                name: "UnitId",
                table: "PerfBenchmarkFixeds");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ActivityId",
                table: "PerfBenchmarkFixeds",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Stages",
                table: "PerfBenchmarkFixeds",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UnitId",
                table: "PerfBenchmarkFixeds",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_PerfBenchmarkFixeds_ActivityId",
                table: "PerfBenchmarkFixeds",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_PerfBenchmarkFixeds_UnitId",
                table: "PerfBenchmarkFixeds",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_PerfBenchmarkFixeds_Activities_ActivityId",
                table: "PerfBenchmarkFixeds",
                column: "ActivityId",
                principalTable: "Activities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PerfBenchmarkFixeds_Units_UnitId",
                table: "PerfBenchmarkFixeds",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
