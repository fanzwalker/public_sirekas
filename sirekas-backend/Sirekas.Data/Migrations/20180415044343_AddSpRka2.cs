﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpRka2 : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[Rka2]
(@unitId INT, @stageId INT)
AS
BEGIN

--DECLARE @unitId INT, @stageId INT
--SET @unitId = 5
--SET @stageId = 1

DECLARE @kdUrusan VARCHAR(100), @urusan VARCHAR(255), 
@kdUnit VARCHAR(100), @unit VARCHAR(255), @curYear INT, @pemda VARCHAR(255)

SELECT @curYear = CAST(wo.Value AS INT) FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun'
SELECT @kdUrusan = u.Code, @urusan = u.Name FROM dbo.Units AS u WHERE u.Level = 2
SELECT @kdUnit = u.Code, @unit = u.Name FROM dbo.Units AS u WHERE u.Id = @unitId
SELECT @pemda = wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda'

DECLARE @BaseQuery TABLE (
	ChartOfAccountId INT,
	Code VARCHAR(100),
	Total MONEY
)

INSERT INTO @BaseQuery
SELECT bd.ChartOfAccountId, coa2.Code, SUM(bd.Total) AS Total FROM dbo.BudgetDets AS bd 
	INNER JOIN dbo.ChartOfAccounts AS coa2
	ON coa2.Id = bd.ChartOfAccountId
	WHERE bd.Type = 2 AND bd.UnitId = @unitId AND bd.Stages = @stageId AND bd.ActivityId IS NOT NULL
	GROUP BY bd.ChartOfAccountId, coa2.Code

SELECT *, @curYear AS ThnAnggaran, @kdUrusan AS KodeUrusan, 
@urusan AS NmUrusan, @kdUnit KodeUnit, @unit NmUnit, @pemda NmPemda 
FROM
(
SELECT coa.CoaLevel, coa.Code, coa.Name, 
ISNULL(CASE WHEN coa.Type = 'D' THEN bq.Total ELSE 
(SELECT SUM(bq2.Total) FROM @BaseQuery AS bq2 WHERE bq2.Code LIKE coa.Code + '%') END, 0) AS Total
FROM dbo.ChartOfAccounts AS coa
LEFT JOIN 
@BaseQuery AS bq
ON bq.ChartOfAccountId = coa.Id
WHERE LEFT(coa.Code, 4) <> '5.1.'
) AS fq
WHERE fq.Total <> 0 AND fq.CoaLevel < 4
ORDER BY fq.Code
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"IF EXISTS(SELECT 1
          FROM   INFORMATION_SCHEMA.ROUTINES
          WHERE  ROUTINE_NAME = 'Rka2'
                 AND SPECIFIC_SCHEMA = 'dbo')
BEGIN
	DROP PROCEDURE Rka2
END");
    }
  }
}
