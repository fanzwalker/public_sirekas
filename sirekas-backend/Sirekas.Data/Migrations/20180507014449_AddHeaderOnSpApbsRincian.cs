﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddHeaderOnSpApbsRincian : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"ALTER PROCEDURE [dbo].[ApbsRincian]
@unitId INT, @stageId INT
AS 
BEGIN

DECLARE @kdUnit VARCHAR(100), @unit VARCHAR(255), @curYear INT, @pemda VARCHAR(255)

SELECT @curYear = CAST(wo.Value AS INT) FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun'
SELECT @kdUnit = u.Code, @unit = u.Name FROM dbo.Units AS u WHERE u.Id = @unitId
SELECT @pemda = wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda'

DECLARE @Apbs TABLE
(
	CoaLevel INT,
	CoaCode VARCHAR(255),
	CoaName VARCHAR(1024),
	[Type] INT,
	BudgetDetCode VARCHAR(255),
	BudgetDetDescription VARCHAR(1024),
	Amount MONEY,
	UnitDesc VARCHAR(32),
	CostPerUnit MONEY,
	Total MONEY,
	BosPusat MONEY,
	Bosda MONEY,
	BudgetSourceId INT
)

INSERT INTO @Apbs
SELECT coa.CoaLevel, coa.Code, coa.Name, b.Type, b.Code, b.Description, b.Amount, b.UnitDesc, 
b.CostPerUnit, b.Total, b.BosPusat, b.Bosda, b.BudgetSourceId 
FROM dbo.ChartOfAccounts AS coa
LEFT JOIN (
SELECT bd.ChartOfAccountId, bd.Code, bd.Description, bd.Type,
bd.Amount, ud.Name AS UnitDesc, bd.CostPerUnit, bd.Total,
(CASE WHEN bd.BudgetSourceId = 2 THEN bd.Total ELSE 0 END) AS BosPusat,
(CASE WHEN bd.BudgetSourceId = 1 THEN bd.Total ELSE 0 END) AS Bosda,
bd.BudgetSourceId
FROM dbo.BudgetDets AS bd
LEFT JOIN dbo.UnitDescs AS ud
ON ud.Id = bd.UnitDesc
WHERE bd.UnitId = @unitId AND bd.Stages = @stageId
UNION ALL
SELECT bd.ChartOfAccountId, NULL, NULL, NULL AS Type, 
NULL AS Amount, NULL AS UnitDesc, NULL AS CostPerUnit, SUM(bd.Total) AS Total,
SUM((CASE WHEN bd.BudgetSourceId = 2 THEN bd.Total ELSE 0 END)) AS BosPusat,
SUM((CASE WHEN bd.BudgetSourceId = 1 THEN bd.Total ELSE 0 END)) AS Bosda,
NULL AS BudgetTypeId
FROM dbo.BudgetDets AS bd
INNER JOIN dbo.ChartOfAccounts AS coa
ON coa.Id = bd.ChartOfAccountId
LEFT JOIN dbo.UnitDescs AS ud
ON ud.Id = bd.UnitDesc
WHERE bd.UnitId = @unitId AND bd.Stages = @stageId AND bd.Type = 2
GROUP BY bd.ChartOfAccountId
) AS b
ON coa.Id = b.ChartOfAccountId

SELECT a.CoaLevel,
	   ISNULL(a.Type, 0) AS Type,
	   a.CoaCode,
       a.CoaName,
       (CASE WHEN a.BudgetDetCode IS NULL THEN a.CoaName ELSE a.BudgetDetDescription END) AS Description,
       ISNULL(a.Amount, 0) AS Amount,
       a.UnitDesc,
       ISNULL(a.CostPerUnit, 0) AS CostPerUnit,
       a.Total,
       a.BosPusat,
	   @curYear AS ThnAnggaran,
	   @kdUnit AS KdUnit,
	   @unit AS Unit,
	   @pemda AS NmPemda,
       a.Bosda FROM
(
SELECT a.CoaLevel,
       a.CoaCode,
       a.CoaName,
       a.Type,
       a.BudgetDetCode,
       a.BudgetDetDescription,
       a.Amount,
       a.UnitDesc,
       a.CostPerUnit,
       (CASE WHEN a.Type IN (1, 2) THEN a.Total ELSE (SELECT SUM(a2.Total) FROM @Apbs AS a2 WHERE a2.CoaCode LIKE a.CoaCode + '%' AND a2.Type = 2) END) AS Total,
	   (CASE WHEN a.Type IN (1, 2) THEN a.BosPusat ELSE (SELECT SUM(a2.BosPusat) FROM @Apbs AS a2 WHERE a2.CoaCode LIKE a.CoaCode + '%' AND a2.Type = 2) END) AS BosPusat,
	   (CASE WHEN a.Type IN (1, 2) THEN a.Bosda ELSE (SELECT SUM(a2.Bosda) FROM @Apbs AS a2 WHERE a2.CoaCode LIKE a.CoaCode + '%' AND a2.Type = 2) END) AS Bosda,
       a.BudgetSourceId FROM @Apbs AS a
) AS a
WHERE a.Total <> 0
ORDER BY a.CoaCode, a.BudgetSourceId, a.BudgetDetCode
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
