﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class UpdateExistingBudgetStandard : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("UPDATE BudgetStandards SET StandardType = 1");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("UPDATE BudgetStandards SET StandardType = 0");
    }
  }
}
