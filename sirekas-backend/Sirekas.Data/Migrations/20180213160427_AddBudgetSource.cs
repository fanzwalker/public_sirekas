﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddBudgetSource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BudgetSourceId",
                table: "Budgets",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "BudgetSources",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetSources", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_BudgetSourceId",
                table: "Budgets",
                column: "BudgetSourceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Budgets_BudgetSources_BudgetSourceId",
                table: "Budgets",
                column: "BudgetSourceId",
                principalTable: "BudgetSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Budgets_BudgetSources_BudgetSourceId",
                table: "Budgets");

            migrationBuilder.DropTable(
                name: "BudgetSources");

            migrationBuilder.DropIndex(
                name: "IX_Budgets_BudgetSourceId",
                table: "Budgets");

            migrationBuilder.DropColumn(
                name: "BudgetSourceId",
                table: "Budgets");
        }
    }
}
