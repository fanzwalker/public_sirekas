﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSPRka221Preview : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[Rka221Preview]
@unitId INT, @activityId INT, @stageId INT
AS
BEGIN
DECLARE @Rkas TABLE
(
	CoaCode VARCHAR(255),
	CoaName VARCHAR(1024),
	[Type] INT,
	BudgetDetCode VARCHAR(255),
	BudgetDetDescription VARCHAR(1024),
	BudgetDetType CHAR(1),
	Expression VARCHAR(255),
	Amount MONEY,
	UnitDesc VARCHAR(32),
	CostPerUnit MONEY,
	Total MONEY,
	Q1 MONEY,
	Q2 MONEY,
	Q3 MONEY,
	Q4 MONEY
)

INSERT INTO @Rkas
(
    CoaCode,
    CoaName,
    [Type],
    BudgetDetCode,
    BudgetDetDescription,
    BudgetDetType,
    Expression,
    Amount,
    UnitDesc,
    CostPerUnit,
    Total,
	Q1,
	Q2,
	Q3,
	Q4
)
SELECT coa.Code, coa.Name, bd.Type, bd.Code, bd.Description, bd.Type, bd.Expression, bd.Amount, bd.UnitDesc,
bd.CostPerUnit, bd.Total, bd.Q1, bd.Q2, bd.Q3, bd.Q4 
FROM dbo.ChartOfAccounts AS coa
LEFT JOIN 
(
SELECT bd.Code, bd.Description, bd.Type, bd.Expression, bd.Amount, bd.UnitDesc, bd.CostPerUnit, bd.Total, 
bd.ChartOfAccountId, bm.Q1, bm.Q2, bm.Q3, bm.Q4 FROM dbo.BudgetDets AS bd
LEFT JOIN dbo.BudgetMonthlies AS bm
ON bd.Id = bm.BudgetDetId
WHERE bd.UnitId = @unitId AND bd.ActivityId = @activityId AND bd.Stages = @stageId
) AS bd
ON bd.ChartOfAccountId = coa.Id
UNION ALL
SELECT coa.Code, coa.Name, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, SUM(bd.Total), NULL, NULL, NULL, NULL
FROM dbo.ChartOfAccounts AS coa
LEFT JOIN dbo.BudgetDets AS bd
ON bd.ChartOfAccountId = coa.Id
WHERE bd.UnitId = @unitId AND bd.ActivityId = @activityId AND bd.Stages = @stageId AND bd.Type = 2
GROUP BY coa.CoaLevel, coa.Code, coa.Name, coa.Type

SELECT ISNULL(Rkas.BudgetDetType, 0) AS Type,Rkas.CoaCode,
CASE WHEN Rkas.BudgetDetCode IS NULL THEN Rkas.CoaName ELSE '- ' + Rkas.BudgetDetDescription END AS Description,
Rkas.Expression, Rkas.Amount, Rkas.UnitDesc, Rkas.CostPerUnit, Rkas.Q1, Rkas.Q2, Rkas.Q3, Rkas.Q4, Rkas.Total
FROM 
(
SELECT r.CoaCode, r.CoaName, r.Type, r.BudgetDetCode, r.BudgetDetDescription, r.BudgetDetType, r.Expression, r.Amount, r.UnitDesc, r.CostPerUnit, 
CASE WHEN r.Type IN (1, 2) THEN r.Total ELSE
(SELECT SUM(r2.Total) FROM @Rkas AS r2 WHERE r2.CoaCode LIKE r.CoaCode + '%' AND r2.Type = 2) END AS Total,
r.Q1, r.Q2, r.Q3, r.Q4
FROM @Rkas AS r
) Rkas WHERE Rkas.Total <> 0
ORDER BY Rkas.CoaCode, Rkas.Type, Rkas.BudgetDetCode
END
");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"IF EXISTS(SELECT 1
          FROM   INFORMATION_SCHEMA.ROUTINES
          WHERE  ROUTINE_NAME = 'Rka221Preview'
                 AND SPECIFIC_SCHEMA = 'dbo')
BEGIN
	DROP PROCEDURE Rka221Preview
END
");
    }
  }
}
