﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class DbClearingPrepareForProduction : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"DELETE FROM BudgetDets; 
DELETE FROM PerfBenchmarks;
DELETE FROM [User] WHERE UserName <> 'admin';
DELETE FROM Employees;
DELETE FROM BosnasBudgetDescs;
DELETE FROM LastYearBalances;
DELETE FROM UnitActivities;
DELETE FROM UnitBudgets;
DELETE FROM Units WHERE Level = 4;");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
