﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class UpdateBudgetTypeOnPerfBenchmarkFixed : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"UPDATE PerfBenchmarkFixeds SET BudgetType = 1
WHERE BudgetType = 0");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"UPDATE PerfBenchmarkFixeds SET BudgetType = 0
WHERE BudgetType = 0");
    }
  }
}
