﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class CreateSpHeader : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[Header]
(@unitId INT, @tglSah DATETIME)
AS

BEGIN
--DECLARE @unitId INT, @tglSah INT

--SET @unitId = 1571;
--SET @tglSah = '07/23/2018'

DECLARE @NmPemda VARCHAR(255), @ThnAngg INT,
@Unit VARCHAR(255), 
@Kecamatan VARCHAR(255)

SET @NmPemda = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda' ORDER BY wo.Id)
SET @ThnAngg = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun' ORDER BY wo.Id)

SELECT 
@Unit = UPPER(u.Name),
@Kecamatan = d.Name
FROM dbo.Units AS u 
LEFT JOIN dbo.Districts AS d
ON d.Id = u.DistrictId
WHERE u.Id = @unitId


SELECT @NmPemda AS NmPemda, @ThnAngg AS ThnAng, @Unit AS Unit, @Kecamatan AS Kecamatan, @tglSah AS TglSah
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
