﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class ClearBudgetStandard : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("DELETE FROM BudgetStandards");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
