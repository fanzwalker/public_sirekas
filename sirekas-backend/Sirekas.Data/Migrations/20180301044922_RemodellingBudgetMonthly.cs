﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class RemodellingBudgetMonthly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BudgetId",
                table: "BudgetMonthlies");

            migrationBuilder.RenameColumn(
                name: "Value",
                table: "BudgetMonthlies",
                newName: "Q4");

            migrationBuilder.RenameColumn(
                name: "MonthNumber",
                table: "BudgetMonthlies",
                newName: "BudgetDetId");

            migrationBuilder.AddColumn<decimal>(
                name: "Q1",
                table: "BudgetMonthlies",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Q2",
                table: "BudgetMonthlies",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Q3",
                table: "BudgetMonthlies",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_BudgetMonthlies_BudgetDetId",
                table: "BudgetMonthlies",
                column: "BudgetDetId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetMonthlies_BudgetDets_BudgetDetId",
                table: "BudgetMonthlies",
                column: "BudgetDetId",
                principalTable: "BudgetDets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetMonthlies_BudgetDets_BudgetDetId",
                table: "BudgetMonthlies");

            migrationBuilder.DropIndex(
                name: "IX_BudgetMonthlies_BudgetDetId",
                table: "BudgetMonthlies");

            migrationBuilder.DropColumn(
                name: "Q1",
                table: "BudgetMonthlies");

            migrationBuilder.DropColumn(
                name: "Q2",
                table: "BudgetMonthlies");

            migrationBuilder.DropColumn(
                name: "Q3",
                table: "BudgetMonthlies");

            migrationBuilder.RenameColumn(
                name: "Q4",
                table: "BudgetMonthlies",
                newName: "Value");

            migrationBuilder.RenameColumn(
                name: "BudgetDetId",
                table: "BudgetMonthlies",
                newName: "MonthNumber");

            migrationBuilder.AddColumn<int>(
                name: "BudgetId",
                table: "BudgetMonthlies",
                nullable: false,
                defaultValue: 0);
        }
    }
}
