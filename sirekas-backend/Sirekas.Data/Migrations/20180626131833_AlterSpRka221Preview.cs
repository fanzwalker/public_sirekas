﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AlterSpRka221Preview : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"ALTER PROCEDURE [dbo].[Rka221Preview]
@unitId INT, @activityId INT, @stageId INT
AS
BEGIN

--DECLARE  @unitId INT, @activityId INT, @stageId INT

--SET @unitId = 1571
--SET @activityId = 1
--SET @stageId = 1

DECLARE @NmPemda VARCHAR(255), @ThnAngg INT, @Urusan VARCHAR(255), 
@Unit VARCHAR(255), @Program VARCHAR(255), @Kegiatan VARCHAR(255),
@Kecamatan VARCHAR(255), @SbDana VARCHAR(100), @Dinas VARCHAR(255)

SET @NmPemda = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda' ORDER BY wo.Id)
SET @ThnAngg = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun' ORDER BY wo.Id)
SET @SbDana = (CASE WHEN @activityId = 1 THEN 'BOSDA' ELSE 'BOSNAS' END)

SELECT 
@Unit = u.Code + u.Name,
@Kecamatan = d.Name,
@Dinas = (SELECT TOP(1) u2.Code + u2.Name FROM Units AS u2 WHERE u2.Level = 3 AND u.Code LIKE u2.Code + '%' ORDER BY u2.Code)
FROM dbo.Units AS u 
LEFT JOIN dbo.Districts AS d
ON d.Id = u.DistrictId
WHERE u.Id = @unitId

SELECT TOP (1)
@Urusan = ISNULL(u.Code, '0.00.') + u.Name,
@Program = p.Code + p.Name,
@Kegiatan = a.Code + a.Name
FROM dbo.Activities AS a
INNER JOIN dbo.Programs AS p
ON p.Id = a.ProgramId
INNER JOIN dbo.Units AS u
ON u.Id = p.UnitId
WHERE a.ActivityType = @activityId
ORDER BY a.Code

DECLARE @capaian VARCHAR(255), @keluaran VARCHAR(255), @hasil VARCHAR(255),
@tCapaian VARCHAR(255), @tKeluaran VARCHAR(255), @tHasil VARCHAR(255)

SELECT TOP(1)
@capaian = pbf.Name,
@tCapaian = pbf.Target
FROM PerfBenchmarkFixeds AS pbf 
WHERE pbf.PerfBenchmarkType = 1 AND pbf.BudgetType = @activityId
ORDER BY pbf.PerfBenchmarkType

SELECT TOP(1)
@keluaran = pbf.Name,
@tKeluaran = pbf.Target
FROM PerfBenchmarkFixeds AS pbf 
WHERE pbf.PerfBenchmarkType = 3 AND pbf.BudgetType = @activityId
ORDER BY pbf.PerfBenchmarkType

SELECT TOP(1)
@hasil = pbf.Name,
@tHasil = pbf.Target
FROM PerfBenchmarkFixeds AS pbf 
WHERE pbf.PerfBenchmarkType = 4 AND pbf.BudgetType = @activityId
ORDER BY pbf.PerfBenchmarkType



DECLARE @Rkas TABLE
(
	CoaCode VARCHAR(255),
	CoaName VARCHAR(1024),
	[Type] INT,
	BudgetDetCode VARCHAR(255),
	BudgetDetDescription VARCHAR(1024),
	BudgetDetType CHAR(1),
	Expression VARCHAR(255),
	Amount MONEY,
	UnitDesc VARCHAR(32),
	CostPerUnit MONEY,
	Total MONEY,
	Q1 MONEY,
	Q2 MONEY,
	Q3 MONEY,
	Q4 MONEY
)

INSERT INTO @Rkas
(
    CoaCode,
    CoaName,
    [Type],
    BudgetDetCode,
    BudgetDetDescription,
    BudgetDetType,
    Expression,
    Amount,
    UnitDesc,
    CostPerUnit,
    Total,
	Q1,
	Q2,
	Q3,
	Q4
)
SELECT coa.Code, coa.Name, bd.Type, bd.Code, bd.Description, bd.Type, bd.Expression, bd.Amount, bd.UnitDesc,
bd.CostPerUnit, bd.Total, bd.Q1, bd.Q2, bd.Q3, bd.Q4 
FROM dbo.ChartOfAccounts AS coa
LEFT JOIN 
(
SELECT bd.Code, bd.Description, bd.Type, bd.Expression, bd.Amount, ud.Name AS UnitDesc, bd.CostPerUnit, bd.Total, 
bd.ChartOfAccountId, bm.Q1, bm.Q2, bm.Q3, bm.Q4 FROM dbo.BudgetDets AS bd
LEFT JOIN dbo.BudgetMonthlies AS bm
ON bd.Id = bm.BudgetDetId
LEFT JOIN dbo.UnitDescs AS ud
ON bd.UnitDesc = ud.Id
WHERE bd.UnitId = @unitId AND bd.ActivityId = @activityId AND bd.Stages = @stageId
) AS bd
ON bd.ChartOfAccountId = coa.Id
UNION ALL
SELECT coa.Code, coa.Name, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, SUM(bd.Total), NULL, NULL, NULL, NULL
FROM dbo.ChartOfAccounts AS coa
LEFT JOIN dbo.BudgetDets AS bd
ON bd.ChartOfAccountId = coa.Id
WHERE bd.UnitId = @unitId AND bd.ActivityId = @activityId AND bd.Stages = @stageId AND bd.Type = 2
GROUP BY coa.CoaLevel, coa.Code, coa.Name, coa.Type

SELECT ISNULL(Rkas.BudgetDetType, 0) AS Type,Rkas.CoaCode,
CASE WHEN Rkas.BudgetDetCode IS NULL THEN Rkas.CoaName ELSE Rkas.BudgetDetDescription END AS Description,
Rkas.Expression, Rkas.Amount, Rkas.UnitDesc, Rkas.CostPerUnit, Rkas.Q1, Rkas.Q2, Rkas.Q3, Rkas.Q4, Rkas.Total,
@NmPemda AS NmPemda, @ThnAngg AS ThnAng, @Urusan AS Urusan, @Dinas AS Dinas, @Unit AS Unit,
@Program AS Program, @Kegiatan AS Kegiatan, @Kecamatan AS Kecamatan, @SbDana AS SbDana,
@capaian AS Capaian, @tCapaian AS TCapaian, @keluaran AS Keluaran, 
@tKeluaran AS TKeluaran, @hasil AS Hasil, @tHasil AS THasil
FROM 
(
SELECT r.CoaCode, r.CoaName, r.Type, r.BudgetDetCode, r.BudgetDetDescription, r.BudgetDetType, r.Expression, r.Amount, r.UnitDesc, r.CostPerUnit, 
CASE WHEN r.Type IN (1, 2) THEN r.Total ELSE
(SELECT SUM(r2.Total) FROM @Rkas AS r2 WHERE r2.CoaCode LIKE r.CoaCode + '%' AND r2.Type = 2) END AS Total,
r.Q1, r.Q2, r.Q3, r.Q4
FROM @Rkas AS r
) Rkas WHERE Rkas.Total <> 0
ORDER BY Rkas.CoaCode, Rkas.BudgetDetCode
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
