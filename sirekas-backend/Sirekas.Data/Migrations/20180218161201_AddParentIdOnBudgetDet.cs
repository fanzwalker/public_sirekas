﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddParentIdOnBudgetDet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ParentId",
                table: "BudgetDets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDets_ParentId",
                table: "BudgetDets",
                column: "ParentId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDets_BudgetDets_ParentId",
                table: "BudgetDets",
                column: "ParentId",
                principalTable: "BudgetDets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDets_BudgetDets_ParentId",
                table: "BudgetDets");

            migrationBuilder.DropIndex(
                name: "IX_BudgetDets_ParentId",
                table: "BudgetDets");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "BudgetDets");
        }
    }
}
