﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddBudgetCategoryIdOnChartOfAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BudgetCategoryId",
                table: "ChartOfAccounts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ChartOfAccounts_BudgetCategoryId",
                table: "ChartOfAccounts",
                column: "BudgetCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChartOfAccounts_BudgetCategories_BudgetCategoryId",
                table: "ChartOfAccounts",
                column: "BudgetCategoryId",
                principalTable: "BudgetCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChartOfAccounts_BudgetCategories_BudgetCategoryId",
                table: "ChartOfAccounts");

            migrationBuilder.DropIndex(
                name: "IX_ChartOfAccounts_BudgetCategoryId",
                table: "ChartOfAccounts");

            migrationBuilder.DropColumn(
                name: "BudgetCategoryId",
                table: "ChartOfAccounts");
        }
    }
}
