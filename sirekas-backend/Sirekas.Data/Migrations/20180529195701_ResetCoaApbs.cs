﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class ResetCoaApbs : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"
DELETE FROM ManualEntries;
DELETE FROM CoaApbs;");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
