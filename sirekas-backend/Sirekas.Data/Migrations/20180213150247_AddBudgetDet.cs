﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddBudgetDet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BudgetDet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityId = table.Column<int>(nullable: true),
                    Amount = table.Column<float>(nullable: false),
                    BudgetId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(maxLength: 255, nullable: true),
                    CostPerUnit = table.Column<float>(nullable: false),
                    Description = table.Column<string>(maxLength: 512, nullable: true),
                    Stages = table.Column<int>(nullable: false),
                    Total = table.Column<float>(nullable: false),
                    UnitDesc = table.Column<string>(maxLength: 128, nullable: true),
                    UnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetDet", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BudgetDet_Activities_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BudgetDet_Budgets_BudgetId",
                        column: x => x.BudgetId,
                        principalTable: "Budgets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BudgetDet_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDet_ActivityId",
                table: "BudgetDet",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDet_BudgetId",
                table: "BudgetDet",
                column: "BudgetId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDet_UnitId",
                table: "BudgetDet",
                column: "UnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BudgetDet");
        }
    }
}
