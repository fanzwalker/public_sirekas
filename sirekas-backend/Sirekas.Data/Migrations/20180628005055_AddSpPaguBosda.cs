﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpPaguBosda : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[PaguBosda]
(@stageId INT)
AS
BEGIN
--DECLARE @stageId INT

--SET @stageId = 1

SELECT u.Name,
       a.StudentNumber,
       a.PerStudentPrice,
       a.TotalPerStudentPrice, 
       a.Rombel,
       a.PerRombelPrice,
       a.TotalPerRombelPrice,
       a.PerUnitPrice,
       a.ExtraPrice,
       a.Total
	   FROM Units AS u 
LEFT JOIN
(
	SELECT ua.ActivityId,
           ua.UnitId,
           ua.Stages,
           ua.Total,
           ua.ExtraPrice,
           ua.PerRombelPrice,
           ua.PerStudentPrice,
           ua.PerUnitPrice,
           ua.Rombel,
           ua.StudentNumber,
           ua.TotalPerRombelPrice,
           ua.TotalPerStudentPrice
           FROM UnitActivities AS ua
	INNER JOIN Activities AS a
	ON a.Id = ua.ActivityId
	WHERE a.ActivityType = 1 AND ua.Stages = @stageId
) AS a
ON a.UnitId = u.Id
WHERE u.Level = 4 AND u.BudgetType IN ('1', '3')
ORDER BY u.Code
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"IF EXISTS(SELECT 1
          FROM   INFORMATION_SCHEMA.ROUTINES
          WHERE  ROUTINE_NAME = 'PaguBosda'
                 AND SPECIFIC_SCHEMA = 'dbo')
BEGIN
	DROP PROCEDURE PaguBosda
END");

    }
  }
}
