﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpKeadaanPeg : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[KeadaanPeg]
(@unitId INT, @tglSah AS DATETIME)
AS
BEGIN

--DECLARE @unitId INT

--SET @unitId = 1573

DECLARE @nmUnit VARCHAR(255), @nmKaSekolah VARCHAR(255), @nipKaSekolah VARCHAR(255)

SELECT @nmUnit = u.Name FROM Units AS u WHERE u.Id = @unitId

SELECT TOP(1) @nmKaSekolah = e.Name, @nipKaSekolah = e.Nip FROM JobTitles AS jt 
INNER JOIN Employees AS e
ON e.JobTitleId = jt.Id
WHERE jt.Name IN ('Kepala Sekolah') 
AND e.Signer = 1 AND e.UnitId = @unitId
ORDER BY e.Id

SELECT 
(CASE ed.EmployeeDataType 
WHEN 1 THEN 'Guru PNS' 
WHEN 2 THEN 'Guru Non PNS'
WHEN 3 THEN 'Pegawai/TU PNS'
WHEN 4 THEN 'Pegawai/TU Non PNS'
ELSE NULL END) AS EmpType,
ed.Female, ed.Male, ed.Total,
(CASE ed.Status
WHEN 1 THEN 'Aktif'
WHEN 2 THEN 'Non Aktif' ELSE '-' END) AS Status, @nmUnit AS NmUnit,
@nmKaSekolah AS NmKaSekolah, @nipKaSekolah AS NipKaSekolah, @tglSah AS TglSah
FROM EmployeeDatas AS ed WHERE ed.UnitId = @unitId
ORDER BY ed.EmployeeDataType
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
