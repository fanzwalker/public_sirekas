﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddBudgetMonthly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDet_Activities_ActivityId",
                table: "BudgetDet");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDet_Budgets_BudgetId",
                table: "BudgetDet");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDet_Units_UnitId",
                table: "BudgetDet");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BudgetDet",
                table: "BudgetDet");

            migrationBuilder.RenameTable(
                name: "BudgetDet",
                newName: "BudgetDets");

            migrationBuilder.RenameIndex(
                name: "IX_BudgetDet_UnitId",
                table: "BudgetDets",
                newName: "IX_BudgetDets_UnitId");

            migrationBuilder.RenameIndex(
                name: "IX_BudgetDet_BudgetId",
                table: "BudgetDets",
                newName: "IX_BudgetDets_BudgetId");

            migrationBuilder.RenameIndex(
                name: "IX_BudgetDet_ActivityId",
                table: "BudgetDets",
                newName: "IX_BudgetDets_ActivityId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BudgetDets",
                table: "BudgetDets",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "BudgetMonthlies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityId = table.Column<int>(nullable: false),
                    BudgetId = table.Column<int>(nullable: false),
                    MonthNumber = table.Column<int>(nullable: false),
                    Stages = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    Value = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetMonthlies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BudgetMonthlies_Activities_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BudgetMonthlies_Budgets_BudgetId",
                        column: x => x.BudgetId,
                        principalTable: "Budgets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BudgetMonthlies_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BudgetMonthlies_ActivityId",
                table: "BudgetMonthlies",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetMonthlies_BudgetId",
                table: "BudgetMonthlies",
                column: "BudgetId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetMonthlies_UnitId",
                table: "BudgetMonthlies",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDets_Activities_ActivityId",
                table: "BudgetDets",
                column: "ActivityId",
                principalTable: "Activities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDets_Budgets_BudgetId",
                table: "BudgetDets",
                column: "BudgetId",
                principalTable: "Budgets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDets_Units_UnitId",
                table: "BudgetDets",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDets_Activities_ActivityId",
                table: "BudgetDets");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDets_Budgets_BudgetId",
                table: "BudgetDets");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDets_Units_UnitId",
                table: "BudgetDets");

            migrationBuilder.DropTable(
                name: "BudgetMonthlies");

            migrationBuilder.DropPrimaryKey(
                name: "PK_BudgetDets",
                table: "BudgetDets");

            migrationBuilder.RenameTable(
                name: "BudgetDets",
                newName: "BudgetDet");

            migrationBuilder.RenameIndex(
                name: "IX_BudgetDets_UnitId",
                table: "BudgetDet",
                newName: "IX_BudgetDet_UnitId");

            migrationBuilder.RenameIndex(
                name: "IX_BudgetDets_BudgetId",
                table: "BudgetDet",
                newName: "IX_BudgetDet_BudgetId");

            migrationBuilder.RenameIndex(
                name: "IX_BudgetDets_ActivityId",
                table: "BudgetDet",
                newName: "IX_BudgetDet_ActivityId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_BudgetDet",
                table: "BudgetDet",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDet_Activities_ActivityId",
                table: "BudgetDet",
                column: "ActivityId",
                principalTable: "Activities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDet_Budgets_BudgetId",
                table: "BudgetDet",
                column: "BudgetId",
                principalTable: "Budgets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDet_Units_UnitId",
                table: "BudgetDet",
                column: "UnitId",
                principalTable: "Units",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
