﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpPaguBosnas : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE PaguBosnas
(@stageId INT)
AS

BEGIN
--DECLARE @stageId INT

--SET @stageId = 1

SELECT u.Code,
       u.Name,
       b.StandardPrice,
       b.Total,
       b.TotalStudent
	   FROM Units AS u 
LEFT JOIN
(
	SELECT bbd.StandardPrice,
           bbd.Total,
           bbd.TotalStudent,
           bbd.UnitId
		   FROM BosnasBudgetDescs AS bbd 
		   WHERE bbd.Stages = @stageId
) AS b
ON b.UnitId = u.Id
WHERE u.BudgetType IN ('2', '3')
ORDER BY u.Code
END
");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"IF EXISTS(SELECT 1
          FROM   INFORMATION_SCHEMA.ROUTINES
          WHERE  ROUTINE_NAME = 'PaguBosnas'
                 AND SPECIFIC_SCHEMA = 'dbo')
BEGIN
	DROP PROCEDURE PaguBosnas
END");
    }
  }
}
