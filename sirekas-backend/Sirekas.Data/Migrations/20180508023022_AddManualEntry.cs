﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddManualEntry : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ManualEntries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CoaApbsId = table.Column<int>(nullable: false),
                    Stages = table.Column<int>(nullable: false),
                    Total = table.Column<decimal>(nullable: false),
                    UnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ManualEntries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ManualEntries_CoaApbs_CoaApbsId",
                        column: x => x.CoaApbsId,
                        principalTable: "CoaApbs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ManualEntries_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ManualEntries_CoaApbsId",
                table: "ManualEntries",
                column: "CoaApbsId");

            migrationBuilder.CreateIndex(
                name: "IX_ManualEntries_UnitId_CoaApbsId",
                table: "ManualEntries",
                columns: new[] { "UnitId", "CoaApbsId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ManualEntries");
        }
    }
}
