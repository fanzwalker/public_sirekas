﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpHeaderRka : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[HeaderRka221]
(@unitId INT, @activityTypeId INT)
AS

BEGIN
--DECLARE @unitId INT, @activityTypeId INT

--SET @unitId = 1571;
--SET @activityTypeId = 1

DECLARE @NmPemda VARCHAR(255), @ThnAngg INT, @Urusan VARCHAR(255), 
@Unit VARCHAR(255), @Program VARCHAR(255), @Kegiatan VARCHAR(255),
@Kecamatan VARCHAR(255), @SbDana VARCHAR(100)

SET @NmPemda = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda' ORDER BY wo.Id)
SET @ThnAngg = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun' ORDER BY wo.Id)
SET @SbDana = (CASE WHEN @activityTypeId = 1 THEN 'BOSDA' ELSE 'BOSNAS' END)

SELECT 
@Unit = u.Code + u.Name,
@Kecamatan = d.Name
FROM dbo.Units AS u 
LEFT JOIN dbo.Districts AS d
ON d.Id = u.DistrictId
WHERE u.Id = @unitId

SELECT TOP (1)
@Urusan = ISNULL(u.Code, '0.00.') + u.Name,
@Program = p.Code + p.Name,
@Kegiatan = a.Code + a.Name
FROM dbo.Activities AS a
INNER JOIN dbo.Programs AS p
ON p.Id = a.ProgramId
INNER JOIN dbo.Units AS u
ON u.Id = p.UnitId
WHERE a.ActivityType = @activityTypeId
ORDER BY a.Code

SELECT @NmPemda AS NmPemda, @ThnAngg AS ThnAng, @Urusan AS Urusan, @Unit AS Unit,
@Program AS Program, @Kegiatan AS Kegiatan, @Kecamatan AS Kecamatan, @SbDana AS SbDana
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"IF EXISTS(SELECT 1
          FROM   INFORMATION_SCHEMA.ROUTINES
          WHERE  ROUTINE_NAME = 'HeaderRka221'
                 AND SPECIFIC_SCHEMA = 'dbo')
BEGIN
	DROP PROCEDURE HeaderRka221
END");
    }
  }
}
