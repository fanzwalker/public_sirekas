﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddBudgetTypeIdOnChartOfAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChartOfAccounts_BudgetCategories_BudgetCategoryId",
                table: "ChartOfAccounts");

            migrationBuilder.DropTable(
                name: "BudgetCategories");

            migrationBuilder.DropIndex(
                name: "IX_ChartOfAccounts_BudgetCategoryId",
                table: "ChartOfAccounts");

            migrationBuilder.DropColumn(
                name: "BudgetCategoryId",
                table: "ChartOfAccounts");

            migrationBuilder.AddColumn<int>(
                name: "BudgetType",
                table: "ChartOfAccounts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BudgetType",
                table: "ChartOfAccounts");

            migrationBuilder.AddColumn<int>(
                name: "BudgetCategoryId",
                table: "ChartOfAccounts",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BudgetCategories",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetCategories", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChartOfAccounts_BudgetCategoryId",
                table: "ChartOfAccounts",
                column: "BudgetCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetCategories_Name",
                table: "BudgetCategories",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_ChartOfAccounts_BudgetCategories_BudgetCategoryId",
                table: "ChartOfAccounts",
                column: "BudgetCategoryId",
                principalTable: "BudgetCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
