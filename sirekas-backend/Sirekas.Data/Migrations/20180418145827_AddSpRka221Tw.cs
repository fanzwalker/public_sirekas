﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpRka221Tw : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[Rka221Tw]
(@unitId INT, @stageId INT, @activityTypeId INT)
AS

--DECLARE @unitId INT, @stageId INT, @activityTypeId INT

--SET @unitId = 1571;
--SET @stageId = 1;
--SET @activityTypeId = 1

BEGIN
DECLARE @activityId INT

SELECT TOP(1) @activityId = a.Id FROM dbo.Activities AS a 
WHERE a.ActivityType = @activityTypeId
ORDER BY a.Code

DECLARE @BaseQuery TABLE (
    ChartOfAccountId INT,
	Code VARCHAR(100),
	Name VARCHAR(255),
	Total MONEY,
	Q1 MONEY,
	Q2 MONEY,
	Q3 MONEY,
	Q4 MONEY
)

INSERT INTO @BaseQuery
SELECT coa.Id, coa.Code, coa.Name, SUM(bd.Total) AS Total, 
SUM(bm.Q1) AS Q1, SUM(bm.Q2) AS Q2, SUM(bm.Q3) AS Q3, SUM(bm.Q4) AS Q4 
FROM dbo.BudgetDets AS bd
INNER JOIN dbo.BudgetMonthlies AS bm
ON bm.BudgetDetId = bd.Id
INNER JOIN dbo.ChartOfAccounts AS coa
ON coa.Id = bd.ChartOfAccountId
WHERE bd.Type = 2 AND bd.Stages = @stageId 
AND bd.UnitId = @unitId AND bd.ActivityId = @activityId
GROUP BY coa.Id, coa.Code, coa.Name

SELECT * FROM 
(
SELECT coa.CoaLevel, coa.Type, coa.Code, coa.Name, 
ISNULL(CASE WHEN coa.Type = 'D' THEN bq.Total
ELSE (SELECT SUM(bq2.Total) FROM @BaseQuery AS bq2 WHERE bq2.Code LIKE coa.Code + '%') 
END, 0) AS Total,
ISNULL(CASE WHEN coa.Type = 'D' THEN bq.Q1
ELSE (SELECT SUM(bq2.Q1) FROM @BaseQuery AS bq2 WHERE bq2.Code LIKE coa.Code + '%') 
END, 0) AS Q1,
ISNULL(CASE WHEN coa.Type = 'D' THEN bq.Q2
ELSE (SELECT SUM(bq2.Q2) FROM @BaseQuery AS bq2 WHERE bq2.Code LIKE coa.Code + '%') 
END, 0) AS Q2,
ISNULL(CASE WHEN coa.Type = 'D' THEN bq.Q3
ELSE (SELECT SUM(bq2.Q3) FROM @BaseQuery AS bq2 WHERE bq2.Code LIKE coa.Code + '%') 
END, 0) AS Q3,
ISNULL(CASE WHEN coa.Type = 'D' THEN bq.Q4
ELSE (SELECT SUM(bq2.Q4) FROM @BaseQuery AS bq2 WHERE bq2.Code LIKE coa.Code + '%') 
END, 0) AS Q4
FROM dbo.ChartOfAccounts AS coa
LEFT JOIN @BaseQuery AS bq 
ON coa.Id = bq.ChartOfAccountId
) AS fq
WHERE fq.Total <> 0
ORDER BY fq.Code
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"IF EXISTS(SELECT 1
          FROM   INFORMATION_SCHEMA.ROUTINES
          WHERE  ROUTINE_NAME = 'Rka221Tw'
                 AND SPECIFIC_SCHEMA = 'dbo')
BEGIN
	DROP PROCEDURE Rka221Tw
END");
    }
  }
}
