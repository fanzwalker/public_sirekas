﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpKendaliBosnas : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE KendaliBosnas
(@stageId INT)
AS
BEGIN
--DECLARE @stageId INT

--SET @stageId = 1

DECLARE @NmPemda VARCHAR(255), @ThnAngg INT

SET @NmPemda = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda' ORDER BY wo.Id)
SET @ThnAngg = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun' ORDER BY wo.Id)

;WITH PaguRka
(UnitId, Pagu, Saldo, Rka)
AS
(
	SELECT a.UnitId, a.Total AS Pagu, b.Total AS Saldo, c.Total AS Rka FROM
	(
		SELECT bbd.UnitId, bbd.Total FROM BosnasBudgetDescs AS bbd
		WHERE bbd.Stages = @stageId
	) AS a
	LEFT JOIN 
	(
		SELECT lyb.UnitId, lyb.Value AS Total FROM LastYearBalances AS lyb
	) AS b ON a.UnitId = b.UnitId
	LEFT JOIN
	(
		SELECT bd.UnitId, SUM(bd.Total) AS Total FROM BudgetDets AS bd 
		INNER JOIN Activities AS a
		ON a.Id = bd.ActivityId
		WHERE bd.Type = 2
		AND a.ActivityType = 2
		AND bd.Stages = @stageId
		GROUP BY bd.UnitId
	) AS c ON a.UnitId = c.UnitId
)

SELECT u.Code,
u.Name, ISNULL(p.Pagu, 0) AS Pagu, ISNULL(p.Saldo, 0) AS SisaSaldo, 
JmlBosnas = ISNULL(p.Pagu + p.Saldo, 0), ISNULL(p.Rka, 0) AS Rka,  
Selisih = ISNULL(p.Pagu + p.Saldo, 0) - ISNULL(p.Rka, 0), @NmPemda AS NmPemda, @ThnAngg AS ThnAngg
FROM Units AS u 
LEFT JOIN PaguRka AS p
ON p.UnitId = u.Id
WHERE u.Level = 4 AND u.BudgetType IN (2, 3)
ORDER BY u.Code
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
