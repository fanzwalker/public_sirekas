﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpTimPenyusun : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[TimPenyusun]
(@unitId INT, @tglSah DATETIME)
AS
BEGIN
--DECLARE @unitId INT, @tglSah DATETIME

--SET @unitId = 1573
--SET @tglSah = '07/09/2018'

DECLARE @nmKaDinas VARCHAR(255), @nipKaDinas VARCHAR(255),
@nmKaSekolah VARCHAR(255), @nipKaSekolah VARCHAR(255),
@nmKomite VARCHAR(255), @curYear INT

SET @nmKaDinas = (SELECT TOP(1) wo.Value FROM WebOptions AS wo WHERE wo.Name = 'NmKaDinas' ORDER BY wo.Id)
SET @nipKaDinas = (SELECT TOP(1) wo.Value FROM WebOptions AS wo WHERE wo.Name = 'NipKaDinas' ORDER BY wo.Id)
SELECT @curYear = CAST(wo.Value AS INT) FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun'

SELECT TOP(1) @nmKaSekolah = e.Name, @nipKaSekolah = e.Nip FROM JobTitles AS jt 
INNER JOIN Employees AS e
ON e.JobTitleId = jt.Id
WHERE jt.Name IN ('Kepala Sekolah') 
AND e.Signer = 1 AND e.UnitId = @unitId
ORDER BY e.Id

SELECT TOP(1) @nmKomite = e.Name FROM JobTitles AS jt 
INNER JOIN Employees AS e
ON e.JobTitleId = jt.Id
WHERE jt.Name IN ('Komite Sekolah') 
AND e.Signer = 1 AND e.UnitId = @unitId
ORDER BY e.Id

SELECT e.Name AS Nama, e.Nip AS Nip, jt.Name AS Jabatan, 
@nipKaDinas AS NipKaDinas, @nmKaDinas AS NmKaDinas, 
@nipKaSekolah AS NipKaSekolah, @nmKaSekolah AS NmKaSekolah, @nmKomite AS NmKomite, @tglSah AS TglSah, @curYear AS CurYear
FROM Employees AS e
INNER JOIN JobTitles AS jt 
ON jt.Id = e.JobTitleId 
WHERE jt.UnitStructure = 4 AND e.UnitId = @unitId AND e.Signer = 1
ORDER BY jt.Id, e.Nip
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
