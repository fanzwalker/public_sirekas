﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AlterSpPaguBosnas : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"ALTER PROCEDURE [dbo].[PaguBosnas]
(@stageId INT)
AS

BEGIN
--DECLARE @stageId INT

--SET @stageId = 1

DECLARE @NmPemda VARCHAR(255), @ThnAngg VARCHAR(255)

SET @NmPemda = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda' ORDER BY wo.Id)
SET @ThnAngg = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun' ORDER BY wo.Id)

SELECT u.Code,
       u.Name,
       b.StandardPrice,
       b.Total,
       b.TotalStudent,
	   UPPER(@NmPemda) AS NmPemda,
	   @ThnAngg AS ThnAngg
	   FROM Units AS u 
LEFT JOIN
(
	SELECT bbd.StandardPrice,
           bbd.Total,
           bbd.TotalStudent,
           bbd.UnitId
		   FROM BosnasBudgetDescs AS bbd 
		   WHERE bbd.Stages = @stageId
) AS b
ON b.UnitId = u.Id
WHERE u.BudgetType IN ('2', '3')
ORDER BY u.Code
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"ALTER PROCEDURE [dbo].[PaguBosnas]
(@stageId INT)
AS

BEGIN
--DECLARE @stageId INT

--SET @stageId = 1

SELECT u.Code,
       u.Name,
       b.StandardPrice,
       b.Total,
       b.TotalStudent
	   FROM Units AS u 
LEFT JOIN
(
	SELECT bbd.StandardPrice,
           bbd.Total,
           bbd.TotalStudent,
           bbd.UnitId
		   FROM BosnasBudgetDescs AS bbd 
		   WHERE bbd.Stages = @stageId
) AS b
ON b.UnitId = u.Id
WHERE u.BudgetType IN ('2', '3')
ORDER BY u.Code
END");
    }
  }
}
