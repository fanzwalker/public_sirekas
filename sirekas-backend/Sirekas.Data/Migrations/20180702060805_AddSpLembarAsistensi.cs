﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpLembarAsistensi : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[LembarAsistensi]
(@unitId INT, @tglSah DATETIME)
AS 
BEGIN
--DECLARE @unitId INT

--SET @unitId = 12

DECLARE @nmKaDinas VARCHAR(255), @nipKaDinas VARCHAR(255), @educationLvl INT

SET @nmKaDinas = (SELECT TOP(1) wo.Value FROM WebOptions AS wo WHERE wo.Name = 'NmKaDinas' ORDER BY wo.Id)
SET @nipKaDinas = (SELECT TOP(1) wo.Value FROM WebOptions AS wo WHERE wo.Name = 'NipKaDinas' ORDER BY wo.Id)

SELECT @educationLvl = u.EducationLvl FROM Units AS u WHERE u.Id = @unitId

IF @educationLvl = 1
BEGIN
SELECT *, @nipKaDinas AS NipKaDinas, @nmKaDinas AS NmKaDinas, @tglSah AS TglSah FROM
(
SELECT CASE LOWER(jt.Name) 
WHEN 'sekretaris dinas' THEN 1 
WHEN 'kabid paud dan dikmas' THEN 2
WHEN 'kasubbag. perencanaan program' THEN 3
WHEN 'kasubbag. keuangan' THEN 4
WHEN 'pptk' THEN 5 ELSE '' END AS KdUrut,
jt.Name, e.Nip, e.Name AS EmployeeName
FROM JobTitles AS jt 
LEFT JOIN Employees AS e
ON e.JobTitleId = jt.Id
WHERE jt.UnitStructure = 3
) AS a 
WHERE a.KdUrut <> 0
ORDER BY a.KdUrut
END
ELSE
BEGIN
SELECT *, @nipKaDinas AS NipKaDinas, @nmKaDinas AS NmKaDinas, @tglSah AS TglSah FROM
(
SELECT CASE LOWER(jt.Name) 
WHEN 'sekretaris dinas' THEN 1 
WHEN 'kabid dikdas' THEN 2 
WHEN 'kasubbag. perencanaan program' THEN 3
WHEN 'kasubbag. keuangan' THEN 4
WHEN 'pptk' THEN 5 ELSE '' END AS KdUrut,
jt.Name, e.Nip, e.Name AS EmployeeName
FROM JobTitles AS jt 
LEFT JOIN Employees AS e
ON e.JobTitleId = jt.Id
WHERE jt.UnitStructure = 3
) AS a 
WHERE a.KdUrut <> 0
ORDER BY a.KdUrut
END
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
