﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class RemoveAllQuarterFieldOnBudgetDet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Q1",
                table: "BudgetDets");

            migrationBuilder.DropColumn(
                name: "Q2",
                table: "BudgetDets");

            migrationBuilder.DropColumn(
                name: "Q3",
                table: "BudgetDets");

            migrationBuilder.DropColumn(
                name: "Q4",
                table: "BudgetDets");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Q1",
                table: "BudgetDets",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Q2",
                table: "BudgetDets",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Q3",
                table: "BudgetDets",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Q4",
                table: "BudgetDets",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
