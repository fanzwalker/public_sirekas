﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class ChangeActivityIdIntoNullableOnBudgetDetMonthly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetMonthlies_Activities_ActivityId",
                table: "BudgetMonthlies");

            migrationBuilder.DropIndex(
                name: "IX_BudgetMonthlies_UnitId_BudgetDetId_ActivityId_Stages",
                table: "BudgetMonthlies");

            migrationBuilder.AlterColumn<int>(
                name: "ActivityId",
                table: "BudgetMonthlies",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_BudgetMonthlies_UnitId_BudgetDetId_ActivityId_Stages",
                table: "BudgetMonthlies",
                columns: new[] { "UnitId", "BudgetDetId", "ActivityId", "Stages" },
                unique: true,
                filter: "[ActivityId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetMonthlies_Activities_ActivityId",
                table: "BudgetMonthlies",
                column: "ActivityId",
                principalTable: "Activities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetMonthlies_Activities_ActivityId",
                table: "BudgetMonthlies");

            migrationBuilder.DropIndex(
                name: "IX_BudgetMonthlies_UnitId_BudgetDetId_ActivityId_Stages",
                table: "BudgetMonthlies");

            migrationBuilder.AlterColumn<int>(
                name: "ActivityId",
                table: "BudgetMonthlies",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BudgetMonthlies_UnitId_BudgetDetId_ActivityId_Stages",
                table: "BudgetMonthlies",
                columns: new[] { "UnitId", "BudgetDetId", "ActivityId", "Stages" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetMonthlies_Activities_ActivityId",
                table: "BudgetMonthlies",
                column: "ActivityId",
                principalTable: "Activities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
