﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AlterSpKendaliBosda : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@" ALTER PROCEDURE KendaliBosda
(@stageId INT)
AS
BEGIN
--DECLARE @stageId INT

--SET @stageId = 1

DECLARE @NmPemda VARCHAR(255), @ThnAngg INT, @nmKaDinas VARCHAR(255), @nipKaDinas VARCHAR(255)

SET @NmPemda = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'NmPemda' ORDER BY wo.Id)
SET @ThnAngg = (SELECT TOP(1) wo.Value FROM dbo.WebOptions AS wo WHERE wo.Name = 'CurTahun' ORDER BY wo.Id)
SET @nmKaDinas = (SELECT TOP(1) wo.Value FROM WebOptions AS wo WHERE wo.Name = 'NmKaDinas' ORDER BY wo.Id)
SET @nipKaDinas = (SELECT TOP(1) wo.Value FROM WebOptions AS wo WHERE wo.Name = 'NipKaDinas' ORDER BY wo.Id)

;WITH PaguRka
(UnitId, Pagu, Rka)
AS
(
	SELECT a.UnitId, a.Total AS Pagu, b.Total AS Rka FROM
	(
		SELECT ua.UnitId, ua.Total FROM UnitActivities AS ua 
		INNER JOIN Activities AS a
		ON a.Id = ua.ActivityId
		WHERE a.ActivityType = 1 AND ua.Stages = @stageId
	) AS a
	INNER JOIN 
	(
		SELECT bd.UnitId, SUM(bd.Total) AS Total FROM BudgetDets AS bd 
		INNER JOIN Activities AS a
		ON a.Id = bd.ActivityId
		WHERE bd.Type = 2
		AND a.ActivityType = 1
		AND bd.Stages = @stageId
		GROUP BY bd.UnitId
	) AS b
	ON a.UnitId = b.UnitId
)

SELECT u.Code,
u.Name, ISNULL(p.Pagu, 0) AS Pagu, ISNULL(p.Rka, 0) AS Rka, 
Selisih = ISNULL(p.Pagu - p.Rka, 0), NULL AS Ket, @NmPemda AS NmPemda, @ThnAngg AS ThnAngg,
@nmKaDinas AS NmKaDinas, @nipKaDinas AS NipKaDinas
FROM Units AS u 
LEFT JOIN PaguRka AS p
ON p.UnitId = u.Id
WHERE u.Level = 4 AND u.BudgetType IN (1, 3)
ORDER BY u.Code
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
