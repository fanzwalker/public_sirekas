﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpRingApbs : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE RingApbs
(@unitId INT, @stageId INT)
AS
BEGIN

--DECLARE @unitId INT, @stageId INT

--SET @unitId = 1575
--SET @stageId = 1

DECLARE @curThn INT, @nmSekolah VARCHAR(255), @kec VARCHAR(255), 
@kab VARCHAR(255), @jmlSiswa INT, @jmlRombel INT, @nmKomite VARCHAR(50), @nipKomite VARCHAR(50),
@nmKepsek VARCHAR(50), @nipKepsek VARCHAR(50), @nmBendBosda VARCHAR(50), @nipBendBosda VARCHAR(50),
@nmBendBosnas VARCHAR(50), @nipBendBosnas VARCHAR(50)

SELECT @curThn = CAST(wo.Value AS INT) FROM WebOptions AS wo WHERE wo.Name = 'CurTahun'

SELECT @nmSekolah = u.Name, @kec = d.Name, @kab = 'Penajam Paser Utara' FROM Units AS u 
INNER JOIN Districts AS d
ON d.Id = u.DistrictId
WHERE u.Id = @unitId AND u.Level = 4

SELECT @jmlSiswa = SUM(sd.Total), @jmlRombel = SUM(sd.Rombel) FROM StudentDatas AS sd WHERE sd.UnitId = @unitId

SELECT 
@nmKomite = CASE WHEN jt.Name = 'Komite Sekolah' THEN e.Name ELSE @nmKomite END,
@nipKomite = CASE WHEN jt.Name = 'Komite Sekolah' THEN e.Nip ELSE @nipKomite END,
@nmKepsek = CASE WHEN jt.Name = 'Kepala Sekolah' THEN e.Name ELSE @nmKepsek END,
@nipKepsek = CASE WHEN jt.Name = 'Kepala Sekolah' THEN e.Nip ELSE @nipKepsek END,
@nmBendBosda = CASE WHEN jt.Name = 'Bendahara BOSDA' THEN e.Name ELSE @nmBendBosda END,
@nipBendBosda = CASE WHEN jt.Name = 'Bendahara BOSDA' THEN e.Nip ELSE @nipBendBosda END,
@nmBendBosnas = CASE WHEN jt.Name = 'Bendahara BOSNAS' THEN e.Name ELSE @nmBendBosnas END,
@nipBendBosnas = CASE WHEN jt.Name = 'Bendahara BOSNAS' THEN e.Nip ELSE @nipBendBosnas END
FROM Employees AS e 
INNER JOIN JobTitles AS jt
ON jt.Id = e.JobTitleId
WHERE e.UnitId = @unitId AND jt.UnitStructure = 4 AND e.Signer = 1

DECLARE @RingApbs TABLE (
	Code VARCHAR(255),
	Name VARCHAR(255),
	LevelTypes INT,
	TransType INT,
	Total Money
)

INSERT INTO @RingApbs
SELECT ca.Code, ca.Name, ca.LevelTypes, ca.TransType, SUM(me.Total) AS Total FROM CoaApbs AS ca
LEFT JOIN ManualEntries AS me
ON me.CoaApbsId = ca.Id
WHERE (me.UnitId = @unitId OR @unitId IS NULL) AND me.Stages = @stageId OR ca.LevelTypes = 1
GROUP BY ca.Code, ca.Name, ca.LevelTypes, ca.TransType

INSERT INTO @RingApbs
SELECT '1.1.1.', 'BOS Pusat',
2, 1, SUM(lyb.Value) 
FROM dbo.LastYearBalances AS lyb WHERE (lyb.UnitId = @unitId OR @unitId IS NULL)

INSERT INTO @RingApbs
SELECT
CASE a.ActivityType WHEN 2 
THEN '1.3.1.' WHEN 1 THEN '1.3.2.' ELSE NULL END AS Code,
CASE a.ActivityType WHEN 2 
THEN 'BOS Pusat' WHEN 1 THEN 'BOSDA Kabupaten' ELSE NULL END AS Name,
2, 1,
SUM(ua.Total) AS Total
FROM UnitActivities AS ua 
INNER JOIN Activities AS a
ON a.Id = ua.ActivityId
WHERE (ua.UnitId = @unitId OR @unitId IS NULL) AND ua.Stages = @stageId
GROUP BY a.ActivityType

INSERT INTO @RingApbs
SELECT '1.2.' + CAST(ROW_NUMBER() OVER(ORDER BY coa.Code) AS VARCHAR(4)) + '.' AS Code, 
coa.Name AS Name, 2, 1, SUM(bd.Total) AS Total 
FROM BudgetDets AS bd 
INNER JOIN ChartOfAccounts AS coa
ON coa.Id = bd.ChartOfAccountId
WHERE bd.ActivityId IS NULL AND (bd.UnitId = @unitId OR @unitId IS NULL) AND bd.Stages = @stageId AND bd.Type = 2
GROUP BY coa.Code, coa.Name

INSERT INTO @RingApbs
SELECT '2.1.' + CAST(ROW_NUMBER() OVER(ORDER BY coa.Code) AS VARCHAR(4)) + '.' AS Code, 
coa.Name AS Name, 2, 1, SUM(bd.Total) AS Total 
FROM BudgetDets AS bd 
INNER JOIN ChartOfAccounts AS coa
ON coa.Id = bd.ChartOfAccountId
WHERE bd.ActivityId IS NULL AND (bd.UnitId = @unitId OR @unitId IS NULL) AND bd.Stages = @stageId AND bd.Type = 2
GROUP BY coa.Code, coa.Name

INSERT INTO @RingApbs
SELECT '2.2.1.0.', 'Saldo Tahun Lalu',
2, 2, SUM(lyb.Value) 
FROM dbo.LastYearBalances AS lyb WHERE (lyb.UnitId = @unitId OR @unitId IS NULL)

INSERT INTO @RingApbs
SELECT '2.2.2.' + CAST(ROW_NUMBER() OVER(ORDER BY b.Code) AS VARCHAR(4)) + '.' AS Code, b.Name, 2, 2, a.Total FROM 
(
SELECT LEFT(coa.Code, 9) AS Code, SUM(bd.Total) AS Total FROM BudgetDets AS bd
INNER JOIN Activities AS a
ON a.Id = bd.ActivityId
INNER JOIN ChartOfAccounts AS coa
ON coa.Id = bd.ChartOfAccountId
WHERE a.ActivityType = 1 AND bd.Stages = @stageId AND (bd.UnitId = @unitId OR @unitId IS NULL) AND bd.Type = 2
GROUP BY LEFT(coa.Code, 9)
) AS a INNER JOIN 
ChartOfAccounts AS b
ON a.Code = b.Code

DECLARE @TotPenerimaan MONEY, @TotPengeluaran MONEY

SELECT @TotPenerimaan = SUM(ra.Total) FROM @RingApbs AS ra WHERE LEFT(ra.Code, 1) = '1' AND ra.LevelTypes = 2
SELECT @TotPengeluaran = SUM(ra.Total) FROM @RingApbs AS ra WHERE LEFT(ra.Code, 1) = '2' AND ra.LevelTypes = 2

INSERT INTO @RingApbs
VALUES
('1.A.', 'Jumlah Penerimaan', 1, 1, @TotPenerimaan),
('2.A.', 'Jumlah Pengeluaran', 1, 1, @TotPengeluaran)

SELECT
ra.Code, ra.Name, ra.LevelTypes, ra.TransType,
CASE WHEN ra.LevelTypes = 2 THEN ra.Total ELSE
(SELECT SUM(ra2.Total) FROM @RingApbs AS ra2 WHERE ra2.Code LIKE ra.Code + '%') END AS Total,
@TotPenerimaan AS TotPenerimaan, @TotPengeluaran AS TotPengeluaran,
@curThn AS CurThn, @nmSekolah AS NmSekolah, @kec AS Kec, @kab AS Kab, @jmlSiswa AS JmlSiswa, @jmlRombel AS JmlRombel,
@nmKomite AS NmKomite, @nipKomite AS NipKomite, @nmKepsek AS NmKepsek, @nipKepsek AS NipKepsek, @nmBendBosda AS NmBendBosda,
@nipBendBosda AS NipBendBosda, @nmBendBosnas AS NmBendBosnas, @nipBendBosnas AS NipBendBosnas
FROM @RingApbs AS ra 
ORDER BY ra.Code
END
");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
