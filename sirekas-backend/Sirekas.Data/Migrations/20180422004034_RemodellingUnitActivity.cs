﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class RemodellingUnitActivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "ExtraPrice",
                table: "UnitActivities",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PerRombelPrice",
                table: "UnitActivities",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PerStudentPrice",
                table: "UnitActivities",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PerUnitPrice",
                table: "UnitActivities",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Rombel",
                table: "UnitActivities",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StudentNumber",
                table: "UnitActivities",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPerRombelPrice",
                table: "UnitActivities",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPerStudentPrice",
                table: "UnitActivities",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExtraPrice",
                table: "UnitActivities");

            migrationBuilder.DropColumn(
                name: "PerRombelPrice",
                table: "UnitActivities");

            migrationBuilder.DropColumn(
                name: "PerStudentPrice",
                table: "UnitActivities");

            migrationBuilder.DropColumn(
                name: "PerUnitPrice",
                table: "UnitActivities");

            migrationBuilder.DropColumn(
                name: "Rombel",
                table: "UnitActivities");

            migrationBuilder.DropColumn(
                name: "StudentNumber",
                table: "UnitActivities");

            migrationBuilder.DropColumn(
                name: "TotalPerRombelPrice",
                table: "UnitActivities");

            migrationBuilder.DropColumn(
                name: "TotalPerStudentPrice",
                table: "UnitActivities");
        }
    }
}
