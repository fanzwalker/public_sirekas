﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class UpdateExistingJobTitles : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("UPDATE JobTitles SET UnitStructure = 4");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("UPDATE JobTitles SET UnitStructure = 0 Where UnitSturcture = 4");
    }
  }
}
