﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class UpdateBudgetTypeOnUnits : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"UPDATE Units SET BudgetType = 1 WHERE Code = '1.01.01.01.001.';
UPDATE Units SET BudgetType = 2 WHERE Code = '1.01.01.01.002.';
UPDATE Units SET BudgetType = 3 WHERE Code = '1.01.01.01.003.';");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"UPDATE Units SET BudgetType = 0 WHERE Code = '1.01.01.01.001.';
UPDATE Units SET BudgetType = 0 WHERE Code = '1.01.01.01.002.';
UPDATE Units SET BudgetType = 0 WHERE Code = '1.01.01.01.003.';");
    }
  }
}
