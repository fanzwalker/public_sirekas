﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddUniqueIndexOnBudgetDetMonthly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BudgetMonthlies_UnitId",
                table: "BudgetMonthlies");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetMonthlies_UnitId_BudgetDetId_ActivityId_Stages",
                table: "BudgetMonthlies",
                columns: new[] { "UnitId", "BudgetDetId", "ActivityId", "Stages" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BudgetMonthlies_UnitId_BudgetDetId_ActivityId_Stages",
                table: "BudgetMonthlies");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetMonthlies_UnitId",
                table: "BudgetMonthlies",
                column: "UnitId");
        }
    }
}
