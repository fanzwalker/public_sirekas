﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class RemoveAllBudgetSources : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("DELETE FROM BudgetSources");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
