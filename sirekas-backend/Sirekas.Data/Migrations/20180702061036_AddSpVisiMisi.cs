﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddSpVisiMisi : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[VisiMisi]
(@unitId INT, @tglSah DATETIME)
AS
BEGIN
--DECLARE @unitId INT

--SET @unitId = 1573

DECLARE @nmUnit VARCHAR(255), @nmKaSekolah VARCHAR(255), @nipKaSekolah VARCHAR(255)

SELECT @nmUnit = u.Name FROM Units AS u WHERE u.Id = @unitId

SELECT TOP(1) @nmKaSekolah = e.Name, @nipKaSekolah = e.Nip FROM JobTitles AS jt 
INNER JOIN Employees AS e
ON e.JobTitleId = jt.Id
WHERE jt.Name IN ('Kepala Sekolah') 
AND e.Signer = 1 AND e.UnitId = @unitId
ORDER BY e.Id

SELECT vm.Type, vm.Id, vm.Description, @nmUnit AS NmUnit, @tglSah AS TglSah,
@nmKaSekolah AS NmKaSekolah, @nipKaSekolah AS NipKaSekolah
FROM VisMises AS vm WHERE vm.UnitId = @unitId
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
