﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class ChangeBudgetSourceIdTypeOnBudgetDets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BudgetSource",
                table: "BudgetDets");

            migrationBuilder.AlterColumn<int>(
                name: "BudgetSourceId",
                table: "BudgetDets",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "BudgetSourceId",
                table: "BudgetDets",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "BudgetSource",
                table: "BudgetDets",
                nullable: false,
                defaultValue: 0);
        }
    }
}
