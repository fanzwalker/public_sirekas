﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class RemoveLvlTypeUniqueIndexOnBudgetStandard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BudgetStandards_EducationLvl",
                table: "BudgetStandards");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_BudgetStandards_EducationLvl",
                table: "BudgetStandards",
                column: "EducationLvl",
                unique: true);
        }
    }
}
