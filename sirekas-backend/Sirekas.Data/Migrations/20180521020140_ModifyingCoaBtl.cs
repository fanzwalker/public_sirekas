﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class ModifyingCoaBtl : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"DELETE FROM ChartOfAccounts WHERE LEFT(Code, 6) = '5.1.1.'
AND LEFT(Code, 9) <> '5.1.1.01.'

DELETE FROM ChartOfAccounts
WHERE LEFT(Code, 9) = '5.1.1.01.'
AND Code NOT IN 
(
'5.1.1.01.01.',
'5.1.1.01.02.',
'5.1.1.01.03.',
'5.1.1.01.04.',
'5.1.1.01.05.',
'5.1.1.01.06.',
'5.1.1.01.07.',
'5.1.1.01.08.',
'5.1.1.01.09.'
)

UPDATE ChartOfAccounts
SET 
	Name = 'Gaji Bulanan PNS'
WHERE Code = '5.1.1.01.01.'

UPDATE ChartOfAccounts
SET 
	Name = 'Gaji ke 13'
WHERE Code = '5.1.1.01.02.'

UPDATE ChartOfAccounts
SET 
	Name = 'Gaji ke 14'
WHERE Code = '5.1.1.01.03.'

UPDATE ChartOfAccounts
SET 
	Name = 'Tunjangan Tambahan Penghasilan Pusat (TPP Pusat)'
WHERE Code = '5.1.1.01.04.'

UPDATE ChartOfAccounts
SET 
	Name = 'Tunjangan Tambahan Penghasilan Pusat (TPP Provinsi)'
WHERE Code = '5.1.1.01.05.'

UPDATE ChartOfAccounts
SET 
	Name = 'Tunjangan Tambahan Penghasilan Pusat (TPP Kabupaten)'
WHERE Code = '5.1.1.01.06.'

UPDATE ChartOfAccounts
SET 
	Name = 'Tunjangan Profesi (Sertifikasi)'
WHERE Code = '5.1.1.01.07.'

UPDATE ChartOfAccounts
SET 
	Name = 'Tunjangan Daerah Guru Honorer'
WHERE Code = '5.1.1.01.08.'

UPDATE ChartOfAccounts
SET 
	Name = 'Tunjangan Daerah (Honorer)'
WHERE Code = '5.1.1.01.09.'
");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
