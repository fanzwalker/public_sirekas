﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class Test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BudgetDets_ActivityId",
                table: "BudgetDets");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDets_ActivityId_Code_Stages_UnitId_ChartOfAccountId",
                table: "BudgetDets",
                columns: new[] { "ActivityId", "Code", "Stages", "UnitId", "ChartOfAccountId" },
                unique: true,
                filter: "[ActivityId] IS NOT NULL AND [Code] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BudgetDets_ActivityId_Code_Stages_UnitId_ChartOfAccountId",
                table: "BudgetDets");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDets_ActivityId",
                table: "BudgetDets",
                column: "ActivityId");
        }
    }
}
