﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class RemoveCodeFromBudgetStandard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "BudgetStandards");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetStandards_EducationLvl",
                table: "BudgetStandards",
                column: "EducationLvl",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_BudgetStandards_EducationLvl",
                table: "BudgetStandards");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "BudgetStandards",
                nullable: true);
        }
    }
}
