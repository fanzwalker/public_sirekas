﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class RemoveAllBudgetDet : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("DELETE FROM BudgetDets");
      migrationBuilder.Sql("DELETE FROM PerfBenchmarks");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
