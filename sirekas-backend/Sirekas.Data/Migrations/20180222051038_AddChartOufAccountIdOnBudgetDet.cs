﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class AddChartOufAccountIdOnBudgetDet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ChartOfAccountId",
                table: "BudgetDets",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDets_ChartOfAccountId",
                table: "BudgetDets",
                column: "ChartOfAccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDets_ChartOfAccounts_ChartOfAccountId",
                table: "BudgetDets",
                column: "ChartOfAccountId",
                principalTable: "ChartOfAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDets_ChartOfAccounts_ChartOfAccountId",
                table: "BudgetDets");

            migrationBuilder.DropIndex(
                name: "IX_BudgetDets_ChartOfAccountId",
                table: "BudgetDets");

            migrationBuilder.DropColumn(
                name: "ChartOfAccountId",
                table: "BudgetDets");
        }
    }
}
