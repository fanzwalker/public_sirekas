﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class UpdateActivityTypeBosdaOnActivities : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"UPDATE Activities SET ActivityType = 1 WHERE Code = '001.'");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"UPDATE Activities SET ActivityType = 0 WHERE Code = '001.'");
    }
  }
}
