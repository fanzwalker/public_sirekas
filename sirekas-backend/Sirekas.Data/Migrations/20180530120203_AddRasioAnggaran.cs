﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class AddRasioAnggaran : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"CREATE PROCEDURE RasioAnggaran
(@unitId INT, @stageId INT)
AS
BEGIN
--DECLARE @unitId INT, @stageId INT

--SET @unitId = NULL;
--SET @stageId = 1;

SELECT 
CASE WHEN a.ActivityType = 1 THEN 'BOSDA' ELSE 'BOSNAS' END AS Name,
SUM(bd.Total) AS Total
FROM BudgetDets AS bd
INNER JOIN Activities AS a
ON a.Id = bd.ActivityId
WHERE bd.ActivityId IS NOT NULL AND (bd.UnitId = @unitId OR @unitId IS NULL) 
AND bd.Stages = @stageId AND bd.Type = 2
GROUP BY a.ActivityType
END");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
