﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class UpdateBudgetSourceIdOnBudgetDet : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("UPDATE BudgetDets SET BudgetSourceId = 3 WHERE Type = 2");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql("UPDATE BudgetDets SET BudgetSourceId = NULL WHERE Type = 2");
    }
  }
}
