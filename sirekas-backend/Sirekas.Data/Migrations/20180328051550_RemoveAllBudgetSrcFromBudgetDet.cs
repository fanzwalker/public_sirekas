﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sirekas.Data.Migrations
{
  public partial class RemoveAllBudgetSrcFromBudgetDet : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.Sql(@"UPDATE dbo.BudgetDets SET BudgetSourceId = NULL
        WHERE BudgetSourceId IS NOT NULL");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

    }
  }
}
