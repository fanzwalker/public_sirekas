﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Sirekas.Data.Migrations
{
    public partial class RemoveBudget : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BudgetDets_Budgets_BudgetId",
                table: "BudgetDets");

            migrationBuilder.DropForeignKey(
                name: "FK_BudgetMonthlies_Budgets_BudgetId",
                table: "BudgetMonthlies");

            migrationBuilder.DropTable(
                name: "Budgets");

            migrationBuilder.DropIndex(
                name: "IX_BudgetMonthlies_BudgetId",
                table: "BudgetMonthlies");

            migrationBuilder.DropIndex(
                name: "IX_BudgetDets_BudgetId",
                table: "BudgetDets");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Budgets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ActivityId = table.Column<int>(nullable: true),
                    BudgetSourceId = table.Column<int>(nullable: false),
                    ChartOfAccountId = table.Column<int>(nullable: false),
                    Stages = table.Column<int>(nullable: false),
                    UnitId = table.Column<int>(nullable: false),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Budgets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Budgets_Activities_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Budgets_BudgetSources_BudgetSourceId",
                        column: x => x.BudgetSourceId,
                        principalTable: "BudgetSources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Budgets_ChartOfAccounts_ChartOfAccountId",
                        column: x => x.ChartOfAccountId,
                        principalTable: "ChartOfAccounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Budgets_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BudgetMonthlies_BudgetId",
                table: "BudgetMonthlies",
                column: "BudgetId");

            migrationBuilder.CreateIndex(
                name: "IX_BudgetDets_BudgetId",
                table: "BudgetDets",
                column: "BudgetId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_ActivityId",
                table: "Budgets",
                column: "ActivityId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_BudgetSourceId",
                table: "Budgets",
                column: "BudgetSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_ChartOfAccountId",
                table: "Budgets",
                column: "ChartOfAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_UnitId",
                table: "Budgets",
                column: "UnitId");

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetDets_Budgets_BudgetId",
                table: "BudgetDets",
                column: "BudgetId",
                principalTable: "Budgets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_BudgetMonthlies_Budgets_BudgetId",
                table: "BudgetMonthlies",
                column: "BudgetId",
                principalTable: "Budgets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
