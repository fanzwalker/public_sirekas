import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {GlobalService} from '../global.service';
import {Title} from '@angular/platform-browser';
declare var $: any;
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  titlePage: string;
  constructor(private globalService: GlobalService,
              private titleTabs: Title) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.globalService.titleComponent$
    );
    combine.subscribe((resp) => {
      const [titleComponent] = resp;
      this.titlePage = titleComponent;
      this.titleTabs.setTitle('APBS | ' + titleComponent);
    });
    $(document).ready(() => {
      const b: any = $('body');
      b.layout('fix');
    });
    $('.sidebar-menu').tree();
    $('.sidebar').slimScroll({height: '702px'});
  }

}
