import { ChangePassUserService } from './../../change-pass-user/change-pass-user.service';
import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../service/users.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userInfo: any;
  constructor(private userService: UsersService,
              private changeUser: ChangePassUserService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
  }
  showChangePass() {
    this.changeUser.showChangeUser(true);
  }
}
