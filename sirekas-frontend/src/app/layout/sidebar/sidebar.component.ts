import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../service/users.service';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  userInfo: any;
  constructor(private userServive: UsersService) {
    this.userInfo = this.userServive.getUserInfo();
  }

  ngOnInit() {
  }

}
