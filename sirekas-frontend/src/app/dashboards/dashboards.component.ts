import { DashboardsService } from './dashboards.service';
import { Observable } from 'rxjs/Observable';
import { animationPage } from './../shared/animation-page';
import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
@Component({
  selector: 'app-dashboards',
  templateUrl: './dashboards.component.html',
  styleUrls: ['./dashboards.component.css'],
  animations: [animationPage]
})
export class DashboardsComponent implements OnInit {
  listBerita: any;
  initialListBerita: any;
  constructor(private service: DashboardsService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listBerita$
    );
    combine.subscribe(resp => {
      const [listBerita] = resp;
      this.listBerita = Object.assign(listBerita);
      this.initialListBerita = Object.assign(_.take(listBerita, 2));
    });
    this.service.getBerita();
  }
  detilBerita(e) {
    this.service.setBeritaDetil(e);
    this.service.showDetilBerita(true);
  }
  showListBerita() {
    this.service.showListBerita(true);
  }
}
