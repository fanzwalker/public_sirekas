import { animationPage } from './../../shared/animation-page';
import { DashboardsService } from './../dashboards.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-list-berita',
  templateUrl: './list-berita.component.html',
  styleUrls: ['./list-berita.component.css'],
  animations: [animationPage]
})
export class ListBeritaComponent implements OnInit {
  showThis: boolean;
  listBerita: any;
  constructor(private service: DashboardsService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayListBerita$,
      this.service.listBerita$
    );
    combine.subscribe(resp => {
      const [displaylistBerita, listBerita] = resp;
      this.showThis = displaylistBerita;
      this.listBerita = Object.assign(listBerita);
    });
  }
  detilBerita(e) {
    this.service.setBeritaDetil(e);
    this.service.showDetilBerita(true);
  }
  onHide() {
    this.service.showListBerita(false);
  }
}
