import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DashboardsService {
  private listBerita = new BehaviorSubject(<any>[]);
  public listBerita$ = this.listBerita.asObservable();

  private beritaDetil = new BehaviorSubject(<any>[]);
  public beritaDetil$ = this.beritaDetil.asObservable();

  private displayDetilBerita = new BehaviorSubject<boolean>(false);
  public displayDetilBerita$ = this.displayDetilBerita.asObservable();

  private displayListBerita = new BehaviorSubject<boolean>(false);
  public displayListBerita$ = this.displayListBerita.asObservable();
  constructor(private http: HttpClient) { }

  getBerita() {
    return this.http.get(`${environment.url}messages`).map(resp => resp)
    .subscribe(data => this.setListBerita(data));
  }
  setListBerita(data) {
    data.sort((a, b) => new Date(b.dateCreate).getTime() - new Date(a.dateCreate).getTime());
    return this.listBerita.next(data);
  }
  setBeritaDetil(data) {
    return this.beritaDetil.next(data);
  }
  showDetilBerita(action: boolean) {
    return this.displayDetilBerita.next(action);
  }
  showListBerita(action: boolean) {
    return this.displayListBerita.next(action);
  }
}
