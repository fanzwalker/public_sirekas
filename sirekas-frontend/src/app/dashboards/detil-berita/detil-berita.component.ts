import { Observable } from 'rxjs/Observable';
import { DashboardsService } from './../dashboards.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detil-berita',
  templateUrl: './detil-berita.component.html',
  styleUrls: ['./detil-berita.component.css']
})
export class DetilBeritaComponent implements OnInit {
  showThis: boolean;
  detilBerita: any;
  constructor(private service: DashboardsService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayDetilBerita$,
      this.service.beritaDetil$
    );
    combine.subscribe(resp => {
      const [displayDetilBerita, detilBerita] = resp;
      this.showThis = displayDetilBerita;
      this.detilBerita = Object.assign(detilBerita);
    });
  }
  onHide() {
    this.service.showDetilBerita(false);
  }
}
