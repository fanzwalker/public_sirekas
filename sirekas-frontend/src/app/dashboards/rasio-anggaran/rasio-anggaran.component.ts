import { UsersService } from './../../service/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RasioAnggaranService } from './rasio-anggaran.service';
import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { SelectItem } from 'primeng/primeng';
import { AllOption } from '../../shared/all-option';
@Component({
  selector: 'app-rasio-anggaran',
  templateUrl: './rasio-anggaran.component.html',
  styleUrls: ['./rasio-anggaran.component.css']
})
export class RasioAnggaranComponent implements OnInit {
  data: any;
  options: any;
  formFilter: FormGroup;
  userInfo: any;
  listSekolah: any; optSekolah: SelectItem[];
  constructor(private service: RasioAnggaranService,
              private fb: FormBuilder,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah: [null]
    });
    this.options = {
      title: {
          display: true,
          text: 'Rasio Anggaran Bosnas & Bosda Sekolah',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      },
      tooltips: {
        callbacks: {
          label: function(tooltipItem, data) {
            const dataset = data.datasets[tooltipItem.datasetIndex];
            const meta = dataset._meta[Object.keys(dataset._meta)[0]];
            const total = meta.total;
            const currentValue = dataset.data[tooltipItem.index];
            const percentage = parseFloat((currentValue / total * 100).toFixed(1));
            return AllOption.convertToRupiah(currentValue) + ' (' + percentage + '%)';
          },
          title: function(tooltipItem, data) {
            return data.labels[tooltipItem[0].index];
          }
        }
      },
    };
  }

  ngOnInit() {
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.onChangeUnit();
    } else {
      this.getSekolah();
    }
    this.onChangeUnit();
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(this.userInfo.stages, 0, 4, 1, 1)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  onChangeUnit() {
    this.service.getRasioAnggaran('RasioAnggaran', this.formFilter.value.sekolah, this.userInfo.stages)
    .subscribe(resp => {
      this.setChart(resp);
    });
  }
  setChart(data) {
    const tempLabels = [];
    const tempDataSets = {
      data: [],
      backgroundColor: []
    };
    if (data.length > 0) {
      _.forEach(data, (x) => {
        tempLabels.push(x.Name);
        tempDataSets.data.push(x.Total);
        tempDataSets.backgroundColor.push(AllOption.getRandomColor());
      });
    }
    this.data = {
      labels: tempLabels,
      datasets: [tempDataSets]
      };
  }
}
