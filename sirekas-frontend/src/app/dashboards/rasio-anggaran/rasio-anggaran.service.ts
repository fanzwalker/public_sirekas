import { environment } from './../../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class RasioAnggaranService {
  constructor(private http: HttpClient) { }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getRasioAnggaran(spName, unitId, stages) {
    const parameters = {};
    parameters['@unitId'] = unitId;
    parameters['@stageId'] = +stages;
    const paramBody = {
      'spName' : spName,
      'parameters' : parameters
    };
    return this.http.post(`${environment.url}charts`, paramBody).map(resp => <any>resp);
  }
}
