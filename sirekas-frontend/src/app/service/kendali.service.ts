import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable()
export class KendaliService {

  constructor(private http: HttpClient) { }
  getData(spName, stages) {
    const parameters = {};
    parameters['@stageId'] = +stages;
    const paramBody = {
      'spName' : spName,
      'parameters' : parameters
    };
    return this.http.post(`${environment.url}charts`, paramBody).map(resp => <any>resp);
  }
}
