import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class UsersService {
  private userInfo = new BehaviorSubject(<any>[]);
  public userInfo$ = this.userInfo.asObservable();
  jwtHelper: JwtHelperService = new JwtHelperService([]);
  constructor(private http: HttpClient) { }
  UserAuth(credentials) {
    return this.http.post(`${environment.url}auth/token` , credentials, { responseType: 'text' });
  }
  getUserInfo() {
    return this.jwtHelper.decodeToken(localStorage.getItem('currentUser'));
  }
  setUserInfo(data) {
    return this.userInfo.next(data);
  }
  logout() {
    localStorage.removeItem('currentUser');
  }
}
