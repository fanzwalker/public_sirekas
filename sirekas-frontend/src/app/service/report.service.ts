import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {ResponseContentType} from '@angular/http';

@Injectable()
export class ReportService {
  constructor(private http: HttpClient) { }
  execPrint(fileName, type, paramBody = null) {
    const params = {
      reportName : fileName,
      formatType : type,
      parameters : paramBody
    };
    return this.http.post(`${environment.urlReport}`, params, { responseType: 'blob'});
  }
  extractData(res, type, filename) {
    const fileType = type === 1 ? 'application/pdf' : type === 2 ? 'application/msword' : 'application/vnd.ms-excel' ;
    const extention = type === 1 ? '.pdf' : type === 2 ? '.doc' : '.xls' ;
    const fileName = filename + extention;
    const myBlob: Blob = new Blob([res], {type: fileType});
    const fileURL = URL.createObjectURL(myBlob);
    const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;

    a.href = fileURL;
    a.download = fileName;
    document.body.appendChild(a);
    a.click();

    document.body.removeChild(a);
    URL.revokeObjectURL(fileURL);
    // window.open(fileURL);
  }
}
