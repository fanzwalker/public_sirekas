import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LoadersService {
  private loaderAction = new BehaviorSubject<boolean>(false);
  public loaderAction$ = this.loaderAction.asObservable();
  constructor() { }
  show() {
    return this.loaderAction.next(true);
  }
  hide() {
    return this.loaderAction.next(false);
  }
}
