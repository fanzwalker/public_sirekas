import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private jwtHelper: JwtHelperService,
              private router: Router) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const user = localStorage.getItem('currentUser');
    if (!user || this.jwtHelper.isTokenExpired(localStorage.getItem('currentUser'))) {
      this.router.navigate(['login'],{ queryParams: {returnUrl: state.url }});
      return false;
    }
    return true;
  }
}
