import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WebOptionService } from '../web-option.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-edit-option',
  templateUrl: './edit-option.component.html',
  styleUrls: ['./edit-option.component.css']
})
export class EditOptionComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  dataEdit: any;
  constructor(private webOptionService: WebOptionService,
              private fb: FormBuilder) {
    this.formAdd = this.fb.group({
      name: ['', Validators.required],
      value: ['', Validators.required],
      description: ['']
    });
    }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.webOptionService.displayEdit$,
      this.webOptionService.dataEdit$
    );
    combine.subscribe(resp => {
      const [displayEdit, dataEdit] = resp;
      this.showThis = displayEdit;
      this.dataEdit = Object.assign(dataEdit);
      if (this.dataEdit) {
        this.formAdd.patchValue({
          name: this.dataEdit.name,
          value: this.dataEdit.value,
          description: this.dataEdit.description
        });
      }
    });
  }
  Simpan() {
    if (this.formAdd.valid) {
      this.webOptionService.putOption(this.dataEdit.id, this.formAdd.value)
      .subscribe(resp => {
        this.webOptionService.getOption();
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data Berhasil Disimpan',
          showConfirmButton: false,
          timer: 3000
        });
      }, error => {
        console.log(error.error.error);
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.webOptionService.showEdit(false);
  }
}
