import { Component, OnInit } from '@angular/core';
import { IlistWebOption } from '../interface/ilist-web-option';
import { WebOptionService } from './web-option.service';
import { Observable } from 'rxjs/Observable';
import { animationPage } from '../shared/animation-page';
import swal from 'sweetalert2';
@Component({
  selector: 'app-web-option',
  templateUrl: './web-option.component.html',
  styleUrls: ['./web-option.component.css'],
  animations: [animationPage]
})
export class WebOptionComponent implements OnInit {
  listWebOption: IlistWebOption[];
  constructor(private webOptionService: WebOptionService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.webOptionService.listWebOption$
    );
    combine.subscribe(resp => {
      const [listWebOption] = resp;
      this.listWebOption = Object.assign(listWebOption);
    });
    this.getWebOption();
  }
  getWebOption() {
    this.webOptionService.getOption();
  }
  showAdd() {
    this.webOptionService.showAdd(true);
  }
  showEdit(e) {
    this.webOptionService.setDataEdit(e);
    this.webOptionService.showEdit(true);
  }
  btnDelete(e) {
    swal({
      title: 'Hapus',
      text: 'Data Akan Dihapus ' + e.name,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.webOptionService.deleteOption(e.id)
          .subscribe(resp => {
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Data Berhasil Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
            this.webOptionService.getOption();
          }, error => {
            swal({
              position: 'top-end',
              type: 'warning',
              title: 'Data Gagal Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
          });
      } else if (result.dismiss === swal.DismissReason.cancel) {
        return false;
      }
    });
  }
}
