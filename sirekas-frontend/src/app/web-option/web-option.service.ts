import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()
export class WebOptionService {
  private listWebOption = new BehaviorSubject(<any>[]);
  public listWebOption$ = this.listWebOption.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  constructor(private http: HttpClient) { }
  getOption() {
    return this.http.get(`${environment.url}web-option`)
    .map(resp => resp).subscribe(data => this.setListOption(data));
  }
  setListOption(data) {
    return this.listWebOption.next(data);
  }
  postOption(paramBody) {
    return this.http.post(`${environment.url}web-option`, paramBody);
  }
  putOption(id, paramBody) {
    return this.http.put(`${environment.url}web-option/${id}`, paramBody);
  }
  deleteOption(id) {
    return this.http.request('DELETE', `${environment.url}web-option/${id}`);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
}
