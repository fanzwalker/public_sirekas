import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebOptionService } from '../web-option.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
@Component({
  selector: 'app-add-option',
  templateUrl: './add-option.component.html',
  styleUrls: ['./add-option.component.css']
})
export class AddOptionComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  constructor(private webOptionService: WebOptionService,
              private fb: FormBuilder) {
    this.formAdd = this.fb.group({
      name: ['', Validators.required],
      value: ['', Validators.required],
      description: ['']
    });
    }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.webOptionService.displayAdd$
    );
    combine.subscribe(resp => {
      const [displayAdd] = resp;
      this.showThis = displayAdd;
    });
  }
  Simpan() {
    if (this.formAdd.valid) {
      this.webOptionService.postOption(this.formAdd.value)
      .subscribe(resp => {
        this.webOptionService.getOption();
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data Berhasil Disimpan',
          showConfirmButton: false,
          timer: 3000
        });
      }, error => {
        console.log(error.error.error);
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.webOptionService.showAdd(false);
  }
}
