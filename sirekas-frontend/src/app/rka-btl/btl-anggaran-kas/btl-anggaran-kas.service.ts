import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class BtlAnggaranKasService {
  private listAnggaranKas = new BehaviorSubject(<any>[]);
  public listAnggaranKas$ = this.listAnggaranKas.asObservable();
  private displayEditAnggaranKas = new BehaviorSubject<boolean>(false);
  public displayEditAnggaranKas$ = this.displayEditAnggaranKas.asObservable();
  private dataEditAnggaranKas = new BehaviorSubject(<any>[]);
  public dataEditAnggaranKas$ = this.dataEditAnggaranKas.asObservable();
  constructor(private http: HttpClient) { }
  getBudgetMonthly(unitId, activityId, stage) {
    return this.http.get(`${environment.url}units/${unitId}/activities/${activityId}/stages/${stage}/budget-monthlies`)
      .map(resp => resp).subscribe(data => this.setBudgetMonthly(data));
  }
  setBudgetMonthly(data) {
    return this.listAnggaranKas.next(data);
  }
  showEdit(action: boolean) {
    return this.displayEditAnggaranKas.next(action);
  }
  setDataEditAnggaranKas(data) {
    return this.dataEditAnggaranKas.next(data);
  }
  putBudgetMonthly(unitId, activityId, stages, budgetMonthlyId, paramBody) {
    return this.http.put(`${environment.url}units/${unitId}/activities/${activityId}/stages/${stages}/budget-monthlies/${budgetMonthlyId}`,
      paramBody);
  }
}
