import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class BtlPreviewService {
  private dataPreview = new BehaviorSubject(<any>[]);
  public dataPreview$ = this.dataPreview.asObservable();
  private unitProgAct = new BehaviorSubject(<any>[]);
  public unitProgAct$ = this.unitProgAct.asObservable();
  constructor(private http: HttpClient) { }

  getPreview(unitId, activityId, stageId) {
    return this.http.get(`${environment.url}units/${unitId}/activities/${activityId}/stages/${stageId}/rka221-previews`)
      .map(resp => resp).subscribe(data => this.setPreview(data));
  }
  setPreview(data) {
    return this.dataPreview.next(data);
  }
  setUnitProgAct(data) {
    return this.unitProgAct.next(data);
  }
  getProgramById(programId) {
    return this.http.get(`${environment.url}programs/${programId}`).map(resp => <any>resp);
  }
  getActivityById(programId, activityId) {
    return this.http.get(`${environment.url}programs/${programId}/activities/${activityId}`).map(resp => <any>resp);
  }
  getUnitById(id) {
    return this.http.get(`${environment.url}units/${id}`).map(resp => <any>resp);
  }
}
