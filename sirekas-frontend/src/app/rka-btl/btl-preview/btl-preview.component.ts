import { BtlPreviewService } from './btl-preview.service';
import { RkaBtlService } from './../rka-btl.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IpreviewRkas } from '../../interface/ipreview-rkas';
import { IunitProgAct } from '../../interface/iunit-prog-act';
import { MenuItem } from 'primeng/primeng';
import { ReportService } from '../../service/report.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-btl-preview',
  templateUrl: './btl-preview.component.html',
  styleUrls: ['./btl-preview.component.css']
})
export class BtlPreviewComponent implements OnInit, OnDestroy {
  previews: IpreviewRkas[];
  unitProgAct: IunitProgAct;
  itemPrints: MenuItem[];
  preProgram: any;
  preActivity: any;
  constructor(private rkaBtlSercice: RkaBtlService,
              private service: BtlPreviewService,
              private reportService: ReportService) {
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.dataPreview$,
      this.service.unitProgAct$,
    );
    combine.subscribe(resp => {
      const [dataPreview, unitProgAct] = resp;
      this.previews = Object.assign(dataPreview);
      this.unitProgAct = Object.assign(unitProgAct);
      if (this.unitProgAct.Programs) {
        this.service.getProgramById(this.unitProgAct.Programs)
        .subscribe(dataProgram => this.preProgram = Object.assign(dataProgram));
      }
      if (this.unitProgAct.Activity) {
        this.service.getActivityById(this.unitProgAct.Programs, this.unitProgAct.Activity)
        .subscribe(dataActivity => this.preActivity = Object.assign(dataActivity));
      }
    });
  }
  convertIndikator(type) {
    switch (type) {
      case 1:
        return 'Capaian Program';
      case 2:
        return 'Masukan';
      case 3:
        return 'Keluaran';
      case 4:
        return 'Hasil';
    }
  }
  btnCetak(type) {
    this.reportService.execPrint('units.rpt', type)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, 'Tes');
      });
  }
  ngOnDestroy() {
    this.service.setPreview([]);
    this.service.setUnitProgAct([]);
  }
}
