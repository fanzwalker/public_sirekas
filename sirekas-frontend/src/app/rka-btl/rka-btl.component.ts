import { BtlPreviewService } from './btl-preview/btl-preview.service';
import { BtlAnggaranKasService } from './btl-anggaran-kas/btl-anggaran-kas.service';
import { BtlKompPenyusunService } from './btl-komp-penyusun/btl-komp-penyusun.service';
import { animationPage } from './../shared/animation-page';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RkaBtlService } from './rka-btl.service';
import { LookupRekeningService } from '../lookup-rekening/lookup-rekening.service';
import { UsersService } from '../service/users.service';
@Component({
  selector: 'app-rka-btl',
  templateUrl: './rka-btl.component.html',
  styleUrls: ['./rka-btl.component.css'],
  animations: [animationPage]
})
export class RkaBtlComponent implements OnInit, OnDestroy {
  listSekolah: any; optSekolah: SelectItem[];
  titleLegend = '';
  formFilter: FormGroup;
  userInfo: any;
  constructor(private fb: FormBuilder,
              private service: RkaBtlService,
              private lookupRekeningService: LookupRekeningService,
              private btlKompPenyusunService: BtlKompPenyusunService,
              private btlAnggaranKasService: BtlAnggaranKasService,
              private btlPreviewService: BtlPreviewService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah : ['', Validators.required],
      program : ['', Validators.required],
      kegiatan: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.onChangeUnit();
    } else {
      this.getSekolah();
    }
    this.getProgram();
  }
  onChangeUnit() {
    this.titleLegend = '';
    this.formFilter.patchValue({
      kegiatan: 0
    });
    this.getKegiatan();
  }
  getRkas(s?) {
    if (s) {
      this.titleLegend = s.label;
    }
    if (this.formFilter.value.sekolah && (this.formFilter.value.kegiatan || this.formFilter.value.kegiatan === 0)) {
      this.btlKompPenyusunService.getBudgetDet(this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.kegiatan, 0);
      this.btlAnggaranKasService.getBudgetMonthly(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      this.btlPreviewService.getPreview(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      this.setUnitProgAct();
      this.service.getUnitBudget(this.formFilter.value.sekolah, this.userInfo.stages)
        .subscribe(resp => {
          if (resp.length > 0) {
            this.titleLegend = resp[0].total;
            this.btlKompPenyusunService.setPaguBtl(resp[0].total);
          }
        });
    } else {
      this.btlKompPenyusunService.setBudgetDet([], 0);
      this.btlAnggaranKasService.setBudgetMonthly([]);
      this.btlPreviewService.setPreview([]);
      this.btlPreviewService.setUnitProgAct([]);
    }
    this.service.setUnitAct(this.formFilter.value.sekolah, this.formFilter.value.kegiatan);
  }
  setUnitProgAct() {
    const data = {
      Units : _.find(this.listSekolah, (e) => e.id === this.formFilter.value.sekolah),
      Programs: this.formFilter.value.program,
      Activity: this.formFilter.value.kegiatan
    };
    if (!this.userInfo.IsAdmin) {
      this.btlPreviewService.getUnitById(this.formFilter.value.sekolah)
      .subscribe(d => {
        data.Units = Object.assign(d);
        this.btlPreviewService.setUnitProgAct(data);
      });
    } else {
      this.btlPreviewService.setUnitProgAct(data);
    }
  }
  onChangeProgram() {
     this.getKegiatan();
  }
  onOpentabs() {
    console.log('open tabs');
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(this.userInfo.stages, 0, 4, 1, 3)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  getProgram() {
    const temp = [];
    this.service.getPrograms()
      .subscribe(resp => {
        this.formFilter.patchValue({
          program: resp[0].id
        });
        this.onChangeUnit();
      });
  }
  getKegiatan() {
    const temp = [];
    this.service.getKegiatan(this.formFilter.value.sekolah, this.formFilter.value.program, this.userInfo.stages, 3)
      .subscribe(resp => {
        if (resp.length > 0) {
          this.formFilter.patchValue({
            kegiatan: 0
          });
          this.getRkas();
        } else {
          this.btlKompPenyusunService.setPaguBtl(0);
          this.service.setUnitAct(this.formFilter.value.sekolah, null);
          this.btlKompPenyusunService.setBudgetDet([], 0);
          this.btlAnggaranKasService.setBudgetMonthly([]);
          this.btlPreviewService.setPreview([]);
          this.btlPreviewService.setUnitProgAct([]);
        }
      });
  }
  ngOnDestroy() {
    this.btlKompPenyusunService.showAddRincian(false);
    this.btlKompPenyusunService.setBudgetDet([], 0);
    this.btlAnggaranKasService.setBudgetMonthly([]);
    this.btlPreviewService.setPreview([]);
    this.btlPreviewService.setUnitProgAct([]);
  }
}
