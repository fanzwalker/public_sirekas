import { BtlPreviewService } from './../../btl-preview/btl-preview.service';
import { BtlAnggaranKasService } from './../../btl-anggaran-kas/btl-anggaran-kas.service';
import { RkaBtlService } from './../../rka-btl.service';
import { BtlKompPenyusunService } from './../btl-komp-penyusun.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import swal from 'sweetalert2';
import * as _ from 'lodash';
import { IlistBudgetDet } from '../../../interface/ilist-budget-det';
import { IlistCoa } from '../../../interface/ilist-coa';
import { SelectItem, Dropdown } from 'primeng/primeng';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../../../service/users.service';
import { Observable } from 'rxjs/Observable';
import { IpostBudgetDet } from '../../../interface/ipost-budget-det';
import { AllOption } from '../../../shared/all-option';
@Component({
  selector: 'app-add-rincian-btl',
  templateUrl: './add-rincian-btl.component.html',
  styleUrls: ['./add-rincian-btl.component.css']
})
export class AddRincianBtlComponent implements OnInit {
  @ViewChild('dd1') dd1: Dropdown;
  @ViewChild('dd2') dd2: Dropdown;
  @ViewChild('dd3') dd3: Dropdown;
  @ViewChild('dd4') dd4: Dropdown;
  @ViewChild('dd5') dd5: Dropdown;
  @ViewChild('dd6') dd6: Dropdown;
  showThis: boolean;
  showThisHeader: boolean;
  coaList: any;
  coaFiltered: any;
  coaSelected: IlistCoa;
  rekening: any;
  listHeader: IlistBudgetDet[];
  optHeader: SelectItem[];
  listSubHeader: IlistBudgetDet[];
  optSubHeader: SelectItem[];
  listSumberDana: any;
  optSumberDana: SelectItem[];
  listUnitDesc: any;
  optUnitDesc: SelectItem[];
  formAdd: FormGroup;
  unitId: number;
  activityId: number;
  formAddHeader: FormGroup;
  listRkas: IlistBudgetDet[];
  typeHeader: number;
  userInfo: any;
  constructor(private service: BtlKompPenyusunService,
              private rkaBtlService: RkaBtlService,
              private fb: FormBuilder,
              private btlAnggaranKasService: BtlAnggaranKasService,
              private btlPreviewService: BtlPreviewService,
              private userService: UsersService) {
    // this.rkaService.dataType$.subscribe(resp => this.dataType = resp[0]);
    // this.rkaService.budgetType$.subscribe(resp => this.budgetType = resp[0]);
    this.userInfo = this.userService.getUserInfo();
    this.formAdd = this.fb.group({
      headerId: [null, Validators.required],
      subHeaderId: [null, Validators.required],
      parentId: [null],
      chartOfAccountId: [null, Validators.required],
      budgetSourceId: [1],
      code: ['', Validators.required],
      description: ['', Validators.required],
      expres1: [null],
      expres2: [null],
      expres3: [null],
      expres4: [null],
      expres5: [null],
      expres6: [null],
      unitDesc: [null],
      costPerUnit: [0],
      type: [2]
    });
    this.formAddHeader = this.fb.group({
      code: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.rkaBtlService.unitId$,
      this.rkaBtlService.activityId$,
      this.service.listRkas$,
      this.service.displayAddRincian$,
      this.service.listBudgetDetHeader$,
      this.service.displayAddHeader$,
      this.service.listBudgetSources$,
      this.service.listUnitDesc$
    );
    combine.subscribe(resp => {
      const [unitId, activityId, listRkas, displayAddRincian, listHeader, displayAddHeader, listBudgetSources, listUnitDesc] = resp;
      this.unitId = unitId;
      this.activityId = 0;
      this.listRkas = listRkas;
      this.showThis = displayAddRincian;
      this.listHeader = Object.assign(listHeader);
      this.showThisHeader = displayAddHeader;
      this.listSumberDana = Object.assign(listBudgetSources);
      this.listUnitDesc = Object.assign(listUnitDesc);
      this.setOptHeader();
      this.setOptBudgetSources();
      this.setOptUnitDesc();
    });
  }
  searchCoa(e) {
    this.service.searchCoa(e.query, 1)
      .subscribe(resp => {
        this.coaFiltered = Object.assign(resp);
      });
  }
  onSelectCoa(e) {
    this.coaSelected = <IlistCoa>e;
    this.service.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 1, this.coaSelected.id);
    this.formAdd.patchValue({
      chartOfAccountId: this.coaSelected.id
    });
  }
  onChangeHeader() {
    this.setOptSub(this.formAdd.value.headerId);
  }
  setOptHeader() {
    this.optHeader = [];
    if (this.listHeader.length > 0) {
        this.optHeader.push({label: '-- Pilih --', value: null});
      _.forEach(this.listHeader, e => {
        this.optHeader.push({label: e.code + ' - ' + e.description, value: e.id});
      });
    } else {
      this.optHeader = [{label: '-- Pilih --', value: null}];
    }
  }
  setOptBudgetSources() {
    this.optSumberDana = [];
    if (this.listSumberDana.length > 0) {
      this.optSumberDana.push({label: '-- Pilih --', value: null});
      _.forEach(this.listSumberDana, e => {
        this.optSumberDana.push({label: e.name, value: e.id});
      });
    } else {
      this.optSumberDana = [{label: '-- Pilih --', value: null}];
    }
  }
  setOptUnitDesc() {
    this.optUnitDesc = [];
    if (this.listUnitDesc.length > 0) {
      this.optUnitDesc.push({label: '-- Pilih --', value: null});
      _.forEach(this.listUnitDesc, e => {
        this.optUnitDesc.push({label: e.name, value: e.id});
      });
    } else {
      this.optUnitDesc = [{label: '-- Pilih --', value: null}];
    }
  }
  setOptSub(budgetDetId, subHeaderId?) {
    this.listSubHeader = [];
    const temp = [];
    this.service.getBudgetDetChild(this.unitId, this.userInfo.stages, this.activityId, 1, budgetDetId)
      .subscribe(resp => {
        if (resp) {
          this.listSubHeader = Object.assign(resp);
          temp.push({label: '-- Pilih --', value: null});
          _.forEach(this.listSubHeader, function (e) {
            temp.push({label: e.code + ' - ' + e.description, value: e.id});
          });
          this.optSubHeader = Object.assign(temp);
          if (subHeaderId) {
            this.formAdd.patchValue({
              subHeaderId: subHeaderId
            });
          }
        }
      });
  }
  showFormHeader(typeHeader: number) {
    this.typeHeader = typeHeader;
    let lastCode = '';
    if (this.typeHeader === 1) {
      if (this.listHeader.length > 0) {
        const lastHeader = _.last(this.listHeader, (e) => {
          return e;
        });
        const tempCode = lastHeader.code.substr(lastHeader.code.length - 4, 4);
        lastCode = this.generateKode(tempCode);
      } else {
        lastCode = '001.';
      }
    } else if (this.typeHeader === 2) {
      const dataHeader = _.find(this.listHeader, (e) => {
        if (e.id === this.formAdd.value.headerId) {
          return e;
        }
      });
      if (this.listSubHeader.length > 0 ) {
        const lastData = _.last(this.listSubHeader, (e) => {
          return e;
        });
        const tempCode = lastData.code.substr(lastData.code.length - 4, 4);
        lastCode = dataHeader.code + this.generateKode(tempCode);
      } else {
        lastCode = dataHeader.code + '001.';
      }
    }
    this.formAddHeader.patchValue({
      code: lastCode
    });
    this.service.showAddSubHeader(true);
  }
  generateKode(lastCode) {
    let newKode = '';
    const splits = lastCode.split('');
    const numberKode = Number(splits[0] + splits[1] + splits[2]) + 1;
    const lengthKode = numberKode.toString().length;
    if (lengthKode === 1) {
      newKode = '00' + numberKode.toString() + '.';
    } else if (lengthKode === 2) {
      newKode = '0' + numberKode.toString() + '.';
    } else {
      newKode = numberKode.toString() + '.';
    }
    return newKode;
  }
  SimpanHeader() {
    const paramBody: IpostBudgetDet = <IpostBudgetDet>this.formAddHeader.value;
    paramBody.type = 1;
    paramBody.costPerUnit = 0;
    paramBody.unitDesc = null;
    paramBody.expression = null;
    paramBody.budgetSourceId = 1;
    paramBody.chartOfAccountId = this.formAdd.value.chartOfAccountId;
    if (this.typeHeader === 1) {
      paramBody.parentId = null;
    } else if (this.typeHeader === 2) {
      paramBody.parentId = this.formAdd.value.headerId;
    }
    this.service.postBudgetDet(this.unitId, this.userInfo.stages, this.activityId, paramBody)
      .subscribe(resp => {
        if (resp) {
          if (this.typeHeader === 1 ) {
            this.setOptSub(resp.id);
            this.onHideAddHeader(resp.id);
          } else if (this.typeHeader === 2) {
            this.setOptSub(this.formAdd.value.headerId, resp.id);
          }
          this.onHideAddHeader();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Header Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }
      });
  }
  BatalHeader() {
    this.onHideAddHeader();
  }
  Simpan() {
    const tempListRkas = Object.assign(this.listRkas);
    const paramBody = <IpostBudgetDet>{};
    paramBody.expression = this.joinExpression();
    paramBody.chartOfAccountId = this.formAdd.value.chartOfAccountId;
    paramBody.budgetSourceId = 1;
    paramBody.unitDesc = this.formAdd.value.unitDesc;
    paramBody.costPerUnit = this.formAdd.value.costPerUnit;
    paramBody.description = this.formAdd.value.description;
    paramBody.type = 2;
    if (this.formAdd.value.headerId !== null && this.formAdd.value.subHeaderId !== null) {
      const SubHeader = _.find(this.listSubHeader, (e) => {
        return e.id === this.formAdd.value.subHeaderId;
      });
      const listSubheader = _.filter(this.listRkas, (e) => {
        return e.type === 2 && e.parentId === SubHeader.id;
      });
      paramBody.parentId = SubHeader.id;
      if (listSubheader.length > 0) {
        const lastData = _.last(listSubheader, (x) => {
          return x.type === 2 && x.parentId === SubHeader.id;
        });
        const tempCode = lastData.code.substr(lastData.code.length - 4, 4);
        paramBody.code = SubHeader.code + this.generateKode(tempCode);
      } else {
        paramBody.code = SubHeader.code + '001.';
      }
    } else if (this.formAdd.value.headerId !== null && this.formAdd.value.subHeaderId === null) {
      const Header = _.find(this.listHeader, (e) => {
        return e.id === this.formAdd.value.headerId;
      });
      paramBody.parentId = Header.id;
      const listDet = _.filter(tempListRkas, (x) => {
        return x.type === 2 && x.parentId === Header.id;
      });
      if (listDet.length > 0) {
        const lastData = _.last(listDet, (e) => {
          return e;
        });
        const tempCode = lastData.code.substr(lastData.code.length - 4, 4);
        paramBody.code = Header.code + this.generateKode(tempCode);
      } else {
        paramBody.code = Header.code + '001.';
      }
    } else if (this.formAdd.value.headerId === null && this.formAdd.value.subHeaderId === null) {
      const listDetChild = _.filter(tempListRkas, (e) => {
        return e.type === 2 && e.parentId === null && e.chartOfAccountId === this.formAdd.value.chartOfAccountId;
      });
      if (listDetChild.length > 0) {
        const lastData = _.last(listDetChild, (e) => {
          return e;
        });
        const tempCode = lastData.code.substr(lastData.code.length - 4, 4);
        paramBody.code = tempCode;
      } else {
        paramBody.code = '001.';
      }
    }
    this.okPost(paramBody);

  }
  okPost(paramBody) {
    this.service.postBudgetDet(this.unitId, this.userInfo.stages, this.activityId, paramBody)
      .subscribe(resp => {
        if (resp) {
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Data Gagal Disimpan [' + error.error.Total[0] + ']',
          showConfirmButton: false,
          timer: 3000
        });
      });
  }
  joinExpression() {
    let ekspresi = '';
    if (this.formAdd.value.expres1) {
      ekspresi += this.formAdd.value.expres1 + ' ' + this.findNameUnit(this.formAdd.value.expres2);
      if (this.formAdd.value.expres3) {
        ekspresi += ' x ' + this.formAdd.value.expres3 + ' ' + this.findNameUnit(this.formAdd.value.expres4);
        if (this.formAdd.value.expres5) {
          ekspresi += ' x ' + this.formAdd.value.expres5 + ' ' + this.findNameUnit(this.formAdd.value.expres6);
        }
      }
    }
    return ekspresi;
  }
  findNameUnit(id) {
    if (this.listUnitDesc.length > 0) {
      const unit =  _.find(this.listUnitDesc, (v) => {
        if (v.id === id) { return v; }
      });
      return unit ? unit.name : '';
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    AllOption.ResetFilterOption([this.dd1, this.dd2, this.dd3, this.dd4, this.dd5, this.dd6]);
    this.formAdd.reset();
    this.coaList = '';
    this.service.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 0);
    this.service.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 1);
    this.btlAnggaranKasService.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
    this.btlPreviewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
    this.setOptSub(0);
    this.service.getUnitDecs();
    this.service.showAddRincian(false);
  }
  onHideAddHeader(headerId?) {
    this.formAddHeader.reset();
    this.service.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 1, this.coaSelected.id);
    if (headerId) {
      this.formAdd.patchValue({
        headerId: headerId
      });
    }
    this.optSubHeader = [{label: '-- Pilih --', value: null}];
    this.service.showAddSubHeader(false);
  }
}
