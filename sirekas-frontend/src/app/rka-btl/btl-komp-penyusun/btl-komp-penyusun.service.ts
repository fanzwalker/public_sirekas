import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpParams, HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class BtlKompPenyusunService {
  private listRkas = new BehaviorSubject(<any>[]);
  public listRkas$ = this.listRkas.asObservable();
  private displayAddRincian = new BehaviorSubject<boolean>(false);
  public displayAddRincian$ = this.displayAddRincian.asObservable();
  private listBudgetDetHeader = new BehaviorSubject(<any>[]);
  public listBudgetDetHeader$ = this.listBudgetDetHeader.asObservable();
  private displayAddHeader = new BehaviorSubject<boolean>(false);
  public displayAddHeader$ = this.displayAddHeader.asObservable();
  private listBudgetSources = new BehaviorSubject(<any>[]);
  public listBudgetSources$ = this.listBudgetSources.asObservable();
  private listUnitDesc = new BehaviorSubject(<any>[]);
  public listUnitDesc$ = this.listUnitDesc.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private dataEditRincian = new BehaviorSubject(<any>[]);
  public dataEditRincian$ = this.dataEditRincian.asObservable();
  private paguBtl = new BehaviorSubject<number>(null);
  public paguBtl$ = this.paguBtl.asObservable();
  constructor(private http: HttpClient) { }
  getBudgetDet(unitId, stages, activityId, type, coaId = null) {
    const params = new HttpParams()
      .set('type', type)
      .set('coaId', coaId);
    this.http.get(`${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets`, {params: params})
      .map(resp => resp).subscribe(data => this.setBudgetDet(data, type));
  }
  getBudgetDetChild(unitId, stages, activityId, type, budgetDetId) {
    const params = new HttpParams()
      .set('type', type);
    return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets/${budgetDetId}/childs`,
      {params: params}).map(resp => <any>resp);
  }
  setBudgetDet(data, type) {
    if (type === 1) {
      return this.listBudgetDetHeader.next(data);
    } else {
      return this.listRkas.next(data);
    }
  }
  postBudgetDet(unitId, stages, activityId, paramBody) {
    return this.http.post(`${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets`,
      paramBody).map(resp => <any>resp);
  }
  putBudgetDet(unitId, stages, activityId, id, paramBody) {
    return this.http.put(`${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets/${id}`,
      paramBody);
  }
  showAddRincian(action: boolean) {
    return this.displayAddRincian.next(action);
  }
  searchCoa(keyword, expensesType) {
    const params = new HttpParams()
      .set('keyword', keyword)
      .set('expensesType', expensesType);
    return this.http.get(`${environment.url}chart-of-accounts`, {params: params});
  }
  showAddSubHeader(action: boolean) {
    return this.displayAddHeader.next(action);
  }
  getBudgetSources() {
    const params = new HttpParams()
      .set('levelType', '2');
    return this.http.get(`${environment.url}budget-sources`, {params: params}).map(resp => resp)
      .subscribe(data => this.setBudgetSources(data));
  }
  setBudgetSources(data) {
    return this.listBudgetSources.next(data);
  }
  getUnitDecs() {
    return this.http.get(`${environment.url}unit-descs`).map(resp => resp)
      .subscribe(data => this.setUnitDesc(data));
  }
  setUnitDesc(data) {
    return this.listUnitDesc.next(data);
  }
  showEditRincian(action: boolean) {
    return this.displayEdit.next(action);
  }
  setDataEditRincian(data) {
    return this.dataEditRincian.next(data);
  }
  deleteBudgetDet(unitId, stages, activityId, paramBody) {
    return this.http.request('DELETE', `${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets`,
      {body: paramBody});
  }
  setPaguBtl(pagu: number): void {
    this.paguBtl.next(pagu);
  }
}
