import { Component, OnInit } from '@angular/core';
import { BtlService } from '../btl.service';
import { IlistBtl } from '../../interface/ilist-btl';
import { Observable } from 'rxjs/Observable';
import { animationPage } from '../../shared/animation-page';
import { ReportService } from '../../service/report.service';
import { MenuItem } from 'primeng/primeng';
import { UserAppService } from '../../user-app/user-app.service';
import { UsersService } from '../../service/users.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-list-btl',
  templateUrl: './list-btl.component.html',
  styleUrls: ['./list-btl.component.css'],
  animations: [animationPage]
})
export class ListBtlComponent implements OnInit {
  listBtl: IlistBtl[];
  unitId: any;
  coaLevel: any;
  itemPrints: MenuItem[];
  userInfo: any;
  constructor(private btlService: BtlService,
              private reportService: ReportService,
              private usersService: UsersService) {
    this.userInfo = this.usersService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
              }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.btlService.listBtl$,
      this.btlService.unitId$,
      this.btlService.coaLevel$
    );
    combine.subscribe(resp => {
      const [listBtl, unitId, coaLevel] = resp;
      this.listBtl = Object.assign(listBtl);
      this.unitId = unitId;
      this.coaLevel = coaLevel;
    });
  }
  btnCetak(type) {
    if (this.coaLevel) {
      const paramBody = {};
      paramBody['@unitId'] = this.unitId;
      paramBody['@stageId'] = +this.userInfo.stages;
      paramBody['@coaLevel'] = this.coaLevel;
      this.reportService.execPrint('ApbsBtl.rpt', type, paramBody)
        .subscribe(resp =>  {
          this.reportService.extractData(resp, type, 'APBS Belanja Tidak Langsung');
        });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Level Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
}
