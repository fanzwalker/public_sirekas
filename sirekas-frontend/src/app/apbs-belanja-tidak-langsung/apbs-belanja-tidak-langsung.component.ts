import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { animationPage } from '../shared/animation-page';
import { UsersService } from '../service/users.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BtlService } from './btl.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-apbs-belanja-tidak-langsung',
  templateUrl: './apbs-belanja-tidak-langsung.component.html',
  styleUrls: ['./apbs-belanja-tidak-langsung.component.css'],
  animations: [animationPage]
})
export class ApbsBelanjaTidakLangsungComponent implements OnInit {
  listSekolah: any; optSekolah: SelectItem[];
  listLevel = ['Bab', 'Kelompok', 'Jenis', 'Objek', 'Rincian', 'Sub Rincian'];
  optLevel: SelectItem[];
  userInfo: any;
  formFilter: FormGroup;
  constructor(private userService: UsersService,
              private fb: FormBuilder,
              private btlService: BtlService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required],
      level: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (this.userInfo.IsAdmin && this.userInfo.unitId !== '0') {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
    } else if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
    }
    this.getSekolah();
    this.setOptLevel();
  }
  getSekolah() {
    const temp = [];
    this.btlService.getUnits()
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, (e) => {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  setOptLevel() {
    const temp = [];
    temp.push({label: 'Pilih Level', value: null});
    _.forEach(this.listLevel, (e) => {
      temp.push({label: e, value: _.indexOf(this.listLevel, e) + 1});
    });
    this.optLevel = temp;
  }
  GetData() {
    this.btlService.setUnitCoa(this.formFilter.value.sekolah, this.formFilter.value.level);
    if (this.formFilter.value.level) {
      this.btlService.getPreview(1, this.userInfo.stages, this.formFilter.value.sekolah, this.formFilter.value.level);
    } else {
      this.btlService.setlistBtl([]);
    }
  }
}
