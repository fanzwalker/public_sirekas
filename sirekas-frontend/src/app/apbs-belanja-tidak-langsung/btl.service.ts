import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class BtlService {
  private listBtl = new BehaviorSubject(<any>[]);
  public listBtl$ = this.listBtl.asObservable();
  private unitId = new BehaviorSubject<number>(null);
  public unitId$ = this.unitId.asObservable();
  private coaLevel = new BehaviorSubject<number>(null);
  public coaLevel$ = this.coaLevel.asObservable();
  constructor(private http: HttpClient) { }
  getUnits() {
    return this.http.get(`${environment.url}units`);
  }
  setUnitCoa(unitId, coaLevel): void {
    this.unitId.next(unitId);
    this.coaLevel.next(coaLevel);
  }
  getPreview(expensesType, stageId, unitId, coaLevel) {
    const queryParam = new HttpParams()
    .set('unitId', unitId)
    .set('coaLevel', coaLevel);
    return this.http.get(`${environment.url}previews/apbs/expenses-type/${expensesType}/stages/${stageId}`, {params: queryParam})
    .map(resp => resp).subscribe(data => this.setlistBtl(data));
  }
  setlistBtl(data) {
    return this.listBtl.next(data);
  }
}
