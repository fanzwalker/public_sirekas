import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class DataUmumService {
  private listUnit = new BehaviorSubject(<any>[]);
  public listUnit$ = this.listUnit.asObservable();
  private unitId = new BehaviorSubject<number>(null);
  public unitId$ = this.unitId.asObservable();
  constructor(private http: HttpClient) { }
  getUnits(stageId) {
    const queryParams = new HttpParams()
    .set('UnitStructure', '4')
    .set('Stages', stageId)
    .set('ExcludeType', '0');
    return this.http.get(`${environment.url}units`, {params: queryParams})
    .map(resp => <any>resp).subscribe(data => this.setListUnit(data));
  }
  setListUnit(data) {
    return this.listUnit.next(data);
  }
  setUnitId(unitId): void {
    this.unitId.next(unitId);
  }
}
