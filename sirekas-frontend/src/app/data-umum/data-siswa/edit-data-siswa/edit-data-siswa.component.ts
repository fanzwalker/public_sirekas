import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { DataSiswaService } from '../data-siswa.service';
import { DataUmumService } from '../../data-umum.service';
import { Observable } from 'rxjs/Observable';
import { AllOption } from '../../../shared/all-option';
@Component({
  selector: 'app-edit-data-siswa',
  templateUrl: './edit-data-siswa.component.html',
  styleUrls: ['./edit-data-siswa.component.css']
})
export class EditDataSiswaComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  formClass: FormGroup;
  unitId: number;
  optKelasRomawi: SelectItem[];
  optKelasHuruf: SelectItem[];
  dataEdit: any;
  constructor(private service: DataSiswaService,
              private dataUmumService: DataUmumService,
              private fb: FormBuilder) {
    this.formEdit = this.fb.group({
      class: ['', Validators.required],
      male: [],
      female: [],
      rombel: []
    });
    this.formClass = this.fb.group({
      romawi: [''],
      huruf: ['']
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayEdit$,
      this.dataUmumService.unitId$,
      this.service.dataEdit$,
    );
    combine.subscribe(resp => {
      const [displayEdit, unitId, dataEdit] = resp;
      this.showThis = displayEdit;
      this.unitId = unitId;
      this.dataEdit = Object.assign(dataEdit);
      if (this.dataEdit.id) {
        this.splitClass(this.dataEdit.class);
        this.formEdit.patchValue({
          male: this.dataEdit.male,
          female: this.dataEdit.female,
          rombel: this.dataEdit.rombel
        });
      }
    });
    this.setOptKelasRomawi();
    this.setOptKelasHuruf();
  }
  setOptKelasRomawi() {
    const temp = [];
    temp.push({label: 'Pilih', value: null});
    _.forEach(AllOption.kelasRomawi(), (x) => {
      temp.push({label: x, value: x});
    });
    this.optKelasRomawi = temp;
  }
  setOptKelasHuruf() {
    const temp = [];
    temp.push({label: 'Pilih', value: null});
    _.forEach(AllOption.kelasHuruf(), (x) => {
      temp.push({label: x, value: x});
    });
    this.optKelasHuruf = temp;
  }
  splitClass(kelas: String) {
    const split = _.split(kelas, ' ');
    this.formClass.patchValue({
      romawi: split[0],
      huruf: split[1]
    });
  }
  Simpan() {
    this.formEdit.patchValue({
      class: this.formClass.value.romawi + ' ' + this.formClass.value.huruf
    });
    this.service.putStudent(this.dataEdit.unitId, this.dataEdit.id, this.formEdit.value)
    .subscribe(resp => {
      if (resp.id) {
        this.onHide();
      }
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.formClass.reset();
    this.service.getStudent(this.unitId);
    this.service.showEdit(false);
  }
}
