import { Observable } from 'rxjs/Observable';
import { DataUmumService } from './../../data-umum.service';
import { DataSiswaService } from './../data-siswa.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { SelectItem } from 'primeng/primeng';
import { AllOption } from '../../../shared/all-option';
@Component({
  selector: 'app-add-data-siswa',
  templateUrl: './add-data-siswa.component.html',
  styleUrls: ['./add-data-siswa.component.css']
})
export class AddDataSiswaComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  formClass: FormGroup;
  unitId: number;
  optKelasRomawi: SelectItem[];
  optKelasHuruf: SelectItem[];
  constructor(private service: DataSiswaService,
              private dataUmumService: DataUmumService,
              private fb: FormBuilder) {
    this.formAdd = this.fb.group({
      class: ['', Validators.required],
      male: [],
      female: [],
      rombel: []
    });
    this.formClass = this.fb.group({
      romawi: [''],
      huruf: ['']
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayAdd$,
      this.dataUmumService.unitId$
    );
    combine.subscribe(resp => {
      const [displayAdd, unitId] = resp;
      this.showThis = displayAdd;
      this.unitId = unitId;
    });
    this.setOptKelasRomawi();
    this.setOptKelasHuruf();
  }
  setOptKelasRomawi() {
    const temp = [];
    temp.push({label: 'Pilih', value: null});
    _.forEach(AllOption.kelasRomawi(), (x) => {
      temp.push({label: x, value: x});
    });
    this.optKelasRomawi = temp;
  }
  setOptKelasHuruf() {
    const temp = [];
    temp.push({label: 'Pilih', value: null});
    _.forEach(AllOption.kelasHuruf(), (x) => {
      temp.push({label: x, value: x});
    });
    this.optKelasHuruf = temp;
  }
  Simpan() {
    this.formAdd.patchValue({
      class: this.formClass.value.romawi + ' ' + this.formClass.value.huruf
    });
    this.service.postStudent(this.unitId, this.formAdd.value)
    .subscribe(resp => {
      if (resp.id) {
        this.onHide();
      }
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAdd.reset();
    this.formClass.reset();
    this.service.getStudent(this.unitId);
    this.service.showAdd(false);
  }
}
