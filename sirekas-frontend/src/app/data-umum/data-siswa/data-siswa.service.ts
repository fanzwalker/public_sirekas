import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class DataSiswaService {
  private listStudent = new BehaviorSubject(<any>[]);
  public listStudent$ = this.listStudent.asObservable();
  private dataEdit =  new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  constructor(private http: HttpClient) { }
  getStudent(unitId) {
    return this.http.get(`${environment.url}units/${unitId}/student-datas`)
    .map(resp => <any>resp).subscribe(data => this.setListStudent(data));
  }
  setListStudent(data) {
    return this.listStudent.next(data);
  }
  postStudent(unitId, postBody) {
    return this.http.post( `${environment.url}units/${unitId}/student-datas`, postBody)
    .map(resp => <any>resp);
  }
  putStudent(unitId, Id, postBody) {
    const queryParam = new HttpParams()
    .set('id', Id);
    return this.http.put( `${environment.url}units/${unitId}/student-datas`, postBody, {params: queryParam})
    .map(resp => <any>resp);
  }
  deleteStudent(unitId, Id) {
    const queryParam = new HttpParams()
    .set('id', Id);
    return this.http.request( 'DELETE', `${environment.url}units/${unitId}/student-datas`, {params: queryParam})
    .map(resp => <any>resp);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
}
