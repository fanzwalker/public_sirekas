import { Observable } from 'rxjs/Observable';
import { DataUmumService } from './../data-umum.service';
import { DataSiswaService } from './data-siswa.service';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import * as _ from 'lodash';
import { UsersService } from '../../service/users.service';
@Component({
  selector: 'app-data-siswa',
  templateUrl: './data-siswa.component.html',
  styleUrls: ['./data-siswa.component.css']
})
export class DataSiswaComponent implements OnInit {
  listStudent: any;
  unitId: number;
  userInfo: any;
  constructor(private service: DataSiswaService,
              private dataUmumService: DataUmumService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listStudent$,
      this.dataUmumService.unitId$
    );
    combine.subscribe(resp => {
      const [listStudent, unitId] = resp;
      this.listStudent = Object.assign(listStudent);
      this.unitId = unitId;
    });
  }
  btnAdd() {
    if (this.unitId) {
      this.service.showAdd(true);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnEdit(e) {
    this.service.setDataEdit(e);
    this.service.showEdit(true);
  }
  btnDelete(e: any) {
    this.service.deleteStudent(e.unitId, e.id)
    .subscribe(resp => {
      this.service.getStudent(this.unitId);
    });
  }
  calculateTotal(x: string) {
    let total = 0;
    switch (x) {
      case 'male' :
        _.forEach(this.listStudent, (s) => {
          total += s.male;
        });
      break;
      case 'female' :
        _.forEach(this.listStudent, (s) => {
          total += s.female;
        });
      break;
      case 'total' :
        _.forEach(this.listStudent, (s) => {
          total += s.total;
        });
      break;
      case 'rombel' :
        _.forEach(this.listStudent, (s) => {
          total += s.rombel;
        });
      break;
    }
    return total;
  }
}
