import { UsersService } from './../service/users.service';
import { DataGuruService } from './data-guru/data-guru.service';
import { DataSiswaService } from './data-siswa/data-siswa.service';
import { DataUmumService } from './data-umum.service';
import { animationPage } from './../shared/animation-page';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import * as _ from 'lodash';
@Component({
  selector: 'app-data-umum',
  templateUrl: './data-umum.component.html',
  styleUrls: ['./data-umum.component.css'],
  animations: [animationPage]
})
export class DataUmumComponent implements OnInit, OnDestroy {
  formFilter:  FormGroup;
  listUnit: any[];
  optUnit: SelectItem[];
  userInfo: any;
  constructor(private service: DataUmumService,
              private siswaService: DataSiswaService,
              private guruService: DataGuruService,
              private userService: UsersService,
              private fb: FormBuilder) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      unitId: [null, Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listUnit$
    );
    combine.subscribe(resp => {
      const [listUnit] = resp;
      this.listUnit = Object.assign(listUnit);
      if (this.listUnit.length > 0) {
        this.setOptUnit();
      }
    });
    if (this.userInfo.IsAdmin) {
      this.service.getUnits(this.userInfo.stages);
    } else {
      this.formFilter.patchValue({
        unitId: this.userInfo.unitId
      });
      this.onChangeUnit();
    }

  }
  setOptUnit() {
    const temp = [];
    temp.push({label: 'Pilih Sekolah', value: null});
      _.forEach(this.listUnit, (e) => {
        temp.push({label: e.code + ' - ' + e.name, value: e.id});
      });
      this.optUnit = temp;
  }
  onChangeUnit() {
    this.service.setUnitId(this.formFilter.value.unitId);
    if (this.formFilter.value.unitId) {
      this.siswaService.getStudent(this.formFilter.value.unitId);
      this.guruService.getEmployee(this.formFilter.value.unitId);
    } else {
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  ngOnDestroy() {
    this.siswaService.setListStudent([]);
    this.guruService.setListEmployee([]);
  }
}
