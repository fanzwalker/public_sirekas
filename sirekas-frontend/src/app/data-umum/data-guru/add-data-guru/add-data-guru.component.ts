import { DataGuruService } from './../data-guru.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import { DataUmumService } from '../../data-umum.service';
import { Observable } from 'rxjs/Observable';
import { AllOption } from '../../../shared/all-option';
import * as _ from 'lodash';
import swal from 'sweetalert2';
@Component({
  selector: 'app-add-data-guru',
  templateUrl: './add-data-guru.component.html',
  styleUrls: ['./add-data-guru.component.css']
})
export class AddDataGuruComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  unitId: number;
  optJenisEmployee: SelectItem[];
  constructor(private service: DataGuruService,
              private dataUmumService: DataUmumService,
              private fb: FormBuilder) {
    this.formAdd = this.fb.group({
      employeeDataType: [null, Validators.required],
      male: [],
      female: [],
      status: [1]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayAdd$,
      this.dataUmumService.unitId$
    );
    combine.subscribe(resp => {
      const [displayAdd, unitId] = resp;
      this.showThis = displayAdd;
      this.unitId = unitId;
    });
    this.setOptJenisEmployee();
  }
  setOptJenisEmployee() {
    const temp = [];
    _.forEach(AllOption.jenisGuru(), (x) => {
      temp.push({label: x.label, value: x.value});
    });
    this.optJenisEmployee = temp;
  }
  Simpan() {
    this.service.postEmployee(this.unitId, this.formAdd.value)
    .subscribe(resp => {
      if (resp.id) {
        this.onHide();
      }
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAdd.reset();
    this.service.getEmployee(this.unitId);
    this.service.showAdd(false);
  }
}
