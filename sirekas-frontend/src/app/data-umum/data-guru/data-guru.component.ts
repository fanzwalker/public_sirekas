import { UsersService } from './../../service/users.service';
import { DataUmumService } from './../data-umum.service';
import { DataGuruService } from './data-guru.service';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-data-guru',
  templateUrl: './data-guru.component.html',
  styleUrls: ['./data-guru.component.css']
})
export class DataGuruComponent implements OnInit {
  listEmployee: any;
  unitId: number;
  userInfo: any;
  constructor(private service: DataGuruService,
              private dataUmumService: DataUmumService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listEmployee$,
      this.dataUmumService.unitId$
    );
    combine.subscribe(resp => {
      const [listEmployee, unitId] = resp;
      this.listEmployee = Object.assign(listEmployee);
      this.unitId = unitId;
    });
  }
  btnAdd() {
    if (this.unitId) {
      this.service.showAdd(true);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnEdit(e) {
    this.service.setDataEdit(e);
    this.service.showEdit(true);
  }
  btnDelete(e: any) {
    this.service.deleteEmployee(e.unitId, e.id)
    .subscribe(resp => {
      this.service.getEmployee(this.unitId);
    });
  }
  calculateTotal(x: string) {
    let total = 0;
    switch (x) {
      case 'male' :
        _.forEach(this.listEmployee, (s) => {
          total += s.male;
        });
      break;
      case 'female' :
        _.forEach(this.listEmployee, (s) => {
          total += s.female;
        });
      break;
      case 'total' :
        _.forEach(this.listEmployee, (s) => {
          total += s.total;
        });
      break;
    }
    return total;
  }
}
