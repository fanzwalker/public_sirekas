import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class DataGuruService {
  private listEmployee = new BehaviorSubject(<any>[]);
  public listEmployee$ = this.listEmployee.asObservable();
  private dataEdit =  new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  constructor(private http: HttpClient) { }
  getEmployee(unitId) {
    return this.http.get(`${environment.url}units/${unitId}/employee-datas`)
    .map(resp => <any>resp).subscribe(data => this.setListEmployee(data));
  }
  setListEmployee(data) {
    return this.listEmployee.next(data);
  }
  postEmployee(unitId, postBody) {
    return this.http.post( `${environment.url}units/${unitId}/employee-datas`, postBody)
    .map(resp => <any>resp);
  }
  putEmployee(unitId, Id, postBody) {
    const queryParam = new HttpParams()
    .set('id', Id);
    return this.http.put( `${environment.url}units/${unitId}/employee-datas`, postBody, {params: queryParam})
    .map(resp => <any>resp);
  }
  deleteEmployee(unitId, Id) {
    const queryParam = new HttpParams()
    .set('id', Id);
    return this.http.request( 'DELETE', `${environment.url}units/${unitId}/employee-datas`, {params: queryParam})
    .map(resp => <any>resp);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }

}
