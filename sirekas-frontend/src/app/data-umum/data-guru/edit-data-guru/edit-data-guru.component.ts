import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { DataGuruService } from '../data-guru.service';
import { SelectItem } from 'primeng/primeng';
import { DataUmumService } from '../../data-umum.service';
import { Observable } from 'rxjs/Observable';
import { AllOption } from '../../../shared/all-option';
@Component({
  selector: 'app-edit-data-guru',
  templateUrl: './edit-data-guru.component.html',
  styleUrls: ['./edit-data-guru.component.css']
})
export class EditDataGuruComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  unitId: number;
  optJenisEmployee: SelectItem[];
  dataEdit: any;
  constructor(private service: DataGuruService,
              private dataUmumService: DataUmumService,
              private fb: FormBuilder) {
    this.formEdit = this.fb.group({
      employeeDataType: [null, Validators.required],
      male: [],
      female: [],
      status: [1]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayEdit$,
      this.dataUmumService.unitId$,
      this.service.dataEdit$,
    );
    combine.subscribe(resp => {
      const [displayEdit, unitId, dataEdit] = resp;
      this.showThis = displayEdit;
      this.unitId = unitId;
      this.dataEdit = Object.assign(dataEdit);
      if (this.dataEdit.id) {
        this.formEdit.patchValue({
          employeeDataType: this.dataEdit.employeeDataType,
          male: this.dataEdit.male,
          female: this.dataEdit.female,
          status: this.dataEdit.status
        });
      }
    });
    this.setOptJenisEmployee();
  }
  setOptJenisEmployee() {
    const temp = [];
    _.forEach(AllOption.jenisGuru(), (x) => {
      temp.push({label: x.label, value: x.value});
    });
    this.optJenisEmployee = temp;
  }
  Simpan() {
    this.service.putEmployee(this.dataEdit.unitId, this.dataEdit.id, this.formEdit.value)
    .subscribe(resp => {
      if (resp.id) {
        this.onHide();
      }
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.service.getEmployee(this.unitId);
    this.service.showEdit(false);
  }
}
