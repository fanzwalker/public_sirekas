import { UsersService } from './../../service/users.service';
import { animationPage } from './../../shared/animation-page';
import { Observable } from 'rxjs/Observable';
import { ManualEntryService } from './../manual-entry.service';
import { Component, OnInit } from '@angular/core';
import { ImanualEntry } from '../../interface/imanual-entry';

@Component({
  selector: 'app-manual-entry-list',
  templateUrl: './manual-entry-list.component.html',
  styleUrls: ['./manual-entry-list.component.css'],
  animations: [animationPage]
})
export class ManualEntryListComponent implements OnInit {
  listManualEntry: ImanualEntry[];
  userInfo: any;
  constructor(private service: ManualEntryService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listManualEntry$
    );
    combine.subscribe(resp => {
      const [listManualEntry] = resp;
      this.listManualEntry = Object.assign(listManualEntry);
    });
  }
  btnEdit(e) {
    this.service.setDataEdit(e);
    this.service.showEdit(true);
  }
}
