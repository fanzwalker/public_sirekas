import { ImanualEntry } from './../../../interface/imanual-entry';
import { Observable } from 'rxjs/Observable';
import { ManualEntryService } from './../../manual-entry.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UsersService } from '../../../service/users.service';

@Component({
  selector: 'app-edit-manual-entry',
  templateUrl: './edit-manual-entry.component.html',
  styleUrls: ['./edit-manual-entry.component.css']
})
export class EditManualEntryComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  dataEdit: ImanualEntry;
  userInfo: any;
  constructor(private service: ManualEntryService,
              private userService: UsersService,
              private fb: FormBuilder) {
    this.userInfo = this.userService.getUserInfo();
    this.formEdit = this.fb.group({
      coaApbsId: [],
      total: []
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayEdit$,
      this.service.dataEdit$
    );
    combine.subscribe(resp => {
      const [displayEdit, dataEdit] = resp;
      this.showThis = displayEdit;
      this.dataEdit = Object.assign(dataEdit);
      this.formEdit.patchValue({
        coaApbsId: this.dataEdit.coaApbsId,
        total: this.dataEdit.total
      });
    });
  }
  Simpan() {
    this.service.putManualEntry(this.dataEdit.unitId, this.userInfo.stages, this.dataEdit.id, this.formEdit.value)
    .subscribe(resp => {
      if (resp.id) {
        this.service.getManualEntry(this.dataEdit.unitId, this.userInfo.stages);
        this.onHide();
      }
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.service.showEdit(false);
  }
}
