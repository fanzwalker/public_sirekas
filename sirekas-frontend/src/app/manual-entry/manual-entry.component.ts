import { animationPage } from './../shared/animation-page';
import { ManualEntryService } from './manual-entry.service';
import { UsersService } from './../service/users.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import swal from 'sweetalert2';
import * as _ from 'lodash';
@Component({
  selector: 'app-manual-entry',
  templateUrl: './manual-entry.component.html',
  styleUrls: ['./manual-entry.component.css'],
  animations: [animationPage]
})
export class ManualEntryComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  userInfo: any;
  listSekolah: any; optSekolah: SelectItem[];
  constructor(private fb: FormBuilder,
              private userService: UsersService,
              private service: ManualEntryService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required]
    });
  }

  ngOnInit() {
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.onChangeUnit();
    } else {
      this.getSekolah();
    }
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(this.userInfo.stages, 0, 4, 1, 1)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  onChangeUnit() {
    if (this.formFilter.valid) {
      this.service.getManualEntry(this.formFilter.value.sekolah, this.userInfo.stages);
    } else {
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Sekolah harus dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  ngOnDestroy() {
    this.service.setListManualEntry([]);
  }
}
