import { environment } from './../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ImanualEntry } from './../interface/imanual-entry';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';

@Injectable()
export class ManualEntryService {
  private listManualEntry = new BehaviorSubject(<any>[]);
  public listManualEntry$ = this.listManualEntry.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  constructor(private http: HttpClient) { }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getManualEntry(unitId, stages) {
    return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/manual-entries`)
    .map(resp => <ImanualEntry>resp).subscribe(data => this.setListManualEntry(data));
  }
  setListManualEntry(data) {
    return this.listManualEntry.next(data);
  }
  putManualEntry(unitId, stages, id, postBody) {
    return this.http.put(`${environment.url}units/${unitId}/stages/${stages}/manual-entries/${id}`, postBody)
    .map(resp => <ImanualEntry>resp);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
}
