import {Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../service/users.service';
import {animationPage} from '../shared/animation-page';
import {Message} from 'primeng/api';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
  animations: [animationPage]
})
export class LoginPageComponent implements OnInit, OnDestroy {
  formLogin: FormGroup;
  returnUrl: string;
  msgs: Message[] = [];
  constructor(private router: Router,
              private route: ActivatedRoute,
              private renderer: Renderer2,
              private fb: FormBuilder,
              private userService: UsersService) {
    this.formLogin = this.fb.group({
      userName: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.renderer.addClass(document.body, 'login-page');
  }

  ngOnInit() {
    this.userService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'home';
  }
  Auth() {
    if (this.formLogin.valid) {
      this.userService.UserAuth(this.formLogin.value)
        .subscribe(resp => {
          const token = resp;
          if (token) {
            localStorage.setItem('currentUser', token);
            this.userService.setUserInfo(this.userService.getUserInfo());
            this.router.navigate([this.returnUrl]);
          }
        }, error => {
          this.msgs = [];
          this.msgs.push({severity: 'error', summary: 'Login Gagal', detail: error.statusText});
          this.formLogin.reset();
        });
    }
    // this.router.navigate(['home']);
  }
  ngOnDestroy() {
    this.renderer.removeClass(document.body, 'login-page');
  }
}
