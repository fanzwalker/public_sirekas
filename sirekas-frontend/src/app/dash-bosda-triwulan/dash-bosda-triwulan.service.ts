import { environment } from './../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { Itriwulan } from '../interface/itriwulan';

@Injectable()
export class DashBosdaTriwulanService {
  private listBosdaTriwulan = new BehaviorSubject(<any>[]);
  public listBosdaTriwulan$ = this.listBosdaTriwulan.asObservable();
  constructor(private http: HttpClient) { }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getBosdaTriwulan(unitId, stageId, activityTypeId) {
    return this.http.get(`${environment.url}previews/rka221-tw/unit/${unitId}/stages/${stageId}/activity-type/${activityTypeId}`)
    .map(resp => <Itriwulan[]>resp).subscribe(data => this.setListBosdaTriwulan(data));
  }
  setListBosdaTriwulan(data) {
    return this.listBosdaTriwulan.next(data);
  }
}
