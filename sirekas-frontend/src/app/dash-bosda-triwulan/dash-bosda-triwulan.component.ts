import { Itriwulan } from './../interface/itriwulan';
import { DashBosdaTriwulanService } from './dash-bosda-triwulan.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsersService } from '../service/users.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem, MenuItem } from 'primeng/api';
import { ReportService } from '../service/report.service';
import * as _ from 'lodash';
import { animationPage } from '../shared/animation-page';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dash-bosda-triwulan',
  templateUrl: './dash-bosda-triwulan.component.html',
  styleUrls: ['./dash-bosda-triwulan.component.css'],
  animations: [animationPage]
})
export class DashBosdaTriwulanComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  userInfo: any;
  listSekolah: any; optSekolah: SelectItem[];
  previews: Itriwulan[];
  itemPrints: MenuItem[];
  unitSelected: any;
  kecamatan: string;
  constructor(private service: DashBosdaTriwulanService,
              private userService: UsersService,
              private fb: FormBuilder,
              private reportService: ReportService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listBosdaTriwulan$
    );
    combine.subscribe(resp => {
      const [listBosdaTriwulan] = resp;
      this.previews = Object.assign(listBosdaTriwulan);
    });
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.unitSelected = {
        code: this.userInfo.unitCode,
        name: this.userInfo.unitName,
        districtName: this.userInfo.unitDistrict
      };
      this.onChangeUnit();
    } else {
      this.getSekolah();
    }
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(this.userInfo.stages, 0, 4, 1, 1)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  onChangeUnit() {
    if (this.formFilter.valid) {
      this.service.getBosdaTriwulan(this.formFilter.value.sekolah, this.userInfo.stages, 1);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
      this.unitSelected = '-';
      this.service.setListBosdaTriwulan([]);
    }
  }
  btnCetak(type) {
    if (this.formFilter.valid) {
      const paramBody = {};
      paramBody['@unitId'] = this.formFilter.value.sekolah;
      paramBody['@stageId'] = +this.userInfo.stages;
      paramBody['@activityTypeId'] = 1;
      this.reportService.execPrint('Rka221Tw.rpt', type, paramBody)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, 'BOSDA Triwulan');
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  ngOnDestroy() {
    this.unitSelected = '-';
    this.service.setListBosdaTriwulan([]);
  }
}
