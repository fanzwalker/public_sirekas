import { Component, OnInit } from '@angular/core';
import { ProgramService } from './program.service';
import { animationPage } from '../../shared/animation-page';
import { IlistProgram } from '../../interface/ilist-program';
import { Observable } from 'rxjs/Observable';
import { KegiatanService } from '../kegiatan/kegiatan.service';
import swal from 'sweetalert2';
import * as _ from 'lodash';
import { ProgKegService } from '../prog-keg.service';
@Component({
  selector: 'app-program',
  templateUrl: './program.component.html',
  styleUrls: ['./program.component.css'],
  animations: [animationPage]
})
export class ProgramComponent implements OnInit {
  listProgram: IlistProgram[];
  programSelected: any = [];
  unitId: any;
  constructor(private programService: ProgramService,
              private kegiatanSerice: KegiatanService,
              private prokegKegiatan: ProgKegService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.programService.listProgram$,
      this.prokegKegiatan.unitId$
    );
    combine.subscribe(resp => {
      const [listProgram, unitId] = resp;
      this.listProgram = Object.assign(listProgram);
      this.unitId = unitId;
    });
  }
  rowSelect(e) {
    this.kegiatanSerice.setProgramId(e.data.id);
    this.kegiatanSerice.getKegiatan(e.data.id, e.data.unitId);
  }
  formEdit(e) {
    this.programService.setDataEdit(e);
    this.programService.showEdit(true);
  }
  showAdd() {
    this.programService.showAdd(true);
  }
  btnDelete() {
    if (this.programSelected.length > 0) {
      swal({
        title: 'Hapus',
        text: 'Data Akan Dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          const listId = [];
          _.forEach(this.programSelected, function (k) {
            listId.push(k.id);
          });
          const dataBody = {
            ids : listId
          };
          this.programService.deleteProgram(dataBody)
          .subscribe(resp => {
              this.programService.getPrograms(this.unitId);
              swal({
                position: 'top-end',
                type: 'success',
                title: 'Berhasil Dihapus',
                showConfirmButton: false,
                timer: 3000
              });
          }, error => {
            swal({
              position: 'top-end',
              type: 'error',
              title: 'Gagal Dihapus ' + error.error.error[0],
              showConfirmButton: false,
              timer: 3000
            });
            return false;
          });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
}
