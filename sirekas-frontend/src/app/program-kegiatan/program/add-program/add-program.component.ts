import { Component, OnInit } from '@angular/core';
import { ProgramService } from '../program.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProgKegService } from '../../prog-keg.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
@Component({
  selector: 'app-add-program',
  templateUrl: './add-program.component.html',
  styleUrls: ['./add-program.component.css']
})
export class AddProgramComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  unitId: any;
  constructor(private programServie: ProgramService,
              private prokegService: ProgKegService,
              private fb: FormBuilder) {
                this.formAdd = this.fb.group({
                  code: ['', Validators.required],
                  name: ['', Validators.required],
                  unitId: [0]
                });
              }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.programServie.displayAdd$,
      this.prokegService.unitId$,
    );
    combine.subscribe(resp => {
      const [displayAdd, unitId] = resp;
      this.showThis = displayAdd;
      this.unitId = unitId;
      this.formAdd.patchValue({
        unitId : this.unitId
      });
    });
  }
  Simpan() {
    this.programServie.postPorgram(this.formAdd.value)
    .subscribe(resp => {
      if (resp) {
        this.programServie.getPrograms(this.unitId);
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Program Berhasil Disimpan',
          showConfirmButton: false,
          timer: 3000
        });
      }
    }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Program Gagal Disimpan',
          showConfirmButton: false,
          timer: 3000
        });
        return false;
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.programServie.showAdd(false);
  }
}
