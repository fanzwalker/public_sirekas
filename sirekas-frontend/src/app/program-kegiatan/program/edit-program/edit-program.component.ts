import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProgKegService } from '../../prog-keg.service';
import { ProgramService } from '../program.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';

@Component({
  selector: 'app-edit-program',
  templateUrl: './edit-program.component.html',
  styleUrls: ['./edit-program.component.css']
})
export class EditProgramComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  dataEdit: any;
  constructor(private programServie: ProgramService,
              private fb: FormBuilder) {
                this.formEdit = this.fb.group({
                  code: ['', Validators.required],
                  name: ['', Validators.required]
                });
              }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.programServie.displayEdit$,
      this.programServie.dataEdit$
    );
    combine.subscribe(resp => {
      const [displayEdit, dataEdit] = resp;
      this.showThis = displayEdit;
      this.dataEdit = Object.assign(dataEdit);
      this.formEdit.patchValue({
        code: this.dataEdit.code,
        name: this.dataEdit.name
      });
    });
  }
  Simpan() {
    this.programServie.putProgram(this.dataEdit.id, this.formEdit.value)
    .subscribe(resp => {
      if (resp) {
        this.programServie.getPrograms(this.dataEdit.unitId);
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Program Berhasil Diubah',
          showConfirmButton: false,
          timer: 3000
        });
      }
    }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
        return false;
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.programServie.showEdit(false);
  }
}
