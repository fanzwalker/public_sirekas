import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class ProgramService {
  private listProgram = new BehaviorSubject(<any>[]);
  public listProgram$ = this.listProgram.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  constructor(private http: HttpClient) { }
  getPrograms(unitId) {
    const paramQuery = new HttpParams()
    .set('unitId', unitId);
    return this.http.get(`${environment.url}programs`, {params: paramQuery})
    .map(resp => resp).subscribe(data => this.setListProgram(data));
  }
  postPorgram(dataBody) {
    return this.http.post(`${environment.url}programs`, dataBody);
  }
  putProgram(programId, dataBody) {
    return this.http.put(`${environment.url}programs/${programId}`, dataBody);
  }
  deleteProgram(paramBody) {
    return this.http.request('DELETE', `${environment.url}programs`, {body: paramBody});
  }
  setListProgram(data) {
    return this.listProgram.next(data);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
}
