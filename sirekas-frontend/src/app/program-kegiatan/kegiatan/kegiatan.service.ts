import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class KegiatanService {
  private listKegiatan = new BehaviorSubject(<any>[]);
  public listKegiatan$ = this.listKegiatan.asObservable();
  private programId = new BehaviorSubject<number>(null);
  public programId$ = this.programId.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  constructor(private http: HttpClient) { }
  setProgramId(programId) {
   return this.programId.next(programId);
  }
  getKegiatan(programId, unitId) {
    const qureyParam = new HttpParams()
    .set('unitId', unitId)
    .set('Stages', '1')
    .set('ExcludeType', '0')
    .set('UnitStructure', '0');
    return this.http.get(`${environment.url}programs/${programId}/activities`, {params: qureyParam})
    .map(resp => resp).subscribe(data => this.setListKegiatan(data));
  }
  postKegiatan(programId, dataBody) {
    return this.http.post(`${environment.url}programs/${programId}/activities`, dataBody);
  }
  putKegiatan(programId, activityId, dataBody) {
    return this.http.put(`${environment.url}programs/${programId}/activities/${activityId}`, dataBody);
  }
  deleteKegiatan(programId, dataBody) {
    return this.http.request('DELETE', `${environment.url}programs/${programId}/activities`, {body: dataBody});
  }
  setListKegiatan(data) {
    return this.listKegiatan.next(data);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
}
