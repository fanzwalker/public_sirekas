import { Component, OnInit } from '@angular/core';
import { IlistKegiatan } from '../../interface/ilist-kegiatan';
import { KegiatanService } from './kegiatan.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import { ProgKegService } from '../prog-keg.service';
import * as _ from 'lodash';
@Component({
  selector: 'app-kegiatan',
  templateUrl: './kegiatan.component.html',
  styleUrls: ['./kegiatan.component.css']
})
export class KegiatanComponent implements OnInit {
  listKegiatan: IlistKegiatan[];
  kegiatanSelected: any = [];
  programId: any;
  unitId: any;
  constructor(private kegiatanService: KegiatanService,
              private prokegServie: ProgKegService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.kegiatanService.listKegiatan$,
      this.kegiatanService.programId$,
      this.prokegServie.unitId$
    );
    combine.subscribe(resp => {
      const [listKegiatan, programId, unitId] = resp;
      this.listKegiatan = Object.assign(listKegiatan);
      this.programId = programId;
      this.unitId = unitId;
    });
  }
  rowSelect(e) {
    console.log(e);
  }
  formEdit(e) {
    this.kegiatanService.setDataEdit(e);
    this.kegiatanService.showEdit(true);
  }
  showAdd() {
    this.kegiatanService.showAdd(true);
  }
  btnDelete() {
    if (this.kegiatanSelected.length > 0 ) {
      swal({
        title: 'Hapus',
        text: 'Data Akan Dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          const listId = [];
          _.forEach(this.kegiatanSelected, function (k) {
            listId.push(k.id);
          });
          const dataBody = {
            ids : listId
          };
          this.kegiatanService.deleteKegiatan(this.programId, dataBody)
          .subscribe(resp => {
            this.kegiatanService.getKegiatan(this.programId, this.unitId);
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Berhasil Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
          }, error => {
            swal({
              position: 'top-end',
              type: 'error',
              title: 'Gagal Dihapus ' + error.error.error[0],
              showConfirmButton: false,
              timer: 3000
            });
            return false;
          });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    } else {
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  castBudgetType(x) {
    if (x === 1) {
      return 'BOSDA';
    } else if (x === 2) {
      return 'BOSNAS';
    } else {
      return 'Not Set';
    }
  }
}
