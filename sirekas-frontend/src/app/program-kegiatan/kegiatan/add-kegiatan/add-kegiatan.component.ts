import { Component, OnInit } from '@angular/core';
import { KegiatanService } from '../kegiatan.service';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProgKegService } from '../../prog-keg.service';
import swal from 'sweetalert2';
import { AllOption } from '../../../shared/all-option';
import * as _ from 'lodash';
@Component({
  selector: 'app-add-kegiatan',
  templateUrl: './add-kegiatan.component.html',
  styleUrls: ['./add-kegiatan.component.css']
})
export class AddKegiatanComponent implements OnInit {
  programId: any;
  formAdd: FormGroup;
  unitId: any;
  showThis: boolean;
  optJenisAnggaran: any;
  constructor(private kegiatanService: KegiatanService,
              private porkegService: ProgKegService,
              private fb: FormBuilder) {
                this.formAdd = this.fb.group({
                  code: ['', Validators.required],
                  name: ['', Validators.required],
                  activityType: ['', Validators]
                });
              }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.kegiatanService.displayAdd$,
      this.kegiatanService.programId$,
      this.porkegService.unitId$
    );
    combine.subscribe(resp => {
      const [displayAdd, programId, unitId] = resp;
      this.showThis = displayAdd;
      this.programId = programId;
      this.unitId = unitId;
    });
    this.setOptJenisAnggaran();
  }
  setOptJenisAnggaran () {
    const filterJenisAnggaran = _.filter(AllOption.JenisAnggaran(), (x) => {
      return x.value !== 3;
    });
    this.optJenisAnggaran = filterJenisAnggaran;
  }
  Simpan() {
    this.kegiatanService.postKegiatan(this.programId, this.formAdd.value)
    .subscribe(resp => {
      if (resp) {
        this.kegiatanService.getKegiatan(this.programId, this.unitId);
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Kegiatan Berhasil Disimpan',
          showConfirmButton: false,
          timer: 3000
        });
      }
    }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Kegiatan Gagal Disimpan',
          showConfirmButton: false,
          timer: 3000
        });
        return false;
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.kegiatanService.showAdd(false);
  }
}
