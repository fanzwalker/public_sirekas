import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProgKegService } from '../../prog-keg.service';
import { ProgramService } from '../../program/program.service';
import { KegiatanService } from '../kegiatan.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import { AllOption } from '../../../shared/all-option';
import * as _ from 'lodash';
@Component({
  selector: 'app-edit-kegiatan',
  templateUrl: './edit-kegiatan.component.html',
  styleUrls: ['./edit-kegiatan.component.css']
})
export class EditKegiatanComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  dataEdit: any;
  unitId: any;
  programId: any;
  optJenisAnggaran: any;
  constructor(private prokegService: ProgKegService,
              private kegiatanService: KegiatanService,
              private fb: FormBuilder) {
                this.formEdit = this.fb.group({
                  code: ['', Validators.required],
                  name: ['', Validators.required],
                  activityType: ['', Validators]
                });
              }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.kegiatanService.displayEdit$,
      this.prokegService.unitId$,
      this.kegiatanService.programId$,
      this.kegiatanService.dataEdit$
    );
    combine.subscribe(resp => {
      const [displayEdit, unitId, programId, dataEdit] = resp;
      this.showThis = displayEdit;
      this.unitId = unitId;
      this.programId = programId;
      this.dataEdit = Object.assign(dataEdit);
      if (this.dataEdit) {
        this.formEdit.patchValue({
          code: this.dataEdit.code,
          name: this.dataEdit.name,
          activityType: this.dataEdit.activityType
        });
      }
    });
    this.setOptJenisAnggaran();
  }
  setOptJenisAnggaran () {
    const filterJenisAnggaran = _.filter(AllOption.JenisAnggaran(), (x) => {
      return x.value !== 3;
    });
    this.optJenisAnggaran = filterJenisAnggaran;
  }
  Simpan() {
    this.kegiatanService.putKegiatan(this.programId, this.dataEdit.id, this.formEdit.value)
    .subscribe(resp => {
      if (resp) {
        this.kegiatanService.getKegiatan(this.programId, this.unitId);
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Program Berhasil Diubah',
          showConfirmButton: false,
          timer: 3000
        });
      }
    }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
        return false;
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.kegiatanService.showEdit(false);
  }
}
