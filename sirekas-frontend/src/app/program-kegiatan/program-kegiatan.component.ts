import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { ProgKegService } from './prog-keg.service';
import * as _ from 'lodash';
import { ProgramService } from './program/program.service';
import { animationPage } from '../shared/animation-page';
import { KegiatanService } from './kegiatan/kegiatan.service';
@Component({
  selector: 'app-program-kegiatan',
  templateUrl: './program-kegiatan.component.html',
  styleUrls: ['./program-kegiatan.component.css'],
  animations: [animationPage]
})
export class ProgramKegiatanComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  listUrusan: any; optUrusan: SelectItem[];
  constructor(private fb: FormBuilder,
              private prokegService: ProgKegService,
              private programService: ProgramService,
              private kegiatanService: KegiatanService) {
                this.formFilter = this.fb.group({
                  units: [0]
                });
              }

  ngOnInit() {
    // tslint:disable-next-line:no-shadowed-variable
    const temp = [];
    this.prokegService.getUrusan()
    .subscribe(resp => {
      this.listUrusan = Object.assign(resp);
      temp.push({label: '0.00.00. Non Urusan', value: 0});
      _.forEach(this.listUrusan, (u) => {
        temp.push({label: u.code + ' ' + u.name, value: u.id});
      });
      this.optUrusan = temp;
      this.loadProgram();
    });
  }
  onChangeUrusan() {
    this.kegiatanService.setListKegiatan([]);
    this.loadProgram();
  }
  loadProgram() {
    this.prokegService.setUnitId(this.formFilter.value.units);
    this.programService.getPrograms(this.formFilter.value.units);
  }
  ngOnDestroy() {
    this.programService.setListProgram([]);
    this.kegiatanService.setListKegiatan([]);
  }
}
