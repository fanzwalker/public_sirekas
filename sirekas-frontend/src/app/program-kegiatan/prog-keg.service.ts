import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../environments/environment';

@Injectable()
export class ProgKegService {
  private listUrusan = new BehaviorSubject(<any>[]);
  public listUrusan$ = this.listUrusan.asObservable();
  private unitId = new BehaviorSubject<string>(null);
  public unitId$ = this.unitId.asObservable();
  constructor(private http: HttpClient) { }
  setUnitId(unitId) {
    return this.unitId.next(unitId);
  }
  getUrusan() {
    const queryParam = new HttpParams()
    .set('Stages', '1')
    .set('ExcludeType', '0')
    .set('UnitStructure', '2');
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
}
