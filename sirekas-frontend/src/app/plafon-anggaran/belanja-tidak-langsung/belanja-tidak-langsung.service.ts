import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../../environments/environment';

@Injectable()
export class BelanjaTidakLangsungService {
  private listUnitBudget = new BehaviorSubject(<any>[]);
  public listUnitBudget$ = this.listUnitBudget.asObservable();
  constructor(private http: HttpClient) {}
  getUnitbudgets(unitId, stages) {
    return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/unit-budgets`)
    .map(resp => resp).subscribe(data => this.setListUnitBudgets(data));
  }
  setListUnitBudgets(data) {
    return this.listUnitBudget.next(data);
  }
  putUnitBudgets(unitId, stages, paramBody) {
    return this.http.put(`${environment.url}units/${unitId}/stages/${stages}/unit-budgets`, paramBody);
  }
}
