import { Component, OnInit, OnDestroy } from '@angular/core';
import { BelanjaTidakLangsungService } from './belanja-tidak-langsung.service';
import { IlistUnitBudgets } from '../../interface/ilist-unit-budgets';
import { Observable } from 'rxjs/Observable';
import { PlafonAnggaranService } from '../plafon-anggaran.service';
import { UsersService } from '../../service/users.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-belanja-tidak-langsung',
  templateUrl: './belanja-tidak-langsung.component.html',
  styleUrls: ['./belanja-tidak-langsung.component.css']
})
export class BelanjaTidakLangsungComponent implements OnInit, OnDestroy {
  listUnitBudget: IlistUnitBudgets[];
  valueDefault = [
    {
    uraian : 'Total Belanja Tidak Langusung',
    total : 0
  }];
  unitId: any;
  userInfo: any;
  constructor(private btlSerivice: BelanjaTidakLangsungService,
              private plafonAnggaranService: PlafonAnggaranService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
              }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonAnggaranService.unitId$,
      this.btlSerivice.listUnitBudget$
    );
    combine.subscribe(resp => {
      const [unitId, listUnitBudget] = resp;
      this.unitId = unitId;
      this.listUnitBudget = Object.assign(listUnitBudget);
      if (this.listUnitBudget.length > 0) {
        this.valueDefault[0].total = this.listUnitBudget[0].total;
      } else {
        this.valueDefault[0].total = 0;
      }
    });
  }
  btnUpdate() {
    const paramBody = {
      total : +this.valueDefault[0].total
    };
    this.btlSerivice.putUnitBudgets(this.unitId, this.userInfo.stages, paramBody)
    .subscribe(resp => {
      if (resp) {
        this.btlSerivice.getUnitbudgets(this.unitId, this.userInfo.stages);
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data Berhasil Diubah',
          showConfirmButton: false,
          timer: 3000
        });
      }
    });
  }
  ngOnDestroy() {
    this.valueDefault[0].total = 0;
  }
}
