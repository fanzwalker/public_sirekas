import { UsersService } from './../../../service/users.service';
import { Component, OnInit } from '@angular/core';
import {PlafonAnggaranService} from '../../plafon-anggaran.service';
import {Observable} from 'rxjs/Observable';
import {IlistActivity} from '../../../interface/ilist-activity';
import * as _ from 'lodash';
import swal from 'sweetalert2';

@Component({
  selector: 'app-lookup-activity',
  templateUrl: './lookup-activity.component.html',
  styleUrls: ['./lookup-activity.component.css']
})
export class LookupActivityComponent implements OnInit {
  showThis: boolean;
  unitId: number;
  programId: number;
  listActivity: IlistActivity[];
  activitySelected: any = [];
  userInfo: any;
  constructor(private plafonAnggaranService: PlafonAnggaranService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonAnggaranService.unitId$,
      this.plafonAnggaranService.programId$,
      this.plafonAnggaranService.displayActivity$,
      this.plafonAnggaranService.listActivity$
    );
    combine.subscribe(resp => {
      const [unitId, programId, displayActivity, listActivity] = resp;
      this.unitId = unitId;
      this.programId = programId;
      this.showThis = displayActivity;
      this.listActivity = Object.assign(listActivity);
      _.forEach(this.listActivity, function(e) {
        e.jumlah = 0;
      });
    });
  }
  btnSimpan() {
    const paramBody = [];
    if (this.activitySelected.length > 0) {
      _.forEach(this.activitySelected, function(k) {
        paramBody.push({
          activityId: k.id,
          total: Number(k.jumlah)
        });
      });
      this.plafonAnggaranService.postUnitActivity(this.unitId, this.programId, this.userInfo.stages, paramBody)
        .subscribe(resp => {
          if (resp) {
            this.plafonAnggaranService.showLookupActivity(false);
            this.plafonAnggaranService.getUnitActivity(this.unitId, this.programId, this.userInfo.stages);
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Data Berhasil Disimpan',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }, error => {
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Data Gagal Disimpan' + error.statusText,
            showConfirmButton: false,
            timer: 3000
          });
        });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnBatal() {
    this.onHide();
  }
  onHide() {
    this.activitySelected = [];
    this.plafonAnggaranService.getActivity(this.programId, this.unitId, 1, this.userInfo.stages);
    this.plafonAnggaranService.showLookupActivity(false);
  }
}
