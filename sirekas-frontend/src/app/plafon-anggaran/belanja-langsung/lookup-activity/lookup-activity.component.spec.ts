import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookupActivityComponent } from './lookup-activity.component';

describe('LookupActivityComponent', () => {
  let component: LookupActivityComponent;
  let fixture: ComponentFixture<LookupActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookupActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookupActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
