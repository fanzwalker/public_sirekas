import { UsersService } from './../../../service/users.service';
import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {PlafonAnggaranService} from '../../plafon-anggaran.service';
import {Message} from 'primeng/api';

@Component({
  selector: 'app-edit-unit-activity',
  templateUrl: './edit-unit-activity.component.html',
  styleUrls: ['./edit-unit-activity.component.css']
})
export class EditUnitActivityComponent implements OnInit {
  showThis: boolean;
  titleThis: string;
  formGroups: FormGroup;
  dataEdit: any;
  unitId: number;
  programId: number;
  msgs: Message[] = [];
  userInfo: any;
  constructor(private fb: FormBuilder,
              private userService: UsersService,
              private plafonAnggaranService: PlafonAnggaranService) {
    this.userInfo = this.userService.getUserInfo();
    this.formGroups = this.fb.group({
      Jumlah: new FormControl()
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonAnggaranService.unitId$,
      this.plafonAnggaranService.programId$,
      this.plafonAnggaranService.titleEditUnitActivity$,
      this.plafonAnggaranService.displayEditUnitActivity$,
      this.plafonAnggaranService.dataEditUnitActivity$
    );
    combine.subscribe(resp => {
      const [unitId, programId, title, displayEdit, dataEdit] = resp;
      this.unitId = unitId;
      this.programId = programId;
      this.titleThis = title;
      this.showThis = displayEdit;
      this.dataEdit = dataEdit;
      this.formGroups.patchValue({
        Jumlah: this.dataEdit.total
      });
    });
  }
  btnUpdate() {
    const paramBody = {
      total: +this.formGroups.value.Jumlah
    };
    this.plafonAnggaranService.putUnitActivity(this.unitId, this.userInfo.stages, this.programId, paramBody, this.dataEdit.activityId)
      .subscribe(resp =>  {
        this.plafonAnggaranService.getUnitActivity(this.unitId, this.programId, this.userInfo.stages);
        this.plafonAnggaranService.showEditUnitActivity(false);
        this.msgs = [];
        this.msgs.push({severity: 'success', summary: 'Berhasil', detail: 'Data Berhasil Diubah'});
      }, error => {
        this.msgs = [];
        this.msgs.push({severity: 'errror', summary: 'Gagal', detail: error.statusText});
      });
  }
  btnCancel() {
    this.onHide();
  }
  onHide() {
    this.plafonAnggaranService.setdataEditUnitActivity([]);
    this.plafonAnggaranService.showEditUnitActivity(false, '');
  }
}
