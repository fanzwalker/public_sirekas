import { UsersService } from './../../service/users.service';
import { Component, OnInit } from '@angular/core';
import {PlafonAnggaranService} from '../plafon-anggaran.service';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import {Message} from 'primeng/api';
import swal from 'sweetalert2';
@Component({
  selector: 'app-belanja-langsung',
  templateUrl: './belanja-langsung.component.html',
  styleUrls: ['./belanja-langsung.component.css']
})
export class BelanjaLangsungComponent implements OnInit {
  listUnitActivity: any;
  UnitActivitySelected: any = [];
  unitId: number;
  programId: number;
  userInfo: any;
  constructor(private plafonAnggaranService: PlafonAnggaranService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonAnggaranService.unitId$,
      this.plafonAnggaranService.programId$,
      this.plafonAnggaranService.listUnitActivity$
    );
    combine.subscribe(resp => {
      const [unitId, programId, listUnitActivity] = resp;
      this.unitId = unitId;
      this.programId = programId;
      this.listUnitActivity = listUnitActivity;
    });
  }
  btnAdd() {
    if (this.unitId && this.programId) {
      this.plafonAnggaranService.getActivity(this.programId, this.unitId, 1, this.userInfo.stages);
      this.plafonAnggaranService.showLookupActivity(true);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah & Program Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnDelete() {
    if (this.UnitActivitySelected.length > 0) {
      swal({
        title: 'Hapus',
        text: 'Data Akan Dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          const listId = [];
          _.forEach(this.UnitActivitySelected, function (k) {
            listId.push(k.activityId);
          });
          const dataBody = {
            ids: listId
          };
          this.plafonAnggaranService.deleteUnitActiviy(this.unitId, 1, this.programId, dataBody)
            .subscribe(resp => {
              this.UnitActivitySelected = [];
              this.plafonAnggaranService.getUnitActivity(this.unitId, this.programId, this.userInfo.stages);
              swal({
                position: 'top-end',
                type: 'success',
                title: 'Data Telah Dihapus',
                showConfirmButton: false,
                timer: 3000
              });
            });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  formEdit(e) {
    this.plafonAnggaranService.setdataEditUnitActivity(e);
    this.plafonAnggaranService.showEditUnitActivity(true, e.activityName);
  }

}
