import {Component, OnDestroy, OnInit} from '@angular/core';
import {PlafonAnggaranService} from './plafon-anggaran.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/api';
import * as _ from 'lodash';
import {animationPage} from '../shared/animation-page';
import {UsersService} from '../service/users.service';
import { BelanjaTidakLangsungService } from './belanja-tidak-langsung/belanja-tidak-langsung.service';
@Component({
  selector: 'app-plafon-anggaran',
  templateUrl: './plafon-anggaran.component.html',
  styleUrls: ['./plafon-anggaran.component.css'],
  animations: [animationPage]
})
export class PlafonAnggaranComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  listSekolah: any; optSekolah: SelectItem[];
  listProgram: any; optProgram: SelectItem[];
  userInfo: any;
  constructor(private plafonAnggaranService: PlafonAnggaranService,
              private fb: FormBuilder,
              private userService: UsersService,
              private btlService: BelanjaTidakLangsungService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah: ['', Validators.required],
      program: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (this.userInfo.IsAdmin && this.userInfo.unitId !== '0') {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.getUnitActivity();
    } else if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.getUnitActivity();
    }
    this.getSekolah();
    this.getProgram();
  }
  getUnitActivity() {
    this.plafonAnggaranService.setUnitProgram(this.formFilter.value.sekolah, this.formFilter.value.program);
    if (this.userInfo.unitId !== '0') {
      this.btlService.getUnitbudgets(this.userInfo.unitId, this.userInfo.stages);
    } else {
      this.btlService.getUnitbudgets(this.formFilter.value.sekolah, this.userInfo.stages);
    }
    if (this.formFilter.value.sekolah && this.formFilter.value.program) {
      this.plafonAnggaranService.getUnitActivity(this.formFilter.value.sekolah, this.formFilter.value.program, this.userInfo.stages);
    } else {
      this.plafonAnggaranService.getUnitActivity(0, 0, 0);
    }
  }
  getSekolah() {
    const temp = [];
    this.plafonAnggaranService.getUnits()
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  getProgram() {
    const temp = [];
    this.plafonAnggaranService.getPrograms()
      .subscribe(resp => {
        this.listProgram = resp;
        temp.push({label: 'Pilih Program', value: null});
        _.forEach(this.listProgram, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optProgram = temp;
      });
  }
  onChangeTabs(e) {
  }
  ngOnDestroy() {
    this.btlService.setListUnitBudgets([]);
    this.plafonAnggaranService.getUnitActivity(0, 0, 0);
  }
}
