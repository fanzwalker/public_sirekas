import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {environment} from '../../environments/environment';

@Injectable()
export class PlafonAnggaranService {
  private dataEditUnitActivity = new BehaviorSubject(<any>[]);
  public dataEditUnitActivity$ = this.dataEditUnitActivity.asObservable();
  private displayEditUnitAcitivity = new BehaviorSubject<boolean>(false);
  public displayEditUnitActivity$ = this.displayEditUnitAcitivity.asObservable();
  private titleEditUnitActivity = new BehaviorSubject<string>('');
  public titleEditUnitActivity$ = this.titleEditUnitActivity.asObservable();

  private listUnitActivity = new BehaviorSubject(<any>[]);
  public listUnitActivity$ = this.listUnitActivity.asObservable();
  private displayActivity = new BehaviorSubject<boolean>(false);
  public displayActivity$ = this.displayActivity.asObservable();
  private unitId = new BehaviorSubject<number>(null);
  public unitId$ = this.unitId.asObservable();
  private programId = new BehaviorSubject<number>(null);
  public programId$ = this.programId.asObservable();
  private listActivity = new BehaviorSubject(<any>[]);
  public listActivity$ = this.listActivity.asObservable();
  constructor(private http: HttpClient) {}
  setUnitProgram(unitId, programId) {
    this.unitId.next(unitId);
    return this.programId.next(programId);
  }
  getUnitActivity(unitId, programId, stages) {
    return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/programs/${programId}/activities`)
      .map(resp => resp).subscribe(data => this.setUnitActivity(data));
  }
  setUnitActivity(data) {
    this.listUnitActivity.next(data);
  }
  postUnitActivity(unitId, programId, stages, dataBody) {
    return this.http.post(`${environment.url}units/${unitId}/stages/${stages}/programs/${programId}/activities`,
      dataBody);
  }
  deleteUnitActiviy(unitId, stages, programId, paramBody) {
    return this.http.request('DELETE', `${environment.url}units/${unitId}/stages/${stages}/programs/${programId}/activities`,
      {body: paramBody});
  }
  putUnitActivity(unitId, stages, programId, dataBody, activityId) {
    const paramQuery = new HttpParams()
      .set('activityId', activityId);
    return this.http.put(`${environment.url}units/${unitId}/stages/${stages}/programs/${programId}/activities`, dataBody,
      {params: paramQuery});
  }
  getActivity(programId, unitId, excludeType, stages) {
    const params = new HttpParams()
      .set('UnitId', unitId)
      .set('ExcludeType', excludeType)
      .set('Stages', stages);
    return this.http.get(`${environment.url}programs/${programId}/activities`, {params: params})
      .map(resp => resp).subscribe(data => this.listActivity.next(data));
  }
  getUnits() {
    return this.http.get(`${environment.url}units`);
  }
  getPrograms() {
    return this.http.get(`${environment.url}programs`);
  }
  showLookupActivity(action: boolean) {
    return this.displayActivity.next(action);
  }
  setdataEditUnitActivity(data: any) {
    return this.dataEditUnitActivity.next(data);
  }
  showEditUnitActivity(action: boolean, title?: string) {
    this.titleEditUnitActivity.next(title);
    return this.displayEditUnitAcitivity.next(action);
  }
}
