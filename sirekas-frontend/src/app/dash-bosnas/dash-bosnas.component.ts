import { IndikatorKenerjaGlobalService } from './../shared/indikator-kenerja-global.service';
import { Observable } from 'rxjs/Observable';
import { DashBosnasService } from './dash-bosnas.service';
import { animationPage } from './../shared/animation-page';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../service/users.service';
import { SelectItem, MenuItem } from 'primeng/api';
import * as _ from 'lodash';
import { IpreviewRkas } from '../interface/ipreview-rkas';
import { ReportService } from '../service/report.service';
@Component({
  selector: 'app-dash-bosnas',
  templateUrl: './dash-bosnas.component.html',
  styleUrls: ['./dash-bosnas.component.css'],
  animations: [animationPage]
})
export class DashBosnasComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  userInfo: any;
  listSekolah: any; optSekolah: SelectItem[];
  previews: IpreviewRkas[];
  listIndikatorKinerja: any;
  Programs: any;
  unitActivity: any;
  itemPrints: MenuItem[];
  total: number;
  constructor(private fb: FormBuilder,
              private service: DashBosnasService,
              private userService: UsersService,
              private indiskatorKinerjaService: IndikatorKenerjaGlobalService,
              private reportService: ReportService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required],
      program: [null, Validators.required],
      kegiatan: [null, Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.dataPreview$,
      this.indiskatorKinerjaService.ListPerfBenchmarkFixed$
    );
    combine.subscribe(resp => {
      const [dataPreview, listIndikatorKinerja] = resp;
      this.previews = Object.assign(dataPreview);
      this.listIndikatorKinerja = Object.assign(listIndikatorKinerja);
      this.total = _.sum(_.filter(this.previews, e => e.type === 2)
        .map(e => e.total));
    });
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
    } else {
      this.getSekolah();
    }
    this.getProgram();
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(this.userInfo.stages, 0, 4, 1, 2)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  getProgram() {
    const temp = [];
    this.service.getPrograms()
      .subscribe(resp => {
        if (resp.length > 0) {
          this.formFilter.patchValue({
            program: resp[0].id
          });
          this.Programs = Object.assign(resp[0]);
        } else {
          this.Programs = [];
        }
        this.onChangeUnit();
      });
  }
  onChangeUnit() {
    this.service.getUnitActivity(this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.program, 2)
    .subscribe(data => {
      if (data.length > 0 ) {
        this.formFilter.patchValue({
          kegiatan: data[0].activityId
        });
        this.unitActivity = Object.assign(data[0]);
        this.indiskatorKinerjaService.getPerfBenchmarkFixed(1, this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.kegiatan);
        this.service.getPreview(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      } else {
        this.unitActivity = Object.assign([]);
        this.service.setPreview([]);
        this.service.setListIndikatorKinerja([]);
      }
    });
  }
  convertIndikator(type) {
    switch (type) {
      case 1:
        return 'Capaian Program';
      case 2:
        return 'Masukan';
      case 3:
        return 'Keluaran';
      case 4:
        return 'Hasil';
    }
  }
  btnCetak(type) {
    const paramBody = {};
    paramBody['@unitId'] = this.unitActivity.unitId;
    paramBody['@activityId'] = this.unitActivity.activityId;
    paramBody['@stageId'] = +this.userInfo.stages;
    this.reportService.execPrint('Rka221.rpt', type, paramBody)
    .subscribe(resp =>  {
      this.reportService.extractData(resp, type, 'RKA221');
    });
  }
  ngOnDestroy() {
    this.unitActivity = Object.assign([]);
    this.service.setPreview([]);
    this.indiskatorKinerjaService.setListPerfBenchmarkFixed([]);
  }
}
