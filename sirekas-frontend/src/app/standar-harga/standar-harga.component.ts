import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { animationPage } from './../shared/animation-page';
import { Component, OnInit } from '@angular/core';
import { StandarHargaService } from './standar-harga.service';
import { Observable } from 'rxjs/Observable';
import { UsersService } from '../service/users.service';
import swal from 'sweetalert2';
import * as _ from 'lodash';
import { AllOption } from '../shared/all-option';
@Component({
  selector: 'app-standar-harga',
  templateUrl: './standar-harga.component.html',
  styleUrls: ['./standar-harga.component.css'],
  animations: [animationPage]
})
export class StandarHargaComponent implements OnInit {
  listBudgetStandar: any;
  userInfo: any;
  optJenisAnggaran: SelectItem[];
  formFilter: FormGroup;
  constructor(private standarHargaService: StandarHargaService,
              private fb: FormBuilder,
              private usersService: UsersService) {
    this.userInfo = this.usersService.getUserInfo();
    this.formFilter = this.fb.group({
      budgetType: ['', Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.standarHargaService.listBudgetStandar$
    );
    combine.subscribe(resp => {
      const [listBudgetStandar] = resp;
      this.listBudgetStandar = Object.assign(listBudgetStandar);
    });
    this.setOptJenisAnggaran();
  }
  setOptJenisAnggaran() {
    const temp = [];
    _.forEach(AllOption.JenisAnggaranStandar(), (x) => {
      temp.push({label: x.label, value: x.value});
    });
    this.optJenisAnggaran = temp;
  }
  onChangeBudgetType() {
    this.standarHargaService.getBudgetStandar(this.formFilter.value.budgetType);
  }
  showEdit(e) {
    this.standarHargaService.setBudgetType(this.formFilter.value.budgetType);
    this.standarHargaService.setDataEdit(e);
    this.standarHargaService.showEdit(true);
  }
}
