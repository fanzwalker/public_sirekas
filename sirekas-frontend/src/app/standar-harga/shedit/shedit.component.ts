import { Observable } from 'rxjs/Observable';
import { StandarHargaService } from './../standar-harga.service';
import {Component, Input, OnInit} from "@angular/core";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
@Component({
  selector: 'app-shedit',
  templateUrl: './shedit.component.html',
  styleUrls: ['./shedit.component.css']
})
export class SheditComponent implements OnInit {
  showThis: boolean;
  dataEdit: any;
  formEdit: FormGroup;
  budgetType: number;
  constructor(private standarHargaService: StandarHargaService,
              private fb: FormBuilder) {
    this.formEdit = this.fb.group({
      value: [null, Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.standarHargaService.dataEdit$,
      this.standarHargaService.displayEdit$,
      this.standarHargaService.budgetType$
    );
    combine.subscribe(resp => {
      const [dataEdit, displayEdit, budgetType] = resp;
      this.dataEdit = Object.assign(dataEdit);
      this.showThis = displayEdit;
      this.budgetType = budgetType;
      this.formEdit.patchValue({
        value: this.dataEdit.value
      });
    });
  }
  Simpan() {
    this.formEdit.patchValue({
      value: +this.formEdit.value.value
    });
    this.standarHargaService.putBudgetStandar(this.budgetType, this.dataEdit.id, this.formEdit.value)
    .subscribe(resp => {
      if (resp) {
        this.standarHargaService.getBudgetStandar(this.budgetType);
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data Berhasil Diubah',
          showConfirmButton: false,
          timer: 3000
        });
      }
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.standarHargaService.setDataEdit([]);
    this.standarHargaService.showEdit(false);
  }
}
