import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class StandarHargaService {
  private listBudgetStandar = new BehaviorSubject(<any>[]);
  public listBudgetStandar$ = this.listBudgetStandar.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private budgetType = new BehaviorSubject<number>(null);
  public budgetType$ = this.budgetType.asObservable();
  constructor(private http: HttpClient) { }
  getBudgetStandar(standardTypeId) {
    return this.http.get(`${environment.url}standard-types/${standardTypeId}/budget-standards`)
    .map(resp => resp).subscribe(data => this.setListBudgetStandar(data));
  }
  setListBudgetStandar(data) {
    return this.listBudgetStandar.next(data);
  }
  putBudgetStandar(standardTypeId, id, postBody) {
    return this.http.put(`${environment.url}standard-types/${standardTypeId}/budget-standards/${id}`, postBody);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  setBudgetType(budgetType): void {
    this.budgetType.next(budgetType);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
 }
