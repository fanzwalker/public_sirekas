import { BudgetType } from './../shared/budget-type';
import { Observable } from 'rxjs/Observable';
import { IndikatorKinerja } from './../interface/indikator-kinerja';
import { animationPage } from './../shared/animation-page';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/api';
import * as _ from 'lodash';
import { IndikatorKinerjaKegiatanService } from './indikator-kinerja-kegiatan.service';
import { AllOption } from '../shared/all-option';
import { ArrayStatic } from '../shared/arrayStatic';
@Component({
  selector: 'app-indikator-kinerja-kegiatan',
  templateUrl: './indikator-kinerja-kegiatan.component.html',
  styleUrls: ['./indikator-kinerja-kegiatan.component.css'],
  animations: [animationPage]
})
export class IndikatorKinerjaKegiatanComponent implements OnInit {
  optJenisAnggaran: SelectItem[];
  formFilter: FormGroup;
  listPerfBenchmarkFixed: IndikatorKinerja[];
  constructor(private service: IndikatorKinerjaKegiatanService,
              private fb: FormBuilder) {
    this.formFilter = this.fb.group({
      budgetType: [null, Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.ListPerfBenchmarkFixed$
    );
    combine.subscribe(resp => {
      const [listPerfBenchmarkFixed] = resp;
      this.listPerfBenchmarkFixed = Object.assign(listPerfBenchmarkFixed);
    });
    this.setOptJenisAnggaran();
  }
  setOptJenisAnggaran() {
    const temp = [];
    _.forEach(AllOption.JenisAnggaran(), (x) => {
      if (x.value !== 3) {
        temp.push({label: x.label, value: x.value});
      }
    });
    this.optJenisAnggaran = temp;
  }
  onChangeBudgetType() {
    this.service.setBudgetTypeSelected(this.formFilter.value.budgetType);
    if (this.formFilter.valid) {
      this.service.getPerfBenchmarkFixed(this.formFilter.value.budgetType);
    } else {
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Jenis Anggaran Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
      this.service.setListPerfBenchmarkFixed([]);
    }
  }
  formEdit(e) {
    this.service.setDataEdit(e);
    this.service.showEdit(true);
  }
}
