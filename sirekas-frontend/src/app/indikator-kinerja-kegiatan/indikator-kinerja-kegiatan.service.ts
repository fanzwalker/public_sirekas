import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { IndikatorKinerja } from '../interface/indikator-kinerja';
import * as _ from 'lodash';
import { ArrayStatic } from '../shared/arrayStatic';
@Injectable()
export class IndikatorKinerjaKegiatanService {
  private ListPerfBenchmarkFixed = new BehaviorSubject(<any>[]);
  public ListPerfBenchmarkFixed$ = this.ListPerfBenchmarkFixed.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private budgetTypeSelected = new BehaviorSubject<number>(null);
  public budgetTypeSelected$ = this.budgetTypeSelected.asObservable();
  constructor(private http: HttpClient) { }
  getPerfBenchmarkFixed(budgetTypeId) {
    return this.http.get(`${environment.url}budget-types/${budgetTypeId}/perf-benchmark-fixed`)
    .map(resp => <IndikatorKinerja[]>resp).subscribe(data => {
      if (data.length > 0) {
        for (let i = 0 ; i < data.length; i++) {
          data[i].indikator = ArrayStatic.listIndikator()[i];
        }
      }
      this.setListPerfBenchmarkFixed(data);
    });
  }
  setListPerfBenchmarkFixed(data) {
    return this.ListPerfBenchmarkFixed.next(data);
  }
  setBudgetTypeSelected(budgetTypeId): void {
    this.budgetTypeSelected.next(budgetTypeId);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
  putPerfBenchmarkFixed(budgetTypeId, id, postBody) {
    return this.http.put(`${environment.url}budget-types/${budgetTypeId}/perf-benchmark-fixed/${id}`, postBody).map(resp => <any>resp);
  }
}
