import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { IndikatorKinerjaKegiatanService } from '../indikator-kinerja-kegiatan.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import swal from 'sweetalert2';
@Component({
  selector: 'app-edit-indikator',
  templateUrl: './edit-indikator.component.html',
  styleUrls: ['./edit-indikator.component.css']
})
export class EditIndikatorComponent implements OnInit {
  showThis: boolean;
  budgetTypeId: number;
  dataEdit: any;
  formEdit: FormGroup;
  constructor(private service: IndikatorKinerjaKegiatanService,
              private fb: FormBuilder) {
    this.formEdit = this.fb.group({
      name: [''],
      target: ['']
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayEdit$,
      this.service.dataEdit$,
      this.service.budgetTypeSelected$
    );
    combine.subscribe(resp => {
      const [displayEdit, dataEdit, budgetTypeSelected] = resp;
      this.showThis = displayEdit;
      this.dataEdit = Object.assign(dataEdit);
      this.budgetTypeId = budgetTypeSelected;
      if (this.dataEdit.id) {
        this.formEdit.patchValue({
          name : this.dataEdit.name,
          target : this.dataEdit.target
        });
      }
    });
  }
  Simpan() {
    this.service.putPerfBenchmarkFixed(this.budgetTypeId, this.dataEdit.id, this.formEdit.value)
    .subscribe(resp => {
      if (resp.id) {
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data Berhasil Disimpan',
          showConfirmButton: false,
          timer: 3000
        });
        this.onHide();
      }
    }, error => {
      console.log(error.error.error);
      swal({
        position: 'top-end',
        type: 'error',
        title: error.error.error[0],
        showConfirmButton: false,
        timer: 3000
      });
    });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.service.setDataEdit([]);
    this.service.getPerfBenchmarkFixed(this.budgetTypeId);
    this.service.showEdit(false);
  }
}
