import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../environments/environment';

@Injectable()
export class SumberDanaService {
  private listSumberDana = new BehaviorSubject(<any>[]);
  public listSumberDana$ = this.listSumberDana.asObservable();
  private listHeader = new BehaviorSubject(<any>[]);
  public listHeader$ = this.listHeader.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayAddHeader = new BehaviorSubject<boolean>(false);
  public displayAddHeader$ = this.displayAddHeader.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  constructor(private http: HttpClient) { }
  getSumberDana(levelType = null) {
    const queryParam = new HttpParams()
    .set('levelType', levelType);
    return this.http.get(`${environment.url}budget-sources`, {params: queryParam})
    .map(resp => resp).subscribe(data => this.setListSumberDana(data));
  }
  setListSumberDana(data) {
    return this.listSumberDana.next(data);
  }
  getHeader(levelType) {
    const queryParam = new HttpParams()
    .set('levelType', levelType);
    return this.http.get(`${environment.url}budget-sources`, {params: queryParam})
    .map(resp => resp).subscribe(data => this.setListHeader(data));
  }
  setListHeader(data) {
    return this.listHeader.next(data);
  }
  postSumberDana(dataBody) {
    return this.http.post(`${environment.url}budget-sources`, dataBody);
  }
  putSumberDana(id, dataBody) {
    return this.http.put(`${environment.url}budget-sources/${id}`, dataBody);
  }
  deleteSumberDana( paramBody) {
    return this.http.request('DELETE', `${environment.url}budget-sources`, {body: paramBody});
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  showAddHeader(action: boolean) {
    return this.displayAddHeader.next(action);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
}
