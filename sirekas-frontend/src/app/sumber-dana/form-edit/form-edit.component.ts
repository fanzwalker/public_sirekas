import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SumberDanaService } from '../sumber-dana.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
@Component({
  selector: 'app-form-edit',
  templateUrl: './form-edit.component.html',
  styleUrls: ['./form-edit.component.css']
})
export class FormEditComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  dataEdit: any;
  constructor(private sumberDanaService: SumberDanaService,
              private fb: FormBuilder) {
    this.formEdit = this.fb.group({
      code: ['', Validators.required],
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.sumberDanaService.displayEdit$,
      this.sumberDanaService.dataEdit$
    );
    combine.subscribe(resp => {
      const [displayEdit, dataEdit] = resp;
      this.showThis = displayEdit;
      this.dataEdit = Object.assign(dataEdit);
      if (this.dataEdit) {
        this.formEdit.patchValue({
          code: this.dataEdit.code,
          name: this.dataEdit.name
        });
      }
    });
  }
  Simpan() {
    if (this.formEdit.valid) {
      this.sumberDanaService.putSumberDana(this.dataEdit.id, this.formEdit.value)
      .subscribe(resp => {
        if (resp) {
          this.sumberDanaService.getSumberDana();
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Diubah',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.sumberDanaService.showEdit(false);
  }
}
