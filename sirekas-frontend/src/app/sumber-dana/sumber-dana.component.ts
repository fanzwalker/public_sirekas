import { Component, OnInit } from '@angular/core';
import { animationPage } from '../shared/animation-page';
import { SumberDanaService } from './sumber-dana.service';

@Component({
  selector: 'app-sumber-dana',
  templateUrl: './sumber-dana.component.html',
  styleUrls: ['./sumber-dana.component.css'],
  animations: [animationPage]
})
export class SumberDanaComponent implements OnInit {

  constructor(private sumberDanaService: SumberDanaService) { }

  ngOnInit() {
    this.getSumberDana();
  }
  getSumberDana() {
    this.sumberDanaService.getSumberDana();
  }
}
