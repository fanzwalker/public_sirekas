import { Component, OnInit } from '@angular/core';
import { SumberDanaService } from '../sumber-dana.service';
import { Observable } from 'rxjs/Observable';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/primeng';
import swal from 'sweetalert2';
import * as _ from 'lodash';
@Component({
  selector: 'app-form-add',
  templateUrl: './form-add.component.html',
  styleUrls: ['./form-add.component.css']
})
export class FormAddComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  listHeader: any; optHeader: SelectItem[];
  constructor(private sumberDanaService: SumberDanaService,
              private fb: FormBuilder) {
    this.formAdd = this.fb.group({
      parentId: [null],
      code: ['', Validators.required],
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.sumberDanaService.displayAdd$,
      this.sumberDanaService.listHeader$
    );
    combine.subscribe(resp => {
      const [displayAdd, listHeader] = resp;
      this.showThis = displayAdd;
      this.listHeader = Object.assign(listHeader);
      this.getHeader();
    });
  }
  getHeader() {
    if (this.listHeader) {
      const temp = [];
      temp.push({label: 'Pilih Header', value: null});
      _.forEach(this.listHeader, function (e) {
        temp.push({label: e.name, value: e.id});
      });
      this.optHeader = temp;
    }
  }
  onChangeHeader(e) {
    const header = _.find(this.listHeader, (x) => {
      return x.id === e.value;
    });
    if (header) {
      this.formAdd.patchValue({
      code: header.code
      });
    } else {
      this.formAdd.patchValue({
        code: null
      });
    }
  }
  Simpan() {
    if(this.formAdd.valid) {
      this.sumberDanaService.postSumberDana(this.formAdd.value)
      .subscribe(resp => {
        if (resp) {
          this.sumberDanaService.getSumberDana();
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
      });
    }
  }
  showAddHeader() {
    this.sumberDanaService.showAddHeader(true);
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAdd.reset();
    this.sumberDanaService.showAdd(false);
  }
}
