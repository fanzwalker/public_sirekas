import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SumberDanaService } from '../sumber-dana.service';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-form-add-header',
  templateUrl: './form-add-header.component.html',
  styleUrls: ['./form-add-header.component.css']
})
export class FormAddHeaderComponent implements OnInit {
  showThis: boolean;
  formAddHeader: FormGroup;
  constructor(private sumberDanaService: SumberDanaService,
              private fb: FormBuilder) {
    this.formAddHeader = this.fb.group({
      parentId: [null],
      code: ['', Validators.required],
      name: ['', Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.sumberDanaService.displayAddHeader$
    );
    combine.subscribe(resp => {
      const [displayAddHeader] = resp;
      this.showThis = displayAddHeader;
    });
  }
  Simpan() {
    if(this.formAddHeader.valid) {
      this.sumberDanaService.postSumberDana(this.formAddHeader.value)
      .subscribe(resp => {
        if (resp) {
          this.sumberDanaService.getHeader(1);
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAddHeader.reset();
    this.sumberDanaService.showAddHeader(false);
  }
}
