import { Component, OnInit } from '@angular/core';
import { IsumberDana } from '../../interface/isumber-dana';
import { SumberDanaService } from '../sumber-dana.service';
import { Observable } from 'rxjs/Observable';
import { animationPage } from '../../shared/animation-page';
import swal from 'sweetalert2';
import * as _ from 'lodash';
@Component({
  selector: 'app-list-sumber-dana',
  templateUrl: './list-sumber-dana.component.html',
  styleUrls: ['./list-sumber-dana.component.css'],
  animations: [animationPage]
})
export class ListSumberDanaComponent implements OnInit {
  listSumberDana: IsumberDana[];
  sumberDanaSelected: any = [];
  constructor(private sumberDanaService: SumberDanaService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.sumberDanaService.listSumberDana$
    );
    combine.subscribe(resp => {
      const [listSumberDana] = resp;
      this.listSumberDana = Object.assign(listSumberDana);
    });
  }
  showAdd() {
    this.sumberDanaService.getHeader(1);
    this.sumberDanaService.showAdd(true);
  }
  showEdit(e) {
    this.sumberDanaService.setDataEdit(e);
    this.sumberDanaService.showEdit(true);
  }
  switchType(e) {
    switch (e) {
      case 1 : {
        return 'H';
      }
      case 2 : {
        return 'D';
      }
    }
  }
  btnDelete() {
    if (this.sumberDanaSelected.length > 0) {
      swal({
        title: 'Hapus',
        text: 'Data Akan Dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          const listId = [];
          _.forEach(this.sumberDanaSelected, function (k) {
            listId.push(k.id);
          });
          const dataBody = {
            ids : listId
          };
          this.sumberDanaService.deleteSumberDana(dataBody)
          .subscribe(resp => {
              this.sumberDanaService.getSumberDana();
              swal({
                position: 'top-end',
                type: 'success',
                title: 'Berhasil Dihapus',
                showConfirmButton: false,
                timer: 3000
              });
          }, error => {
            swal({
              position: 'top-end',
              type: 'error',
              title: 'Gagal Dihapus ' + error.error.error[0],
              showConfirmButton: false,
              timer: 3000
            });
            return false;
          });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
}
