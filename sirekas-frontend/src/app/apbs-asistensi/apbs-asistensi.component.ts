import { ApbsAsistensiService } from './apbs-asistensi.service';
import { animationPage } from './../shared/animation-page';
import { Component, OnInit } from '@angular/core';
import { UsersService } from '../service/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectItem, MenuItem } from 'primeng/primeng';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
import { ReportService } from '../service/report.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-apbs-asistensi',
  templateUrl: './apbs-asistensi.component.html',
  styleUrls: ['./apbs-asistensi.component.css'],
  animations: [animationPage]
})
export class ApbsAsistensiComponent implements OnInit {
  datas: any;
  userInfo: any;
  formFilter: FormGroup;
  listUnit: any[];
  optUnit: SelectItem[];
  itemPrints: MenuItem[];
  lembarType: any;
  constructor(private userService: UsersService,
              private service: ApbsAsistensiService,
              private reportService: ReportService,
              private fb: FormBuilder) {
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: (x) => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      unitId: [null, Validators.required],
      tglSah: [new Date()]
    });
    this.datas = [
      {desc : 'SAMPUL', lembarType: 4, rptName: 'Sampul.rpt', outName: 'Sampul'},
      {desc : 'LEMBAR PENGESAHAN', lembarType: 1, rptName: 'LembarPengesahan.rpt', outName: 'Lembar Pengesahan'}, // 1 = pengesahan
      {desc : 'LEMBAR ASISTENSI', lembarType: 2, rptName: 'LembarAsistensi.rpt', outName: 'Lembar Asistensi'}, // 2 = asistensi
      {desc : 'TIM PENYUSUN', lembarType: 3, rptName: 'TimPenyusun.rpt', outName: 'Tim Penyusun'}
    ];
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listUnit$
    );
    combine.subscribe(resp => {
      const [listUnit] = resp;
      this.listUnit = Object.assign(listUnit);
      if (this.listUnit.length > 0) {
        this.setOptUnit();
      }
    });
    if (this.userInfo.IsAdmin) {
      this.service.getUnits();
    } else {
      this.formFilter.patchValue({
        unitId: this.userInfo.unitId
      });
    }
  }
  setOptUnit() {
    const temp = [];
    temp.push({label: 'Pilih Sekolah', value: null});
      _.forEach(this.listUnit, (e) => {
        temp.push({label: e.code + ' - ' + e.name, value: e.id});
      });
      this.optUnit = temp;
  }
  setLembarType(x) {
    this.lembarType = x;
  }
  btnCetak(type) {
    if (this.formFilter.valid) {
      const paramBody = {};
      paramBody['@unitId'] = this.formFilter.value.unitId;
      paramBody['@tglSah'] = this.getFormattedDate(this.formFilter.value.tglSah);
      const reportObj = _.find(this.datas, el => el.lembarType === this.lembarType);
      if (reportObj) {
        this.reportService.execPrint(reportObj.rptName, type, paramBody)
        .subscribe(resp =>  {
          this.reportService.extractData(resp, type, reportObj.outName);
        });
      }
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  getFormattedDate(date) {
    const year = date.getFullYear();
    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    let day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  }
}
