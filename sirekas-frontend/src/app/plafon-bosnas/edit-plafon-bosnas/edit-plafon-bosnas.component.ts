import { UsersService } from './../../service/users.service';
import { IbosnasLimit } from './../../interface/ibosnas-limit';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';
import { animationPage } from './../../shared/animation-page';
import { PlafonBosnasService } from './../plafon-bosnas.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-edit-plafon-bosnas',
  templateUrl: './edit-plafon-bosnas.component.html',
  styleUrls: ['./edit-plafon-bosnas.component.css']
})
export class EditPlafonBosnasComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  budgetStandar: any;
  dataEdit: IbosnasLimit;
  userInfo: any;
  constructor(private plafonBosnasService: PlafonBosnasService,
              private userService: UsersService,
              private fb: FormBuilder) {
    this.userInfo = this.userService.getUserInfo();
    this.formEdit = this.fb.group({
      unitName: [0, Validators.required],
      totalStudent: [0, Validators.required],
      standardPrice: [0, Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonBosnasService.displayEdit$,
      this.plafonBosnasService.editData$,
      this.plafonBosnasService.budgetStandar$
    );
    combine.subscribe(resp => {
      const [displayEdit, dataEdit, budgetStandar] = resp;
      this.showThis = displayEdit;
      this.dataEdit = Object.assign(dataEdit);
      this.budgetStandar = Object.assign(budgetStandar);
      this.formEdit.patchValue({
        unitName: this.dataEdit.unitName,
        totalStudent: this.dataEdit.totalStudent,
        standardPrice: this.budgetStandar.value
      });
    });
  }
  Simpan() {
    if (this.formEdit.valid) {
      const postBody = {
        bosnasBudgetDesc: {
          totalStudent: +this.formEdit.value.totalStudent,
          standardPrice: +this.formEdit.value.standardPrice
        }
      };
      this.plafonBosnasService.putTransactBosnasBudgetDesc(this.dataEdit.unitId, this.userInfo.stages, postBody)
      .subscribe(resp => {
        this.plafonBosnasService.getBosnasLimit(this.userInfo.stages);
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data berhasil diubah',
          showConfirmButton: false,
          timer: 3000
        });
        this.onHide();
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.plafonBosnasService.showEdit(false);
  }


}
