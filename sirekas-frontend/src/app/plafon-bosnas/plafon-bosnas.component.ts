import { MenuItem } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import { PlafonBosnasService } from './plafon-bosnas.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { animationPage } from '../shared/animation-page';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { UsersService } from '../service/users.service';
import * as _ from 'lodash';
import { IbosnasLimit } from '../interface/ibosnas-limit';
import { ReportService } from '../service/report.service';
@Component({
  selector: 'app-plafon-bosnas',
  templateUrl: './plafon-bosnas.component.html',
  styleUrls: ['./plafon-bosnas.component.css'],
  animations: [animationPage]
})
export class PlafonBosnasComponent implements OnInit, OnDestroy {
  userInfo: any;
  listBosnasLimit: IbosnasLimit[];
  total: number;
  itemPrints: MenuItem[];
  constructor(private userService: UsersService,
               private reportService: ReportService,
              private plafonBosnasService: PlafonBosnasService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonBosnasService.listBosnasLimit$
    );
    combine.subscribe(resp => {
      const [listBosnasLimit] = resp;
      this.listBosnasLimit = Object.assign(listBosnasLimit);
      this.total = _.sum(_.map(this.listBosnasLimit, l => l.total));
    });
    this.plafonBosnasService.getBosnasLimit(1);
  }
  showEdit(e) {
    this.plafonBosnasService.getBudgetStandar(1, e.unitEducationLvl);
    this.plafonBosnasService.setDataEdit(e);
    this.plafonBosnasService.showEdit(true);
  }
  btnCetak(type) {
    const paramBody = {};
    paramBody['@stageId'] = +this.userInfo.stages;
    this.reportService.execPrint('PaguBosnas.rpt', type, paramBody)
    .subscribe(resp =>  {
      this.reportService.extractData(resp, type, 'Pagu Bosnas');
    });
  }
  ngOnDestroy() {
   this.plafonBosnasService.setListBosnasLimit([]);
  }
}
