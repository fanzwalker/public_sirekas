import { IbosnasLimit } from './../interface/ibosnas-limit';
import { environment } from './../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IlastYearBalance } from '../interface/ilast-year-balance';

@Injectable()
export class PlafonBosnasService {
  private listBosnasLimit = new BehaviorSubject(<any>[]);
  public listBosnasLimit$ = this.listBosnasLimit.asObservable();
  private editData = new BehaviorSubject(<any>[]);
  public editData$ = this.editData.asObservable();
  private unitId = new BehaviorSubject(<any>[]);
  public unitId$ = this.unitId.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private budgetStandar = new BehaviorSubject(<any>[]);
  public budgetStandar$ = this.budgetStandar.asObservable();
  constructor(private http: HttpClient) { }
  getBosnasLimit(stages) {
    return this.http.get(`${environment.url}stages/${stages}/bosnas-limit`).map(resp => <IbosnasLimit>resp)
    .subscribe(data => this.setListBosnasLimit(data));
  }
  setListBosnasLimit(data) {
    return this.listBosnasLimit.next(data);
  }
  getBudgetStandar(standardTypeId, educationLvl) {
    const queryParam = new HttpParams()
    .set('educationLvl', educationLvl);
    return this.http.get(`${environment.url}standard-types/${standardTypeId}/budget-standards`, {params: queryParam})
    .map(resp => resp).subscribe(data => this.setListBudgetStandar(data[0]));
  }
  setListBudgetStandar(data) {
    return this.budgetStandar.next(data);
  }
  setUnitId(unitId) {
    this.unitId.next(unitId);
  }
  setDataEdit(data) {
    return this.editData.next(data);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
  putTransactBosnasBudgetDesc(unitId, stages, postBody) {
    return this.http.put(`${environment.url}transact/units/${unitId}/stages/${stages}/bosnas-budget-desc`, postBody)
    .map(resp => <any>resp);
  }
}
