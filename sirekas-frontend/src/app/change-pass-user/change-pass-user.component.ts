import { UsersService } from './../service/users.service';
import { Observable } from 'rxjs/Observable';
import { ChangePassUserService } from './change-pass-user.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-change-pass-user',
  templateUrl: './change-pass-user.component.html',
  styleUrls: ['./change-pass-user.component.css']
})
export class ChangePassUserComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  userInfo: any;
  constructor(private service: ChangePassUserService,
              private fb: FormBuilder,
              private user: UsersService) {
    this.userInfo = this.user.getUserInfo();
    this.formEdit = this.fb.group({
      fullName: [''],
      newPassword: ['', Validators.required],
      stages: [0]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayThis$
    );
    combine.subscribe(resp => {
      const [displayThis] = resp;
      this.showThis = displayThis;
      this.formEdit.patchValue({
        fullName: this.userInfo.given_name,
        stages: +this.userInfo.stages
      });
    });
  }
  Simpan() {
    if (this.formEdit.valid) {
      this.service.putUser(2, this.userInfo.sub, this.formEdit.value)
      .subscribe(resp => {
        this.onHide();
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.service.showChangeUser(false);
  }


}
