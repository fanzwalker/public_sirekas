import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ChangePassUserService {
  private displayThis = new BehaviorSubject<boolean>(false);
  public displayThis$ = this.displayThis.asObservable();
  constructor(private http: HttpClient) { }
  showChangeUser(action: boolean) {
    this.displayThis.next(action);
  }
  putUser(roleId, userName, postBody) {
    return this.http.put(`${environment.url}roles/${roleId}/users/${userName}`, postBody)
    .map(resp => resp);
  }
}
