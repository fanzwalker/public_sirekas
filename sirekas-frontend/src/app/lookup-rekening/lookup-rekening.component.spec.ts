import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LookupRekeningComponent } from './lookup-rekening.component';

describe('LookupRekeningComponent', () => {
  let component: LookupRekeningComponent;
  let fixture: ComponentFixture<LookupRekeningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LookupRekeningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LookupRekeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
