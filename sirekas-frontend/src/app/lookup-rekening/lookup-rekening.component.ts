import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {LookupRekeningService} from './lookup-rekening.service';
import {SelectItem} from 'primeng/api';
import * as _ from 'lodash';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Irekening} from '../interface/irekening';
import {RkaService} from '../rencana-kerja-anggaran/rka.service';

@Component({
  selector: 'app-lookup-rekening',
  templateUrl: './lookup-rekening.component.html',
  styleUrls: ['./lookup-rekening.component.css']
})
export class LookupRekeningComponent implements OnInit {
  titleLookup: string;
  display: boolean;
  listRekening: Irekening[];
  rekeningSelected: any = [];
  listSumberDana: any; optSumberDana: SelectItem[];
  formGroups: FormGroup;
  unitId: number;
  activityId: number;
  errors = [];
  constructor(private lookupRekeningService: LookupRekeningService,
              private fb: FormBuilder,
              private rkaService: RkaService) {
    this.formGroups = this.fb.group({
      SumberDana: new FormControl()
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.lookupRekeningService.titleLookup$,
      this.lookupRekeningService.showThis$,
      this.lookupRekeningService.unitId$,
      this.lookupRekeningService.activityId$,
      this.lookupRekeningService.listRekenings$
    );
    combine.subscribe(resp => {
      const [titleLookup, showLookUp, unitId, activityId, listRekenings] = resp;
      this.titleLookup = titleLookup;
      this.display = showLookUp;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listRekening = Object.assign(listRekenings);
      _.forEach(this.listRekening, function(e) {
        e.sumberdana = 1;
        e.jumlah = 0;
      });
      this.getSumberDana();
    });
  }
  onRowClick(e) {

  }
  btnBatal() {
    this.onHide();
  }
  btnSimpan() {
    const paramBody = [];
    if (this.rekeningSelected.length > 0) {
      _.forEach(this.rekeningSelected, function (k) {
        paramBody.push({
          stages : 1,
          chartOfAccountId: k.id,
          budgetSourceId: k.sumberdana,
          value: Number(k.jumlah)
        });
      });
      this.rkaService.postRkaHeader(this.unitId, this.activityId, paramBody)
        .subscribe(resp => {
          this.rekeningSelected = [];
          this.rkaService.getRkaHeader(this.unitId, this.activityId)
            .subscribe(data => this.rkaService.setRkaHeader(data));
          this.onHide();
        }, err => {
          if (err.status === 400) {
            const validationErrDict = err.error;
            for (const fieldName in validationErrDict) {
              if (validationErrDict.hasOwnProperty(fieldName)) {
                this.errors = [...validationErrDict[fieldName]];
              }
            }
          } else {
            this.errors.push('Unexpected error');
          }
        });
    } else {
      console.log('apa lah isi');
    }
  }
  getSumberDana() {
    const temp = [];
    this.lookupRekeningService.getSumberDana()
      .subscribe(resp => {
        this.listSumberDana = resp;
        _.forEach(this.listSumberDana, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSumberDana = temp;
      });
  }
  onChangeSumberDana(e) {
    const sumberDanaId = this.formGroups.value.SumberDana;
    _.find(this.listRekening, function (s) {
      if (s.id === e.id) {
        s.sumberdana = sumberDanaId;
      }
    });
  }
  onHide() {
    this.formGroups.patchValue({SumberDana: 1 });
    this.lookupRekeningService.getRekening(this.unitId, this.activityId);
    this.lookupRekeningService.show(false, '');
  }
}
