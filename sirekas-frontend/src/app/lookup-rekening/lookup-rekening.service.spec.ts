import { TestBed, inject } from '@angular/core/testing';

import { LookupRekeningService } from './lookup-rekening.service';

describe('LookupRekeningService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LookupRekeningService]
    });
  });

  it('should be created', inject([LookupRekeningService], (service: LookupRekeningService) => {
    expect(service).toBeTruthy();
  }));
});
