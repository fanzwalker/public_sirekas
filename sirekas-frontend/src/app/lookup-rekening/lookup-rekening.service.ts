import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class LookupRekeningService {
  private titleLookup = new BehaviorSubject<string>('');
  public titleLookup$ = this.titleLookup.asObservable();
  private showThis = new BehaviorSubject<boolean>(false);
  public showThis$ = this.showThis.asObservable();
  private unitId = new BehaviorSubject<number>(0)
  public unitId$ = this.unitId.asObservable();
  private activityId = new BehaviorSubject<number>(0);
  public activityId$ = this.activityId.asObservable();
  private listRekenings = new BehaviorSubject(<any> []);
  public listRekenings$ = this.listRekenings.asObservable();
  constructor(private http: HttpClient) {}
  show(action: boolean, titleLookup?: string) {
    this.titleLookup.next(titleLookup);
    return this.showThis.next(action);
  }
  getRekening(unitId, activityId, Stages?, ExpensesType?) {
    const Query = {
      Stages: Stages ? Stages : '1',
      ExpensesType: ExpensesType ? ExpensesType : '2'
    };
    const params = new HttpParams()
      .set('Stages', Query['Stages'])
      .set('ExpensesType', Query['ExpensesType']);
    return this.http.get(`${environment.url}units/${unitId}/activities/${activityId}/chart-of-accounts`, {params: params})
      .map(resp => resp).subscribe(data => this.listRekenings.next(data));
  }
  getSumberDana() {
    return this.http.get(`${environment.url}budget-sources`);
  }
  setUnitId(value: number) {
    return this.unitId.next(value);
  }
  setActivityId(value: number) {
    return this.activityId.next(value);
  }
}
