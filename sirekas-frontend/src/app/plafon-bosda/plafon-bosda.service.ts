import { IbosdaLimit } from './../interface/ibosda-limit';
import { environment } from './../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { IlistBudgetUnit } from '../interface/ilist-budget-unit';
import { Iunits } from '../interface/iunits';
import {BudgetType} from '../shared/budget-type';
@Injectable()
export class PlafonBosdaService {
  private listBosdaLimit = new BehaviorSubject(<any>[]);
  public listBosdaLimit$ = this.listBosdaLimit.asObservable();
  private dataEditBosdaLimit = new BehaviorSubject(<any>[]);
  public dataEditBosdaLimit$ = this.dataEditBosdaLimit.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private titleEdit = new BehaviorSubject<string>(null);
  public titleEdit$ = this.titleEdit.asObservable();
  private listUnits = new BehaviorSubject(<any>[]);
  public listUnist$ = this.listUnits.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  constructor(private http: HttpClient) { }
  getBosdaLimit(stageId) {
    return this.http.get(`${environment.url}stages/${stageId}/bosda-limit`)
    .map(resp => <IbosdaLimit>resp).subscribe(data => this.setLilstBosdaLimit(data));
  }
  setLilstBosdaLimit(data) {
    return this.listBosdaLimit.next(data);
  }
  setDataEditBosdaLimit(data) {
    return this.dataEditBosdaLimit.next(<IbosdaLimit>data);
  }
  showEdit(action: boolean, titleForm?: string) {
    this.titleEdit.next(titleForm);
    return this.displayEdit.next(action);
  }
  putBudgetUnit(stageId, activityTypeId, postBody) {
    return this.http.put(`${environment.url}stages/${stageId}/act-types/${activityTypeId}/units`, postBody);
  }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType) {
    const paramQuery = new HttpParams()
      .set('Stages', Stages)
      .set('ExcludeType', ExcludeType)
      .set('UnitStructure', UnitStructure)
      .set('ActivityType', ActivityType)
      .set('BudgetType', BudgetType.BOSDA);
    return this.http.get(`${environment.url}units`, {params: paramQuery})
    .map(resp => <Iunits>resp).subscribe(data => this.setListUnits(data));
  }
  setListUnits(data) {
    return this.listUnits.next(data);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  postBudgetUnit(stageId, activityTypeId, postBody) {
    return this.http.post(`${environment.url}stages/${stageId}/act-types/${activityTypeId}/units`, postBody);
  }
  deleteBudgetUnits(stageId, activityTypeId, postBody) {
    return this.http.request('DELETE', `${environment.url}stages/${stageId}/act-types/${activityTypeId}/units`,
      {body: postBody});
  }
}
