import { UsersService } from './../../service/users.service';
import { Observable } from 'rxjs/Observable';
import { PlafonBosdaService } from './../plafon-bosda.service';
import { Component, OnInit } from '@angular/core';
import { Iunits } from '../../interface/iunits';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as _ from 'lodash';
import swal from 'sweetalert2';
@Component({
  selector: 'app-add-plafon-bosda',
  templateUrl: './add-plafon-bosda.component.html',
  styleUrls: ['./add-plafon-bosda.component.css']
})
export class AddPlafonBosdaComponent implements OnInit {
  showThis: boolean;
  listUnist: Iunits[];
  formAdd: FormGroup;
  unitSelected: any = [];
  userInfo: any;
  constructor(private plafonBosdaService: PlafonBosdaService,
              private userService: UsersService,
              private fb: FormBuilder) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonBosdaService.displayAdd$,
      this.plafonBosdaService.listUnist$
    );
    combine.subscribe(resp => {
      const [displayAdd, listUnit] = resp;
      this.showThis = displayAdd;
      this.listUnist = Object.assign(listUnit);
      _.forEach(this.listUnist, (e) => {
        e.jumlah = 0;
      });
    });
  }
  btnSimpan() {
    const paramBody = [];
    if (this.unitSelected.length > 0) {
      _.forEach(this.unitSelected, (k) => {
        paramBody.push({
          unitId: k.id,
          total: +k.jumlah
        });
      });
      this.plafonBosdaService.postBudgetUnit(this.userInfo.stages, 1, paramBody)
        .subscribe(resp => {
          if (resp) {
            this.plafonBosdaService.showAdd(false);
            // this.plafonBosdaService.getBudgetUnits(1, 1);
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Data Berhasil Disimpan',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }, error => {
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Data Gagal Disimpan' + error.statusText,
            showConfirmButton: false,
            timer: 3000
          });
        });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnBatal() {
    this.onHide();
  }
  onHide() {
    this.unitSelected = [];
    // this.plafonBosdaService.getBudgetUnits(1, 1);
    this.plafonBosdaService.showAdd(false);
  }
}
