import { environment } from './../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class EditPlafonBosdaService {

  constructor(private http: HttpClient) { }
  getBudgetStandard(standardTypeId, educationLvl) {
    const queryParam = new HttpParams()
    .set('educationLvl', educationLvl);
    return this.http.get(`${environment.url}standard-types/${standardTypeId}/budget-standards`, {params: queryParam})
    .map(resp => <any[]>resp);
  }
}
