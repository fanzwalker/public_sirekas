import { UsersService } from './../../service/users.service';
import { EditPlafonBosdaService } from './edit-plafon-bosda.service';
import { IlistBudgetUnit } from './../../interface/ilist-budget-unit';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { PlafonBosdaService } from './../plafon-bosda.service';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { IbosdaLimit } from '../../interface/ibosda-limit';
@Component({
  selector: 'app-edit-plafon-bosda',
  templateUrl: './edit-plafon-bosda.component.html',
  styleUrls: ['./edit-plafon-bosda.component.css']
})
export class EditPlafonBosdaComponent implements OnInit {
  showThis: boolean;
  titleForm: string;
  dataEdit: IbosdaLimit;
  formEdit: FormGroup;
  listPerUnit: any[];
  userInfo: any;
  constructor(private plafonBosdaService: PlafonBosdaService,
              private service: EditPlafonBosdaService,
              private userService: UsersService,
              private fb: FormBuilder) {
    this.userInfo = this.userService.getUserInfo();
    this.formEdit = this.fb.group({
      unitName: [null, Validators.required],
      unitId: [null, Validators.required],
      studentNumber: [0, Validators.required],
      rombel: [0, Validators.required],
      extraPrice: [0]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonBosdaService.displayEdit$,
      this.plafonBosdaService.titleEdit$,
      this.plafonBosdaService.dataEditBosdaLimit$
    );
    combine.subscribe(resp => {
      const [displayEdit, titleEdit, dataEdit] = resp;
      this.showThis = displayEdit;
      this.titleForm = titleEdit;
      this.dataEdit = Object.assign(dataEdit);
      if (this.dataEdit.unitId) {
        this.formEdit.patchValue({
          unitName: this.dataEdit.unitName,
          unitId: this.dataEdit.unitId,
          studentNumber: this.dataEdit.studentNumber,
          rombel: this.dataEdit.rombel,
          perUnitPrice: this.dataEdit.perUnitPrice,
          extraPrice: this.dataEdit.extraPrice
        });
        this.getPerSiswa();
        this.getPerRombel();
        this.getPerUnit();
      }
    });
  }
  onTypeStudent() {
    if (this.listPerUnit.length > 0) {
      if (this.formEdit.value.studentNumber <= 168) {
        this.formEdit.patchValue({
          perUnitPrice: this.listPerUnit[0].value
        });
      } else if (this.formEdit.value.studentNumber <= 337) {
        this.formEdit.patchValue({
          perUnitPrice: this.listPerUnit[1].value
        });
      } else {
        this.formEdit.patchValue({
          perUnitPrice: this.listPerUnit[2].value
        });
      }
    } else {
      this.formEdit.patchValue({
        perUnitPrice: 0
      });
    }
  }
  getPerSiswa() {
    this.service.getBudgetStandard(2, this.dataEdit.unitEducationLvl)
    .subscribe(resp => {
      if (resp.length > 0) {
        this.formEdit.patchValue({
          perStudentPrice: resp[0].value
        });
      }
    });
  }
  getPerRombel() {
    this.service.getBudgetStandard(3, this.dataEdit.unitEducationLvl)
    .subscribe(resp => {
      if (resp.length > 0) {
        this.formEdit.patchValue({
          perRombelPrice: resp[0].value
        });
      }
    });
  }
  getPerUnit() {
    this.service.getBudgetStandard(4, this.dataEdit.unitEducationLvl)
    .subscribe(resp => {
      this.listPerUnit = Object.assign(resp);
    });
  }
  btnUpdate() {
    if (this.formEdit.valid) {
      const postBody = {
        unitId: this.formEdit.value.unitId,
        studentNumber: +this.formEdit.value.studentNumber,
        perStudentPrice: +this.formEdit.value.perStudentPrice,
        rombel: +this.formEdit.value.rombel,
        perRombelPrice: +this.formEdit.value.perRombelPrice,
        perUnitPrice: +this.formEdit.value.perUnitPrice,
        extraPrice: +this.formEdit.value.extraPrice
      };
      this.plafonBosdaService.putBudgetUnit(this.userInfo.stages, 1, postBody)
      .subscribe(resp =>  {
        this.plafonBosdaService.getBosdaLimit(this.userInfo.stages);
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data berhasil diubah',
          showConfirmButton: false,
          timer: 3000
        });
      }, error => {
        console.log(error);
      });
    }

  }
  btnCancel() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.plafonBosdaService.setDataEditBosdaLimit([]);
    this.plafonBosdaService.showEdit(false, '');
  }
}
