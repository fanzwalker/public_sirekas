import { IbosdaLimit } from './../interface/ibosda-limit';
import { getTestBed } from '@angular/core/testing';
import { animationPage } from './../shared/animation-page';
import { Observable } from 'rxjs/Observable';
import { PlafonBosdaService } from './plafon-bosda.service';
import { Component, OnInit } from '@angular/core';
import { IlistBudgetUnit } from '../interface/ilist-budget-unit';
import swal from 'sweetalert2';
import * as _ from 'lodash';
import { UsersService } from '../service/users.service';
import { MenuItem } from 'primeng/primeng';
import { ReportService } from '../service/report.service';
@Component({
  selector: 'app-plafon-bosda',
  templateUrl: './plafon-bosda.component.html',
  styleUrls: ['./plafon-bosda.component.css'],
  animations: [animationPage]
})
export class PlafonBosdaComponent implements OnInit {
  listBosdaLimit: IbosdaLimit[];
  budgetUnitSelected: any = [];
  userInfo: any;
  itemPrints: MenuItem[];
  constructor(private plafonBosdaService: PlafonBosdaService,
              private reportService: ReportService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonBosdaService.listBosdaLimit$
    );
    combine.subscribe(resp => {
      const [listBosdaLimit] = resp;
      this.listBosdaLimit = Object.assign(listBosdaLimit);
    });
    this.getBosdaLimit();
  }
  getBosdaLimit() {
    this.plafonBosdaService.getBosdaLimit(this.userInfo.stages);
  }
  formEdit(e) {
    this.plafonBosdaService.setDataEditBosdaLimit(e);
    this.plafonBosdaService.showEdit(true);
  }
  calculateTotal(x: string) {
    let total = 0;
    switch (x) {
      case 'totalPerStudentPrice' :
        _.forEach(this.listBosdaLimit, (s) => {
          total += s.totalPerStudentPrice;
        });
      break;
      case 'totalPerRombelPrice' :
        _.forEach(this.listBosdaLimit, (s) => {
          total += s.totalPerRombelPrice;
        });
      break;
      case 'total' :
        _.forEach(this.listBosdaLimit, (s) => {
          total += s.total;
        });
      break;
    }
    return total;
  }
  btnAdd() {
    this.plafonBosdaService.getUnits(this.userInfo.stages, 1, 4, 1);
    this.plafonBosdaService.showAdd(true);
  }
  btnCetak(type) {
    const paramBody = {};
    paramBody['@stageId'] = +this.userInfo.stages;
    this.reportService.execPrint('PaguBosda.rpt', type, paramBody)
    .subscribe(resp =>  {
      this.reportService.extractData(resp, type, 'Pagu Bosda');
    });
  }
}
