export interface IlistRkaPreview {
  type: number;
  coaCode: string;
  description: string;
  expression: null;
  amount: null;
  unitDesc: null;
  costPerUnit: null;
  q1: null;
  q2: null;
  q3: null;
  q4: null;
  total: number;
}
