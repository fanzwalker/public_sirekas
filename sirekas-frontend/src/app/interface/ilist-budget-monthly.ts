export interface IlistBudgetMonthly {
  id: number;
  budgetDetId: number;
  code: string;
  description: string;
  activityId: number;
  chartOfAccountId: number;
  chartOfAccountCode: string;
  chartOfAccountName: string;
  unitId: number;
  total: number;
  q1: number;
  q2: number;
  q3: number;
  q4: number;
  type: number;
}
