export interface IlistProgram {
  id: number;
  code: string;
  name: string;
  unitId: number;
}
