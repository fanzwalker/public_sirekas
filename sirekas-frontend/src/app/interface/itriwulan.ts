export interface Itriwulan {
  CoaLevel: number;
  Type: string;
  Code: string;
  Name: string;
  Total: number;
  Q1: number;
  Q2: number;
  Q3: number;
  Q4: number;
}
