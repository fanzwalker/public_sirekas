export interface IpostBosdaBudgetDet {
  headerId: number;
  subHeaderId: number;
  chartOfAccountId: number
  description: string;
  expression: string;
  unitDesc: string;
  costPerUnit: number;
}
