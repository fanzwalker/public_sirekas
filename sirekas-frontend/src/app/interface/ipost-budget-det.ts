export interface IpostBudgetDet {
  parentId: number;
  chartOfAccountId: number;
  budgetSourceId: number;
  code: string;
  description: string;
  expression: string;
  unitDesc: string;
  costPerUnit: number;
  type: number;
}
