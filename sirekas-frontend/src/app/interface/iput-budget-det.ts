export interface IputBudgetDet {
  budgetSourceId: number;
  description: string;
  expression: string;
  unitDesc: string;
  costPerUnit: number;
  q1: number;
  q2: number;
  q3: number;
  q4: number;
  type: number;
}
