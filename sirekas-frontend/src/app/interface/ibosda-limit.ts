export interface IbosdaLimit {
  unitId: number;
  unitCode: string;
  unitName: string;
  unitEducationLvl: number;
  activityId: number;
  activityCode: string;
  activityName: string;
  stages: number;
  studentNumber: number;
  perStudentPrice: number;
  totalPerStudentPrice: number;
  rombel: number;
  perRombelPrice: number;
  totalPerRombelPrice: number;
  perUnitPrice: number;
  extraPrice: number;
  total: number;
}
