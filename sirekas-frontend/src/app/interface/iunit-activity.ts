export interface IunitActivity {
  unitId: number;
  unitCode: string;
  unitName: string;
  activityId: number;
  activityCode: string;
  activityName: string;
  stages: number;
  total: number;
}
