export interface IlistCoa {
  id: number;
  code: string;
  name: string;
  coaLevel: number;
  type: string;
}
