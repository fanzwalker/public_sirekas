export interface IlistWebOption {
  name: string;
  value: string;
  description: string;
}
