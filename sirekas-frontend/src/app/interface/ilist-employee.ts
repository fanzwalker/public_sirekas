export interface IlistEmployee {
  id: number;
  nip: string;
  name: string;
  unitId: number;
  unitName: string;
  jobTitleId: number;
  jobTitleName: string;
  jobDesc: string;
  signer: number;
}
