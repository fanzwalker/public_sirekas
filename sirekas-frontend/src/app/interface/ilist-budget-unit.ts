export interface IlistBudgetUnit {
  unitId: number;
  unitCode: string;
  unitName: string;
  activityId: number;
  activityCode: string;
  activityName: string;
  stages: number;
  total: number;
}
