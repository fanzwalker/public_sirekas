export interface IlistUser {
  id: number;
  userName: string;
  fullName: string;
  stages: number;
  unitId: null;
}
