export interface IndikatorKinerja {
  id: number;
  perfBenchmarkType: number;
  name: string;
  target: string;
  indikator: string;
}
