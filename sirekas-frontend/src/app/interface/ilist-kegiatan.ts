export interface IlistKegiatan {
  code: string;
  id: number;
  name: string;
  programId: number;
}
