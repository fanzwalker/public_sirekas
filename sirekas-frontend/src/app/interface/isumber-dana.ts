export interface IsumberDana {
  id: number;
  parentId: null;
  code: string;
  name: string;
  type: number;
}
