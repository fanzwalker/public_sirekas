export interface Irekening {
  coaLevel: number;
  code: string;
  id: number;
  name: string;
  type: string;
  sumberdana: number;
  jumlah: number;
}
