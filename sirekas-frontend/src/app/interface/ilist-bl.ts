export interface IlistBl {
  Type: string;
  Code: string;
  Desc: string;
  BudgetSrc: string;
  Total: number;
  DetailDesc: string;
  Amount: number;
  CostPerUnit: number;
}
