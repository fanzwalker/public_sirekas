export interface IlistUnits {
  id: number;
  code: string;
  name: string;
  level: number;
  type: string;
  budgetType: number;
  districtId: number;
  districtName: null,
  educationLvl: number;
  npsn: string;
}
