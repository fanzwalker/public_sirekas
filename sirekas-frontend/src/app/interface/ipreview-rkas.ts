export interface IpreviewRkas {
  type: number;
  coaCode: string;
  description: string;
  expression: string;
  amount: number;
  unitDesc: string;
  costPerUnit: number;
  q1: number;
  q2: number;
  q3: number;
  q4: number;
  total: number;
}
