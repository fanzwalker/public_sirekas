export interface IlistUnitBudgets {
  id: number;
  unitId: number;
  unitName: string;
  stages: number;
  total: number;
}
