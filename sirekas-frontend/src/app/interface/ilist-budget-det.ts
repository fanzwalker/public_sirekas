export interface IlistBudgetDet {
  activityId: number;
  amount: number;
  budgetSourceId: number;
  budgetSourceName: string;
  chartOfAccountCode: string;
  chartOfAccountId: number;
  chartOfAccountName: string;
  code: string;
  costPerUnit: number;
  description: string;
  expression: string;
  id: number;
  parentId: number;
  stage: number;
  total: number;
  type: number;
  unitDesc: string;
  unitId: number;
}
