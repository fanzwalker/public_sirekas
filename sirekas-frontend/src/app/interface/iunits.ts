export interface Iunits {
  id: number;
  level: number;
  code: string;
  name: string;
  type: string;
  jumlah: number;
  budgetType: number;
}
