export interface IunitProgAct {
  Activity: {
    activityCode: string;
    activityId: number;
    activityName: string;
    stages: number;
    total: number;
    unitCode: string;
    unitId: number;
    unitName: string;
  };
  Programs: {
    code: string;
    id: number;
    name: string;
    unitId: number;
  };
  Units: {
    code: string;
    id: number;
    level: number;
    name: string;
    type: string;
  };
}
