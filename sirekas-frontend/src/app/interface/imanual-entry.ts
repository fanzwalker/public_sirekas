export interface ImanualEntry {
  unitId: number;
  category: string;
  stages: number;
  coaApbsCode: string;
  coaApbsName: string;
  coaApbsId: number;
  id: number;
  total: number;
}
