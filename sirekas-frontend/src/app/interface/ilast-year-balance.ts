export interface IlastYearBalance {
  id: number;
  unit: Units;
  unitId: number;
  value: number;
}
interface Units {
  level: number;
  code: string;
  type: string;
  budgetType: number;
  budgetDets: null;
  budgetMonthlies: null;
  unitActivities: null;
  id: number;
  name: string;
}
