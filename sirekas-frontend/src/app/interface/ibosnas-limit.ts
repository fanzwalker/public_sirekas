export interface IbosnasLimit {
  unitId: number;
  unitCode: string;
  unitName: string;
  unitEducationLvl: number;
  totalStudent: number;
  standardPrice: number;
  total: number;
}

