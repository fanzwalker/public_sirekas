import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class PlafonBtlService {
  private listUnitBudget = new BehaviorSubject(<any>[]);
  public listUnitBudget$ = this.listUnitBudget.asObservable();
  constructor(private http: HttpClient) {}
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getUnitbudgets(unitId, stages) {
    return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/unit-budgets`)
    .map(resp => resp).subscribe(data => this.setListUnitBudgets(data));
  }
  setListUnitBudgets(data) {
    return this.listUnitBudget.next(data);
  }
  putUnitBudgets(unitId, stages, paramBody) {
    return this.http.put(`${environment.url}units/${unitId}/stages/${stages}/unit-budgets`, paramBody);
  }

}
