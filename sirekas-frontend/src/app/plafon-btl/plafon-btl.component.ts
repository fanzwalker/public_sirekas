import { SelectItem } from 'primeng/primeng';
import { PlafonBtlService } from './plafon-btl.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { animationPage } from '../shared/animation-page';
import { IlistUnitBudgets } from '../interface/ilist-unit-budgets';
import { UsersService } from '../service/users.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as _ from 'lodash';
@Component({
  selector: 'app-plafon-btl',
  templateUrl: './plafon-btl.component.html',
  styleUrls: ['./plafon-btl.component.css'],
  animations: [animationPage]
})
export class PlafonBtlComponent implements OnInit, OnDestroy {
  listUnitBudget: IlistUnitBudgets[];
  valueDefault = [
    {
    uraian : 'Total Belanja Tidak Langusung',
    total : 0
  }];
  userInfo: any;
  formFilter: FormGroup;
  listSekolah: any; optSekolah: SelectItem[];
  constructor(private plafonBtlService: PlafonBtlService,
              private fb: FormBuilder,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah : [null, Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.plafonBtlService.listUnitBudget$
    );
    combine.subscribe(resp => {
      const [listUnitBudget] = resp;
      this.listUnitBudget = Object.assign(listUnitBudget);
      if (this.listUnitBudget.length > 0) {
        this.valueDefault[0].total = this.listUnitBudget[0].total;
      } else {
        this.valueDefault[0].total = 0;
      }
    });
    if (this.userInfo.unitId) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.onChangeUnit();
    }
    this.getSekolah();
  }
  getSekolah() {
    const temp = [];
    this.plafonBtlService.getUnits(this.userInfo.stages, 0, 4, 1, 3)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  onChangeUnit() {
    this.plafonBtlService.getUnitbudgets(this.formFilter.value.sekolah, this.userInfo.stages);
  }
  btnUpdate() {
    const paramBody = {
      total : +this.valueDefault[0].total
    };
    this.plafonBtlService.putUnitBudgets(this.formFilter.value.sekolah, this.userInfo.stages, paramBody)
    .subscribe(resp => {
      if (resp) {
        this.plafonBtlService.getUnitbudgets(this.formFilter.value.sekolah, this.userInfo.stages);
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data Berhasil Diubah',
          showConfirmButton: false,
          timer: 3000
        });
      }
    });
  }
  ngOnDestroy() {
    this.valueDefault[0].total = 0;
  }
}
