import { LoadersService } from './service/loaders.service';
import { LoaderInterceptService } from './service/loader-intercept.service';
import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {AppRoutes} from './app-routes';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HomePageComponent } from './home-page/home-page.component';
import { HeaderComponent } from './layout/header/header.component';
import { SidebarComponent } from './Layout/sidebar/sidebar.component';
import { FooterComponent } from './Layout/footer/footer.component';
import { ControlSidebarComponent } from './Layout/control-sidebar/control-sidebar.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { BlankComponent } from './blank/blank.component';
import { TesPageComponent } from './tes-page/tes-page.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  AutoCompleteModule, CheckboxModule, DataTableModule, DropdownModule, FieldsetModule, MessagesModule, RadioButtonModule, SplitButtonModule,
  TabViewModule,
  KeyFilterModule,
  ChartModule
} from 'primeng/primeng';
import {TesService} from './tes-page/tes.service';
import { RencanaKerjaAnggaranComponent } from './rencana-kerja-anggaran/rencana-kerja-anggaran.component';
import {RkaService} from './rencana-kerja-anggaran/rka.service';
import { IndikatorKinerjaComponent } from './rencana-kerja-anggaran/indikator-kinerja/indikator-kinerja.component';
import { PreviewComponent } from './rencana-kerja-anggaran/preview/preview.component';
import {ButtonModule} from 'primeng/button';
import {GlobalService} from './global.service';
import { LookupRekeningComponent } from './lookup-rekening/lookup-rekening.component';
import {DialogModule} from 'primeng/dialog';
import {LookupRekeningService} from './lookup-rekening/lookup-rekening.service';
import {MessageService} from 'primeng/components/common/messageservice';
import {GrowlModule} from 'primeng/growl';
import { PlafonAnggaranComponent } from './plafon-anggaran/plafon-anggaran.component';
import { BelanjaLangsungComponent } from './plafon-anggaran/belanja-langsung/belanja-langsung.component';
import { BelanjaTidakLangsungComponent } from './plafon-anggaran/belanja-tidak-langsung/belanja-tidak-langsung.component';
import {PlafonAnggaranService} from './plafon-anggaran/plafon-anggaran.service';
import { CurrencyRupiahPipe } from './shared/currency-rupiah.pipe';
import {MessageModule} from 'primeng/message';
import { LookupActivityComponent } from './plafon-anggaran/belanja-langsung/lookup-activity/lookup-activity.component';
import { EditUnitActivityComponent } from './plafon-anggaran/belanja-langsung/edit-unit-activity/edit-unit-activity.component';
import { KompPenyusunComponent } from './rencana-kerja-anggaran/komp-penyusun/komp-penyusun.component';
import {KomPenyusunService} from './rencana-kerja-anggaran/komp-penyusun/kom-penyusun.service';
import { AddRincianBelanjaComponent } from './rencana-kerja-anggaran/komp-penyusun/add-rincian-belanja/add-rincian-belanja.component';
import { EditRincianBelanjaComponent } from './rencana-kerja-anggaran/komp-penyusun/edit-rincian-belanja/edit-rincian-belanja.component';
import {UsersService} from './service/users.service';
import {JwtModule} from '@auth0/angular-jwt';
import {environment} from '../environments/environment';
import {AuthGuardService} from './service/auth-guard.service';
import {LoadingModule} from 'ngx-loading';
import { AnggaranKasComponent } from './rencana-kerja-anggaran/anggaran-kas/anggaran-kas.component';
import {AnggaranKasService} from './rencana-kerja-anggaran/anggaran-kas/anggaran-kas.service';
import { EditAnggaranKasComponent } from './rencana-kerja-anggaran/anggaran-kas/edit-anggaran-kas/edit-anggaran-kas.component';
import { TableModule} from 'primeng/table';
import {PreviewService} from './rencana-kerja-anggaran/preview/preview.service';
import {IndikatorKinerjaService} from './rencana-kerja-anggaran/indikator-kinerja/indikator-kinerja.service';
import {NgProgressModule} from '@ngx-progressbar/core';
import {NgProgressRouterModule} from '@ngx-progressbar/router';
import {ReportService} from './service/report.service';
import {registerLocaleData} from '@angular/common';
import localId from '@angular/common/locales/id';
import {PegawaiComponent} from './pegawai/pegawai.component';
import {JabatanComponent} from './pegawai/jabatan/jabatan.component';
import {ListPegawaiComponent} from './pegawai/list-pegawai/list-pegawai.component';
import {JabatanService} from './pegawai/jabatan/jabatan.service';
import {EmployeeService} from './pegawai/employee.service';
import { TambahPegawaiComponent } from './pegawai/list-pegawai/tambah-pegawai/tambah-pegawai.component';
import { UbahPegawaiComponent } from './pegawai/list-pegawai/ubah-pegawai/ubah-pegawai.component';
import { UserAppComponent } from './user-app/user-app.component';
import {UserAppService} from './user-app/user-app.service';
import { AddUserComponent } from './user-app/add-user/add-user.component';
import { EditUserComponent } from './user-app/edit-user/edit-user.component';
import { UnitsComponent } from './units/units.component';
import { UnitsService } from './units/units.service';
import { AddUnitsComponent } from './units/add-units/add-units.component';
import { EditUnitsComponent } from './units/edit-units/edit-units.component';
import { SatuanComponent } from './satuan/satuan.component';
import { AddSatuanComponent } from './satuan/add-satuan/add-satuan.component';
import { EditSatuanComponent } from './satuan/edit-satuan/edit-satuan.component';
import { SatuanService } from './satuan/satuan.service';
import { ProgramKegiatanComponent } from './program-kegiatan/program-kegiatan.component';
import { ProgramComponent } from './program-kegiatan/program/program.component';
import { KegiatanComponent } from './program-kegiatan/kegiatan/kegiatan.component';
import { ProgKegService } from './program-kegiatan/prog-keg.service';
import { ProgramService } from './program-kegiatan/program/program.service';
import { KegiatanService } from './program-kegiatan/kegiatan/kegiatan.service';
import { AddProgramComponent } from './program-kegiatan/program/add-program/add-program.component';
import { EditProgramComponent } from './program-kegiatan/program/edit-program/edit-program.component';
import { AddKegiatanComponent } from './program-kegiatan/kegiatan/add-kegiatan/add-kegiatan.component';
import { EditKegiatanComponent } from './program-kegiatan/kegiatan/edit-kegiatan/edit-kegiatan.component';
import { BelanjaTidakLangsungService } from './plafon-anggaran/belanja-tidak-langsung/belanja-tidak-langsung.service';
import { WebOptionComponent } from './web-option/web-option.component';
import { WebOptionService } from './web-option/web-option.service';
import { AddOptionComponent } from './web-option/add-option/add-option.component';
import { EditOptionComponent } from './web-option/edit-option/edit-option.component';
import { InputTextareaModule } from 'primeng/components/inputtextarea/inputtextarea';
import { ApbsBelanjaTidakLangsungComponent } from './apbs-belanja-tidak-langsung/apbs-belanja-tidak-langsung.component';
import { BtlService } from './apbs-belanja-tidak-langsung/btl.service';
import { ListBtlComponent } from './apbs-belanja-tidak-langsung/list-btl/list-btl.component';
import { ApbsBelanjaLangsungComponent } from './apbs-belanja-langsung/apbs-belanja-langsung.component';
import { BlService } from './apbs-belanja-langsung/bl.service';
import { ListBlComponent } from './apbs-belanja-langsung/list-bl/list-bl.component';
import { SumberDanaComponent } from './sumber-dana/sumber-dana.component';
import { SumberDanaService } from './sumber-dana/sumber-dana.service';
import { ListSumberDanaComponent } from './sumber-dana/list-sumber-dana/list-sumber-dana.component';
import { FormAddComponent } from './sumber-dana/form-add/form-add.component';
import { FormAddHeaderComponent } from './sumber-dana/form-add-header/form-add-header.component';
import { FormEditComponent } from './sumber-dana/form-edit/form-edit.component';
import { ApbsSumberDanaComponent } from './apbs-sumber-dana/apbs-sumber-dana.component';
import { SdService } from './apbs-sumber-dana/sd.service';
import { ListSdComponent } from './apbs-sumber-dana/list-sd/list-sd.component';
import { PlafonBosdaComponent } from './plafon-bosda/plafon-bosda.component';
import { PlafonBosdaService } from './plafon-bosda/plafon-bosda.service';
import { EditPlafonBosdaComponent } from './plafon-bosda/edit-plafon-bosda/edit-plafon-bosda.component';
import { AddPlafonBosdaComponent } from './plafon-bosda/add-plafon-bosda/add-plafon-bosda.component';
import { PlafonBtlComponent } from './plafon-btl/plafon-btl.component';
import { PlafonBtlService } from './plafon-btl/plafon-btl.service';
import { StandarHargaComponent } from './standar-harga/standar-harga.component';
import { StandarHargaService } from './standar-harga/standar-harga.service';
import { PlafonBosnasComponent } from './plafon-bosnas/plafon-bosnas.component';
import { PlafonBosnasService } from './plafon-bosnas/plafon-bosnas.service';
import { EditPlafonBosnasComponent } from './plafon-bosnas/edit-plafon-bosnas/edit-plafon-bosnas.component';
import { DashBosnasComponent } from './dash-bosnas/dash-bosnas.component';
import { DashBosnasService } from './dash-bosnas/dash-bosnas.service';
import { DashBosdaComponent } from './dash-bosda/dash-bosda.component';
import { DashBosdaService } from './dash-bosda/dash-bosda.service';
import { DashBtlComponent } from './dash-btl/dash-btl.component';
import { DashBtlService } from './dash-btl/dash-btl.service';
import { RekapBelanjaLangsungComponent } from './rekap-belanja-langsung/rekap-belanja-langsung.component';
import { RekapBelanjaLangsungService } from './rekap-belanja-langsung/rekap-belanja-langsung.service';
import { SheditComponent } from './standar-harga/shedit/shedit.component';
import { SaldoAwalComponent } from './saldo-awal/saldo-awal.component';
import { SaldoAwalService } from './saldo-awal/saldo-awal.service';
import { EditSaldoAwalComponent } from './saldo-awal/edit-saldo-awal/edit-saldo-awal.component';
import { DashBosdaTriwulanComponent } from './dash-bosda-triwulan/dash-bosda-triwulan.component';
import { DashBosdaTriwulanService } from './dash-bosda-triwulan/dash-bosda-triwulan.service';
import { DashBosnasTriwulanComponent } from './dash-bosnas-triwulan/dash-bosnas-triwulan.component';
import { DashBosnasTriwulanService } from './dash-bosnas-triwulan/dash-bosnas-triwulan.service';
import { RkaBosnasComponent } from './rka-bosnas/rka-bosnas.component';
import { RkaBosnasService } from './rka-bosnas/rka-bosnas.service';
import { BosnasKompPenyusunComponent } from './rka-bosnas/bosnas-komp-penyusun/bosnas-komp-penyusun.component';
import { BosnasKompPenyusunService } from './rka-bosnas/bosnas-komp-penyusun/bosnas-komp-penyusun.service';
import { BosnasAnggaranKasComponent } from './rka-bosnas/bosnas-anggaran-kas/bosnas-anggaran-kas.component';
import { BosnasAnggaranKasService } from './rka-bosnas/bosnas-anggaran-kas/bosnas-anggaran-kas.service';
import { BosnasPreviewComponent } from './rka-bosnas/bosnas-preview/bosnas-preview.component';
import { BosnasPreviewService } from './rka-bosnas/bosnas-preview/bosnas-preview.service';
import { AddKompPenyusunComponent } from './rka-bosnas/bosnas-komp-penyusun/add-komp-penyusun/add-komp-penyusun.component';
import { EditKompPenyusunComponent } from './rka-bosnas/bosnas-komp-penyusun/edit-komp-penyusun/edit-komp-penyusun.component';
import { BonasAnggaranKasEditComponent } from './rka-bosnas/bosnas-anggaran-kas/bonas-anggaran-kas-edit/bonas-anggaran-kas-edit.component';
import { RekeningBosdaComponent } from './rekening-bosda/rekening-bosda.component';
import { RekeningBosdaService } from './rekening-bosda/rekening-bosda.service';
import { AddRekeningBosdaComponent } from './rekening-bosda/add-rekening-bosda/add-rekening-bosda.component';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CurrencyMaskConfig, CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { MappingHeaderBosdaComponent } from './rekening-bosda/mapping-header-bosda/mapping-header-bosda.component';
import { MappingHeaderBosdaService } from './rekening-bosda/mapping-header-bosda/mapping-header-bosda.service';
import { AddMappingHeaderComponent } from './rekening-bosda/mapping-header-bosda/add-mapping-header/add-mapping-header.component';
import { IndikatorKinerjaKegiatanComponent } from './indikator-kinerja-kegiatan/indikator-kinerja-kegiatan.component';
import { IndikatorKinerjaKegiatanService } from './indikator-kinerja-kegiatan/indikator-kinerja-kegiatan.service';
import { EditIndikatorComponent } from './indikator-kinerja-kegiatan/edit-indikator/edit-indikator.component';
import { EditPlafonBosdaService } from './plafon-bosda/edit-plafon-bosda/edit-plafon-bosda.service';
import { RkaBosdaComponent } from './rka-bosda/rka-bosda.component';
import { RkaBosdaService } from './rka-bosda/rka-bosda.service';
import { BosdaAnggaranKasComponent } from './rka-bosda/bosda-anggaran-kas/bosda-anggaran-kas.component';
import { BosdaKompPenyusunComponent } from './rka-bosda/bosda-komp-penyusun/bosda-komp-penyusun.component';
import { BosdaPreviewComponent } from './rka-bosda/bosda-preview/bosda-preview.component';
import { AddKompPenyusunBosdaComponent } from './rka-bosda/bosda-komp-penyusun/add-komp-penyusun-bosda/add-komp-penyusun-bosda.component';
import { EditKompPenyusunBosdaComponent } from './rka-bosda/bosda-komp-penyusun/edit-komp-penyusun-bosda/edit-komp-penyusun-bosda.component';
import { BosdaKompPenyusunService } from './rka-bosda/bosda-komp-penyusun/bosda-komp-penyusun.service';
import { RkaBtlComponent } from './rka-btl/rka-btl.component';
import { RkaBtlService } from './rka-btl/rka-btl.service';
import { BtlKompPenyusunComponent } from './rka-btl/btl-komp-penyusun/btl-komp-penyusun.component';
import { BtlPreviewComponent } from './rka-btl/btl-preview/btl-preview.component';
import { BtlAnggaranKasComponent } from './rka-btl/btl-anggaran-kas/btl-anggaran-kas.component';
import { BtlAnggaranKasService } from './rka-btl/btl-anggaran-kas/btl-anggaran-kas.service';
import { BtlKompPenyusunService } from './rka-btl/btl-komp-penyusun/btl-komp-penyusun.service';
import { BtlPreviewService } from './rka-btl/btl-preview/btl-preview.service';
import { AddRincianBtlComponent } from './rka-btl/btl-komp-penyusun/add-rincian-btl/add-rincian-btl.component';
import { EditRincianBtlComponent } from './rka-btl/btl-komp-penyusun/edit-rincian-btl/edit-rincian-btl.component';
import { EditAnggaranKasBtlComponent } from './rka-btl/btl-anggaran-kas/edit-anggaran-kas-btl/edit-anggaran-kas-btl.component';
import { BosdaAnggaranKasService } from './rka-bosda/bosda-anggaran-kas/bosda-anggaran-kas.service';
import { BosdaPreviewService } from './rka-bosda/bosda-preview/bosda-preview.service';
import { BosdaAnggaranKasEditComponent } from './rka-bosda/bosda-anggaran-kas/bosda-anggaran-kas-edit/bosda-anggaran-kas-edit.component';
import { IndikatorKenerjaGlobalService } from './shared/indikator-kenerja-global.service';
import { VisiMisiComponent } from './visi-misi/visi-misi.component';
import { VisiMisiService } from './visi-misi/visi-misi.service';
import { VisiComponent } from './visi-misi/visi/visi.component';
import { MisiComponent } from './visi-misi/misi/misi.component';
import { TujuanComponent } from './visi-misi/tujuan/tujuan.component';
import { AddVisiMisiComponent } from './visi-misi/add-visi-misi/add-visi-misi.component';
import { EditVisiMisiComponent } from './visi-misi/edit-visi-misi/edit-visi-misi.component';
import { DataUmumComponent } from './data-umum/data-umum.component';
import { DataSiswaComponent } from './data-umum/data-siswa/data-siswa.component';
import { DataUmumService } from './data-umum/data-umum.service';
import { DataSiswaService } from './data-umum/data-siswa/data-siswa.service';
import { AddDataSiswaComponent } from './data-umum/data-siswa/add-data-siswa/add-data-siswa.component';
import { EditDataSiswaComponent } from './data-umum/data-siswa/edit-data-siswa/edit-data-siswa.component';
import { DataGuruComponent } from './data-umum/data-guru/data-guru.component';
import { AddDataGuruComponent } from './data-umum/data-guru/add-data-guru/add-data-guru.component';
import { EditDataGuruComponent } from './data-umum/data-guru/edit-data-guru/edit-data-guru.component';
import { DataGuruService } from './data-umum/data-guru/data-guru.service';
import { ApbsRincianComponent } from './apbs-rincian/apbs-rincian.component';
import { ApbsRincianService } from './apbs-rincian/apbs-rincian.service';
import { ManualEntryComponent } from './manual-entry/manual-entry.component';
import { ManualEntryService } from './manual-entry/manual-entry.service';
import { ManualEntryListComponent } from './manual-entry/manual-entry-list/manual-entry-list.component';
import { EditManualEntryComponent } from './manual-entry/manual-entry-list/edit-manual-entry/edit-manual-entry.component';
import { ReplaceDollerPipe } from './Shared/replace-doller.pipe';
import { ApbsRingkasanComponent } from './apbs-ringkasan/apbs-ringkasan.component';
import { ApbsRingkasanService } from './apbs-ringkasan/apbs-ringkasan.service';
import { BeritaComponent } from './berita/berita.component';
import { BeritaService } from './berita/berita.service';
import { AddBeritaComponent } from './berita/add-berita/add-berita.component';
import { EditBeritaComponent } from './berita/edit-berita/edit-berita.component';
import { DashboardsComponent } from './dashboards/dashboards.component';
import { DashboardsService } from './dashboards/dashboards.service';
import { DetilBeritaComponent } from './dashboards/detil-berita/detil-berita.component';
import { RasioAnggaranComponent } from './dashboards/rasio-anggaran/rasio-anggaran.component';
import { RasioAnggaranService } from './dashboards/rasio-anggaran/rasio-anggaran.service';
import { ListBeritaComponent } from './dashboards/list-berita/list-berita.component';
import { ChangePassUserComponent } from './change-pass-user/change-pass-user.component';
import { ChangePassUserService } from './change-pass-user/change-pass-user.service';
import { ApbsAsistensiComponent } from './apbs-asistensi/apbs-asistensi.component';
import { ApbsAsistensiService } from './apbs-asistensi/apbs-asistensi.service';
import { ApbsVisiMisiComponent } from './apbs-visi-misi/apbs-visi-misi.component';
import { ApbsVisiMisiService } from './apbs-visi-misi/apbs-visi-misi.service';
import {CalendarModule} from 'primeng/calendar';
import { KendaliBosdaComponent } from './kendali-bosda/kendali-bosda.component';
import { KendaliService } from './service/kendali.service';
import { KendaliBosnasComponent } from './kendali-bosnas/kendali-bosnas.component';
export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: true,
  decimal: ',',
  precision: 2,
  prefix: 'Rp ',
  suffix: '',
  thousands: '.'
};
registerLocaleData(localId, 'id');
export function tokkenGetter() {
  return localStorage.getItem('currentUser');
}
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    ControlSidebarComponent,
    LoginPageComponent,
    BlankComponent,
    TesPageComponent,
    RencanaKerjaAnggaranComponent,
    IndikatorKinerjaComponent,
    PreviewComponent,
    LookupRekeningComponent,
    PlafonAnggaranComponent,
    BelanjaLangsungComponent,
    BelanjaTidakLangsungComponent,
    CurrencyRupiahPipe,
    LookupActivityComponent,
    EditUnitActivityComponent,
    KompPenyusunComponent,
    AddRincianBelanjaComponent,
    EditRincianBelanjaComponent,
    AnggaranKasComponent,
    EditAnggaranKasComponent,
    PegawaiComponent,
    JabatanComponent,
    ListPegawaiComponent,
    TambahPegawaiComponent,
    UbahPegawaiComponent,
    UserAppComponent,
    AddUserComponent,
    EditUserComponent,
    UnitsComponent,
    AddUnitsComponent,
    EditUnitsComponent,
    SatuanComponent,
    AddSatuanComponent,
    EditSatuanComponent,
    ProgramKegiatanComponent,
    ProgramComponent,
    KegiatanComponent,
    AddProgramComponent,
    EditProgramComponent,
    AddKegiatanComponent,
    EditKegiatanComponent,
    WebOptionComponent,
    AddOptionComponent,
    EditOptionComponent,
    ApbsBelanjaTidakLangsungComponent,
    ListBtlComponent,
    ApbsBelanjaLangsungComponent,
    ListBlComponent,
    SumberDanaComponent,
    ListSumberDanaComponent,
    FormAddComponent,
    FormAddHeaderComponent,
    FormEditComponent,
    ApbsSumberDanaComponent,
    ListSdComponent,
    PlafonBosdaComponent,
    EditPlafonBosdaComponent,
    AddPlafonBosdaComponent,
    PlafonBtlComponent,
    StandarHargaComponent,
    PlafonBosnasComponent,
    EditPlafonBosnasComponent,
    DashBosnasComponent,
    DashBosdaComponent,
    DashBtlComponent,
    RekapBelanjaLangsungComponent,
    SheditComponent,
    SaldoAwalComponent,
    EditSaldoAwalComponent,
    DashBosdaTriwulanComponent,
    DashBosnasTriwulanComponent,
    RkaBosnasComponent,
    BosnasKompPenyusunComponent,
    BosnasAnggaranKasComponent,
    BosnasPreviewComponent,
    AddKompPenyusunComponent,
    EditKompPenyusunComponent,
    BonasAnggaranKasEditComponent,
    RekeningBosdaComponent,
    AddRekeningBosdaComponent,
    MappingHeaderBosdaComponent,
    AddMappingHeaderComponent,
    IndikatorKinerjaKegiatanComponent,
    EditIndikatorComponent,
    RkaBosdaComponent,
    BosdaAnggaranKasComponent,
    BosdaKompPenyusunComponent,
    BosdaPreviewComponent,
    AddKompPenyusunBosdaComponent,
    EditKompPenyusunBosdaComponent,
    RkaBtlComponent,
    BtlKompPenyusunComponent,
    BtlPreviewComponent,
    BtlAnggaranKasComponent,
    AddRincianBtlComponent,
    EditRincianBtlComponent,
    EditAnggaranKasBtlComponent,
    BosdaAnggaranKasEditComponent,
    VisiMisiComponent,
    VisiComponent,
    MisiComponent,
    TujuanComponent,
    AddVisiMisiComponent,
    EditVisiMisiComponent,
    DataUmumComponent,
    DataSiswaComponent,
    AddDataSiswaComponent,
    EditDataSiswaComponent,
    DataGuruComponent,
    AddDataGuruComponent,
    EditDataGuruComponent,
    ApbsRincianComponent,
    ManualEntryComponent,
    ManualEntryListComponent,
    EditManualEntryComponent,
    ReplaceDollerPipe,
    ApbsRingkasanComponent,
    BeritaComponent,
    AddBeritaComponent,
    EditBeritaComponent,
    DashboardsComponent,
    DetilBeritaComponent,
    RasioAnggaranComponent,
    ListBeritaComponent,
    ChangePassUserComponent,
    ApbsAsistensiComponent,
    ApbsVisiMisiComponent,
    KendaliBosdaComponent,
    KendaliBosnasComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    CheckboxModule,
    GrowlModule,
    CalendarModule,
    ReactiveFormsModule,
    AppRoutes,
    MessageModule,
    MessagesModule,
    BrowserAnimationsModule,
    InputTextareaModule,
    SplitButtonModule,
    DataTableModule,
    FieldsetModule,
    DropdownModule,
    TabViewModule,
    ButtonModule,
    DialogModule,
    RadioButtonModule,
    AutoCompleteModule,
    TableModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokkenGetter,
        whitelistedDomains: [environment.domainServer]
      }
    }),
    LoadingModule,
    NgProgressModule.forRoot(),
    NgProgressRouterModule,
    CurrencyMaskModule,
    KeyFilterModule,
    ChartModule
  ],
  providers: [
    MessageService,
    TesService,
    GlobalService,
    RkaService,
    LookupRekeningService,
    PlafonAnggaranService,
    KomPenyusunService,
    UsersService,
    AuthGuardService,
    AnggaranKasService,
    PreviewService,
    IndikatorKinerjaService,
    ReportService,
    JabatanService,
    EmployeeService,
    UserAppService,
    UnitsService,
    SatuanService,
    {provide: LOCALE_ID, useValue: 'id'},
    KegiatanService,
    ProgramService,
    ProgKegService,
    BelanjaTidakLangsungService,
    WebOptionService,
    BtlService,
    BlService,
    SumberDanaService,
    SdService,
    PlafonBosdaService,
    PlafonBtlService,
    StandarHargaService,
    PlafonBosnasService,
    DashBosnasService,
    DashBosdaService,
    DashBtlService,
    RekapBelanjaLangsungService,
    SaldoAwalService,
    DashBosdaTriwulanService,
    DashBosnasTriwulanService,
    RkaBosnasService,
    BosnasKompPenyusunService,
    BosnasAnggaranKasService,
    BosnasPreviewService,
    RekeningBosdaService,
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig},
    MappingHeaderBosdaService,
    IndikatorKinerjaKegiatanService,
    EditPlafonBosdaService,
    RkaBosdaService,
    BosdaKompPenyusunService,
    RkaBtlService,
    BtlAnggaranKasService,
    BtlKompPenyusunService,
    BtlPreviewService,
    BosdaAnggaranKasService,
    BosdaPreviewService,
    IndikatorKenerjaGlobalService,
    LoadersService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptService,
      multi: true
    },
    VisiMisiService,
    DataUmumService,
    DataSiswaService,
    DataGuruService,
    ApbsRincianService,
    ManualEntryService,
    ApbsRingkasanService,
    BeritaService,
    DashboardsService,
    RasioAnggaranService,
    ChangePassUserService,
    ApbsAsistensiService,
    ApbsVisiMisiService,
    KendaliService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
