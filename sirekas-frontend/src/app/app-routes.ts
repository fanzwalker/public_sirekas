import { KendaliBosnasComponent } from './kendali-bosnas/kendali-bosnas.component';
import { KendaliBosdaComponent } from './kendali-bosda/kendali-bosda.component';
import { ApbsVisiMisiComponent } from './apbs-visi-misi/apbs-visi-misi.component';
import { ApbsAsistensiComponent } from './apbs-asistensi/apbs-asistensi.component';
import { BeritaComponent } from './berita/berita.component';
import { ApbsRingkasanComponent } from './apbs-ringkasan/apbs-ringkasan.component';
import { ManualEntryComponent } from './manual-entry/manual-entry.component';
import { DataUmumComponent } from './data-umum/data-umum.component';
import { VisiMisiComponent } from './visi-misi/visi-misi.component';
import { RkaBtlComponent } from './rka-btl/rka-btl.component';
import { RkaBosdaComponent } from './rka-bosda/rka-bosda.component';
import { DashBosnasTriwulanComponent } from './dash-bosnas-triwulan/dash-bosnas-triwulan.component';
import { DashBosdaTriwulanComponent } from './dash-bosda-triwulan/dash-bosda-triwulan.component';
import { SaldoAwalComponent } from './saldo-awal/saldo-awal.component';
import { RekapBelanjaLangsungComponent } from './rekap-belanja-langsung/rekap-belanja-langsung.component';
import { DashBtlComponent } from './dash-btl/dash-btl.component';
import { DashBosdaComponent } from './dash-bosda/dash-bosda.component';
import { DashBosnasComponent } from './dash-bosnas/dash-bosnas.component';
import { PlafonBosnasComponent } from './plafon-bosnas/plafon-bosnas.component';
import { StandarHargaComponent } from './standar-harga/standar-harga.component';
import { PlafonBtlComponent } from './plafon-btl/plafon-btl.component';
import { PlafonBosdaComponent } from './plafon-bosda/plafon-bosda.component';
import { RouterModule, Routes } from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {HomePageComponent} from './home-page/home-page.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {BlankComponent} from './blank/blank.component';
import {TesPageComponent} from './tes-page/tes-page.component';
import {RencanaKerjaAnggaranComponent} from './rencana-kerja-anggaran/rencana-kerja-anggaran.component';
import {GlobalService} from './global.service';
import {PlafonAnggaranComponent} from './plafon-anggaran/plafon-anggaran.component';
import {AuthGuardService} from './service/auth-guard.service';
import {PegawaiComponent} from './pegawai/pegawai.component';
import {UserAppComponent} from './user-app/user-app.component';
import { UnitsComponent } from './units/units.component';
import { SatuanComponent } from './satuan/satuan.component';
import { ProgramKegiatanComponent } from './program-kegiatan/program-kegiatan.component';
import { WebOptionComponent } from './web-option/web-option.component';
import { ApbsBelanjaTidakLangsungComponent } from './apbs-belanja-tidak-langsung/apbs-belanja-tidak-langsung.component';
import { ApbsBelanjaLangsungComponent } from './apbs-belanja-langsung/apbs-belanja-langsung.component';
import { SumberDanaComponent } from './sumber-dana/sumber-dana.component';
import { ApbsSumberDanaComponent } from './apbs-sumber-dana/apbs-sumber-dana.component';
import { RkaBosnasComponent } from './rka-bosnas/rka-bosnas.component';
import { RekeningBosdaComponent } from './rekening-bosda/rekening-bosda.component';
import { IndikatorKinerjaKegiatanComponent } from './indikator-kinerja-kegiatan/indikator-kinerja-kegiatan.component';
import { ApbsRincianComponent } from './apbs-rincian/apbs-rincian.component';
import { DashboardsComponent } from './dashboards/dashboards.component';

export const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {
    path: 'login',
    component: LoginPageComponent,
    canActivate: [GlobalService],
    data: {setTitle: ['Login']}},
  {
    path: 'home',
    component: HomePageComponent,
    canActivate: [GlobalService, AuthGuardService],
    data: {setTitle: ['Home']},
    children: [
      {path: '', redirectTo: 'dashboards', pathMatch: 'full'},
      {
        path: 'tes-page',
        component: TesPageComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Tes Page']}
      },
      {
        path: 'dashboards',
        component: DashboardsComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Dashboard']}
      },
      {
        path: 'kendali-bosda',
        component: KendaliBosdaComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Kendali Bosda']}
      },
      {
        path: 'kendali-bosnas',
        component: KendaliBosnasComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Kendali Bosnas']}
      },
      {
        path: 'apbs-rincian',
        component: ApbsRincianComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['APBS - Rincian']}
      },
      {
        path: 'apbs-ringkasan',
        component: ApbsRingkasanComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['APBS - Ringkasan']}
      },
      {
        path: 'apbs-asistensi',
        component: ApbsAsistensiComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['APBS - Lembar Asistensi & Pengesahan']}
      },
      {
        path: 'apbs-visi-misi',
        component: ApbsVisiMisiComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['APBS - Visi Misi & Data Sekolah']}
      },
      {
        path: 'manual-entry',
        component: ManualEntryComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['APBS - Manual Entry']}
      },
      {
        path: 'apbs-sumber-dana',
        component: ApbsSumberDanaComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['APBS - Sumber Dana']}
      },
      {
        path: 'apbs-belanja-langsung',
        component: ApbsBelanjaLangsungComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['APBS - Belanja Langsung']}
      },
      {
        path: 'apbs-belanja-tidak-langsung',
        component: ApbsBelanjaTidakLangsungComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['APBS - Belanja Tidak Langsung']}
      },
      {
        path: 'dash-rekap-bl/:actId',
        component: RekapBelanjaLangsungComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['RKAS - Rekap Belanja Langsung']}
      },
      {
        path: 'dash-btl',
        component: DashBtlComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['RKAS - Belanja Tidak Langsung']}
      },
      {
        path: 'dash-bosnas',
        component: DashBosnasComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['RKAS - BOSNAS']}
      },
      {
        path: 'dash-bosnas-triwulan',
        component: DashBosnasTriwulanComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['RKAS - BOSNAS TRIWULAN']}
      },
      {
        path: 'dash-bosda',
        component: DashBosdaComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['RKAS - BOSDA']}
      },
      {
        path: 'dash-bosda-triwulan',
        component: DashBosdaTriwulanComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['RKAS - BOSDA TRIWULAN']}
      },
      {
        path: 'saldo-awal',
        component: SaldoAwalComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Saldo Awal']}
      },
      {
        path: 'rka-bosda',
        component: RkaBosdaComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['RKA - BOSDA']}
      },
      {
        path: 'rka-bosnas',
        component: RkaBosnasComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['RKA - BOSNAS']}
      },
      {
        path: 'rka-btl',
        component: RkaBtlComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['RKA - Belanja Tidak Langsung']}
      },
      {
        path: 'plafon-bosda',
        component: PlafonBosdaComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Plafon BOSDA']}
      },
      {
        path: 'plafon-bosnas',
        component: PlafonBosnasComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Plafon BOSNAS']}
      },
      {
        path: 'plafon-btl',
        component: PlafonBtlComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Plafon Bantuan Tidak Langsung']}
      },
      {
        path: 'indikator-kinerja-kegiatan',
        component: IndikatorKinerjaKegiatanComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Data Master - Indikator Kinerja Kegiatan']}
      },
      {
        path: 'rekening-bosda',
        component: RekeningBosdaComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Data Master - Rekening BOSDA']}
      },
      {
        path: 'sumber-dana',
        component: SumberDanaComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Data Master - Sumber Dana']}
      },
      {
        path: 'pegawai',
        component: PegawaiComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Data Master - Pegawai']}
      },
      {
        path: 'daftar-user',
        component: UserAppComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Data Master - Daftar User / Pengguna']}
      },
      {
        path: 'daftar-units',
        component: UnitsComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Data Master - Daftar Unit']}
      },
      {
        path: 'daftar-satuan',
        component: SatuanComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Data Master - Daftar Satuan']}
      },
      {
        path: 'program-kegiatan',
        component: ProgramKegiatanComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Data Master - Program Kegiatan']}
      },
      {
        path: 'web-option',
        component: WebOptionComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Konfigurasi Aplikasi']}
      },
      {
        path: 'standar-harga',
        component: StandarHargaComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Standar Harga']}
      },
      {
        path: 'visi-misi',
        component: VisiMisiComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Visi, Misi & Tujuan']}
      },
      {
        path: 'data-umum',
        component: DataUmumComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Data Umum']}
      },
      {
        path: 'master-berita',
        component: BeritaComponent,
        canActivate: [GlobalService],
        data: {setTitle: ['Master Berita']}
      },
      {path: '**', component: BlankComponent}
    ]
  }
];
export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
