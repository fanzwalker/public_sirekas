import { LoadersService } from './service/loaders.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  loaders: boolean;
  constructor(private loaderService: LoadersService) {
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.loaderService.loaderAction$
    );
    combine.subscribe(resp => {
      const [action] = resp;
      this.loaders = action;
    });
  }
}
