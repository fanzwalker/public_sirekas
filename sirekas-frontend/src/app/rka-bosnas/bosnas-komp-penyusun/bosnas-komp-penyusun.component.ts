import { BosnasAnggaranKasService } from './../bosnas-anggaran-kas/bosnas-anggaran-kas.service';
import { BosnasPreviewService } from './../bosnas-preview/bosnas-preview.service';
import { BosnasKompPenyusunService } from './bosnas-komp-penyusun.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { IlistBudgetDet } from '../../interface/ilist-budget-det';
import { RkaBosnasService } from '../rka-bosnas.service';
import { UsersService } from '../../service/users.service';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-bosnas-komp-penyusun',
  templateUrl: './bosnas-komp-penyusun.component.html',
  styleUrls: ['./bosnas-komp-penyusun.component.css']
})
export class BosnasKompPenyusunComponent implements OnInit, OnDestroy {
  listRkas: IlistBudgetDet[];
  rkasSelected: any = [];
  unitId: number;
  activityId: number;
  paguBosnas: number;
  userInfo: any;
  listUnitDesc: any[];
  constructor(private service: BosnasKompPenyusunService,
              private rkaBosnasService: RkaBosnasService,
              private bosnasPreviewService: BosnasPreviewService,
              private bosnasAnggaranKasService: BosnasAnggaranKasService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.rkaBosnasService.unitId$,
      this.rkaBosnasService.activityId$,
      this.service.listRkas$,
      this.service.listUnitDesc$,
      this.service.paguBosnas$
    );
    combine.subscribe(resp => {
      const [unitId, activityId, listRkas, listUnitDesc, paguBosnas] = resp;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listRkas = Object.assign(<IlistBudgetDet>listRkas);
      this.listUnitDesc = Object.assign(listUnitDesc);
      this.paguBosnas = paguBosnas;
    });
    this.service.getUnitDecs();
  }
  calculateTotal(x, y) {
    let total = 0;
    if (this.listRkas) {
      // tslint:disable-next-line:prefer-const
      for (let data of this.listRkas) {
        switch (x) {
          case data.chartOfAccountCode:
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.total;
            } break;
        }

      }
    }
    return total;
  }
  btnAdd() {
    if (this.unitId) {
      if (this.paguBosnas === 0) {
        swal({
          position: 'top-end',
          type: 'warning',
          title: 'Pagu Belum diset pada Plafon',
          showConfirmButton: false,
          timer: 3000
        });
      } else {
        this.service.getBudgetSources();
        this.service.getUnitDecs();
        this.service.showAddRincian(true);
      }
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnDelete() {
    if (this.rkasSelected.length > 0) {
      swal({
        title: 'Hapus',
        text: 'Data Akan Dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          const listId = [];
          _.forEach(this.rkasSelected, function (k) {
            listId.push(k.id);
          });
          const dataBody = {
            ids : listId
          };
          this.service.deleteBudgetDet(this.unitId, this.userInfo.stages, this.activityId, dataBody)
            .subscribe(resp => {
              this.rkasSelected = [];
              this.service.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 0);
              this.bosnasAnggaranKasService.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
              this.bosnasPreviewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
              swal({
                position: 'top-end',
                type: 'success',
                title: 'Data Berhasil Dihapus',
                showConfirmButton: false,
                timer: 3000
              });
            }, error => {
              swal({
                position: 'top-end',
                type: 'error',
                title: 'Data Gagal Dihapus [' + error.statusText + ']',
                showConfirmButton: false,
                timer: 3000
              });
            });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnEdit(e) {
    this.service.setDataEditRincian(e);
    this.service.getBudgetSources();
    this.service.getUnitDecs();
    this.service.showEditRincian(true);
  }
  ngOnDestroy() {
    this.service.setBudgetDet([], 0);
    this.service.setBudgetSources([]);
    this.service.setUnitDesc([]);
  }
  castSatuanUnit(a) {
    if (a) {
      const unitSatuan = _.find(this.listUnitDesc, (s) => {
        return s.id === +a;
      });
      return unitSatuan.name;
    }
  }
}
