import { BosnasPreviewService } from './../../bosnas-preview/bosnas-preview.service';
import { BosnasAnggaranKasService } from './../../bosnas-anggaran-kas/bosnas-anggaran-kas.service';
import { BosnasKompPenyusunService } from './../bosnas-komp-penyusun.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { RkaBosnasService } from '../../rka-bosnas.service';
import { UsersService } from '../../../service/users.service';
import { Observable } from 'rxjs/Observable';
import { IputBudgetDet } from '../../../interface/iput-budget-det';
import { Dropdown } from 'primeng/primeng';
import { AllOption } from '../../../shared/all-option';
@Component({
  selector: 'app-edit-komp-penyusun',
  templateUrl: './edit-komp-penyusun.component.html',
  styleUrls: ['./edit-komp-penyusun.component.css']
})
export class EditKompPenyusunComponent implements OnInit {
  @ViewChild('dd1') dd1: Dropdown;
  @ViewChild('dd2') dd2: Dropdown;
  @ViewChild('dd3') dd3: Dropdown;
  @ViewChild('dd4') dd4: Dropdown;
  showThis: boolean;
  formEdit: FormGroup;
  unitId: number;
  activityId: number;
  listSumberDana: any;
  optSumberDana: SelectItem[];
  listUnitDesc: any;
  optUnitDesc: SelectItem[];
  dataEdit: any;
  userInfo: any;
constructor(private fb: FormBuilder,
              private rkaBosnasService: RkaBosnasService,
              private service: BosnasKompPenyusunService,
              private bosnasAnggaranService: BosnasAnggaranKasService,
              private bosnasPreviewService: BosnasPreviewService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
    this.formEdit = this.fb.group({
      budgetSourceId: [0, Validators.required],
      description: [null],
      expression: [null],
      unitDesc: [null],
      costPerUnit: [0],
      type: [0],
      expres1: [null],
      expres2: [null],
      expres3: [null],
      expres4: [null],
      expres5: [null],
      expres6: [null],
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayEdit$,
      this.rkaBosnasService.unitId$,
      this.rkaBosnasService.activityId$,
      this.service.listBudgetSources$,
      this.service.listUnitDesc$,
      this.service.dataEditRincian$,
    );
    combine.subscribe(resp => {
      const [displayEdit, unitId, activityId, listBudgetSources, listUnitDesc, dataEdit] = resp;
      this.showThis = displayEdit;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listSumberDana = Object.assign(listBudgetSources);
      this.listUnitDesc = Object.assign(listUnitDesc);
      this.dataEdit = Object.assign(dataEdit);
      this.setOptBudgetSources();
      this.setOptUnitDesc();
      if (this.dataEdit.id && this.dataEdit.expression && this.dataEdit.expression !== '-') {
        const hasilangka = this.dataEdit.expression.match(/([\d]+)/g);
        const hasilkata = this.dataEdit.expression.match(/(?=[^\d\s])([^\d\s]*)/g);
        this.formEdit.patchValue({
          budgetSourceId: this.dataEdit.budgetSourceId,
          description: this.dataEdit.description,
          expression: this.dataEdit.expression,
          unitDesc: this.dataEdit.unitDesc,
          costPerUnit: this.dataEdit.costPerUnit,
          type: this.dataEdit.type,
          expres1: [hasilangka[0]],
          expres2: this.getIdUnit(hasilkata[0]),
          expres3: [hasilangka[1]],
          expres4: this.getIdUnit(hasilkata[2]),
          expres5: [hasilangka[2]],
          expres6: this.getIdUnit(hasilkata[4])
        });
      }
    });
  }
  getIdUnit(data) {
    if (this.listUnitDesc.length > 0) {
      const temp = _.find(this.listUnitDesc, (e) => {
        return e.name === data;
      });
      if (temp) {
        return temp.id;
      }
    }
  }
  setOptBudgetSources() {
    this.optSumberDana = [];
    if (this.listSumberDana.length > 0) {
      this.optSumberDana.push({label: '-- Pilih --', value: null});
      _.forEach(this.listSumberDana, e => {
        this.optSumberDana.push({label: e.name, value: e.id});
      });
    } else {
      this.optSumberDana = [{label: '-- Pilih --', value: null}];
    }
  }
  setOptUnitDesc() {
    this.optUnitDesc = [];
    if (this.listUnitDesc.length > 0) {
      this.optUnitDesc.push({label: '-- Pilih --', value: null});
      _.forEach(this.listUnitDesc, e => {
        this.optUnitDesc.push({label: e.name, value: e.id});
      });
    } else {
      this.optUnitDesc = [{label: '-- Pilih --', value: null}];
    }
  }
  btnSimpan() {
    const paramBody = <IputBudgetDet>{};
    paramBody.budgetSourceId = this.dataEdit.budgetSourceId;
    paramBody.costPerUnit = this.formEdit.value.costPerUnit;
    paramBody.description = this.formEdit.value.description;
    paramBody.expression = this.joinExpression();
    paramBody.unitDesc = this.formEdit.value.unitDesc;
    paramBody.type = this.formEdit.value.type;
    this.service.putBudgetDet(this.unitId, this.userInfo.stages, this.activityId, this.dataEdit.id, paramBody).
      subscribe(resp => {
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data Berhasil Diubah',
          showConfirmButton: false,
          timer: 3000
        });
    }, error => {
      error.error.error.forEach(e => {
        swal({
          position: 'top-end',
          type: 'error',
          title: e,
          showConfirmButton: false,
          timer: 3000
        });
      });
    });
  }
  joinExpression() {
    let ekspresi = '';
    if (this.formEdit.value.expres1[0] !== undefined || this.formEdit.value.expres1) {
      ekspresi += this.formEdit.value.expres1 + ' ' + this.findNameUnit(this.formEdit.value.expres2);
      if (this.formEdit.value.expres3[0] !== undefined || this.formEdit.value.expres3) {
        ekspresi += ' x ' + this.formEdit.value.expres3 + ' ' + this.findNameUnit(this.formEdit.value.expres4);
        if (this.formEdit.value.expres5[0] !== undefined || this.formEdit.value.expres5) {
          ekspresi += ' x ' + this.formEdit.value.expres5 + ' ' + this.findNameUnit(this.formEdit.value.expres6);
        }
      }
    }
    return ekspresi;
  }
  findNameUnit(id) {
    if (this.listUnitDesc.length > 0) {
      const unit =  _.find(this.listUnitDesc, (v) => {
        if (v.id === id) { return v; }
      });
      return unit ? unit.name : '';
    }
  }
  btnBatal() {
    this.onHide();
  }
  onHide() {
    AllOption.ResetFilterOption([this.dd1, this.dd2, this.dd3, this.dd4]);
    this.formEdit.reset();
    this.service.getUnitDecs();
    this.service.setDataEditRincian([]);
    this.service.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 0);
    this.bosnasAnggaranService.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
    this.bosnasPreviewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
    this.service.showEditRincian(false);
  }

}
