import { UsersService } from './../../service/users.service';
import { BosnasAnggaranKasService } from './bosnas-anggaran-kas.service';
import { RkaBosnasService } from './../rka-bosnas.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IlistBudgetMonthly } from '../../interface/ilist-budget-monthly';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-bosnas-anggaran-kas',
  templateUrl: './bosnas-anggaran-kas.component.html',
  styleUrls: ['./bosnas-anggaran-kas.component.css']
})
export class BosnasAnggaranKasComponent implements OnInit, OnDestroy {
  unitId: number;
  activityId: number;
  listAnggaranKas: IlistBudgetMonthly[];
  anggaranKasSelected: any = [];
  userInfo: any;
  constructor(private rksBosnasService: RkaBosnasService,
              private service: BosnasAnggaranKasService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine =  Observable.combineLatest(
      this.rksBosnasService.unitId$,
      this.rksBosnasService.activityId$,
      this.service.listAnggaranKas$
    );
    combine.subscribe(resp => {
      const [unitId, activityId, listAnggaranKas] = resp;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listAnggaranKas = listAnggaranKas;
    });
  }
  calculateTotal(x, y) {
    let total = 0;
    if (this.listAnggaranKas) {
      for (const data of this.listAnggaranKas) {
        switch (x) {
          case data.chartOfAccountCode:
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.total;
            } break;
          case 'q1':
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.q1;
            } break;
          case 'q2':
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.q2;
            } break;
          case 'q3':
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.q3;
            } break;
          case 'q4':
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.q4;
            } break;
        }
      }
    }
    return total;
  }
  btnEdit(e) {
    this.service.showEdit(true);
    this.service.setDataEditAnggaranKas(e);
  }
  btnAdd() {

  }
  ngOnDestroy() {
    this.service.setBudgetMonthly([]);
  }
}
