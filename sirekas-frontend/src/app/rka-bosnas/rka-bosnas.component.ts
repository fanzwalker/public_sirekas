import { IndikatorKenerjaGlobalService } from './../shared/indikator-kenerja-global.service';
import { BosnasAnggaranKasService } from './bosnas-anggaran-kas/bosnas-anggaran-kas.service';
import { BosnasKompPenyusunService } from './bosnas-komp-penyusun/bosnas-komp-penyusun.service';
import { BosnasPreviewService } from './bosnas-preview/bosnas-preview.service';
import { UsersService } from './../service/users.service';
import { SelectItem } from 'primeng/api';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RkaBosnasService } from './rka-bosnas.service';
import * as _ from 'lodash';
import { animationPage } from '../shared/animation-page';
@Component({
  selector: 'app-rka-bosnas',
  templateUrl: './rka-bosnas.component.html',
  styleUrls: ['./rka-bosnas.component.css'],
  animations: [animationPage]
})
export class RkaBosnasComponent implements OnInit, OnDestroy {
  listSekolah: any; optSekolah: SelectItem[];
  titleLegend = '';
  formFilter: FormGroup;
  userInfo: any;
  unitActivitySelected: any;
  constructor(private fb: FormBuilder,
              private service: RkaBosnasService,
              private userService: UsersService,
              private bosnasPreviewService: BosnasPreviewService,
              private bosnasKompPeyusunService: BosnasKompPenyusunService,
              private bosnasAnggaranKasService: BosnasAnggaranKasService,
              private indikatorKinerjaService: IndikatorKenerjaGlobalService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah: ['', Validators.required],
      program: ['', Validators.required],
      kegiatan: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (!this.userInfo.IsAdmin) {
      this.getSekolah();
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.onChangeUnit();
    } else {
      this.getSekolah();
    }
    this.getProgram();
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(this.userInfo.stages, 0, 4, 1, 2)
    .subscribe(resp => {
      this.listSekolah = resp;
      temp.push({label: 'Pilih Sekolah', value: null});
      _.forEach(this.listSekolah, (e) => {
        temp.push({label: e.code + ' - ' + e.name, value: e.id});
      });
      this.optSekolah = temp;
    });
  }
  getProgram() {
    const temp = [];
    this.service.getPrograms()
    .subscribe(resp => {
      this.formFilter.patchValue({
        program: resp[0].id
      });
      this.onChangeUnit();
    });
  }
  getKegiatan() {
    const temp = [];
    this.service.getKegiatan(this.formFilter.value.sekolah, this.formFilter.value.program, this.userInfo.stages, 2)
    .subscribe(resp => {
      if (resp.length > 0) {
        this.formFilter.patchValue({
          kegiatan: resp[0].activityId
        });
        this.service.getLastYearBalance(this.formFilter.value.sekolah)
        .subscribe(resp2 =>  {
          if (resp2.id) {
            this.titleLegend = (resp[0].total + resp2.value).toString();
            this.bosnasKompPeyusunService.setPaguBosnas(resp[0].total + resp2.value);
          } else {
            this.titleLegend = resp[0].total.toString();
          }
        });
        this.getRkas();
      } else {
        this.service.setUnitAct(this.formFilter.value.sekolah, null);
        this.bosnasKompPeyusunService.setPaguBosnas(0);
        this.bosnasKompPeyusunService.setBudgetDet([], 0);
        this.bosnasAnggaranKasService.setBudgetMonthly([]);
        this.bosnasPreviewService.setPreview([]);
        this.bosnasPreviewService.setUnitProgAct([]);
      }
    });
  }
  setUnitProgAct() {
    const data = {
      Units : _.find(this.listSekolah, (e) => e.id === this.formFilter.value.sekolah),
      Programs: this.formFilter.value.program,
      Activity: this.formFilter.value.kegiatan
    };
    if (!this.userInfo.IsAdmin) {
      this.bosnasPreviewService.getUnitById(this.formFilter.value.sekolah)
      .subscribe(d => {
        data.Units = Object.assign(d);
        this.bosnasPreviewService.setUnitProgAct(data);
      });
    } else {
      this.bosnasPreviewService.setUnitProgAct(data);
    }
  }
  onOpentabs() {
    console.log('open tabs');
  }
  onChangeUnit() {
    this.titleLegend = '';
    this.getKegiatan(); // Bosda & bosnas
  }
  getRkas(s?) {
    if (s) {
      this.titleLegend = s.label;
    }
    if (this.formFilter.value.sekolah && (this.formFilter.value.kegiatan || this.formFilter.value.kegiatan === 0)) {
      this.bosnasKompPeyusunService.getBudgetDet(this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.kegiatan, 0);
      this.bosnasAnggaranKasService.getBudgetMonthly(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      this.bosnasPreviewService.getPreview(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      this.setUnitProgAct();
      this.indikatorKinerjaService.getPerfBenchmarkFixed(2, this.formFilter.value.sekolah,
        this.userInfo.stages, this.formFilter.value.kegiatan);
    } else {
      this.bosnasKompPeyusunService.setBudgetDet([], 0);
      this.bosnasAnggaranKasService.setBudgetMonthly([]);
      this.bosnasPreviewService.setPreview([]);
      this.bosnasPreviewService.setUnitProgAct([]);
    }
    this.service.setUnitAct(this.formFilter.value.sekolah, this.formFilter.value.kegiatan);

  }
  ngOnDestroy() {
    this.bosnasKompPeyusunService.showAddRincian(false);
    this.bosnasKompPeyusunService.setBudgetDet([], 0);
    this.bosnasAnggaranKasService.setBudgetMonthly([]);
    this.bosnasPreviewService.setPreview([]);
    this.bosnasPreviewService.setUnitProgAct([]);
  }
}
