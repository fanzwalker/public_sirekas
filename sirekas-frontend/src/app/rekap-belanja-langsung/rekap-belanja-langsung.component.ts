import { RekapBelanjaLangsungService } from './rekap-belanja-langsung.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ReportService } from '../service/report.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectItem, MenuItem } from 'primeng/api';
import { UsersService } from '../service/users.service';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';
import { animationPage } from '../shared/animation-page';
import { IpreviewRekapBl } from '../interface/ipreview-rekap-bl';
import {ActivatedRoute} from '@angular/router';
import {ISubscription} from 'rxjs/Subscription';
@Component({
  selector: 'app-rekap-belanja-langsung',
  templateUrl: './rekap-belanja-langsung.component.html',
  styleUrls: ['./rekap-belanja-langsung.component.css'],
  animations: [animationPage]
})
export class RekapBelanjaLangsungComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  userInfo: any;
  listSekolah: any; optSekolah: SelectItem[];
  itemPrints: MenuItem[];
  previews: IpreviewRekapBl[];
  activity: any;
  unitSelected: any;
  subscription: ISubscription;
  constructor(private service: RekapBelanjaLangsungService,
              private reportService: ReportService,
              private userService: UsersService,
              private fb: FormBuilder,
              private route: ActivatedRoute) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required]
    });
  }

  ngOnInit() {
    this.subscription = this.route.paramMap.subscribe(params => {
      const combine = Observable.combineLatest(
        this.service.dataPreview$,
        this.service.getActivity(params.get('actId'))
      );
      combine.subscribe(resp => {
        const [dataPreview, activities] = resp;
        this.previews = Object.assign(dataPreview);
        if (activities.length > 0) {
          this.activity = activities[0];
        }
      });

      if (!this.userInfo.IsAdmin) {
        this.formFilter.patchValue({
          sekolah: +this.userInfo.unitId
        });
        this.getSekolah();
      } else {
        this.getSekolah();
      }
    });
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(this.userInfo.stages, 0, 4, 1, 3)
      .subscribe(resp => {
        this.listSekolah = resp;
        if (!this.userInfo.IsAdmin) {
          this.onChangeUnit();
        }
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  onChangeUnit() {
    if (this.formFilter.valid) {
      const unitSelected = _.find(this.listSekolah, (x) => {
        return x.id === this.formFilter.value.sekolah;
      });
      this.unitSelected = Object.assign(unitSelected);
      this.service.getPreview(this.formFilter.value.sekolah, this.userInfo.stages, this.activity.id);
    } else {
      this.unitSelected = '-';
      this.previews = Object.assign([]);
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnCetak(type) {
    if (this.formFilter.valid) {
      const paramBody = {};
      paramBody['@unitId'] = this.formFilter.value.sekolah;
      paramBody['@activityId'] = this.activity.id;
      paramBody['@stageId'] = +this.userInfo.stages;
      this.reportService.execPrint('Rka2.rpt', type, paramBody)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, 'Rekap Belanja Langsung');
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  totals() {
    let totals = 0;
    if (this.previews.length > 0 ) {
      _.forEach(this.previews, (e) => {
        if (e.code.length > 4) {
          totals += e.total;
        }
      });
    }
    return totals;
  }
  ngOnDestroy() {
    this.previews = Object.assign([]);
    this.unitSelected = '-';
    this.subscription.unsubscribe();
  }
}
