import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class RekapBelanjaLangsungService {
  private dataPreview = new BehaviorSubject(<any>[]);
  public dataPreview$ = this.dataPreview.asObservable();
  constructor(private http: HttpClient) { }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getPreview(unitId, stageId, activityId) {
    return this.http.get(`${environment.url}previews/rka2/unit/${unitId}/stages/${stageId}/activities/${activityId}`)
      .map(resp => resp).subscribe(data => this.setPreview(data));
  }
  setPreview(data) {
    return this.dataPreview.next(data);
  }
  getActivity(actType) {
    return this.http.get<any[]>(`${environment.url}programs`).switchMap(data => {
      if (data.length > 0) {
        const params = new HttpParams()
          .set('activityType', actType);
        return this.http.get<any[]>(`${environment.url}programs/${data[0].id}/activities`, {params: params});
      }
    });
  }
}
