import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApbsBelanjaLangsungComponent } from './apbs-belanja-langsung.component';

describe('ApbsBelanjaLangsungComponent', () => {
  let component: ApbsBelanjaLangsungComponent;
  let fixture: ComponentFixture<ApbsBelanjaLangsungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApbsBelanjaLangsungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApbsBelanjaLangsungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
