import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class BlService {
  private listBl = new BehaviorSubject(<any>[]);
  public listBl$ = this.listBl.asObservable();
  private unitId = new BehaviorSubject<number>(null);
  public unitId$ = this.unitId.asObservable();
  constructor(private http: HttpClient) { }
  getUnits() {
    return this.http.get(`${environment.url}units`);
  }
  setUnit(unitId): void {
    return this.unitId.next(unitId);
  }
  getPreview(expensesType, stageId, unitId, coaLevel) {
    const queryParam = new HttpParams()
    .set('unitId', unitId)
    .set('coaLevel', coaLevel);
    return this.http.get(`${environment.url}previews/apbs/expenses-type/${expensesType}/stages/${stageId}`, {params: queryParam})
    .map(resp => resp).subscribe(data => this.setlistBtl(data));
  }
  setlistBtl(data) {
    return this.listBl.next(data);
  }
}
