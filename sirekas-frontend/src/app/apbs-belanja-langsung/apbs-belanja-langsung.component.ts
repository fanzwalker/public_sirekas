import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../service/users.service';
import { BlService } from './bl.service';
import { animationPage } from '../shared/animation-page';
import * as _ from 'lodash';
@Component({
  selector: 'app-apbs-belanja-langsung',
  templateUrl: './apbs-belanja-langsung.component.html',
  styleUrls: ['./apbs-belanja-langsung.component.css'],
  animations: [animationPage]
})
export class ApbsBelanjaLangsungComponent implements OnInit {
  listSekolah: any; optSekolah: SelectItem[];
  userInfo: any;
  formFilter: FormGroup;
  constructor(private userService: UsersService,
              private fb: FormBuilder,
              private blService: BlService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required]
    });
  }

  ngOnInit() {
    if (this.userInfo.IsAdmin && this.userInfo.unitId !== '0') {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
    } else if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
    }
    this.getSekolah();
    this.GetData();
  }
  getSekolah() {
    const temp = [];
    this.blService.getUnits()
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, (e) => {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  GetData() {
    this.blService.setUnit(this.formFilter.value.sekolah);
    if (this.formFilter.value.sekolah) {
      this.blService.getPreview(2, this.userInfo.stages, this.formFilter.value.sekolah, null);
    } else {
      this.blService.setlistBtl([]);
    }
  }
}
