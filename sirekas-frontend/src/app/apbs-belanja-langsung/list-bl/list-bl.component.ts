import { Component, OnInit } from '@angular/core';
import { IlistBl } from '../../interface/ilist-bl';
import { MenuItem } from 'primeng/primeng';
import { BlService } from '../bl.service';
import { ReportService } from '../../service/report.service';
import { UsersService } from '../../service/users.service';
import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';
import { animationPage } from '../../shared/animation-page';
@Component({
  selector: 'app-list-bl',
  templateUrl: './list-bl.component.html',
  styleUrls: ['./list-bl.component.css'],
  animations: [animationPage]
})
export class ListBlComponent implements OnInit {
  listBl: IlistBl[];
  unitId: any;
  itemPrints: MenuItem[];
  userInfo: any;
  constructor(private blService: BlService,
              private reportService: ReportService,
              private usersService: UsersService) {
    this.userInfo = this.usersService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.blService.listBl$,
      this.blService.unitId$
    );
    combine.subscribe(resp => {
      const [listBtl, unitId] = resp;
      this.listBl = Object.assign(listBtl);
      this.unitId = unitId;
    });
  }
  btnCetak(type) {
    if (this.unitId) {
      const paramBody = {};
      paramBody['@unitId'] = this.unitId;
      paramBody['@stageId'] = +this.userInfo.stages;
      this.reportService.execPrint('ApbsBl.rpt', type, paramBody)
        .subscribe(resp =>  {
          this.reportService.extractData(resp, type, 'APBS Belanja Langsung');
        });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Unit Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
}
