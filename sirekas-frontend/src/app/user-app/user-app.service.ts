import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class UserAppService {

  constructor(private http: HttpClient) { }
  private listUsers = new BehaviorSubject(<any>[]);
  public listUsers$ = this.listUsers.asObservable();
  private roleId = new BehaviorSubject<any>(null);
  public roldeId$ = this.roleId.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  setRoleId(roleId: number, roleName: string) {
    return this.roleId.next({id: roleId, name: roleName});
  }
  getRole() {
    return this.http.get(`${environment.url}roles`);
  }
  getUsers(roleId) {
    return this.http.get(`${environment.url}roles/${roleId}/users`)
      .map(resp => resp).subscribe(data => this.setListUsers(data));
  }
  setListUsers(data) {
    return this.listUsers.next(data);
  }
  postUser(roleId, bodyParam) {
    return this.http.post(`${environment.url}roles/${roleId}/users`, bodyParam);
  }
  putUser(roleId, userId, bodyParam) {
    return this.http.put(`${environment.url}roles/${roleId}/users/${userId}`, bodyParam);
  }
  deleteUser(roleId, userId) {
    return this.http.request('DELETE', `${environment.url}roles/${roleId}/users/${userId}`);
  }
  getUnits() {
    return this.http.get(`${environment.url}units`);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
}
