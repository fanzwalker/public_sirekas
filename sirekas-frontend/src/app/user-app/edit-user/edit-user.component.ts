import { Component, OnInit } from '@angular/core';
import {animationPage} from '../../shared/animation-page';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/api';
import {UserAppService} from '../user-app.service';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import swal from 'sweetalert2';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css'],
  animations: [animationPage]
})
export class EditUserComponent implements OnInit {
  role: any;
  formEdit: FormGroup;
  showThis: boolean;
  listSekolah: any;  optSekolah: SelectItem[];
  dataEdit: any;
  constructor(private userAppService: UserAppService,
              private fb: FormBuilder) {
    this.formEdit = this.fb.group({
      fullName: ['', Validators.required],
      newPassword: ['', Validators.required],
      stages: 1
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.userAppService.displayEdit$,
      this.userAppService.roldeId$,
      this.userAppService.dataEdit$
    );
    combine.subscribe(resp => {
      const [displayAdd, role, dataEdit] = resp;
      this.showThis = displayAdd;
      this.role = role;
      this.dataEdit = Object.assign(dataEdit);
      if (this.dataEdit.id) {
        this.formEdit.patchValue({
          fullName: this.dataEdit.fullName
        });
      }
    });
    this.getSekolah();
  }
  getSekolah() {
    const temp = [];
    this.userAppService.getUnits()
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  Simpan() {
    if (this.formEdit.valid) {
      console.log(this.formEdit.value);
      this.userAppService.putUser(this.role.id, this.dataEdit.id, this.formEdit.value)
        .subscribe(resp => {
          if (resp) {
            this.userAppService.getUsers(this.role.id);
            this.onHide();
            swal({
              position: 'top-end',
              type: 'success',
              title: 'User Berhasil Diubah',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }, error => {
          swal({
            position: 'top-end',
            type: 'error',
            title: 'User Gagal Diubah [' + error.statusText + ']',
            showConfirmButton: false,
            timer: 3000
          });
          return false;
        });
    } else {
      console.log('wew');
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.formEdit.patchValue({stages: 1});
    this.userAppService.showEdit(false);
  }
}
