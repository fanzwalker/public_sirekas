import { Component, OnInit } from '@angular/core';
import {animationPage} from '../shared/animation-page';
import {UsersService} from '../service/users.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/api';
import {UserAppService} from './user-app.service';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import {Observable} from 'rxjs/Observable';
import {IlistUser} from '../interface/ilist-user';
@Component({
  selector: 'app-user-app',
  templateUrl: './user-app.component.html',
  styleUrls: ['./user-app.component.css'],
  animations: [animationPage]
})
export class UserAppComponent implements OnInit {
  userInfo: any;
  listRole: any;
  optRole: SelectItem[];
  formFilterRole: FormGroup;
  listUsers: IlistUser[];
  constructor(private userService: UsersService,
              private fb: FormBuilder,
              private userAppService: UserAppService) {
    this.formFilterRole = this.fb.group({
      roleUser: ['', Validators.required]
    });
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.userAppService.listUsers$
    );
    combine.subscribe(resp => {
      const [listUser] = resp;
      this.listUsers = Object.assign(listUser);
    });
    this.getSRole();
  }
  getSRole() {
    const temp = [];
    this.userAppService.getRole()
      .subscribe(resp => {
        this.listRole = resp;
        temp.push({label: 'Pilih Role', value: null});
        _.forEach(this.listRole, function (e) {
          temp.push({label: e.name , value: e.id});
        });
        this.optRole = temp;
      });
  }
  onChangeRole() {
    if (this.formFilterRole.valid) {
      const role = _.find(this.listRole, l => l.id === this.formFilterRole.value.roleUser);
      this.userAppService.setRoleId(this.formFilterRole.value.roleUser, role.name);
      this.userAppService.getUsers(this.formFilterRole.value.roleUser);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Role Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  showAdd() {
    if (this.formFilterRole.value.roleUser) {
      this.userAppService.showAdd(true);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Role Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  showEdit(e) {
    this.userAppService.setDataEdit(e);
    this.userAppService.showEdit(true);
  }
  hapusUser(e) {
    swal({
      title: 'Hapus',
      text: 'Data Akan Dihapus ' + e.fullName,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.userAppService.deleteUser(this.formFilterRole.value.roleUser, e.id)
          .subscribe(resp => {
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Data Berhasil Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
            this.userAppService.getUsers(this.formFilterRole.value.roleUser);
          }, error => {
            swal({
              position: 'top-end',
              type: 'warning',
              title: 'Data Gagal Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
          });
      } else if (result.dismiss === swal.DismissReason.cancel) {
        return false;
      }
    });
  }
}
