import { Component, OnInit } from '@angular/core';
import {UserAppService} from '../user-app.service';
import {Observable} from 'rxjs/Observable';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SelectItem} from 'primeng/api';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import {animationPage} from '../../shared/animation-page';
import { UsersService } from '../../service/users.service';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css'],
  animations: [animationPage]
})
export class AddUserComponent implements OnInit {
  userInfo: any;
  role: any;
  formAdd: FormGroup;
  showThis: boolean;
  listSekolah: any;  optSekolah: SelectItem[];
  constructor(private userService: UsersService,
              private userAppService: UserAppService,
              private fb: FormBuilder) {
    this.userInfo = this.userService.getUserInfo();
    this.formAdd = this.fb.group({
      userName: ['', Validators.required],
      fullName: ['', Validators.required],
      password: ['', Validators.required],
      stages: 1,
      unitId: [''],
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.userAppService.displayAdd$,
      this.userAppService.roldeId$
    );
    combine.subscribe(resp => {
      const [displayAdd, roleId] = resp;
      this.showThis = displayAdd;
      this.role = roleId;
    });
    if (this.userInfo.IsAdmin && (this.userInfo.unitId !== null || this.userInfo.unitId !== 0)) {
      this.formAdd.patchValue({
        unitId : this.userInfo.unitId
      });
    }
      this.getSekolah();
  }
  getSekolah() {
    const temp = [];
    this.userAppService.getUnits()
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  Simpan() {
    if (this.formAdd.valid) {
      this.userAppService.postUser(this.role.id, this.formAdd.value)
        .subscribe(resp => {
          if (resp) {
            this.userAppService.getUsers(this.role.id);
            this.onHide();
            swal({
              position: 'top-end',
              type: 'success',
              title: 'User Berhasil Disimpan',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }, error => {
          swal({
            position: 'top-end',
            type: 'error',
            title: 'User Gagal Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
          return false;
        });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAdd.reset();
    this.userAppService.showAdd(false);
  }
}
