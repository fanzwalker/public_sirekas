import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ApbsRincianService {
  private listRincian = new BehaviorSubject(<any>[]);
  public listRincian$ = this.listRincian.asObservable();
  constructor(private http: HttpClient) { }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getRincian(unitId, stages) {
    return this.http.get(`${environment.url}previews/apbs-rincian/unit/${unitId}/stages/${stages}`)
    .map(resp => <any>resp).subscribe(data => this.setListRincian(data));
  }
  setListRincian(data) {
    return this.listRincian.next(data);
  }
}
