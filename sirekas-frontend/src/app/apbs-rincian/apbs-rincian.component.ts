import { ApbsRincianService } from './apbs-rincian.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { animationPage } from '../shared/animation-page';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { UsersService } from '../service/users.service';
import { ReportService } from '../service/report.service';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import swal from 'sweetalert2';
@Component({
  selector: 'app-apbs-rincian',
  templateUrl: './apbs-rincian.component.html',
  styleUrls: ['./apbs-rincian.component.css'],
  animations: [animationPage]
})
export class ApbsRincianComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  userInfo: any;
  listSekolah: any; optSekolah: SelectItem[];
  listRincian: any;
  itemPrints: MenuItem[];
  unitSelected: any;
  constructor(private fb: FormBuilder,
              private service: ApbsRincianService,
              private userService: UsersService,
              private reportService: ReportService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listRincian$
    );
    combine.subscribe(resp => {
      const [listRincian] = resp;
      this.listRincian = Object.assign(listRincian);
    });
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.onChangeUnit();
    } else {
      this.getSekolah();
    }
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(1, 0, 4, 1, 1)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  onChangeUnit() {
    if (this.formFilter.valid) {
      this.unitSelected = _.find(this.listSekolah, (x) => {
        return x.id === this.formFilter.value.sekolah;
      });
      this.service.getRincian(this.formFilter.value.sekolah, this.userInfo.stages);
    } else {
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Sekolah harus dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnCetak(type) {
    if (this.formFilter.valid) {
      const paramBody = {};
      paramBody['@unitId'] = this.formFilter.value.sekolah;
      paramBody['@stageId'] = +this.userInfo.stages;
      this.reportService.execPrint('ApbsRincian.rpt', type, paramBody)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, 'APBS Rincian');
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  ngOnDestroy() {
    this.service.setListRincian([]);
  }
}
