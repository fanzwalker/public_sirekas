import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Itriwulan } from '../interface/itriwulan';

@Injectable()
export class DashBosnasTriwulanService {
  private listBosnasTriwulan = new BehaviorSubject(<any>[]);
  public listBosnasTriwulan$ = this.listBosnasTriwulan.asObservable();
  constructor(private http: HttpClient) { }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getBosnasTriwulan(unitId, stageId, activityTypeId) {
    return this.http.get(`${environment.url}previews/rka221-tw/unit/${unitId}/stages/${stageId}/activity-type/${activityTypeId}`)
    .map(resp => <Itriwulan[]>resp).subscribe(data => this.setListBosnasTriwulan(data));
  }
  setListBosnasTriwulan(data) {
    return this.listBosnasTriwulan.next(data);
  }
}
