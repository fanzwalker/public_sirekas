import { IunitActivity } from './../interface/iunit-activity';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {GlobalService} from '../global.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/combineLatest';
import {RkaService} from './rka.service';
import {SelectItem} from 'primeng/api';
import * as _ from 'lodash';
import {LookupRekeningService} from '../lookup-rekening/lookup-rekening.service';
import {KomPenyusunService} from './komp-penyusun/kom-penyusun.service';
import {AnggaranKasService} from './anggaran-kas/anggaran-kas.service';
import {PreviewService} from './preview/preview.service';
import {IndikatorKinerjaService} from './indikator-kinerja/indikator-kinerja.service';
import {animationPage} from '../shared/animation-page';
import {UsersService} from '../service/users.service';
import {BudgetType} from '../shared/budget-type';

@Component({
  selector: 'app-rencana-kerja-anggaran',
  templateUrl: './rencana-kerja-anggaran.component.html',
  styleUrls: ['./rencana-kerja-anggaran.component.css'],
  animations: [animationPage]
})
export class RencanaKerjaAnggaranComponent implements OnInit, OnDestroy {
  listSekolah: any; optSekolah: SelectItem[];
  titleLegend = '';
  formFilter: FormGroup;
  userInfo: any;
  dataType: any;
  budgetType: any;
  unitActivitySelected: any;
  constructor(private fb: FormBuilder,
              private rkaService: RkaService,
              private lookupRekeningService: LookupRekeningService,
              private komPenyusunService: KomPenyusunService,
              private anggaranKasService: AnggaranKasService,
              private previewService: PreviewService,
              private indikatorKinerjaService: IndikatorKinerjaService,
              private userService: UsersService) {
    this.rkaService.dataType$.subscribe(resp => this.dataType = resp[0]);
    this.rkaService.budgetType$.subscribe(resp => this.budgetType = resp[0]);
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah : ['', Validators.required],
      program : ['', Validators.required],
      kegiatan: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
    } else {
      this.getSekolah();
    }
    this.getProgram();
  }
  onChangeUnit() {
    this.titleLegend = '';
    if (this.dataType === 1) {
      this.formFilter.patchValue({
        kegiatan: 0
      });
      this.getRkas();
    } else {
      this.getKegiatan();
    }
  }
  getRkas(s?) {
    if (s) {
      this.titleLegend = s.label;
    }
    if (this.formFilter.value.sekolah && (this.formFilter.value.kegiatan || this.formFilter.value.kegiatan === 0)) {
      this.komPenyusunService.getBudgetDet(this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.kegiatan, 0);
      this.anggaranKasService.getBudgetMonthly(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      this.previewService.getPreview(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      if (this.dataType === 2) {
        this.indikatorKinerjaService.getListIndikator(this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.kegiatan);
      }
      this.setUnitProgAct();
      if (this.dataType === 1) {
        this.rkaService.getUnitBudget(this.formFilter.value.sekolah, this.userInfo.stages)
        .subscribe(resp => {
          if (resp.length > 0) {
            this.titleLegend = resp[0].total;
          }
        });
      } else {
        const combine = Observable.combineLatest(
          this.rkaService.getUnitActivity(this.formFilter.value.sekolah, this.userInfo.stages,
            this.formFilter.value.program, this.budgetType),
          this.rkaService.getSaldoAwal(this.formFilter.value.sekolah)
        );

        combine.subscribe(data => {
          const [unitAct, saldo] = data;
          if (unitAct.length > 0) {
            this.titleLegend = this.budgetType === 2 ? unitAct[0].total + (saldo.value || 0) : unitAct[0].total;
          }
        });

        this.rkaService.getUnitActivity(this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.program, this.budgetType)
        .subscribe(resp => {
          if (resp.length > 0) {
            this.titleLegend = resp[0].total;
          }
        });
      }
    } else {
      this.komPenyusunService.setBudgetDet([], 0);
      this.anggaranKasService.setBudgetMonthly([]);
      this.previewService.setPreview([]);
      this.previewService.setUnitProgAct([]);
    }
    this.rkaService.setUnitAct(this.formFilter.value.sekolah, this.formFilter.value.kegiatan);

  }
  setUnitProgAct() {
    const data = {
      Units : _.find(this.listSekolah, (e) => e.id === this.formFilter.value.sekolah),
      Programs: this.formFilter.value.program,
      Activity: this.formFilter.value.kegiatan
    };
    this.previewService.setUnitProgAct(data);
  }
  onChangeProgram() {
     this.getKegiatan();
  }
  onOpentabs() {
    console.log('open tabs');
  }
  getSekolah() {
    const temp = [];
    this.rkaService.getUnits(this.userInfo.stages, 0, 4, 1, this.budgetType)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  getProgram() {
    const temp = [];
    this.rkaService.getPrograms()
      .subscribe(resp => {
        this.formFilter.patchValue({
          program: resp[0].id
        });
        this.onChangeUnit();
      });
  }
  getKegiatan() {
    const temp = [];
    this.rkaService.getKegiatan(this.formFilter.value.sekolah, this.formFilter.value.program, 1, this.budgetType)
      .subscribe(resp => {
        if (resp.length > 0) {
          this.formFilter.patchValue({
            kegiatan: resp[0].activityId
          });
          this.getRkas();
        } else {
          this.komPenyusunService.setBudgetDet([], 0);
          this.anggaranKasService.setBudgetMonthly([]);
          this.previewService.setPreview([]);
          this.previewService.setUnitProgAct([]);
        }
      });
  }
  ngOnDestroy() {
    this.komPenyusunService.showAddRincian(false);
    this.komPenyusunService.setBudgetDet([], 0);
    this.anggaranKasService.setBudgetMonthly([]);
    this.previewService.setPreview([]);
    this.previewService.setUnitProgAct([]);
  }
}
