import { TestBed, inject } from '@angular/core/testing';

import { RkaService } from './rka.service';

describe('RkaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RkaService]
    });
  });

  it('should be created', inject([RkaService], (service: RkaService) => {
    expect(service).toBeTruthy();
  }));
});
