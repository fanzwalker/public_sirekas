import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpRequest} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { IunitActivity } from '../interface/iunit-activity';

@Injectable()
export class RkaService {

  private unitId = new BehaviorSubject<number>(null);
  public unitId$ = this.unitId.asObservable();
  private activityId = new BehaviorSubject<number>(null);
  public activityId$ = this.activityId.asObservable();
  private dataType = new BehaviorSubject<number>(null);
  public dataType$ = this.dataType.asObservable();
  private budgetType = new BehaviorSubject<number>(null);
  public budgetType$ = this.budgetType.asObservable();
  constructor(private http: HttpClient) { }
  setDataType(dataType) {
    // --- 1 = belanja tidak langsung, 2 = belanja lansung
    return this.dataType.next(dataType);
  }
  setBudgetType(budgetType) {
    // 1 = bosad , 2 = bosnas
    return this.budgetType.next(budgetType);
  }
  setUnitAct(unitId, activityId) {
    this.unitId.next(unitId);
    return this.activityId.next(activityId);
  }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getPrograms() {
    return this.http.get(`${environment.url}programs`);
  }
  getKegiatan(unitId, programId, stages, activityType?) {
    const params = new HttpParams()
    .set('activityType', activityType);
    if (activityType) {
      return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/programs/${programId}/activities`, {params: params})
      .map(resp => <IunitActivity[]>resp);
    } else {
      return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/programs/${programId}/activities`)
      .map(resp => <IunitActivity[]>resp);
    }

  }
  getRkaHeader(unitId, activityId) {
    return this.http.get(`${environment.url}units/${unitId}/activities/${activityId}/budgets`);
  }
  setRkaHeader(data: any) {
    return false;
  }
  postRkaHeader(unitId, activityId, dataBody) {
    return this.http.post(`${environment.url}units/${unitId}/activities/${activityId}/budgets`, dataBody);
  }
  getUnitActivity(unitId, stages, programId, budgetType) {
    const queryParam = new HttpParams()
    .set('activityType', budgetType);
    return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/programs/${programId}/activities`, {params: queryParam})
    .map(resp => <any>resp);
  }
  getUnitBudget(unitId, stages) {
    return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/unit-budgets`).map(resp => <any>resp);
  }
  getSaldoAwal(unitId) {
    return this.http.get<any>(`${environment.url}units/${unitId}/last-year-balance`);
  }
}
