import {Component, OnDestroy, OnInit} from '@angular/core';
import {KomPenyusunService} from './kom-penyusun.service';
import {Observable} from 'rxjs/Observable';
import {RkaService} from '../rka.service';
import {IlistBudgetDet} from '../../interface/ilist-budget-det';
import * as _ from 'lodash';
import {AnggaranKasService} from '../anggaran-kas/anggaran-kas.service';
import {PreviewService} from '../preview/preview.service';
import swal from 'sweetalert2';
import { UsersService } from '../../service/users.service';
@Component({
  selector: 'app-komp-penyusun',
  templateUrl: './komp-penyusun.component.html',
  styleUrls: ['./komp-penyusun.component.css']
})
export class KompPenyusunComponent implements OnInit, OnDestroy {
  listRkas: IlistBudgetDet[];
  rkasSelected: any = [];
  unitId: number;
  activityId: number;
  dataType: any;
  userInfo: any;
  listUnitDesc: any[];
  constructor(private komPenyusunService: KomPenyusunService,
              private rkaService: RkaService,
              private anggaranKasService: AnggaranKasService,
              private previewService: PreviewService,
              private userService: UsersService) {
    this.rkaService.dataType$.subscribe(resp => this.dataType = resp[0]);
    this.userInfo = this.userService.getUserInfo();
    }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.rkaService.unitId$,
      this.rkaService.activityId$,
      this.komPenyusunService.listRkas$,
      this.komPenyusunService.listUnitDesc$
    );
    combine.subscribe(resp => {
      const [unitId, activityId, listRkas, listUnitDesc] = resp;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listRkas = Object.assign(<IlistBudgetDet>listRkas);
      this.listUnitDesc = Object.assign(listUnitDesc);
    });
    this.komPenyusunService.getUnitDecs();
  }
  calculateTotal(x, y) {
    let total = 0;
    if (this.listRkas) {
      // tslint:disable-next-line:prefer-const
      for (let data of this.listRkas) {
        switch (x) {
          case data.chartOfAccountCode:
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.total;
            } break;
        }

      }
    }
    return total;
  }
  btnAdd() {
    if (this.unitId && (this.activityId || this.activityId === 0)) {
      this.komPenyusunService.getBudgetSources();
      this.komPenyusunService.getUnitDecs();
      this.komPenyusunService.showAddRincian(true);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah & Kegiatan Harus Di pilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnDelete() {
    if (this.rkasSelected.length > 0) {
      swal({
        title: 'Hapus',
        text: 'Data Akan Dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          const listId = [];
          _.forEach(this.rkasSelected, function (k) {
            listId.push(k.id);
          });
          const dataBody = {
            ids : listId
          };
          this.komPenyusunService.deleteBudgetDet(this.unitId, this.userInfo.stages, this.activityId, dataBody)
            .subscribe(resp => {
              this.rkasSelected = [];
              this.komPenyusunService.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 0);
              this.anggaranKasService.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
              this.previewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
              swal({
                position: 'top-end',
                type: 'success',
                title: 'Data Berhasil Dihapus',
                showConfirmButton: false,
                timer: 3000
              });
            }, error => {
              swal({
                position: 'top-end',
                type: 'error',
                title: 'Data Gagal Dihapus [' + error.statusText + ']',
                showConfirmButton: false,
                timer: 3000
              });
            });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnEdit(e) {
    this.komPenyusunService.setDataEditRincian(e);
    this.komPenyusunService.getBudgetSources();
    this.komPenyusunService.getUnitDecs();
    this.komPenyusunService.showEditRincian(true);
  }
  ngOnDestroy() {
    this.komPenyusunService.setBudgetDet([], 0);
    this.komPenyusunService.setBudgetSources([]);
    this.komPenyusunService.setUnitDesc([]);
  }
  castSatuanUnit(a) {
    if (a) {
      const unitSatuan = _.find(this.listUnitDesc, (s) => {
        return s.id === +a;
      });
      return unitSatuan.name;
    }
  }
}
