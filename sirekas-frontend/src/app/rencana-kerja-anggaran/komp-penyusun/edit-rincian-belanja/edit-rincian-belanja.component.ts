import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RkaService} from '../../rka.service';
import {KomPenyusunService} from '../kom-penyusun.service';
import {Observable} from 'rxjs/Observable';
import {SelectItem} from 'primeng/api';
import * as _ from 'lodash';
import {IputBudgetDet} from '../../../interface/iput-budget-det';
import {AnggaranKasService} from '../../anggaran-kas/anggaran-kas.service';
import {PreviewService} from '../../preview/preview.service';
import swal from 'sweetalert2';
import { UsersService } from '../../../service/users.service';
@Component({
  selector: 'app-edit-rincian-belanja',
  templateUrl: './edit-rincian-belanja.component.html',
  styleUrls: ['./edit-rincian-belanja.component.css']
})
export class EditRincianBelanjaComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  unitId: number;
  activityId: number;
  listSumberDana: any;
  optSumberDana: SelectItem[];
  listUnitDesc: any;
  optUnitDesc: SelectItem[];
  dataEdit: any;
  userInfo: any;
  dataType: any;
  budgetType: any;
  constructor(private fb: FormBuilder,
              private rkaService: RkaService,
              private komPenyusunService: KomPenyusunService,
              private anggaranKasService: AnggaranKasService,
              private previewService: PreviewService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
    this.rkaService.dataType$.subscribe(resp => this.dataType = resp[0]);
    this.rkaService.budgetType$.subscribe(resp => this.budgetType = resp[0]);
    this.formEdit = this.fb.group({
      budgetSourceId: [0, Validators.required],
      description: [null],
      expression: [null],
      unitDesc: [null],
      costPerUnit: [0],
      type: [0],
      expres1: [null],
      expres2: [null],
      expres3: [null],
      expres4: [null],
      expres5: [null],
      expres6: [null],
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.komPenyusunService.displayEdit$,
      this.rkaService.unitId$,
      this.rkaService.activityId$,
      this.komPenyusunService.listBudgetSources$,
      this.komPenyusunService.listUnitDesc$,
      this.komPenyusunService.dataEditRincian$,
    );
    combine.subscribe(resp => {
      const [displayEdit, unitId, activityId, listBudgetSources, listUnitDesc, dataEdit] = resp;
      this.showThis = displayEdit;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listSumberDana = Object.assign(listBudgetSources);
      this.listUnitDesc = Object.assign(listUnitDesc);
      this.dataEdit = Object.assign(dataEdit);
      this.setOptBudgetSources();
      this.setOptUnitDesc();
      if (this.dataEdit.id && this.dataEdit.expression && this.dataEdit.expression !== '-') {
        const hasilangka = this.dataEdit.expression.match(/([\d]+)/g);
        const hasilkata = this.dataEdit.expression.match(/(?=[^\d\s])([^\d\s]*)/g);
        this.formEdit.patchValue({
          budgetSourceId: this.dataEdit.budgetSourceId,
          description: this.dataEdit.description,
          expression: this.dataEdit.expression,
          unitDesc: this.dataEdit.unitDesc,
          costPerUnit: this.dataEdit.costPerUnit,
          type: this.dataEdit.type,
          expres1: [hasilangka[0]],
          expres2: this.getIdUnit(hasilkata[0]),
          expres3: [hasilangka[1]],
          expres4: this.getIdUnit(hasilkata[2]),
          expres5: [hasilangka[2]],
          expres6: this.getIdUnit(hasilkata[4])
        });
      }
    });
  }
  getIdUnit(data) {
    if (this.listUnitDesc.length > 0) {
      const temp = _.find(this.listUnitDesc, (e) => {
        return e.name === data;
      });
      if (temp) {
        return temp.id;
      }
    }
  }
  setOptBudgetSources() {
    this.optSumberDana = [];
    if (this.listSumberDana.length > 0) {
      this.optSumberDana.push({label: '-- Pilih --', value: null});
      _.forEach(this.listSumberDana, e => {
        this.optSumberDana.push({label: e.name, value: e.id});
      });
    } else {
      this.optSumberDana = [{label: '-- Pilih --', value: null}];
    }
  }
  setOptUnitDesc() {
    this.optUnitDesc = [];
    if (this.listUnitDesc.length > 0) {
      this.optUnitDesc.push({label: '-- Pilih --', value: null});
      _.forEach(this.listUnitDesc, e => {
        this.optUnitDesc.push({label: e.name, value: e.id});
      });
    } else {
      this.optUnitDesc = [{label: '-- Pilih --', value: null}];
    }
  }
  btnSimpan() {
    const paramBody = <IputBudgetDet>{};
    paramBody.budgetSourceId = this.dataEdit.budgetSourceId;
    paramBody.costPerUnit = this.formEdit.value.costPerUnit;
    paramBody.description = this.formEdit.value.description;
    paramBody.expression = this.joinExpression();
    paramBody.unitDesc = this.formEdit.value.unitDesc;
    paramBody.type = this.formEdit.value.type;
    this.komPenyusunService.putBudgetDet(this.unitId, this.userInfo.stages, this.activityId, this.dataEdit.id, paramBody).
      subscribe(resp => {
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data Berhasil Diubah',
          showConfirmButton: false,
          timer: 3000
        });
    }, error => {
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Data Gagal Diubah [' + error.statusText + ']',
        showConfirmButton: false,
        timer: 3000
      });
    });
  }
  joinExpression() {
    let ekspresi = '';
    if (this.formEdit.value.expres1[0] !== undefined) {
      ekspresi += this.formEdit.value.expres1 + ' ' + this.findNameUnit(this.formEdit.value.expres2);
      if (this.formEdit.value.expres3[0] !== undefined) {
        ekspresi += ' x ' + this.formEdit.value.expres3 + ' ' + this.findNameUnit(this.formEdit.value.expres4);
        if (this.formEdit.value.expres5[0] !== undefined) {
          ekspresi += ' x ' + this.formEdit.value.expres5 + ' ' + this.findNameUnit(this.formEdit.value.expres6);
        }
      }
    }
    return ekspresi;
    // return this.formEdit.value.expres1 + ' ' + this.findNameUnit(this.formEdit.value.expres2) + ' x ' +
    //   this.formEdit.value.expres3 + ' ' + this.findNameUnit(this.formEdit.value.expres4) + ' x ' +
    //   this.formEdit.value.expres5 + ' ' + this.findNameUnit(this.formEdit.value.expres6);
  }
  findNameUnit(id) {
    if (this.listUnitDesc.length > 0) {
      const unit =  _.find(this.listUnitDesc, (v) => {
        if (v.id === id) { return v; }
      });
      return unit ? unit.name : '';
    }
  }
  btnBatal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.komPenyusunService.setDataEditRincian([]);
    this.komPenyusunService.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 0);
    this.anggaranKasService.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
    this.previewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
    this.komPenyusunService.showEditRincian(false);
  }
}
