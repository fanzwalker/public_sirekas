import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng/api';
import {KomPenyusunService} from '../kom-penyusun.service';
import {Observable} from 'rxjs/Observable';
import {Form, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IlistCoa} from '../../../interface/ilist-coa';
import {IlistBudgetDet} from '../../../interface/ilist-budget-det';
import * as _ from 'lodash';
import {RkaService} from '../../rka.service';
import {IpostBudgetDet} from '../../../interface/ipost-budget-det';
import {AnggaranKasService} from '../../anggaran-kas/anggaran-kas.service';
import {PreviewService} from '../../preview/preview.service';
import swal from 'sweetalert2';
import { UsersService } from '../../../service/users.service';
@Component({
  selector: 'app-add-rincian-belanja',
  templateUrl: './add-rincian-belanja.component.html',
  styleUrls: ['./add-rincian-belanja.component.css']
})
export class AddRincianBelanjaComponent implements OnInit {
  showThis: boolean;
  showThisHeader: boolean;
  coaList: any;
  coaFiltered: any;
  coaSelected: IlistCoa;
  rekening: any;
  listHeader: IlistBudgetDet[];
  optHeader: SelectItem[];
  listSubHeader: IlistBudgetDet[];
  optSubHeader: SelectItem[];
  listSumberDana: any;
  optSumberDana: SelectItem[];
  listUnitDesc: any;
  optUnitDesc: SelectItem[];
  formAdd: FormGroup;
  unitId: number;
  activityId: number;
  formAddHeader: FormGroup;
  listRkas: IlistBudgetDet[];
  typeHeader: number;
  userInfo: any;
  dataType: any;
  budgetType: any;
  constructor(private komPenyusunService: KomPenyusunService,
              private rkaService: RkaService,
              private fb: FormBuilder,
              private anggaranKasService: AnggaranKasService,
              private previewService: PreviewService,
              private userService: UsersService) {
    // this.rkaService.dataType$.subscribe(resp => this.dataType = resp[0]);
    // this.rkaService.budgetType$.subscribe(resp => this.budgetType = resp[0]);
    this.userInfo = this.userService.getUserInfo();
    this.formAdd = this.fb.group({
      headerId: [null, Validators.required],
      subHeaderId: [null, Validators.required],
      parentId: [null],
      chartOfAccountId: [null, Validators.required],
      budgetSourceId: [this.budgetType, Validators.required],
      code: ['', Validators.required],
      description: ['', Validators.required],
      expres1: [null],
      expres2: [null],
      expres3: [null],
      expres4: [null],
      expres5: [null],
      expres6: [null],
      unitDesc: [null],
      costPerUnit: [0],
      type: [2]
    });
    this.formAddHeader = this.fb.group({
      code: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.rkaService.unitId$,
      this.rkaService.activityId$,
      this.komPenyusunService.listRkas$,
      this.komPenyusunService.displayAddRincian$,
      this.komPenyusunService.listBudgetDetHeader$,
      this.komPenyusunService.displayAddHeader$,
      this.komPenyusunService.listBudgetSources$,
      this.komPenyusunService.listUnitDesc$
    );
    combine.subscribe(resp => {
      const [unitId, activityId, listRkas, displayAddRincian, listHeader, displayAddHeader, listBudgetSources, listUnitDesc] = resp;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listRkas = listRkas;
      this.showThis = displayAddRincian;
      this.listHeader = Object.assign(listHeader);
      this.showThisHeader = displayAddHeader;
      this.listSumberDana = Object.assign(listBudgetSources);
      this.listUnitDesc = Object.assign(listUnitDesc);
      this.setOptHeader();
      this.setOptBudgetSources();
      this.setOptUnitDesc();
    });
  }
  searchCoa(e) {
    this.komPenyusunService.searchCoa(e.query, this.dataType === 2 ? 2 : 1)
      .subscribe(resp => {
        this.coaFiltered = Object.assign(resp);
      });
  }
  onSelectCoa(e) {
    this.coaSelected = <IlistCoa>e;
    this.komPenyusunService.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 1, this.coaSelected.id);
    this.formAdd.patchValue({
      chartOfAccountId: this.coaSelected.id
    });
  }
  onChangeHeader() {
    this.setOptSub(this.formAdd.value.headerId);
  }
  setOptHeader() {
    this.optHeader = [];
    if (this.listHeader.length > 0) {
        this.optHeader.push({label: '-- Pilih --', value: null});
      _.forEach(this.listHeader, e => {
        this.optHeader.push({label: e.code + ' - ' + e.description, value: e.id});
      });
    } else {
      this.optHeader = [{label: '-- Pilih --', value: null}];
    }
  }
  setOptBudgetSources() {
    this.optSumberDana = [];
    if (this.listSumberDana.length > 0) {
      this.optSumberDana.push({label: '-- Pilih --', value: null});
      _.forEach(this.listSumberDana, e => {
        this.optSumberDana.push({label: e.name, value: e.id});
      });
    } else {
      this.optSumberDana = [{label: '-- Pilih --', value: null}];
    }
  }
  setOptUnitDesc() {
    this.optUnitDesc = [];
    if (this.listUnitDesc.length > 0) {
      this.optUnitDesc.push({label: '-- Pilih --', value: null});
      _.forEach(this.listUnitDesc, e => {
        this.optUnitDesc.push({label: e.name, value: e.id});
      });
    } else {
      this.optUnitDesc = [{label: '-- Pilih --', value: null}];
    }
  }
  setOptSub(budgetDetId, subHeaderId?) {
    this.listSubHeader = [];
    const temp = [];
    this.komPenyusunService.getBudgetDetChild(this.unitId, this.userInfo.stages, this.activityId, 1, budgetDetId)
      .subscribe(resp => {
        if (resp) {
          this.listSubHeader = Object.assign(resp);
          temp.push({label: '-- Pilih --', value: null});
          _.forEach(this.listSubHeader, function (e) {
            temp.push({label: e.code + ' - ' + e.description, value: e.id});
          });
          this.optSubHeader = Object.assign(temp);
          if (subHeaderId) {
            this.formAdd.patchValue({
              subHeaderId: subHeaderId
            });
          }
        }
      });
  }
  showFormHeader(typeHeader: number) {
    this.typeHeader = typeHeader;
    let lastCode = '';
    if (this.typeHeader === 1) {
      if (this.listHeader.length > 0) {
        const lastHeader = _.last(this.listHeader, (e) => {
          return e;
        });
        const tempCode = lastHeader.code.substr(lastHeader.code.length - 4, 4);
        lastCode = this.generateKode(tempCode);
      } else {
        lastCode = '001.';
      }
    } else if (this.typeHeader === 2) {
      const dataHeader = _.find(this.listHeader, (e) => {
        if (e.id === this.formAdd.value.headerId) {
          return e;
        }
      });
      if (this.listSubHeader.length > 0 ) {
        const lastData = _.last(this.listSubHeader, (e) => {
          return e;
        });
        const tempCode = lastData.code.substr(lastData.code.length - 4, 4);
        lastCode = dataHeader.code + this.generateKode(tempCode);
      } else {
        lastCode = dataHeader.code + '001.';
      }
    }
    this.formAddHeader.patchValue({
      code: lastCode
    });
    this.komPenyusunService.showAddSubHeader(true);
  }
  generateKode(lastCode) {
    let newKode = '';
    const splits = lastCode.split('');
    const numberKode = Number(splits[0] + splits[1] + splits[2]) + 1;
    const lengthKode = numberKode.toString().length;
    if (lengthKode === 1) {
      newKode = '00' + numberKode.toString() + '.';
    } else if (lengthKode === 2) {
      newKode = '0' + numberKode.toString() + '.';
    } else {
      newKode = numberKode.toString() + '.';
    }
    return newKode;
  }
  SimpanHeader() {
    const paramBody: IpostBudgetDet = <IpostBudgetDet>this.formAddHeader.value;
    paramBody.type = 1;
    paramBody.costPerUnit = 0;
    paramBody.unitDesc = null;
    paramBody.expression = null;
    paramBody.budgetSourceId = this.budgetType === 3 ? 1 : this.budgetType;
    paramBody.chartOfAccountId = this.formAdd.value.chartOfAccountId;
    if (this.typeHeader === 1) {
      paramBody.parentId = null;
    } else if (this.typeHeader === 2) {
      paramBody.parentId = this.formAdd.value.headerId;
    }
    this.komPenyusunService.postBudgetDet(this.unitId, this.userInfo.stages, this.activityId, paramBody)
      .subscribe(resp => {
        if (resp) {
          if (this.typeHeader === 1 ) {
            this.setOptSub(resp.id);
            this.onHideAddHeader(resp.id);
          } else if (this.typeHeader === 2) {
            this.setOptSub(this.formAdd.value.headerId, resp.id);
          }
          this.onHideAddHeader();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Header Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }
      });
  }
  BatalHeader() {
    this.onHideAddHeader();
  }
  Simpan() {
    const tempListRkas = Object.assign(this.listRkas);
    const paramBody = <IpostBudgetDet>{};
    paramBody.expression = this.joinExpression();
    paramBody.chartOfAccountId = this.formAdd.value.chartOfAccountId;
    paramBody.budgetSourceId = this.budgetType === 3 ? 1 : this.budgetType;
    paramBody.unitDesc = this.formAdd.value.unitDesc;
    paramBody.costPerUnit = this.formAdd.value.costPerUnit;
    paramBody.description = this.formAdd.value.description;
    paramBody.type = 2;
    if (this.formAdd.value.headerId !== null && this.formAdd.value.subHeaderId !== null) {
      const SubHeader = _.find(this.listSubHeader, (e) => {
        return e.id === this.formAdd.value.subHeaderId;
      });
      const listSubheader = _.filter(this.listRkas, (e) => {
        return e.type === 2 && e.parentId === SubHeader.id;
      });
      paramBody.parentId = SubHeader.id;
      if (listSubheader.length > 0) {
        const lastData = _.last(listSubheader, (x) => {
          return x.type === 2 && x.parentId === SubHeader.id;
        });
        const tempCode = lastData.code.substr(lastData.code.length - 4, 4);
        paramBody.code = SubHeader.code + this.generateKode(tempCode);
      } else {
        paramBody.code = SubHeader.code + '001.';
      }
    } else if (this.formAdd.value.headerId !== null && this.formAdd.value.subHeaderId === null) {
      const Header = _.find(this.listHeader, (e) => {
        return e.id === this.formAdd.value.headerId;
      });
      paramBody.parentId = Header.id;
      const listDet = _.filter(tempListRkas, (x) => {
        return x.type === 2 && x.parentId === Header.id;
      });
      if (listDet.length > 0) {
        const lastData = _.last(listDet, (e) => {
          return e;
        });
        const tempCode = lastData.code.substr(lastData.code.length - 4, 4);
        paramBody.code = Header.code + this.generateKode(tempCode);
      } else {
        paramBody.code = Header.code + '001.';
      }
    } else if (this.formAdd.value.headerId === null && this.formAdd.value.subHeaderId === null) {
      const listDetChild = _.filter(tempListRkas, (e) => {
        return e.type === 2 && e.parentId === null && e.chartOfAccountId === this.formAdd.value.chartOfAccountId;
      });
      if (listDetChild.length > 0) {
        const lastData = _.last(listDetChild, (e) => {
          return e;
        });
        const tempCode = lastData.code.substr(lastData.code.length - 4, 4);
        paramBody.code = tempCode;
      } else {
        paramBody.code = '001.';
      }
    }
    this.okPost(paramBody);

  }
  okPost(paramBody) {
    this.komPenyusunService.postBudgetDet(this.unitId, this.userInfo.stages, this.activityId, paramBody)
      .subscribe(resp => {
        if (resp) {
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Data Gagal Disimpan [' + error.error.Total[0] + ']',
          showConfirmButton: false,
          timer: 3000
        });
      });
  }
  joinExpression() {
    let ekspresi = '';
    if (this.formAdd.value.expres1) {
      ekspresi += this.formAdd.value.expres1 + ' ' + this.findNameUnit(this.formAdd.value.expres2);
      if (this.formAdd.value.expres3) {
        ekspresi += ' x ' + this.formAdd.value.expres3 + ' ' + this.findNameUnit(this.formAdd.value.expres4);
        if (this.formAdd.value.expres5) {
          ekspresi += ' x ' + this.formAdd.value.expres5 + ' ' + this.findNameUnit(this.formAdd.value.expres6);
        }
      }
    }
    return ekspresi;
  }
  findNameUnit(id) {
    if (this.listUnitDesc.length > 0) {
      const unit =  _.find(this.listUnitDesc, (v) => {
        if (v.id === id) { return v; }
      });
      return unit ? unit.name : '';
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAdd.reset();
    this.coaList = '';
    this.komPenyusunService.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 0);
    this.komPenyusunService.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 1);
    this.anggaranKasService.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
    this.previewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
    this.setOptSub(0);
    this.komPenyusunService.showAddRincian(false);
  }
  onHideAddHeader(headerId?) {
    this.formAddHeader.reset();
    this.komPenyusunService.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 1, this.coaSelected.id);
    if (headerId) {
      this.formAdd.patchValue({
        headerId: headerId
      });
    }
    this.optSubHeader = [{label: '-- Pilih --', value: null}];
    this.komPenyusunService.showAddSubHeader(false);
  }
}
