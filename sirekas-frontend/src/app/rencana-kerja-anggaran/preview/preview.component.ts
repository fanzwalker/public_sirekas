import { RkaService } from './../rka.service';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {PreviewService} from './preview.service';
import {IpreviewRkas} from '../../interface/ipreview-rkas';
import {Observable} from 'rxjs/Observable';
import {IunitProgAct} from './iunit-prog-act';
import {IndikatorKinerjaService} from '../indikator-kinerja/indikator-kinerja.service';
import {MenuItem} from 'primeng/api';
import {ReportService} from '../../service/report.service';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit, OnDestroy {
  previews: IpreviewRkas[];
  unitProgAct: IunitProgAct;
  listIndikatorKinerja: any;
  itemPrints: MenuItem[];
  preProgram: any;
  preActivity: any;
  dataType: any;
  constructor(private rkaService: RkaService,
              private previewService: PreviewService,
              private indikatorKinerjaService: IndikatorKinerjaService,
              private reportService: ReportService) {
    // this.rkaService.dataType$.subscribe(resp => this.dataType = resp[0]);
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.previewService.dataPreview$,
      this.previewService.unitProgAct$,
      this.indikatorKinerjaService.listIndikatorKinerja$,
    );
    combine.subscribe(resp => {
      const [dataPreview, unitProgAct, listIndikatorKinerja] = resp;
      this.previews = Object.assign(dataPreview);
      this.unitProgAct = Object.assign(unitProgAct);
      this.listIndikatorKinerja = listIndikatorKinerja;
      if (this.unitProgAct.Programs) {
        this.previewService.getProgramById(this.unitProgAct.Programs)
        .subscribe(dataProgram => this.preProgram = Object.assign(dataProgram));
      }
      if (this.unitProgAct.Activity) {
        this.previewService.getActivityById(this.unitProgAct.Programs, this.unitProgAct.Activity)
        .subscribe(dataActivity => this.preActivity = Object.assign(dataActivity));
      }
    });
  }
  convertIndikator(type) {
    switch (type) {
      case 1:
        return 'Capaian Program';
      case 2:
        return 'Masukan';
      case 3:
        return 'Keluaran';
      case 4:
        return 'Hasil';
    }
  }
  btnCetak(type) {
    this.reportService.execPrint('units.rpt', type)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, 'Tes');
      });
  }
  ngOnDestroy() {
    this.previewService.setPreview([]);
    this.previewService.setUnitProgAct([]);
  }
}
