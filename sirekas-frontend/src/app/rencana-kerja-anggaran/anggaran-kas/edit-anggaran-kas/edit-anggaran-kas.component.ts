import { Component, OnInit } from '@angular/core';
import {RkaService} from '../../rka.service';
import {AnggaranKasService} from '../anggaran-kas.service';
import {Observable} from 'rxjs/Observable';
import {IlistBudgetMonthly} from '../../../interface/ilist-budget-monthly';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PreviewService} from '../../preview/preview.service';
import swal from 'sweetalert2';
import { UsersService } from '../../../service/users.service';
@Component({
  selector: 'app-edit-anggaran-kas',
  templateUrl: './edit-anggaran-kas.component.html',
  styleUrls: ['./edit-anggaran-kas.component.css']
})
export class EditAnggaranKasComponent implements OnInit {
  showThis: boolean;
  unitId: number;
  activityId: number;
  dataEdit: IlistBudgetMonthly;
  formEdit: FormGroup;
  userInfo: any;
  constructor(private fb: FormBuilder,
              private rkaService: RkaService,
              private anggaranKasService: AnggaranKasService,
              private previewService: PreviewService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
    this.formEdit = this.fb.group({
      q1: [0],
      q2: [0],
      q3: [0],
      q4: [0]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.anggaranKasService.displayEditAnggaranKas$,
      this.rkaService.unitId$,
      this.rkaService.activityId$,
      this.anggaranKasService.dataEditAnggaranKas$
    );
    combine.subscribe(resp => {
      const [diplayEdit , unitId, activityId, dataEdit] = resp;
      this.showThis = diplayEdit;
      this.unitId = unitId;
      this.activityId = activityId;
      this.dataEdit = Object.assign(dataEdit);
      if (this.dataEdit.id) {
        this.formEdit.patchValue({
          q1: this.dataEdit.q1,
          q2: this.dataEdit.q2,
          q3: this.dataEdit.q3,
          q4: this.dataEdit.q4
        });
      }
    });
  }
  btnSimpan() {
    this.anggaranKasService.putBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages, this.dataEdit.id, this.formEdit.value)
      .subscribe(resp => {
        if (resp) {
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Diubah',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Data Gagal Disimpan [' + error.error.error[0] + ']',
          showConfirmButton: false,
          timer: 3000
        });
      });

  }
  btnBatal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.anggaranKasService.setDataEditAnggaranKas([]);
    this.anggaranKasService.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
    this.previewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
    this.anggaranKasService.showEdit(false);
  }
}
