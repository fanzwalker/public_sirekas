import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnggaranKasComponent } from './anggaran-kas.component';

describe('AnggaranKasComponent', () => {
  let component: AnggaranKasComponent;
  let fixture: ComponentFixture<AnggaranKasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnggaranKasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnggaranKasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
