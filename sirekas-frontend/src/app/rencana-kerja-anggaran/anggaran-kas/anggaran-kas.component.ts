import {Component, OnDestroy, OnInit} from '@angular/core';
import {RkaService} from '../rka.service';
import {Observable} from 'rxjs/Observable';
import {IlistBudgetMonthly} from '../../interface/ilist-budget-monthly';
import {AnggaranKasService} from './anggaran-kas.service';

@Component({
  selector: 'app-anggaran-kas',
  templateUrl: './anggaran-kas.component.html',
  styleUrls: ['./anggaran-kas.component.css']
})
export class AnggaranKasComponent implements OnInit, OnDestroy {
  unitId: number;
  activityId: number;
  listAnggaranKas: IlistBudgetMonthly[];
  anggaranKasSelected: any = [];
  constructor(private rkaService: RkaService,
              private anggranKasService: AnggaranKasService) { }

  ngOnInit() {
    const combine =  Observable.combineLatest(
      this.rkaService.unitId$,
      this.rkaService.activityId$,
      this.anggranKasService.listAnggaranKas$
    );
    combine.subscribe(resp => {
      const [unitId, activityId, listAnggaranKas] = resp;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listAnggaranKas = listAnggaranKas;
    });
  }
  calculateTotal(x, y) {
    let total = 0;
    if (this.listAnggaranKas) {
      for (let data of this.listAnggaranKas) {
        switch (x) {
          case data.chartOfAccountCode:
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.total;
            } break;
          case 'q1':
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.q1;
            } break;
          case 'q2':
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.q2;
            } break;
          case 'q3':
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.q3;
            } break;
          case 'q4':
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.q4;
            } break;
        }
      }
    }
    return total;
  }
  btnEdit(e) {
    this.anggranKasService.showEdit(true);
    this.anggranKasService.setDataEditAnggaranKas(e);
  }
  btnAdd() {

  }
  ngOnDestroy() {
    this.anggranKasService.setBudgetMonthly([]);
  }
}
