import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RencanaKerjaAnggaranComponent } from './rencana-kerja-anggaran.component';

describe('RencanaKerjaAnggaranComponent', () => {
  let component: RencanaKerjaAnggaranComponent;
  let fixture: ComponentFixture<RencanaKerjaAnggaranComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RencanaKerjaAnggaranComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RencanaKerjaAnggaranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
