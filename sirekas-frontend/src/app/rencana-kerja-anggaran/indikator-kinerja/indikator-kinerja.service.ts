import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {environment} from '../../../environments/environment';

@Injectable()
export class IndikatorKinerjaService {
  private listIndikatorKinerja = new BehaviorSubject([]);
  public listIndikatorKinerja$ = this.listIndikatorKinerja.asObservable();

  constructor(private http: HttpClient) {
  }

  getListIndikator(unitId, stageId, activityId) {
    this.http.get(`${environment.url}units/${unitId}/stages/${stageId}/activities/${activityId}/perf-benchmarks`)
      .map(res => <any[]>res).subscribe(res => this.listIndikatorKinerja.next(res));
  }

  postIndikator(unitId, stageId, activityId, data) {
    return this.http.post(`${environment.url}units/${unitId}/stages/${stageId}/activities/${activityId}/perf-benchmarks`, data);
  }
}
