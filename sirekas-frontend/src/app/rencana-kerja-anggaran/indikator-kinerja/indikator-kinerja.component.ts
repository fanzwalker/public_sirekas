import { UsersService } from './../../service/users.service';
import { Component, OnInit } from '@angular/core';
import {IndikatorKinerjaService} from './indikator-kinerja.service';
import {RkaService} from '../rka.service';
import {IndikatorKinerja} from './indikator-kinerja.model';
import * as _ from 'lodash';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-indikator-kinerja',
  templateUrl: './indikator-kinerja.component.html',
  styleUrls: ['./indikator-kinerja.component.css']
})
export class IndikatorKinerjaComponent implements OnInit {
  listIndikatorKinerja: any;
  unitId;
  activityId;
  userInfo: any;
  constructor(private service: IndikatorKinerjaService, private rkaService: RkaService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listIndikatorKinerja$,
      this.rkaService.unitId$,
      this.rkaService.activityId$
    );

    combine.subscribe(data => {
      const [listIndikatorKinerja, unitId, activityId] = data;
      this.listIndikatorKinerja = listIndikatorKinerja;
      this.unitId = unitId;
      this.activityId = activityId;
    });
  }

  onSave() {
    this.service.postIndikator(this.unitId, this.userInfo.stages, this.activityId,
      _.map(this.listIndikatorKinerja,
          l => <IndikatorKinerja>{ name: l.name, perfBenchmarkType: l.perfBenchmarkType, target: l.target}))
      .subscribe();
  }

  convertIndikator(type) {
    switch (type) {
      case 1:
        return 'Capaian Program';
      case 2:
        return 'Masukan';
      case 3:
        return 'Keluaran';
      case 4:
        return 'Hasil';
    }
  }
}
