export interface IndikatorKinerja {
  perfBenchmarkType: number;
  name: string;
  target: string;
}
