import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { RkaService } from './rencana-kerja-anggaran/rka.service';

@Injectable()
export class GlobalService implements CanActivate {
  private titleComponent = new BehaviorSubject<string>(null);
  public titleComponent$ = this.titleComponent.asObservable();
  constructor(private rkaService: RkaService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const title = route.data['setTitle'] as string;
    const dataType = route.data['dataType'] as number;
    const budgetType = route.data['budgetType'] as number;
    if (dataType) {
      this.rkaService.setDataType(dataType);
      this.rkaService.setBudgetType(budgetType);
    }
    if (title) {
      this.titleComponent.next(title);
    } else {
      this.titleComponent.next('');
    }
    return true;
  }
}
