import { animationPage } from './../shared/animation-page';
import { UsersService } from './../service/users.service';
import { KendaliService } from './../service/kendali.service';
import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../../node_modules/primeng/primeng';
import { ReportService } from '../service/report.service';
import * as _ from 'lodash';
@Component({
  selector: 'app-kendali-bosda',
  templateUrl: './kendali-bosda.component.html',
  styleUrls: ['./kendali-bosda.component.css'],
  animations: [animationPage]
})
export class KendaliBosdaComponent implements OnInit {
  userInfo: any;
  listKendali: any;
  itemPrints: MenuItem[];
  thnAng: any;
  constructor(private service: KendaliService,
              private userService: UsersService,
              private reportService: ReportService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
  }

  ngOnInit() {
    this.getData();
  }
  getData() {
    this.service.getData('KendaliBosda', this.userInfo.stages)
    .subscribe(resp =>  {
      this.listKendali = Object.assign(resp);
      this.thnAng = this.listKendali[0].ThnAngg;
    });
  }
  btnCetak(type) {
    const paramBody = {};
      paramBody['@stageId'] = +this.userInfo.stages;
      this.reportService.execPrint('KendaliBosda.rpt', type, paramBody)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, 'Kendali Bosda');
      });
  }
  calculateTotal(x: string) {
    let total = 0;
    switch (x) {
      case 'Pagu' :
        _.forEach(this.listKendali, (s) => {
          total += s.Pagu;
        });
      break;
      case 'Rka' :
        _.forEach(this.listKendali, (s) => {
          total += s.Rka;
        });
      break;
      case 'Selisih' :
        _.forEach(this.listKendali, (s) => {
          total += s.Selisih;
        });
      break;
    }
    return total;
  }
}
