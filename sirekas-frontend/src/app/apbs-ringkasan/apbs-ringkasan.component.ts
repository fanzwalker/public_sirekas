import { animationPage } from './../shared/animation-page';
import { ApbsRingkasanService } from './apbs-ringkasan.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../service/users.service';
import { ReportService } from '../service/report.service';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-apbs-ringkasan',
  templateUrl: './apbs-ringkasan.component.html',
  styleUrls: ['./apbs-ringkasan.component.css'],
  animations: [animationPage]
})
export class ApbsRingkasanComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  userInfo: any;
  listSekolah: any; optSekolah: SelectItem[];
  listRingkasan: any;
  itemPrints: MenuItem[];
  firstRincian: any;
  constructor(private fb: FormBuilder,
              private service: ApbsRingkasanService,
              private userService: UsersService,
              private reportService: ReportService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listRingkasan$
    );
    combine.subscribe(resp => {
      const [listRingkasan] = resp;
      this.listRingkasan = Object.assign(listRingkasan);
      if (this.listRingkasan.length > 0) {
        this.firstRincian = Object.assign(_.first(this.listRingkasan));
      } else {
        this.firstRincian = Object.assign([]);
      }
    });
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.onChangeUnit();
    } else {
      this.getSekolah();
    }
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(1, 0, 4, 1, 1)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  onChangeUnit() {
    this.service.getRingkasan(this.formFilter.value.sekolah, this.userInfo.stages);
  }
  btnCetak(type) {
    const paramBody = {};
      paramBody['@unitId'] = this.formFilter.value.sekolah;
      paramBody['@stageId'] = +this.userInfo.stages;
      this.reportService.execPrint('ApbsRing.rpt', type, paramBody)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, 'APBS Ringkasan');
      });
  }
  ngOnDestroy() {
    this.service.setListRingkasan([]);
  }
}
