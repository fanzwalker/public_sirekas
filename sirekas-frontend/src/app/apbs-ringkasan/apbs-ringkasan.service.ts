import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class ApbsRingkasanService {
  private listRingkasan = new BehaviorSubject(<any>[]);
  public listRingkasan$ = this.listRingkasan.asObservable();
  constructor(private http: HttpClient) { }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getRingkasan(unitId, stages) {
    const parameters = {};
      parameters['@unitId'] = unitId;
      parameters['@stageId'] = +stages;
    const paramBody = {
      'spName' : 'RingApbs',
      'parameters' : parameters
    };
    return this.http.post(`${environment.url}charts`, paramBody)
    .map(resp => <any>resp).subscribe(data => this.setListRingkasan(data));
  }
  setListRingkasan(data) {
    return this.listRingkasan.next(data);
  }
}
