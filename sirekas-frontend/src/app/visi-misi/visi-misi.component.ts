import { UsersService } from './../service/users.service';
import { EvisiMisi } from './../shared/evisi-misi';
import { animationPage } from './../shared/animation-page';
import { Observable } from 'rxjs/Observable';
import { SelectItem } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { VisiMisiService } from './visi-misi.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import * as _ from 'lodash';
import swal from 'sweetalert2';
@Component({
  selector: 'app-visi-misi',
  templateUrl: './visi-misi.component.html',
  styleUrls: ['./visi-misi.component.css'],
  animations: [animationPage]
})
export class VisiMisiComponent implements OnInit, OnDestroy {
  formFilter:  FormGroup;
  listUnit: any[];
  optUnit: SelectItem[];
  userInfo: any;
  constructor(private service: VisiMisiService,
              private userService: UsersService,
              private fb: FormBuilder) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      unitId: [null, Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listUnit$
    );
    combine.subscribe(resp => {
      const [listUnit] = resp;
      this.listUnit = Object.assign(listUnit);
      if (this.listUnit.length > 0) {
        this.setOptUnit();
      }
    });
    if (this.userInfo.IsAdmin) {
      this.service.getUnits(this.userInfo.stages);
    } else {
      this.formFilter.patchValue({
        unitId: this.userInfo.unitId
      });
      this.onChangeUnit();
    }
  }
  setOptUnit() {
    const temp = [];
    temp.push({label: 'Pilih Sekolah', value: null});
      _.forEach(this.listUnit, (e) => {
        temp.push({label: e.code + ' - ' + e.name, value: e.id});
      });
      this.optUnit = temp;
  }
  onChangeUnit() {
    this.service.setUnitId(this.formFilter.value.unitId);
    if (this.formFilter.value.unitId) {
      this.service.getVisiMisi(this.formFilter.value.unitId, EvisiMisi.Visi);
      this.service.getVisiMisi(this.formFilter.value.unitId, EvisiMisi.Misi);
      this.service.getVisiMisi(this.formFilter.value.unitId, EvisiMisi.Tujuan);
    } else {
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  ngOnDestroy() {
    this.service.setListMisi([]);
    this.service.setListVisi([]);
    this.service.setListTujuan([]);
  }
}
