import { UsersService } from './../../service/users.service';
import { Component, OnInit } from '@angular/core';
import { VisiMisiService } from '../visi-misi.service';
import { Observable } from 'rxjs/Observable';
import { EvisiMisi } from '../../shared/evisi-misi';
import swal from 'sweetalert2';
@Component({
  selector: 'app-misi',
  templateUrl: './misi.component.html',
  styleUrls: ['./misi.component.css']
})
export class MisiComponent implements OnInit {
  listMisi: any;
  misiSelected: any[];
  unitId: number;
  userInfo: any;
  constructor(private service: VisiMisiService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listMisi$,
      this.service.unitId$
    );
    combine.subscribe(resp => {
      const [listMisi, unitId] = resp;
      this.listMisi = Object.assign(listMisi);
      this.unitId = unitId;
    });
  }
  btnAdd() {
    if (this.unitId) {
      this.service.setvisMisType(EvisiMisi.Misi);
      this.service.showAdd(true);
    } else {
      swal({
        position: 'top-end',
        type: 'error',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  btnEdit(e: any) {
    if (e.id) {
      this.service.setvisMisType(EvisiMisi.Misi);
      this.service.setDataEdit(e);
      this.service.showEdit(true);
    }
  }
  btnDelete(e: any) {
    if (this.unitId && e.id) {
      swal({
        title: 'Hapus',
        text:  e.description + ' Akan Dihapus ',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          this.service.deleteVisiMisi(this.unitId, e.id)
          .subscribe(resp => {
            this.service.getVisiMisi(this.unitId, EvisiMisi.Misi);
              swal({
                position: 'top-end',
                type: 'success',
                title: e.description + ' Berhasil Dihapus',
                showConfirmButton: false,
                timer: 3000
              });
          }, error => {
            swal({
              position: 'top-end',
              type: 'error',
              title: e.description + ' Gagal Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
            return false;
          });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    }
  }
}
