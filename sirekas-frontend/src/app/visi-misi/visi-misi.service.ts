import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class VisiMisiService {
  private listUnit = new BehaviorSubject(<any>[]);
  public listUnit$ = this.listUnit.asObservable();
  private listVisi = new BehaviorSubject(<any>[]);
  public listVisi$ = this.listVisi.asObservable();
  private listMisi = new BehaviorSubject(<any>[]);
  public listMisi$ = this.listMisi.asObservable();
  private listTujuan = new BehaviorSubject(<any>[]);
  public listTujuan$ = this.listTujuan.asObservable();
  private visMisType = new BehaviorSubject<number>(null);
  public visMisType$ = this.visMisType.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private unitId = new BehaviorSubject<number>(null);
  public unitId$ = this.unitId.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  constructor(private http: HttpClient) { }
  getUnits(stageId) {
    const queryParams = new HttpParams()
    .set('UnitStructure', '4')
    .set('Stages', stageId)
    .set('ExcludeType', '0');
    return this.http.get(`${environment.url}units`, {params: queryParams})
    .map(resp => <any>resp).subscribe(data => this.setListUnit(data));
  }
  setListUnit(data) {
    return this.listUnit.next(data);
  }
  getVisiMisi(unitId, visMisType) {
    const queryParam = new HttpParams()
    .set('visMisType', visMisType);
    return this.http.get(`${environment.url}units/${unitId}/vismises`, {params: queryParam})
    .map(resp => <any>resp).subscribe(data => {
      if (visMisType === 1) {
        this.setListVisi(data);
      } else if (visMisType === 2) {
        this.setListMisi(data);
      } else {
        this.setListTujuan(data);
      }
    });
  }
  setListVisi(data) {
    return this.listVisi.next(data);
  }
  setListMisi(data) {
    return this.listMisi.next(data);
  }
  setListTujuan(data) {
    return this.listTujuan.next(data);
  }
  postVisiMisi(unitId, postBody) {
    return this.http.post(`${environment.url}units/${unitId}/vismises`, postBody)
    .map(resp => <any>resp);
  }
  setvisMisType(visMisType: number): void {
    this.visMisType.next(visMisType);
  }
  setUnitId(unitId): void {
    this.unitId.next(unitId);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
  putVisiMisi(unitId, id, postBody) {
    return this.http.put(`${environment.url}units/${unitId}/vismises/${id}`, postBody)
    .map(resp => <any>resp);
  }
  deleteVisiMisi(unitId, id) {
    return this.http.request('DELETE', `${environment.url}units/${unitId}/vismises/${id}`)
    .map(resp => <any>resp);
  }
}
