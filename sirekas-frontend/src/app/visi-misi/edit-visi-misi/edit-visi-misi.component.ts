import { EvisiMisi } from './../../shared/evisi-misi';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { VisiMisiService } from '../visi-misi.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-edit-visi-misi',
  templateUrl: './edit-visi-misi.component.html',
  styleUrls: ['./edit-visi-misi.component.css']
})
export class EditVisiMisiComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  visMisType: number;
  unitId: number;
  dataEdit: any;
  title: string;
  constructor(private service: VisiMisiService,
              private fb: FormBuilder) {
    this.formEdit = this.fb.group({
      description: ['', Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayEdit$,
      this.service.visMisType$,
      this.service.unitId$,
      this.service.dataEdit$
    );
    combine.subscribe(resp => {
      const [displayAdd, visMisType, unitId, dataEdit] = resp;
      this.showThis = displayAdd;
      this.visMisType = visMisType;
      this.unitId = unitId;
      this.dataEdit = Object.assign(dataEdit);
      this.formEdit.patchValue({
        description: this.dataEdit.description
      });
      this.title = EvisiMisi[visMisType];
    });
  }
  Simpan() {
    if (this.formEdit.valid) {
      this.service.putVisiMisi(this.unitId, this.dataEdit.id, this.formEdit.value)
      .subscribe(resp => {
        if (resp.id) {
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Diubah',
            showConfirmButton: false,
            timer: 3000
          });
        }
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.service.getVisiMisi(this.unitId, this.visMisType);
    this.service.showEdit(false);
  }
}
