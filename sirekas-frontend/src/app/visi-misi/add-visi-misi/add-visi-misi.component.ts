import { Observable } from 'rxjs/Observable';
import { VisiMisiService } from './../visi-misi.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import { EvisiMisi } from '../../shared/evisi-misi';
@Component({
  selector: 'app-add-visi-misi',
  templateUrl: './add-visi-misi.component.html',
  styleUrls: ['./add-visi-misi.component.css']
})
export class AddVisiMisiComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  visMisType: number;
  unitId: number;
  title: string;
  constructor(private service: VisiMisiService,
              private fb: FormBuilder) {
    this.formAdd = this.fb.group({
      type: [null, Validators.required],
      description: ['', Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayAdd$,
      this.service.visMisType$,
      this.service.unitId$
    );
    combine.subscribe(resp => {
      const [displayAdd, visMisType, unitId] = resp;
      this.showThis = displayAdd;
      this.visMisType = visMisType;
      this.unitId = unitId;
      this.formAdd.patchValue({
        type: this.visMisType
      });
      this.title = EvisiMisi[visMisType];
    });
  }
  Simpan() {
    if (this.formAdd.valid) {
      this.service.postVisiMisi(this.unitId, this.formAdd.value)
      .subscribe(resp => {
        if (resp.id) {
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAdd.reset();
    this.service.getVisiMisi(this.unitId, this.visMisType);
    this.service.showAdd(false);
  }
}
