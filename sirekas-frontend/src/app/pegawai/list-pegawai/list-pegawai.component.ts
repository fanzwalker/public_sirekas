import {Component, OnDestroy, OnInit} from '@angular/core';
import {JabatanService} from '../jabatan/jabatan.service';
import {Observable} from 'rxjs/Observable';
import {IlistEmployee} from '../../interface/ilist-employee';
import {EmployeeService} from '../employee.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-list-pegawai',
  templateUrl: './list-pegawai.component.html',
  styleUrls: ['./list-pegawai.component.css']
})
export class ListPegawaiComponent implements OnInit, OnDestroy {
  jabatanSelected: any;
  listPegawai: IlistEmployee[];
  unitId: number;
  constructor(private jabatanService: JabatanService,
              private employeeService: EmployeeService) {}

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.jabatanService.jabatanSelected$,
      this.employeeService.listPegawai$,
      this.employeeService.unitId$
    );
    combine.subscribe(resp => {
      const [jabatanSelected, listPegawai, unitId] = resp;
      this.jabatanSelected = jabatanSelected;
      this.listPegawai = Object.assign(listPegawai);
      this.unitId = unitId;
    });
  }
  btnEdit(e) {
    this.employeeService.setPegawaiSelected(e);
    this.employeeService.showEdit(true);
  }
  showAdd() {
    if (this.jabatanSelected.id) {
      this.employeeService.showAdd(true);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Jabatan Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });

    }
  }
  btnHapus(e) {
    swal({
      title: 'Hapus',
      text: 'Data Akan Dihapus ' + e.name,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.employeeService.deleteEmployee(this.unitId, this.jabatanSelected.id, e.id)
          .subscribe(resp => {
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Data Berhasil Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
            this.employeeService.getlistPegawai(this.unitId, this.jabatanSelected.id);
          }, error => {
            swal({
              position: 'top-end',
              type: 'warning',
              title: 'Data Gagal Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
          });
      } else if (result.dismiss === swal.DismissReason.cancel) {
        return false;
      }
    });
  }
  ngOnDestroy() {
    this.employeeService.setListPegawai([]);
    this.jabatanService.setJabatanSelected([]);
    this.employeeService.setUnitId(null);
  }
}
