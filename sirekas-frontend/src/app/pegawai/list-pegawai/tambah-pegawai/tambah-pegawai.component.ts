import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../../employee.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {JabatanService} from '../../jabatan/jabatan.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-tambah-pegawai',
  templateUrl: './tambah-pegawai.component.html',
  styleUrls: ['./tambah-pegawai.component.css']
})
export class TambahPegawaiComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  unitId: number;
  jabatanSelected: any;
  constructor(private employeeService: EmployeeService,
              private jabatanService: JabatanService,
              private fb: FormBuilder) {
    this.formAdd = this.fb.group({
      nip: ['', Validators.required],
      name: ['', Validators.required],
      jobDesc: ['', Validators.required],
      signer: [0, Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.employeeService.displayAdd$,
      this.employeeService.unitId$,
      this.jabatanService.jabatanSelected$,
    );
    combine.subscribe(resp => {
      const [displayAdd, unitId, jabatanSelected] = resp;
      this.showThis = displayAdd;
      this.unitId = unitId;
      this.jabatanSelected = Object.assign(jabatanSelected);
    });
  }
  Simpan() {
    if (this.formAdd.valid) {
      this.employeeService.postEmployee(this.unitId, this.jabatanSelected.id, this.formAdd.value)
        .subscribe(resp => {
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }, error => {
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Data Gagal Disimpan [' + error.statusText + ']',
            showConfirmButton: false,
            timer: 3000
          });
        });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAdd.reset();
    this.employeeService.getlistPegawai(this.unitId, this.jabatanSelected.id);
    this.employeeService.showAdd(false);
  }
}
