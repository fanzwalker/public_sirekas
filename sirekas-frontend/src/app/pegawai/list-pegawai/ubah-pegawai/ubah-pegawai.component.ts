import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {JabatanService} from '../../jabatan/jabatan.service';
import {EmployeeService} from '../../employee.service';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import swal from 'sweetalert2';
@Component({
  selector: 'app-ubah-pegawai',
  templateUrl: './ubah-pegawai.component.html',
  styleUrls: ['./ubah-pegawai.component.css']
})
export class UbahPegawaiComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  unitId: number;
  jabatanSelected: any;
  employeSelected: any;
  constructor(private fb: FormBuilder,
              private employeeService: EmployeeService,
              private jabatanService: JabatanService) {
    this.formEdit = this.fb.group({
      nip: ['', Validators.required],
      name: ['', Validators.required],
      jobDesc: ['', Validators.required],
      signer: [0, Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.employeeService.displayEdit$,
      this.employeeService.unitId$,
      this.jabatanService.jabatanSelected$,
      this.employeeService.pegawaiSelected$
    );
    combine.subscribe(resp => {
      const [displayEdit, unitId, jabatanSelected, pegawaiSelected] = resp;
      this.showThis = displayEdit;
      this.unitId = unitId;
      this.jabatanSelected = Object.assign(jabatanSelected);
      this.employeSelected = Object.assign(pegawaiSelected);
      if (this.employeSelected.id) {
        this.formEdit.patchValue({
          nip : this.employeSelected.nip,
          name : this.employeSelected.name,
          jobDesc: this.employeSelected.jobDesc,
          signer : this.employeSelected.signer
        });
      }
    });
  }
  Simpan() {
    if (this.formEdit.valid) {
      this.employeeService.putEmployee(this.unitId, this.jabatanSelected.id, this.employeSelected.id, this.formEdit.value)
        .subscribe(resp => {
          if (resp) {
            this.employeeService.getlistPegawai(this.unitId, this.jabatanSelected.id);
            this.onHide();
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Data Pegawai Berhasil Diubah',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }, error => {
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Data Pegawai Gagal Diubah [' + error.statusText + ']',
            showConfirmButton: false,
            timer: 3000
          });
          return false;
        });
    } else {
      console.log('wew');
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.employeeService.getlistPegawai(this.unitId, this.jabatanSelected.id);
    this.employeeService.showEdit(false);
  }
}
