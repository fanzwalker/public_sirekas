import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UbahPegawaiComponent } from './ubah-pegawai.component';

describe('UbahPegawaiComponent', () => {
  let component: UbahPegawaiComponent;
  let fixture: ComponentFixture<UbahPegawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UbahPegawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UbahPegawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
