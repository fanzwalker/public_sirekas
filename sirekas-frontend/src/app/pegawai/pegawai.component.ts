import {Component, OnDestroy, OnInit} from '@angular/core';
import {SelectItem} from 'primeng/api';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as _ from 'lodash';
import {UsersService} from '../service/users.service';

import {animationPage} from '../shared/animation-page';
import {EmployeeService} from './employee.service';
import {ActivatedRoute} from '@angular/router';
import {ISubscription} from 'rxjs/Subscription';
import {JabatanService} from "./jabatan/jabatan.service";

@Component({
  selector: 'app-pegawai',
  templateUrl: './pegawai.component.html',
  styleUrls: ['./pegawai.component.css'],
  animations: [animationPage]
})
export class PegawaiComponent implements OnInit, OnDestroy {
  listSekolah: any; optSekolah: SelectItem[];
  formFilter: FormGroup;
  userInfo: any;
  unitStructure: number;
  sub: ISubscription;

  constructor(private employeeService: EmployeeService,
              private jabatanService: JabatanService,
              private fb: FormBuilder,
              private userService: UsersService,
              private route: ActivatedRoute) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah : ['', Validators.required]
    });
  }

  ngOnInit() {
    this.sub = this.route.queryParams.subscribe(params => {
      this.unitStructure = +params['unitStructure'] || 0;
      if (!this.userInfo.IsAdmin || this.unitStructure === 3) {
        this.formFilter.patchValue({
          sekolah: this.userInfo.unitId || 0
        });
        this.onChangeUnit();
      }
      this.getSekolah();
      this.jabatanService.getJobs(this.unitStructure);
    });
  }
  getSekolah() {
    const temp = [];
    this.employeeService.getUnits()
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  onChangeUnit() {
    this.employeeService.setUnitId(this.formFilter.value.sekolah);
  }
  ngOnDestroy() {
    this.employeeService.setUnitId(null);
    this.sub.unsubscribe();
  }
}
