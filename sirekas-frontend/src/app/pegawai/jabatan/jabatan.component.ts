import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {JabatanService} from './jabatan.service';
import {Observable} from 'rxjs/Observable';
import {IlistJobs} from '../../interface/ilist-jobs';
import {animationPage} from '../../shared/animation-page';
import {EmployeeService} from '../employee.service';
import swal from 'sweetalert2';
@Component({
  selector: 'app-jabatan',
  templateUrl: './jabatan.component.html',
  styleUrls: ['./jabatan.component.css'],
  animations: [animationPage]
})
export class JabatanComponent implements OnInit, OnDestroy {
  unitId: number;
  listJobs: IlistJobs[];
  constructor(private employeeService: EmployeeService,
              private jabatanService: JabatanService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.employeeService.unitId$,
      this.jabatanService.listJabatan$
    );
    combine.subscribe(resp => {
      const [unitId, jabatan] = resp;
      this.unitId = unitId;
      this.listJobs = jabatan;
    });
    // this.getJobs();
  }
  // getJobs() {
  //   this.jabatanService.getJobs()
  //     .subscribe(resp => this.listJobs = Object.assign(resp));
  // }
  ngOnDestroy() {
    this.employeeService.setUnitId(null);
  }
  rowSelected(e) {
    if (this.unitId || this.unitId === 0) {
      this.jabatanService.setJabatanSelected(e.data);
      this.employeeService.getlistPegawai(this.unitId, e.data.id);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
}
