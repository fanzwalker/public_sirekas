import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable()
export class JabatanService {
  private listJabatan = new BehaviorSubject(<any>[]);
  public listJabatan$ = this.listJabatan.asObservable();
  private jabatanSelected = new BehaviorSubject(<any>[]);
  public jabatanSelected$  = this.jabatanSelected.asObservable();
  constructor(private http: HttpClient) { }

  getJobs(unitStructure) {
    const params = new HttpParams()
      .set('unitStructure', unitStructure)
    this.http.get(`${environment.url}job-titles`, {params: params}).subscribe(res => {
      this.listJabatan.next(res);
    });
  }

  setJabatanSelected(data) {
    return this.jabatanSelected.next(data);
  }
}
