import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class EmployeeService {
  private unitId = new BehaviorSubject<number>(null);
  public unitId$ = this.unitId.asObservable();
  private listPegawai = new BehaviorSubject(<any>[]);
  public listPegawai$ = this.listPegawai.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private pegawaiSelected = new BehaviorSubject(<any>[]);
  public pegawaiSelected$ = this.pegawaiSelected.asObservable();
  constructor(private http: HttpClient) { }
  getUnits() {
    return this.http.get(`${environment.url}units`);
  }
  setUnitId(unitId: number) {
    return this.unitId.next(unitId);
  }
  setListPegawai(data) {
    return this.listPegawai.next(data);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
  setPegawaiSelected(data) {
    return this.pegawaiSelected.next(data);
  }
  getlistPegawai(unitId, jobId) {
    return this.http.get(`${environment.url}units/${unitId}/job-titles/${jobId}/employees`)
      .map(resp => resp).subscribe(data => this.setListPegawai(data));
  }
  postEmployee(unitId, jobId, paramBody) {
    return this.http.post(`${environment.url}units/${unitId}/job-titles/${jobId}/employees`, paramBody);
  }
  putEmployee(unitId, jobId, employeeId, paramBody) {
    return this.http.put(`${environment.url}units/${unitId}/job-titles/${jobId}/employees/${employeeId}`, paramBody);
  }
  deleteEmployee(unitId, jobId, employeeId) {
    return this.http.request('DELETE', `${environment.url}units/${unitId}/job-titles/${jobId}/employees/${employeeId}`);
  }
}
