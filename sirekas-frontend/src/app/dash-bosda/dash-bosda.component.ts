import { DashBosdaService } from './dash-bosda.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { animationPage } from '../shared/animation-page';
import { UsersService } from '../service/users.service';
import { SelectItem, MenuItem } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IpreviewRkas } from '../interface/ipreview-rkas';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import { ReportService } from '../service/report.service';
import { IndikatorKenerjaGlobalService } from '../shared/indikator-kenerja-global.service';

@Component({
  selector: 'app-dash-bosda',
  templateUrl: './dash-bosda.component.html',
  styleUrls: ['./dash-bosda.component.css'],
  animations: [animationPage]
})
export class DashBosdaComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  userInfo: any;
  listSekolah: any; optSekolah: SelectItem[];
  previews: IpreviewRkas[];
  listIndikatorKinerja: any;
  Programs: any;
  unitActivity: any;
  itemPrints: MenuItem[];
  total: number;
  constructor(private fb: FormBuilder,
              private service: DashBosdaService,
              private userService: UsersService,
              private indikatorKinerjaService: IndikatorKenerjaGlobalService,
              private reportService: ReportService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required],
      program: [null, Validators.required],
      kegiatan: [null, Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.dataPreview$,
      this.indikatorKinerjaService.ListPerfBenchmarkFixed$
    );
    combine.subscribe(resp => {
      const [dataPreview, listIndikatorKinerja] = resp;
      this.previews = Object.assign(dataPreview);
      this.listIndikatorKinerja = Object.assign(listIndikatorKinerja);
      this.total = _.sum(_.filter(this.previews, e => e.type === 2)
        .map(e => e.total));
    });
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
    } else {
      this.getSekolah();
    }
    this.getProgram();
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(this.userInfo.stages, 0, 4, 1, 1)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  getProgram() {
    const temp = [];
    this.service.getPrograms()
      .subscribe(resp => {
        if (resp.length > 0) {
          this.formFilter.patchValue({
            program: resp[0].id
          });
          this.Programs = Object.assign(resp[0]);
        } else {
          this.Programs = [];
        }
        this.onChangeUnit();
      });
  }
  onChangeUnit() {
    this.service.getUnitActivity(this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.program, 1)
    .subscribe(data => {
      if (data.length > 0 ) {
        this.formFilter.patchValue({
          kegiatan: data[0].activityId
        });
        this.unitActivity = Object.assign(data[0]);
        this.indikatorKinerjaService.getPerfBenchmarkFixed(2, this.formFilter.value.sekolah,
          this.userInfo.stages, this.formFilter.value.kegiatan);
        this.service.getPreview(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      } else {
        this.unitActivity = Object.assign([]);
        this.service.setPreview([]);
        this.service.setListIndikatorKinerja([]);
      }
    });
  }
  convertIndikator(type) {
    switch (type) {
      case 1:
        return 'Capaian Program';
      case 2:
        return 'Masukan';
      case 3:
        return 'Keluaran';
      case 4:
        return 'Hasil';
    }
  }
  btnCetak(type) {
    const paramBody = {};
    paramBody['@unitId'] = this.unitActivity.unitId;
    paramBody['@activityId'] = this.unitActivity.activityId;
    paramBody['@stageId'] = +this.userInfo.stages;
    this.reportService.execPrint('Rka221.rpt', type, paramBody)
    .subscribe(resp =>  {
      this.reportService.extractData(resp, type, 'RKA221');
    });
  }
  ngOnDestroy() {
    this.unitActivity = Object.assign([]);
    this.service.setPreview([]);
    this.indikatorKinerjaService.setListPerfBenchmarkFixed([]);
  }
}
