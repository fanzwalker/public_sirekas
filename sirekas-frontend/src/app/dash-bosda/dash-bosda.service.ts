import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DashBosdaService {
  private dataPreview = new BehaviorSubject(<any>[]);
  public dataPreview$ = this.dataPreview.asObservable();
  private listIndikatorKinerja = new BehaviorSubject([]);
  public listIndikatorKinerja$ = this.listIndikatorKinerja.asObservable();
  constructor(private http: HttpClient) { }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getPrograms() {
    return this.http.get(`${environment.url}programs`).map(resp => <any>resp);
  }
  getUnitActivity(unitId, stagesId, programId, activityType) {
    const queryParam = new HttpParams()
    .set('activityType', activityType);
    return this.http.get(`${environment.url}units/${unitId}/stages/${stagesId}/programs/${programId}/activities`, {params: queryParam})
    .map(resp => <any>resp);
  }
  getPreview(unitId, activityId, stageId) {
    return this.http.get(`${environment.url}units/${unitId}/activities/${activityId}/stages/${stageId}/rka221-previews`)
      .map(resp => resp).subscribe(data => this.setPreview(data));
  }
  setPreview(data) {
    return this.dataPreview.next(data);
  }
  getListIndikator(unitId, stageId, activityId) {
    this.http.get(`${environment.url}units/${unitId}/stages/${stageId}/activities/${activityId}/perf-benchmarks`)
      .map(res => <any[]>res).subscribe(res => this.listIndikatorKinerja.next(res));
  }
  setListIndikatorKinerja(data) {
    this.listIndikatorKinerja.next(data);
  }
}
