import { animationPage } from './../shared/animation-page';
import { ApbsVisiMisiService } from './apbs-visi-misi.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem, MenuItem } from 'primeng/primeng';
import { UsersService } from '../service/users.service';
import { ReportService } from '../service/report.service';
import swal from 'sweetalert2';
import * as _ from 'lodash';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-apbs-visi-misi',
  templateUrl: './apbs-visi-misi.component.html',
  styleUrls: ['./apbs-visi-misi.component.css'],
  animations: [animationPage]
})
export class ApbsVisiMisiComponent implements OnInit {
  datas: any;
  userInfo: any;
  formFilter: FormGroup;
  listUnit: any[];
  optUnit: SelectItem[];
  itemPrints: MenuItem[];
  lembarType: any;
  constructor(private userService: UsersService,
              private service: ApbsVisiMisiService,
              private reportService: ReportService,
              private fb: FormBuilder) {
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: (x) => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      unitId: [null, Validators.required],
      tglSah: [new Date()]
    });
    this.datas = [
      {'desc' : 'VISI, MISI & TUJUAN', 'lembarType': 1}, // 1 = visi misi
      {'desc' : 'DATA UMUM SEKOLAH', 'lembarType': 2} // 2 = data umum
    ];
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listUnit$
    );
    combine.subscribe(resp => {
      const [listUnit] = resp;
      this.listUnit = Object.assign(listUnit);
      if (this.listUnit.length > 0) {
        this.setOptUnit();
      }
    });
    if (this.userInfo.IsAdmin) {
      this.service.getUnits();
    } else {
      this.formFilter.patchValue({
        unitId: this.userInfo.unitId
      });
    }
  }
  setOptUnit() {
    const temp = [];
    temp.push({label: 'Pilih Sekolah', value: null});
      _.forEach(this.listUnit, (e) => {
        temp.push({label: e.code + ' - ' + e.name, value: e.id});
      });
      this.optUnit = temp;
  }
  setLembarType(x) {
    this.lembarType = x;
  }
  btnCetak(type) {
    if (this.formFilter.valid) {
      const paramBody = {};
      paramBody['@unitId'] = this.formFilter.value.unitId;
      paramBody['@tglSah'] = this.getFormattedDate(this.formFilter.value.tglSah);
      this.reportService.execPrint(this.lembarType === 1 ? 'VisiMisi.rpt' : 'KeadaanPeg.rpt', type, paramBody)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, this.lembarType === 1 ? 'Visi Misi & Tujuan' : 'Data Umum');
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  getFormattedDate(date) {
    const year = date.getFullYear();
    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    let day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  }
}
