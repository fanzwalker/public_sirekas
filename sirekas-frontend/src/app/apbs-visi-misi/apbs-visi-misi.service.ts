import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../environments/environment';

@Injectable()
export class ApbsVisiMisiService {
  private listUnit = new BehaviorSubject(<any>[]);
  public listUnit$ = this.listUnit.asObservable();
  constructor(private http: HttpClient) { }
  getUnits() {
    const queryParams = new HttpParams()
    .set('UnitStructure', '4')
    .set('Stages', '1')
    .set('ExcludeType', '0');
    return this.http.get(`${environment.url}units`, {params: queryParams})
    .map(resp => <any>resp).subscribe(data => this.setListUnit(data));
  }
  setListUnit(data) {
    return this.listUnit.next(data);
  }
}
