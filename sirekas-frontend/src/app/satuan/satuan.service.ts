import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export class SatuanService {
  private listSatuan = new BehaviorSubject(<any>[]);
  public listSatuan$ = this.listSatuan.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  constructor(private http: HttpClient) { }
  getSatuan() {
    return this.http.get(`${environment.url}unit-descs`).map(resp => resp)
    .subscribe(data => this.setListSatuan(data));
  }
  setListSatuan(data) {
    return this.listSatuan.next(data);
  }
  postSatuan(paramBody) {
    return this.http.post(`${environment.url}unit-descs`, paramBody);
  }
  putSatuan(idUnitDesc, paramBody) {
    return this.http.put(`${environment.url}unit-descs/${idUnitDesc}`, paramBody);
  }
  deleteSatuan(idUnitDesc) {
    const queryParam = new HttpParams().set('id', idUnitDesc);
    return this.http.request('DELETE', `${environment.url}unit-descs`, {params: queryParam});
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
}
