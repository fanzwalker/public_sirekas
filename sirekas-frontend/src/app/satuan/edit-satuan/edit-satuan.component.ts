import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { animationPage } from '../../shared/animation-page';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SatuanService } from '../satuan.service';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-edit-satuan',
  templateUrl: './edit-satuan.component.html',
  styleUrls: ['./edit-satuan.component.css'],
  animations: [animationPage]
})
export class EditSatuanComponent implements OnInit {
  showThis: boolean;
  dataEdit: any;
  formEdit: FormGroup;
  constructor(private fb: FormBuilder,
              private satuanService: SatuanService) {
                this.formEdit = this.fb.group({
                  name : ['', Validators.required]
                });
              }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.satuanService.displayEdit$,
      this.satuanService.dataEdit$
    );
    combine.subscribe(resp => {
      const [displayEdit, dataEdit] = resp;
      this.showThis = displayEdit;
      this.dataEdit = dataEdit;
      if (this.dataEdit.id) {
        this.formEdit.patchValue({
          name : this.dataEdit.name
        });
      }
    });
  }
  Simpan() {
    if (this.formEdit.valid) {
      this.satuanService.putSatuan(this.dataEdit.id, this.formEdit.value)
      .subscribe(resp => {
        this.satuanService.getSatuan();
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Satuan Berhasil Ubah',
          showConfirmButton: false,
          timer: 3000
        });
      }, error => {
        console.log(error.error.error);
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.satuanService.showEdit(false);
  }
}
