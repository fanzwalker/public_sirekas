import { Component, OnInit } from '@angular/core';
import { animationPage } from '../shared/animation-page';
import { SatuanService } from './satuan.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
@Component({
  selector: 'app-satuan',
  templateUrl: './satuan.component.html',
  styleUrls: ['./satuan.component.css'],
  animations: [animationPage]
})
export class SatuanComponent implements OnInit {
  listSatuan: any;
  constructor(private satuanService: SatuanService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.satuanService.listSatuan$
    );
    combine.subscribe(resp => {
      const [listSatuan] = resp;
      this.listSatuan = listSatuan;
    });
    this.getSatuan();
  }
  getSatuan() {
    this.satuanService.getSatuan();
  }
  showAdd() {
    this.satuanService.showAdd(true);
  }
  showEdit(e) {
    this.satuanService.setDataEdit(e);
    this.satuanService.showEdit(true);
  }
  Hapus(e) {
    swal({
      title: 'Hapus',
      text: 'Data Akan Dihapus ' + e.name,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.satuanService.deleteSatuan(e.id)
        .subscribe(resp => {
          if (resp) {
            this.satuanService.getSatuan();
            swal({
              position: 'top-end',
              type: 'success',
              title: e.name + ' Berhasil Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }, error => {
          swal({
            position: 'top-end',
            type: 'error',
            title: e.name + ' Gagal Dihapus',
            showConfirmButton: false,
            timer: 3000
          });
          return false;
        });
      } else if (result.dismiss === swal.DismissReason.cancel) {
        return false;
      }
    });
  }
}
