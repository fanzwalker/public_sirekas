import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SatuanService } from '../satuan.service';
import { Observable } from 'rxjs/Observable';
import { animationPage } from '../../shared/animation-page';
@Component({
  selector: 'app-add-satuan',
  templateUrl: './add-satuan.component.html',
  styleUrls: ['./add-satuan.component.css'],
  animations: [animationPage]
})
export class AddSatuanComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  constructor(private satuanService: SatuanService,
              private fb: FormBuilder) {
                this.formAdd = this.fb.group({
                  name: ['', Validators.required]
                });
              }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.satuanService.displayAdd$
    );
    combine.subscribe(resp => {
      const [displaAdd] = resp;
      this.showThis = displaAdd;
    });
  }
  Simpan() {
    if (this.formAdd.valid) {
      this.satuanService.postSatuan(this.formAdd.value)
      .subscribe(resp => {
        this.satuanService.getSatuan();
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Satuan Berhasil Disimpan',
          showConfirmButton: false,
          timer: 3000
        });
      }, error => {
        console.log(error.error.error);
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAdd.reset();
    this.satuanService.showAdd(false);
  }

}
