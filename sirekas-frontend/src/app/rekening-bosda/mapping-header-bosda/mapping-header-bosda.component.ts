import { Observable } from 'rxjs/Observable';
import { MappingHeaderBosdaService } from './mapping-header-bosda.service';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import * as _ from 'lodash';
@Component({
  selector: 'app-mapping-header-bosda',
  templateUrl: './mapping-header-bosda.component.html',
  styleUrls: ['./mapping-header-bosda.component.css']
})
export class MappingHeaderBosdaComponent implements OnInit {
  bosdaSelected: any;
  listBosdaCoaHeader: any;
  bosdaCoaHeaderSelected: any = [];
  constructor(private service: MappingHeaderBosdaService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.bosdaSelected$,
      this.service.listBosdaCoaHeader$
    );
    combine.subscribe(resp => {
      const [bosdaSelected, listBosdaCoaHeader] = resp;
      this.bosdaSelected = Object.assign(bosdaSelected);
      this.listBosdaCoaHeader = Object.assign(listBosdaCoaHeader);
    });
  }
  formEdit(e) {
    this.service.setDataEdit(e);
    this.showAddEdit(2);
  }
  showAddEdit(formMode: number) {
    if (formMode === 1) {
      this.service.setDataEdit([]);
    }
    this.service.showAddEdit(true, formMode);
  }
  Delete() {
    if (this.bosdaCoaHeaderSelected.length > 0) {
      swal({
        title: 'Hapus',
        text: 'Data Akan Dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          const listId = [];
          _.forEach(this.bosdaCoaHeaderSelected, function (k) {
            listId.push(k.id);
          });
          const dataBody = {
            ids : listId
          };
          this.service.deleteBosdaCoaHeader(this.bosdaSelected.id, dataBody)
          .subscribe(resp => {
              this.service.getBosdaCoaHeader(this.bosdaSelected.id);
              swal({
                position: 'top-end',
                type: 'success',
                title: 'Berhasil Dihapus',
                showConfirmButton: false,
                timer: 3000
              });
          }, error => {
            swal({
              position: 'top-end',
              type: 'error',
              title: 'Gagal Dihapus ' + error.error.error[0],
              showConfirmButton: false,
              timer: 3000
            });
            return false;
          });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
}
