import { Observable } from 'rxjs/Observable';
import { MappingHeaderBosdaService } from './../mapping-header-bosda.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import swal from 'sweetalert2';
@Component({
  selector: 'app-add-mapping-header',
  templateUrl: './add-mapping-header.component.html',
  styleUrls: ['./add-mapping-header.component.css']
})
export class AddMappingHeaderComponent implements OnInit {
  showThis: boolean;
  formInput: FormGroup;
  dataEdit: any;
  modeForm: number;
  bosdaSelected: any;
  constructor(private service: MappingHeaderBosdaService,
              private fb: FormBuilder) {
    this.formInput = this.fb.group({
      name: ['', Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayAddEdit$,
      this.service.modeForm$,
      this.service.dataEdit$,
      this.service.bosdaSelected$
    );
    combine.subscribe(resp => {
      const [displayAddEdit, modeForm, dataEdit, bosdaSelected] = resp;
      this.showThis = displayAddEdit;
      this.modeForm = modeForm;
      this.dataEdit = Object.assign(dataEdit);
      this.bosdaSelected = Object.assign(bosdaSelected);
      if (this.dataEdit.id) {
        this.formInput.patchValue({
          name: this.dataEdit.name
        });
      } else {
        this.formInput.patchValue({
          name: ''
        });
      }
    });
  }
  Simpan() {
    if (this.formInput.valid) {
      if (this.modeForm === 1) {
        this.service.postBosdaCoaHeader(this.bosdaSelected.id, this.formInput.value)
        .subscribe(resp => {
          if (resp) {
            this.onHide();
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Data Berhasil Disimpan',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }, error => {
          error.error.error.forEach(e => {
            swal({
              position: 'top-end',
              type: 'error',
              title: e,
              showConfirmButton: false,
              timer: 3000
            });
          });
        });
      } else {
        this.service.putBosdaCoaHeader(this.bosdaSelected.id, this.dataEdit.id, this.formInput.value)
        .subscribe(resp => {
          if (resp) {
            this.onHide();
            swal({
              position: 'top-end',
              type: 'success',
              title: 'Data Berhasil DiUbah',
              showConfirmButton: false,
              timer: 3000
            });
          }
        }, error => {
          error.error.error.forEach(e => {
            swal({
              position: 'top-end',
              type: 'error',
              title: e,
              showConfirmButton: false,
              timer: 3000
            });
          });
        });
      }
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formInput.reset();
    this.service.getBosdaCoaHeader(this.bosdaSelected.id);
    this.service.showAddEdit(false, null);
  }
}
