import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class MappingHeaderBosdaService {
  private listBosdaCoaHeader = new BehaviorSubject(<any>[]);
  public listBosdaCoaHeader$ = this.listBosdaCoaHeader.asObservable();
  private bosdaSelected = new BehaviorSubject(<any>[]);
  public bosdaSelected$ = this.bosdaSelected.asObservable();
  private displayAddEdit = new BehaviorSubject<boolean>(false);
  public displayAddEdit$ = this.displayAddEdit.asObservable();
  private modeForm = new BehaviorSubject<number>(null);
  public modeForm$ = this.modeForm.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  constructor(private http: HttpClient) { }
  setBosdaSelected(data) {
    return this.bosdaSelected.next(data);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  showAddEdit(action: boolean, modeForm: number): void {
    this.displayAddEdit.next(action);
    this.modeForm.next(modeForm);
  }
  getBosdaCoaHeader(chartOfAccountId) {
    return this.http.get(`${environment.url}chart-of-accounts/${chartOfAccountId}/bosda-coa-headers`)
    .map(resp => <any>resp).subscribe(data => this.setListBosdaCoaHeader(data));
  }
  setListBosdaCoaHeader(data) {
    return this.listBosdaCoaHeader.next(data);
  }
  postBosdaCoaHeader(chartOfAccountId, postBody) {
    return this.http.post(`${environment.url}chart-of-accounts/${chartOfAccountId}/bosda-coa-headers`, postBody)
    .map(resp => <any>resp);
  }
  putBosdaCoaHeader(chartOfAccountId, id, postBody) {
    return this.http.put(`${environment.url}chart-of-accounts/${chartOfAccountId}/bosda-coa-headers/${id}`, postBody);
  }
  deleteBosdaCoaHeader(chartOfAccountId, postBody) {
    return this.http.request('DELETE', `${environment.url}chart-of-accounts/${chartOfAccountId}/bosda-coa-headers`, {body: postBody});
  }
}
