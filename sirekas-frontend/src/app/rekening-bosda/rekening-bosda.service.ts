import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class RekeningBosdaService {
  private listCoa = new BehaviorSubject(<any>[]);
  public listCoa$ = this.listCoa.asObservable();
  private listCoaBefore = new BehaviorSubject(<any>[]);
  public listCoaBefore$ = this.listCoaBefore.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  constructor(private http: HttpClient) { }
  getCoa(expensesType, budgetTypeId) {
    const queryParam = new HttpParams()
    .set('expensesType', expensesType)
    .set('budgetTypeId', budgetTypeId);
    return this.http.get(`${environment.url}chart-of-accounts`, {params: queryParam})
    .map(resp => <any>resp).subscribe(data => {
      if (budgetTypeId === 0) {
        this.setListCoaBefore(data);
      } else {
        this.setListCoa(data);
      }
    });
  }
  setListCoa(data) {
    return this.listCoa.next(data);
  }
  setListCoaBefore(data) {
    return this.listCoaBefore.next(data);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  putCoa(postBody) {
    return this.http.put(`${environment.url}chart-of-accounts`, postBody);
  }
}
