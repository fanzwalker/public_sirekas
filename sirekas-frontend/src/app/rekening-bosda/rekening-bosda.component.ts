import { MappingHeaderBosdaService } from './mapping-header-bosda/mapping-header-bosda.service';
import swal from 'sweetalert2';
import { animationPage } from './../shared/animation-page';
import { Observable } from 'rxjs/Observable';
import { RekeningBosdaService } from './rekening-bosda.service';
import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
@Component({
  selector: 'app-rekening-bosda',
  templateUrl: './rekening-bosda.component.html',
  styleUrls: ['./rekening-bosda.component.css'],
  animations: [animationPage]
})
export class RekeningBosdaComponent implements OnInit {
  listCoa: any;
  coaSelected: any = [];
  constructor(private service: RekeningBosdaService,
              private mappingHeaderService: MappingHeaderBosdaService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listCoa$
    );
    combine.subscribe(resp => {
      const [listCoa] = resp;
      this.listCoa = Object.assign(listCoa);
    });
    this.service.getCoa(2, 1);
  }
  btnAdd() {
    this.service.getCoa(2, 0);
    this.service.showAdd(true);
  }
  rowSelect(e) {
    this.mappingHeaderService.setBosdaSelected(e);
    this.mappingHeaderService.getBosdaCoaHeader(e.id);
  }
  btnDelete() {
    if (this.coaSelected.length > 0) {
      swal({
        title: 'Hapus',
        text: 'Data Akan Dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          const listId = [];
          _.forEach(this.coaSelected, function (k) {
            listId.push(k.id);
          });
          const dataBody = {
            budgetType: 0,
            ids : listId
          };
          this.service.putCoa(dataBody)
            .subscribe(resp => {
              this.coaSelected = [];
              this.service.getCoa(2, 1);
              swal({
                position: 'top-end',
                type: 'success',
                title: 'Data Berhasil Dihapus',
                showConfirmButton: false,
                timer: 3000
              });
            }, error => {
              swal({
                position: 'top-end',
                type: 'error',
                title: 'Data Gagal Dihapus [' + error.statusText + ']',
                showConfirmButton: false,
                timer: 3000
              });
            });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
}
