import swal from 'sweetalert2';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { RekeningBosdaService } from '../rekening-bosda.service';
import * as _ from 'lodash';
@Component({
  selector: 'app-add-rekening-bosda',
  templateUrl: './add-rekening-bosda.component.html',
  styleUrls: ['./add-rekening-bosda.component.css']
})
export class AddRekeningBosdaComponent implements OnInit {
  showThis: boolean;
  listCoaBefore: any;
  coaBeforeSelected: any = [];
  constructor(private service: RekeningBosdaService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayAdd$,
      this.service.listCoaBefore$
    );
    combine.subscribe(resp => {
      const [displayAdd, listCoaBefore] = resp;
      this.showThis = displayAdd;
      this.listCoaBefore = Object.assign(listCoaBefore);
    });
  }
  Simpan() {
    if (this.coaBeforeSelected.length > 0) {
      const listId = [];
      _.forEach(this.coaBeforeSelected, function (k) {
        listId.push(k.id);
      });
      const dataBody = {
        budgetType: 1,
        ids : listId
      };
      this.service.putCoa(dataBody)
        .subscribe(resp => {
          this.coaBeforeSelected = [];
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }, error => {
          swal({
            position: 'top-end',
            type: 'error',
            title: 'Data Gagal Disimpan [' + error.statusText + ']',
            showConfirmButton: false,
            timer: 3000
          });
        });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.service.getCoa(2, 1);
    this.service.showAdd(false);
  }
}
