import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { IlastYearBalance } from '../interface/ilast-year-balance';

@Injectable()
export class SaldoAwalService {
  private bosnasBudgetDesc = new BehaviorSubject(<any>[]);
  public bosnasBudgetDesc$ = this.bosnasBudgetDesc.asObservable();
  private lastYearBalance = new BehaviorSubject(<any>[]);
  public lastYearBalance$ = this.lastYearBalance.asObservable();
  private editData = new BehaviorSubject(<any>[]);
  public editData$ = this.editData.asObservable();
  private unitId = new BehaviorSubject(<any>[]);
  public unitId$ = this.unitId.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private budgetStandar = new BehaviorSubject(<any>[]);
  public budgetStandar$ = this.budgetStandar.asObservable();
  constructor(private http: HttpClient) { }
  getBudgetStandar() {
    return this.http.get(`${environment.url}budget-standards`)
    .map(resp => resp).subscribe(data => this.setListBudgetStandar(data));
  }
  setListBudgetStandar(data) {
    return this.budgetStandar.next(data);
  }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getBosnasBudgetDesc(unitId, stages) {
    return this.http.get(`${environment.url}stages/${stages}/units/${unitId}/bosnas-budget-desc`)
    .map(resp => resp).subscribe(data => this.setBosnasBudgetDesc(data));
  }
  setBosnasBudgetDesc(data) {
    return this.bosnasBudgetDesc.next(data);
  }
  getLastYearBalance(unitId) {
    return this.http.get(`${environment.url}units/${unitId}/last-year-balance`)
    .map(resp => resp).subscribe(data => this.setLastYearBalance(data));
  }
  setLastYearBalance(data) {
    return this.lastYearBalance.next(data);
  }
  setUnitId(unitId) {
    this.unitId.next(unitId);
  }
  setDataEdit(data) {
    return this.editData.next(data);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
  putLastYearBalancec(unitId, postBody) {
    return this.http.put(`${environment.url}units/${unitId}/last-year-balance`, postBody)
    .map(resp => <IlastYearBalance>resp);
  }
}
