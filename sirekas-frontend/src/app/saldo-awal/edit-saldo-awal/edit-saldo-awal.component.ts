import { SaldoAwalService } from './../saldo-awal.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { UsersService } from '../../service/users.service';
import swal from 'sweetalert2';
import * as _ from 'lodash';
@Component({
  selector: 'app-edit-saldo-awal',
  templateUrl: './edit-saldo-awal.component.html',
  styleUrls: ['./edit-saldo-awal.component.css']
})
export class EditSaldoAwalComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  unitId: number;
  userInfo: any;
  dataEdit: any;
  constructor(private saldoAwalService: SaldoAwalService,
              private userService: UsersService,
              private fb: FormBuilder) {
    this.userInfo = this.userService.getUserInfo();
    this.formEdit = this.fb.group({
      lastYearBalance: this.fb.group({
        value: [0, Validators.required]
      })
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.saldoAwalService.displayEdit$,
      this.saldoAwalService.unitId$,
      this.saldoAwalService.editData$
    );
    combine.subscribe(resp => {
      const [displayEdit, unitId, dataEdit] = resp;
      this.showThis = displayEdit;
      this.unitId = unitId;
      this.dataEdit = Object.assign(dataEdit);
      if (dataEdit.lastYearBalance) {
        this.formEdit.get('lastYearBalance.value').setValue(dataEdit.lastYearBalance.value);
      }
    });
  }
  Simpan() {
    if (this.formEdit.valid) {
      this.formEdit.get('lastYearBalance.value').setValue(+this.formEdit.get('lastYearBalance.value').value);
      const postBody = {
        value: this.formEdit.get('lastYearBalance.value').value
      };
      this.saldoAwalService.putLastYearBalancec(this.unitId, postBody)
      .subscribe(resp => {
        this.saldoAwalService.getBosnasBudgetDesc(this.unitId, this.userInfo.stages);
        this.saldoAwalService.getLastYearBalance(this.unitId);
        swal({
          position: 'top-end',
          type: 'success',
          title: 'Data berhasil diubah',
          showConfirmButton: false,
          timer: 3000
        });
        this.onHide();
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.saldoAwalService.showEdit(false);
  }
}
