import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSaldoAwalComponent } from './edit-saldo-awal.component';

describe('EditSaldoAwalComponent', () => {
  let component: EditSaldoAwalComponent;
  let fixture: ComponentFixture<EditSaldoAwalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSaldoAwalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSaldoAwalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
