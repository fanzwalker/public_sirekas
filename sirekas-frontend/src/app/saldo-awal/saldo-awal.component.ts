import { SaldoAwalService } from './saldo-awal.service';
import { animationPage } from './../shared/animation-page';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { UsersService } from '../service/users.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import * as _ from 'lodash';
@Component({
  selector: 'app-saldo-awal',
  templateUrl: './saldo-awal.component.html',
  styleUrls: ['./saldo-awal.component.css'],
  animations: [animationPage]
})
export class SaldoAwalComponent implements OnInit, OnDestroy {
  userInfo: any;
  formFilter: FormGroup;
  listSekolah: any; optSekolah: SelectItem[];
  bosnasBudgetDesc: any;
  lastYearBalance: any;
  legendTotal: number;
  constructor(private fb: FormBuilder,
              private userService: UsersService,
              private saldoAwalService: SaldoAwalService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah : [null, Validators.required]
    });
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.saldoAwalService.bosnasBudgetDesc$,
      this.saldoAwalService.lastYearBalance$
    );
    combine.subscribe(resp => {
      const [bosnasBudgetDesc, lastYearBalance] = resp;
      this.bosnasBudgetDesc = bosnasBudgetDesc;
      this.lastYearBalance = lastYearBalance;
      if (this.bosnasBudgetDesc && this.lastYearBalance) {
        this.legendTotal = this.bosnasBudgetDesc.total + lastYearBalance.value;
      } else {
        this.legendTotal = 0;
      }
    });

    if (this.userInfo.unitId) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.onChangeUnit();
    }

    this.getSekolah();
  }
  getSekolah() {
    const temp = [];
    this.saldoAwalService.getUnits(this.userInfo.stages, 0, 4, 1, 2)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  onChangeUnit() {
    this.saldoAwalService.getBosnasBudgetDesc(this.formFilter.value.sekolah, this.userInfo.stages);
    this.saldoAwalService.getLastYearBalance(this.formFilter.value.sekolah);
  }
  showEdit() {
    if (this.formFilter.valid) {
      const dataEdit = {
        bosnasBudgetDesc: this.bosnasBudgetDesc,
        lastYearBalance: this.lastYearBalance
      };
      this.saldoAwalService.setDataEdit(dataEdit);
      this.saldoAwalService.setUnitId(this.formFilter.value.sekolah);
      this.saldoAwalService.showEdit(true);
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah harus dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
  ngOnDestroy() {
   this.saldoAwalService.setBosnasBudgetDesc([]);
   this.saldoAwalService.setLastYearBalance([]);
  }

}
