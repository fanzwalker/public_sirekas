import { Dropdown } from 'primeng/primeng';
import * as _ from 'lodash';
export abstract class AllOption {
  public static JenisAnggaran() {
    return [
      {label: 'Pilih Jenis Anggaran', value: null},
      {label: 'BOSDA', value: 1},
      {label: 'BOSNAS', value: 2},
      {label: 'BOSDA & BOSNAS', value: 3}
    ];
  }
  public static Pendidikan() {
    return [
      {label: 'Pilih Pendidikan', value: null},
      {label: 'TK', value: 1},
      {label: 'SD', value: 2},
      {label: 'SMP', value: 3}
    ];
  }
  public static JenisAnggaranStandar() {
    return [
      {label: 'Pilih Jenis Anggaran', value: null},
      {label: 'BOSNAS', value: 1},
      {label: 'BOSDA Per Siswa', value: 2},
      {label: 'BOSDA Per Rombel', value: 3},
      {label: 'BOSDA Per Sekolah', value: 4}
    ];
  }
  public static ResetFilterOption(dropdown: Dropdown[]) {
    if (dropdown.length > 0) {
      _.forEach(dropdown,  (x) => {
        x.filterValue = '';
      });
    }
  }
  public static kelasRomawi() {
    return ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'];
  }
  public static kelasHuruf() {
    return [
      'A', 'B', 'C', 'D', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
      'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    ];
  }
  public static jenisGuru() {
    return [
      {label: 'Pilih', value: null},
      {label: 'Guru PNS', value: 1},
      {label: 'Guru Non PNS', value: 2},
      {label: 'Pegawai/TU PNS', value: 3},
      {label: 'Pegawai/TU Non PNS', value: 4}
    ];
  }
  public static jenisBerita() {
    return [
      {label: 'Pilih Jenis', value: null},
      {label: 'Pengumuman', value: 1},
      {label: 'Peringatan', value: 2}
    ];
  }
  public static getRandomColor() {
    const letters = '0123456789ABCDEF'.split('');
    let color = '#';
    for (let i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  public static convertToRupiah(angka) {
    let rupiah = '';
    const angkarev = angka.toString().split('').reverse().join('');
    for (let i = 0; i < angkarev.length; i++) { if (i % 3 === 0) { rupiah += angkarev.substr(i, 3) + '.'; } }
    return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
  }
  public static getDateNow() {
    let now = new Date();
    now = new Date(Date.UTC(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(),
        now.getMinutes(), now.getSeconds()));
    return now;
  }
}
