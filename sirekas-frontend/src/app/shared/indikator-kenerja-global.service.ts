import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../environments/environment';
import { IndikatorKinerja } from '../interface/indikator-kinerja';
import { ArrayStatic } from './arrayStatic';
import {HttpClient, HttpParams} from "@angular/common/http";

@Injectable()
export class IndikatorKenerjaGlobalService {
  private ListPerfBenchmarkFixed = new BehaviorSubject(<any>[]);
  public ListPerfBenchmarkFixed$ = this.ListPerfBenchmarkFixed.asObservable();
  constructor(private http: HttpClient) { }
  getPerfBenchmarkFixed(budgetTypeId, unitId?, stageId?, activityId?) {
    const params = new HttpParams()
      .set('unitId', unitId)
      .set('stageId', stageId)
      .set('activityId', activityId);
    return this.http.get(`${environment.url}budget-types/${budgetTypeId}/perf-benchmark-fixed`, {params: params})
    .map(resp => <IndikatorKinerja[]>resp).subscribe(data => {
      if (data.length > 0) {
        for (let i = 0 ; i < data.length; i++) {
          data[i].indikator = ArrayStatic.listIndikator()[i];
        }
      }
      this.setListPerfBenchmarkFixed(data);
    });
  }
  setListPerfBenchmarkFixed(data) {
    return this.ListPerfBenchmarkFixed.next(data);
  }
}
