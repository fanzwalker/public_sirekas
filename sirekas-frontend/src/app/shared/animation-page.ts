import {animate, state, style, transition, trigger} from '@angular/animations';

export let animationPage = trigger('animationPage', [
  state('void', style({
    opacity: 0
  })),
  transition(':enter, :leave', [
    animate(500)
  ])
]);
