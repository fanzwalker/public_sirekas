import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceDoller'
})
export class ReplaceDollerPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value.indexOf('$') >= 0 ) {
      return value.replace('$', 'Rp. ');
    } else {
      return value;
    }
  }

}
