import { Component, OnInit } from '@angular/core';
import { animationPage } from '../shared/animation-page';
import { SelectItem } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../service/users.service';
import * as _ from 'lodash';
import { SdService } from './sd.service';
@Component({
  selector: 'app-apbs-sumber-dana',
  templateUrl: './apbs-sumber-dana.component.html',
  styleUrls: ['./apbs-sumber-dana.component.css'],
  animations: [animationPage]
})
export class ApbsSumberDanaComponent implements OnInit {
  listSekolah: any; optSekolah: SelectItem[];
  userInfo: any;
  formFilter: FormGroup;
  constructor(private userService: UsersService,
              private fb: FormBuilder,
              private sumberDanaService: SdService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required]
    });
  }

  ngOnInit() {
    if (this.userInfo.IsAdmin && this.userInfo.unitId !== '0') {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.GetData();
    } else if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
    }
    this.getSekolah();
  }
  getSekolah() {
    const temp = [];
    this.sumberDanaService.getUnits()
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, (e) => {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  GetData() {
    this.sumberDanaService.setUnit(this.formFilter.value.sekolah);
    this.sumberDanaService.getPreview(this.userInfo.stages, this.formFilter.value.sekolah);
  }
}
