import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class SdService {
  private listSumberDana = new BehaviorSubject(<any>[]);
  public listSumberDana$ = this.listSumberDana.asObservable();
  private unitId = new BehaviorSubject<number>(null);
  public unitId$ = this.unitId.asObservable();
  constructor(private http: HttpClient) { }
  setUnit(unitId): void {
    return this.unitId.next(unitId);
  }
  getUnits() {
    return this.http.get(`${environment.url}units`);
  }
  getPreview(stageId, unitId) {
    const queryParam = new HttpParams()
    .set('unitId', unitId);
    return this.http.get(`${environment.url}previews/apbs-sbdana/stages/${stageId}`, {params: queryParam})
    .map(resp => resp).subscribe(data => this.setlistSumberDana(data));
  }
  setlistSumberDana(data) {
    return this.listSumberDana.next(data);
  }
}
