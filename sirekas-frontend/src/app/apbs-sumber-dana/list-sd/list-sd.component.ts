import { Component, OnInit } from '@angular/core';
import { IlistSd } from '../../interface/ilist-sd';
import { MenuItem } from 'primeng/primeng';
import { SdService } from '../sd.service';
import { ReportService } from '../../service/report.service';
import { UsersService } from '../../service/users.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import { animationPage } from '../../shared/animation-page';
@Component({
  selector: 'app-list-sd',
  templateUrl: './list-sd.component.html',
  styleUrls: ['./list-sd.component.css'],
  animations: [animationPage]
})
export class ListSdComponent implements OnInit {
  listSd: IlistSd[];
  unitId: any;
  itemPrints: MenuItem[];
  userInfo: any;
  constructor(private sumberDanaService: SdService,
              private reportService: ReportService,
              private usersService: UsersService) {
    this.userInfo = this.usersService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.sumberDanaService.listSumberDana$,
      this.sumberDanaService.unitId$
    );
    combine.subscribe(resp => {
      const [listSumberDana, unitId] = resp;
      this.listSd = Object.assign(listSumberDana);
      this.unitId = unitId;
    });
  }
  calculateTotal() {
    let totals = 0;
    if (this.listSd) {
      for (const data of this.listSd) {
        if (data.code.length < 4) {
          totals += data.total;
        }
      }
    }
    return totals;
  }
  btnCetak(type) {
    const paramBody = {};
    paramBody['@unitId'] = this.unitId;
    paramBody['@stageId'] = +this.userInfo.stages;
    this.reportService.execPrint('ApbsSbDana.rpt', type, paramBody)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, 'APBS Sumber Dana');
      });
  }
}
