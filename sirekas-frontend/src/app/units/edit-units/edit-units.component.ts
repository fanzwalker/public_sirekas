import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UnitsService } from '../units.service';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import { AllOption } from '../../shared/all-option';
import * as _ from 'lodash';
import { SelectItem } from 'primeng/api';
import { IlistDistricts } from './../../interface/ilist-districts';
@Component({
  selector: 'app-edit-units',
  templateUrl: './edit-units.component.html',
  styleUrls: ['./edit-units.component.css']
})
export class EditUnitsComponent implements OnInit {
  showThis: boolean;
  dataEdit: any;
  formEdit: FormGroup;
  optJenisAnggaran: any;
  optPendidikan: any;
  listDistricts: IlistDistricts[];
  optDistrict: SelectItem[];
  constructor(private fb: FormBuilder,
              private unitService: UnitsService) {
                this.formEdit = this.fb.group({
                  code: ['', Validators.required],
                  name : ['', Validators.required],
                  budgetType: ['', Validators],
                  districtId: [null, Validators.required],
                  educationLvl: [null, Validators.required],
                  npsn: ['', Validators.required]
                });
              }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.unitService.displayEdit$,
      this.unitService.dataEdit$,
      this.unitService.districts$
    );
    combine.subscribe(resp => {
      const [displayEdit, dataEdit, districts] = resp;
      this.showThis = displayEdit;
      this.dataEdit = dataEdit;
      this.listDistricts = Object.assign(districts);
      if (this.dataEdit.id) {
        this.formEdit.patchValue({
          code: this.dataEdit.code,
          name : this.dataEdit.name,
          budgetType: this.dataEdit.budgetType,
          districtId: this.dataEdit.districtId,
          educationLvl: this.dataEdit.educationLvl,
          npsn: this.dataEdit.npsn
        });
      }
      this.setOptJenisAnggaran();
      this.setOptPendidikan();
      this.setOptDistrict();
    });
  }
  setOptJenisAnggaran () {
    this.optJenisAnggaran = AllOption.JenisAnggaran();
  }
  setOptPendidikan() {
    this.optPendidikan = AllOption.Pendidikan();
  }
  setOptDistrict() {
    const temp = [];
    temp.push({label: 'Pilih Kecamatan', value: null});
    _.forEach(this.listDistricts, (e) => {
      temp.push({label: e.name, value: e.id});
    });
    this.optDistrict = temp;
  }
  Simpan() {
    if (this.formEdit.valid) {
      this.unitService.putUnit(this.dataEdit.id, this.formEdit.value)
      .subscribe(resp => {
        this.unitService.getUnits();
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'User Berhasil Ubah',
          showConfirmButton: false,
          timer: 3000
        });
      }, error => {
        console.log(error.error.error);
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.unitService.showEdit(false);
  }
}
