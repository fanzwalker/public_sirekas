import { IlistDistricts } from './../../interface/ilist-districts';
import { Component, OnInit } from '@angular/core';
import { UnitsService } from '../units.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import swal from 'sweetalert2';
import { AllOption } from '../../shared/all-option';
import { SelectItem } from 'primeng/api';
import * as _ from 'lodash';
@Component({
  selector: 'app-add-units',
  templateUrl: './add-units.component.html',
  styleUrls: ['./add-units.component.css']
})
export class AddUnitsComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  optJenisAnggaran: any;
  optPendidikan: any;
  listDistricts: IlistDistricts[];
  optDistrict: SelectItem[];
  constructor(private unitService: UnitsService,
              private fb: FormBuilder) {
                this.formAdd = this.fb.group({
                  code: ['1.01.01.01.', Validators.required],
                  name: ['', Validators.required],
                  budgetType: ['', Validators.required],
                  districtId: [null, Validators.required],
                  educationLvl: [null, Validators.required],
                  npsn: ['', Validators.required]
                });
              }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.unitService.displayAdd$,
      this.unitService.districts$
    );
    combine.subscribe(resp => {
      const [displaAdd, districts] = resp;
      this.showThis = displaAdd;
      this.listDistricts = Object.assign(districts);
      this.setOptJenisAnggaran();
      this.setOptDistrict();
      this.setOptPendidikan();
    });
  }
  setOptJenisAnggaran () {
    this.optJenisAnggaran = AllOption.JenisAnggaran();
  }
  setOptPendidikan() {
    this.optPendidikan = AllOption.Pendidikan();
  }
  setOptDistrict() {
    const temp = [];
    temp.push({label: 'Pilih Sekolah', value: null});
    _.forEach(this.listDistricts, (e) => {
      temp.push({label: e.name, value: e.id});
    });
    this.optDistrict = temp;
  }
  Simpan() {
    if (this.formAdd.valid) {
      this.unitService.postUnits(this.formAdd.value)
      .subscribe(resp => {
        this.unitService.getUnits();
        this.onHide();
        swal({
          position: 'top-end',
          type: 'success',
          title: 'User Berhasil Disimpan',
          showConfirmButton: false,
          timer: 3000
        });
      }, error => {
        console.log(error.error.error);
        swal({
          position: 'top-end',
          type: 'error',
          title: error.error.error[0],
          showConfirmButton: false,
          timer: 3000
        });
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.unitService.showAdd(false);
  }
}
