import { Component, OnInit } from '@angular/core';
import { UnitsService } from './units.service';
import { UsersService } from '../service/users.service';
import {Observable} from 'rxjs/Observable';
import { IlistUnits } from '../interface/ilist-units';
import { animationPage } from '../shared/animation-page';
import * as _ from 'lodash';
@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css'],
  animations: [animationPage]
})
export class UnitsComponent implements OnInit {
  userInfo: any;
  listUnits: IlistUnits[];
  unitSelected: any = [];
  constructor(private unitService: UnitsService,
              private userService: UsersService) {
                this.userInfo = this.userService.getUserInfo();
              }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.unitService.listUnit$
    );
    combine.subscribe(resp => {
      const [listUnit] = resp;
      this.listUnits = listUnit;
    });
    this.getUnits();
  }
  getUnits() {
    this.unitService.getUnits();
  }
  showAdd() {
    this.unitService.getDistrict();
    this.unitService.showAdd(true);
  }
  showEdit(e) {
    this.unitService.getDistrict();
    this.unitService.setDataEdit(e);
    this.unitService.showEdit(true);
  }
  actionLock(action: number) {// 1 = lockAll, 2 = unlockAll
    if (this.unitSelected.length > 0 ) {
      const listId = [];
      _.forEach(this.unitSelected, function (k) {
        listId.push(k.id);
      });
      const dataBody = {
        ids : listId,
        isLocked: action === 1 ? true : false
      };
      this.unitService.lockAll(dataBody)
      .subscribe(resp =>  {
        if (resp.length > 0) {
          this.unitSelected = Object.assign([]);
          this.getUnits();
        }
      });
    }
  }
  actionSingle(e, action: number) {// 1 = lock, 2 = unlock
    const postBody = [{
      op: 'replace',
      path: '/isLocked',
      value: action === 1 ? true : false
    }];
    this.unitService.lockSingle(e.id, postBody)
    .subscribe(resp => {
      if (resp.id) {
        this.getUnits();
      }
    });
  }
  castBudgetType(x) {
    if (x === 1) {
      return 'BOSDA';
    } else if (x === 2) {
      return 'BOSNAS';
    } else if (x === 3) {
      return 'BOSNAS & BOSDA';
    } else {
      return 'Not Set';
    }
  }
  castPendidikan(x) {
    if (x === 1) {
      return 'TK';
    } else if (x === 2) {
      return 'SD';
    } else if (x === 3) {
      return 'SMP';
    }
  }
}
