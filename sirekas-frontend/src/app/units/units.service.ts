import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../environments/environment';
import { IlistDistricts } from '../interface/ilist-districts';

@Injectable()
export class UnitsService {
  private listUnit = new BehaviorSubject(<any>[]);
  public listUnit$ = this.listUnit.asObservable();
  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  private districts = new BehaviorSubject(<any>[]);
  public districts$ = this.districts.asObservable();
  constructor(private http: HttpClient) { }
  getUnits() {
    const queryParams = new HttpParams()
    .set('UnitStructure', '4')
    .set('Stages', '1')
    .set('ExcludeType', '0');
    return this.http.get(`${environment.url}units`, {params: queryParams})
    .map(resp => resp).subscribe(data => this.setListUnit(data));
  }
  setListUnit(data) {
    return this.listUnit.next(data);
  }
  postUnits(dataBody) {
    return this.http.post(`${environment.url}units`, dataBody);
  }
  putUnit(unitId, dataBody) {
    return this.http.put(`${environment.url}units/${unitId}`, dataBody);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  getDistrict() {
    return this.http.get(`${environment.url}districts`).map(resp => <IlistDistricts>resp).subscribe(data => this.setListDistricts(data));
  }
  setListDistricts(data) {
    return this.districts.next(data);
  }
  lockSingle(id, postBody) {
    return this.http.patch<any>(`${environment.url}units/${id}`, postBody).map(resp => <any>resp);
  }
  lockAll(postBody) {
    return this.http.put(`${environment.url}units/`, postBody)
    .map(resp => <any>resp);
  }
}
