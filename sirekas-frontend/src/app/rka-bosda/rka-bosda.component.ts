import { IndikatorKenerjaGlobalService } from './../shared/indikator-kenerja-global.service';
import { BosdaPreviewService } from './bosda-preview/bosda-preview.service';
import { BosdaAnggaranKasService } from './bosda-anggaran-kas/bosda-anggaran-kas.service';
import { BosdaKompPenyusunService } from './bosda-komp-penyusun/bosda-komp-penyusun.service';
import { animationPage } from './../shared/animation-page';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RkaBosdaService } from './rka-bosda.service';
import { UsersService } from '../service/users.service';
import * as _ from 'lodash';
@Component({
  selector: 'app-rka-bosda',
  templateUrl: './rka-bosda.component.html',
  styleUrls: ['./rka-bosda.component.css'],
  animations: [animationPage]
})
export class RkaBosdaComponent implements OnInit, OnDestroy {
  listSekolah: any; optSekolah: SelectItem[];
  titleLegend = '';
  formFilter: FormGroup;
  userInfo: any;
  constructor(private fb: FormBuilder,
              private service: RkaBosdaService,
              private userService: UsersService,
              private bosdaKompPenyusunService: BosdaKompPenyusunService,
              private bosdaAnggaranKasService: BosdaAnggaranKasService,
              private bosdaPreviewService: BosdaPreviewService,
              private indikatorKinerjaService: IndikatorKenerjaGlobalService) {
    this.userInfo = this.userService.getUserInfo();
    this.formFilter = this.fb.group({
      sekolah: ['', Validators.required],
      program: ['', Validators.required],
      kegiatan: ['', Validators.required]
    });
  }

  ngOnInit() {
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
      this.onChangeUnit();
    } else {
      this.getSekolah();
    }
    this.getProgram();
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(1, 0, 4, 1, 1)
    .subscribe(resp => {
      this.listSekolah = resp;
      temp.push({label: 'Pilih Sekolah', value: null});
      _.forEach(this.listSekolah, (e) => {
        temp.push({label: e.code + ' - ' + e.name, value: e.id});
      });
      this.optSekolah = temp;
    });
  }
  getProgram() {
    const temp = [];
    this.service.getPrograms()
    .subscribe(resp => {
      this.formFilter.patchValue({
        program: resp[0].id
      });
      this.onChangeUnit();
    });
  }
  getKegiatan() {
    const temp = [];
    this.service.getKegiatan(this.formFilter.value.sekolah, this.formFilter.value.program, this.userInfo.stages, 1)
    .subscribe(resp => {
      if (resp.length > 0) {
        this.formFilter.patchValue({
          kegiatan: resp[0].activityId
        });
        this.titleLegend = resp[0].total.toString();
        this.bosdaKompPenyusunService.setPaguBosda(resp[0].total);
        this.getRkas();
      } else {
        this.service.setUnitAct(this.formFilter.value.sekolah, null);
        this.bosdaKompPenyusunService.setPaguBosda(0);
        this.bosdaKompPenyusunService.setListBosdaBudgetDet([]);
        this.bosdaAnggaranKasService.setBudgetMonthly([]);
        this.bosdaPreviewService.setPreview([]);
        this.bosdaPreviewService.setUnitProgAct([]);
      }
    });
  }
  setUnitProgAct() {
    const data = {
      Units : _.find(this.listSekolah, (e) => e.id === this.formFilter.value.sekolah),
      Programs: this.formFilter.value.program,
      Activity: this.formFilter.value.kegiatan
    };
    this.bosdaPreviewService.setUnitProgAct(data);
  }
  onOpentabs() {
    console.log('open tabs');
  }
  onChangeUnit() {
    this.titleLegend = '';
    this.getKegiatan(); // Bosda & bosnas
  }
  getRkas(s?) {
    if (s) {
      this.titleLegend = s.label;
    }
    if (this.formFilter.value.sekolah && (this.formFilter.value.kegiatan || this.formFilter.value.kegiatan === 0)) {
      this.bosdaKompPenyusunService.getBudgetDet(this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.kegiatan, 0);
      this.bosdaAnggaranKasService.getBudgetMonthly(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      this.bosdaPreviewService.getPreview(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
      this.indikatorKinerjaService.getPerfBenchmarkFixed(1, this.formFilter.value.sekolah, this.userInfo.stages, this.formFilter.value.kegiatan);
      this.setUnitProgAct();
    } else {
      this.bosdaKompPenyusunService.setListBosdaBudgetDet([]);
      this.bosdaAnggaranKasService.setBudgetMonthly([]);
      this.bosdaPreviewService.setPreview([]);
      this.bosdaPreviewService.setUnitProgAct([]);
    }
    this.service.setUnitAct(this.formFilter.value.sekolah, this.formFilter.value.kegiatan);
  }
  ngOnDestroy() {
    this.bosdaKompPenyusunService.showAddRincian(false);
    this.bosdaKompPenyusunService.setListBosdaBudgetDet([]);
    this.bosdaAnggaranKasService.setBudgetMonthly([]);
    this.bosdaPreviewService.setPreview([]);
    this.bosdaPreviewService.setUnitProgAct([]);
  }
}
