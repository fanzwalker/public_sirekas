import { environment } from './../../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';

@Injectable()
export class BosdaKompPenyusunService {
  private listBosdaBudgetDet = new BehaviorSubject(<any>[]);
  public listBosdaBudgetDet$ = this.listBosdaBudgetDet.asObservable();
  private listUnitDesc = new BehaviorSubject(<any>[]);
  public listUnitDesc$ = this.listUnitDesc.asObservable();
  private displayAddRincian = new BehaviorSubject<boolean>(false);
  public displayAddRincian$ = this.displayAddRincian.asObservable();
  private listBudgetDetHeader = new BehaviorSubject(<any>[]);
  public listBudgetDetHeader$ = this.listBudgetDetHeader.asObservable();
  private displayAddHeader = new BehaviorSubject<boolean>(false);
  public displayAddHeader$ = this.displayAddHeader.asObservable();
  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();
  private dataEditRincian = new BehaviorSubject(<any>[]);
  public dataEditRincian$ = this.dataEditRincian.asObservable();
  private paguBosda = new BehaviorSubject<number>(null);
  public paguBosda$ = this.paguBosda.asObservable();
  constructor(private http: HttpClient) { }
  getBudgetDet(unitId, stages, activityId, type, coaId = null) {
    const params = new HttpParams()
      .set('type', type)
      .set('coaId', coaId);
    this.http.get(`${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets`, {params: params})
      .map(resp => resp).subscribe(data => this.setListBosdaBudgetDet(data));
  }
  setListBosdaBudgetDet(data) {
    return this.listBosdaBudgetDet.next(data);
  }
  getUnitDecs() {
    return this.http.get(`${environment.url}unit-descs`).map(resp => resp)
      .subscribe(data => this.setUnitDesc(data));
  }
  setUnitDesc(data) {
    return this.listUnitDesc.next(data);
  }
  showAddRincian(action: boolean) {
    return this.displayAddRincian.next(action);
  }
  searchCoa(keyword, expensesType, budgetTypeId) {
    const params = new HttpParams()
      .set('keyword', keyword)
      .set('expensesType', expensesType)
      .set('budgetTypeId', budgetTypeId);
    return this.http.get(`${environment.url}chart-of-accounts`, {params: params});
  }
  getBosdaCoaHeader(chartOfAccountId) {
    return this.http.get(`${environment.url}chart-of-accounts/${chartOfAccountId}/bosda-coa-headers`)
    .map(resp => <any>resp).subscribe(data => this.setListBudgetDetHeader(data));
  }
  setListBudgetDetHeader(data) {
    return this.listBudgetDetHeader.next(data);
  }
  getBosdaSubHeader(unitId, stages, levelTypes, coaId = null, code = null) {
    const queryParam = new HttpParams()
    .set('levelTypes', levelTypes)
    .set('coaId', coaId)
    .set('code', code);
    return this.http.get(`${environment.url}bosda/units/${unitId}/stages/${stages}/budgetdets`, {params: queryParam})
    .map(resp => <any>resp);
  }
  getBudgetDetChild(unitId, stages, activityId, type, budgetDetId) {
    const params = new HttpParams()
      .set('type', type);
    return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets/${budgetDetId}/childs`,
      {params: params}).map(resp => <any>resp);
  }
  showAddSubHeader(action: boolean) {
    return this.displayAddHeader.next(action);
  }
  postBudgetDet(unitId, stages, activityId, paramBody) {
    return this.http.post(`${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets`,
      paramBody).map(resp => <any>resp);
  }
  postBosdaBudgetDet(unitId, stages, postBody) {
    return this.http.post(`${environment.url}bosda/units/${unitId}/stages/${stages}/budgetdets`, postBody);
  }
  showEditRincian(action: boolean) {
    return this.displayEdit.next(action);
  }
  setDataEditRincian(data) {
    return this.dataEditRincian.next(data);
  }
  deleteBudgetDet(unitId, stages, activityId, paramBody) {
    return this.http.request('DELETE', `${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets`,
      {body: paramBody});
  }
  putBudgetDet(unitId, stages, activityId, id, paramBody) {
    return this.http.put(`${environment.url}units/${unitId}/stages/${stages}/activities/${activityId}/budgetdets/${id}`,
      paramBody);
  }
  setPaguBosda(pagu: number): void {
    this.paguBosda.next(pagu);
  }
}
