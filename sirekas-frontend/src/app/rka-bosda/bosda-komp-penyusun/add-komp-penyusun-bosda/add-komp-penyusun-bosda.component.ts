import { Dropdown } from 'primeng/primeng';
import { BosdaPreviewService } from './../../bosda-preview/bosda-preview.service';
import { BosdaAnggaranKasService } from './../../bosda-anggaran-kas/bosda-anggaran-kas.service';
import { IpostBosdaBudgetDet } from './../../../interface/ipost-bosda-budget-det';
import { Observable } from 'rxjs/Observable';
import { BosdaKompPenyusunService } from './../bosda-komp-penyusun.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, QueryList } from '@angular/core';
import { IlistCoa } from '../../../interface/ilist-coa';
import { RkaBosdaService } from '../../rka-bosda.service';
import { UsersService } from '../../../service/users.service';
import { IlistBudgetDet } from '../../../interface/ilist-budget-det';
import { SelectItem } from 'primeng/api';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { IpostBudgetDet } from '../../../interface/ipost-budget-det';
import { AllOption } from '../../../shared/all-option';
@Component({
  selector: 'app-add-komp-penyusun-bosda',
  templateUrl: './add-komp-penyusun-bosda.component.html',
  styleUrls: ['./add-komp-penyusun-bosda.component.css']
})
export class AddKompPenyusunBosdaComponent implements OnInit {
  @ViewChild('dd1') dd1: Dropdown;
  @ViewChild('dd2') dd2: Dropdown;
  @ViewChild('dd3') dd3: Dropdown;
  @ViewChild('dd4') dd4: Dropdown;
  @ViewChild('dd5') dd5: Dropdown;
  @ViewChild('dd6') dd6: Dropdown;
  showThis: boolean;
  showThisHeader: boolean;
  coaList: any;
  coaFiltered: any;
  coaSelected: IlistCoa;
  userInfo: any;
  formAdd: FormGroup;
  unitId: number;
  activityId: number;
  listBudgetDetHeader: IlistBudgetDet[];
  optHeader: SelectItem[];
  listSubHeader: IlistBudgetDet[];
  optSubHeader: SelectItem[];
  formAddHeader: FormGroup;
  listUnitDesc: any;
  listBosdaBudgetDet: IlistBudgetDet[];
  optUnitDesc: SelectItem[];
  disableSet: boolean;
  constructor(private fb: FormBuilder,
              private service: BosdaKompPenyusunService,
              private rkaBosdaService: RkaBosdaService,
              private bosdaAnggaranKasService: BosdaAnggaranKasService,
              private bosdaPreviewService: BosdaPreviewService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
    this.formAdd = this.fb.group({
      chartOfAccountId: [null, Validators.required],
      headerId: [null, Validators.required],
      subHeaderId: [null],
      parentId: [null],
      budgetSourceId: [1],
      code: [''],
      description: ['', Validators.required],
      expres1: [null, Validators.required],
      expres2: [null],
      expres3: [null],
      expres4: [null],
      expres5: [null],
      expres6: [null],
      unitDesc: [null, Validators.required],
      costPerUnit: [0, Validators.min(1)],
      type: [2]
    });
    this.formAddHeader = this.fb.group({
      code: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayAddRincian$,
      this.rkaBosdaService.unitId$,
      this.rkaBosdaService.activityId$,
      this.service.listBudgetDetHeader$,
      this.service.displayAddHeader$,
      this.service.listUnitDesc$,
      this.service.listBosdaBudgetDet$
    );
    combine.subscribe(resp => {
      const [displayAddRincian, unitId, activityId, listBudgetDetHeader, displayAddHeader, listUnitDesc] = resp;
      this.showThis = displayAddRincian;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listBudgetDetHeader = Object.assign(listBudgetDetHeader);
      this.showThisHeader = displayAddHeader;
      this.listUnitDesc = Object.assign(listUnitDesc);
      this.setOptHeader();
      this.setOptUnitDesc();
    });
  }
  searchCoa(e) {
    this.service.searchCoa(e.query, 2, 1)
      .subscribe(resp => {
        this.coaFiltered = Object.assign(resp);
      });
  }
  onSelectCoa(e) {
    this.coaSelected = <IlistCoa>e;
    this.formAdd.patchValue({
      chartOfAccountId: this.coaSelected.id
    });
    this.service.getBosdaCoaHeader(this.formAdd.value.chartOfAccountId);
  }
  setOptHeader() {
    this.optHeader = [];
    if (this.listBudgetDetHeader.length > 0) {
        this.optHeader.push({label: '-- Pilih --', value: null});
      _.forEach(this.listBudgetDetHeader, e => {
        this.optHeader.push({label: e.code + ' - ' + e.name, value: e.id});
      });
    } else {
      this.optHeader = [{label: '-- Pilih --', value: null}];
    }
  }
  setOptUnitDesc() {
    this.optUnitDesc = [];
    if (this.listUnitDesc.length > 0) {
      this.optUnitDesc.push({label: '-- Pilih --', value: null});
      _.forEach(this.listUnitDesc, e => {
        this.optUnitDesc.push({label: e.name, value: e.id});
      });
    } else {
      this.optUnitDesc = [{label: '-- Pilih --', value: null}];
    }
  }
  onChangeHeader(e) {
    if (e.value) {
      this.disableSet = false;
      const budgetDetHeaderSelected = _.find(this.listBudgetDetHeader, (x) => {
        return x.id === e.value;
      });
      if (budgetDetHeaderSelected.id) {
        this.formAddHeader.patchValue({
          code: budgetDetHeaderSelected.code,
          description: budgetDetHeaderSelected.name
        });
      }
    } else {
      this.formAddHeader.reset();
    }
  }
  setOptSub(budgetDetId, subHeaderId?) {
    this.listSubHeader = [];
    const temp = [];
    this.service.getBudgetDetChild(this.unitId, this.userInfo.stages, this.activityId, 1, budgetDetId)
      .subscribe(resp => {
        if (resp) {
          this.listSubHeader = Object.assign(resp);
          temp.push({label: '-- Pilih --', value: null});
          _.forEach(this.listSubHeader, function (e) {
            temp.push({label: e.code + ' - ' + e.description, value: e.id});
          });
          this.optSubHeader = Object.assign(temp);
          if (subHeaderId) {
            this.formAdd.patchValue({
              subHeaderId: subHeaderId
            });
          }
        }
      });
  }
  showFormHeader() {
    // let lastCode = '';
    // const dataHeader = _.find(this.listBudgetDetHeader, (e) => {
    //   if (e.id === this.formAdd.value.headerId) {
    //     return e;
    //   }
    // });
    // if (this.listSubHeader.length > 0 ) {
    //   const lastData = _.last(this.listSubHeader, (e) => {
    //     return e;
    //   });
    //   const tempCode = lastData.code.substr(lastData.code.length - 4, 4);
    //   lastCode = dataHeader.code + this.generateKode(tempCode);
    // } else {
    //   lastCode = dataHeader.code + '001.';
    // }
    // this.formAddHeader.patchValue({
    //   code: lastCode
    // });
    this.service.showAddSubHeader(true);
  }
  generateKode(lastCode) {
    let newKode = '';
    const splits = lastCode.split('');
    const numberKode = Number(splits[0] + splits[1] + splits[2]) + 1;
    const lengthKode = numberKode.toString().length;
    if (lengthKode === 1) {
      newKode = '00' + numberKode.toString() + '.';
    } else if (lengthKode === 2) {
      newKode = '0' + numberKode.toString() + '.';
    } else {
      newKode = numberKode.toString() + '.';
    }
    return newKode;
  }
  SetHeader() {
    if (this.dd1.value === undefined || this.dd1.value === null) {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Header Belum Di pilih',
        showConfirmButton: false,
        timer: 3000
      });
    } else {
      this.disableSet = true;
      this.SimpanHeader(true);
    }
  }
  SimpanHeader(isHeader = false) {
    const paramBody: IpostBudgetDet = <IpostBudgetDet>this.formAddHeader.value;
    paramBody.type = 1;
    paramBody.costPerUnit = 0;
    paramBody.unitDesc = null;
    paramBody.expression = null;
    paramBody.budgetSourceId = 1;
    paramBody.code = this.formAddHeader.value.code;
    paramBody.chartOfAccountId = this.formAdd.value.chartOfAccountId;
    paramBody.parentId = isHeader ? null : this.formAdd.value.headerId;
    this.service.postBudgetDet(this.unitId, this.userInfo.stages, this.activityId, paramBody)
      .subscribe(resp => {
        if (resp) {
          if (isHeader) {
            this.formAdd.patchValue({
              headerId: resp.id
            });
            this.setOptSub(resp.id);
            this.onHideAddHeader(resp.id);
          } else {
            this.setOptSub(this.formAdd.value.headerId, resp.id);
          }
          this.onHideAddHeader();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Header Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }
      });
  }
  BatalHeader() {
    this.onHideAddHeader();
  }
  Simpan() {
    const paramBody = <IpostBudgetDet>{};
    paramBody.chartOfAccountId = this.formAdd.value.chartOfAccountId;
    paramBody.description = this.formAdd.value.description;
    paramBody.expression = this.joinExpression();
    paramBody.unitDesc = this.formAdd.value.unitDesc;
    paramBody.costPerUnit = this.formAdd.value.costPerUnit;
    paramBody.parentId = this.formAdd.value.subHeaderId || this.formAdd.value.headerId;
    paramBody.budgetSourceId = 1;
    paramBody.type = 2;
    // console.log(paramBody);
    this.okPost(paramBody);
  }
  okPost(paramBody) {
    this.service.postBudgetDet(this.unitId, this.userInfo.stages, this.activityId, paramBody)
      .subscribe(resp => {
        if (resp) {
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Disimpan',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Data Gagal Disimpan [' + error.error.Total[0].replace('$', 'Rp. ') + ']',
          showConfirmButton: false,
          timer: 3000
        });
      });
  }
  joinExpression() {
    let ekspresi = '';
    if (this.formAdd.value.expres1) {
      ekspresi += this.formAdd.value.expres1 + ' ' + this.findNameUnit(this.formAdd.value.expres2);
      if (this.formAdd.value.expres3) {
        ekspresi += ' x ' + this.formAdd.value.expres3 + ' ' + this.findNameUnit(this.formAdd.value.expres4);
        if (this.formAdd.value.expres5) {
          ekspresi += ' x ' + this.formAdd.value.expres5 + ' ' + this.findNameUnit(this.formAdd.value.expres6);
        }
      }
    }
    return ekspresi;
  }
  findNameUnit(id) {
    if (this.listUnitDesc.length > 0) {
      const unit =  _.find(this.listUnitDesc, (v) => {
        if (v.id === id) { return v; }
      });
      return unit ? unit.name : '';
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.dd1.value = null; // set value manual, karena dropdwon ini tidak masuk ke form
    AllOption.ResetFilterOption([this.dd1, this.dd2, this.dd3, this.dd4, this.dd5, this.dd6]);
    this.formAdd.reset();
    this.formAddHeader.reset();
    this.service.setListBudgetDetHeader([]);
    this.optHeader = null;
    this.coaList = '';
    this.setOptSub(0);
    this.service.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 0);
    this.bosdaAnggaranKasService.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
    this.bosdaPreviewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
    this.service.getUnitDecs();
    this.service.showAddRincian(false);
  }
  onHideAddHeader(headerId?) {
    this.formAddHeader.reset();
    // this.service.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 1, this.coaSelected.id);
    if (headerId) {
      this.formAdd.patchValue({
        headerId: headerId
      });
    }
    this.optSubHeader = [{label: '-- Pilih --', value: null}];
    this.service.showAddSubHeader(false);
  }
}
