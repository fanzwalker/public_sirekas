import { BosdaPreviewService } from './../bosda-preview/bosda-preview.service';
import { BosdaAnggaranKasService } from './../bosda-anggaran-kas/bosda-anggaran-kas.service';
import { BosdaKompPenyusunService } from './bosda-komp-penyusun.service';
import { Observable } from 'rxjs/Observable';
import { UsersService } from './../../service/users.service';
import { Component, OnInit } from '@angular/core';
import { RkaBosdaService } from '../rka-bosda.service';
import * as _ from 'lodash';
import swal from 'sweetalert2';
@Component({
  selector: 'app-bosda-komp-penyusun',
  templateUrl: './bosda-komp-penyusun.component.html',
  styleUrls: ['./bosda-komp-penyusun.component.css']
})
export class BosdaKompPenyusunComponent implements OnInit {
  unitId: number;
  activityId: number;
  paguBosda: number;
  userInfo: any;
  listBosdaBudgetDet: any[];
  bosdaBudgetDetSelected: any = [];
  listUnitDesc: any[];
  constructor(private rkaBosdaService: RkaBosdaService,
              private userService: UsersService,
              private service: BosdaKompPenyusunService,
              private bosdaAnggaranKasService: BosdaAnggaranKasService,
              private bosdaPreviewService: BosdaPreviewService) {
   this.userInfo = this.userService.getUserInfo();
  }
  ngOnInit() {
    const combine = Observable.combineLatest(
      this.rkaBosdaService.unitId$,
      this.rkaBosdaService.activityId$,
      this.service.listBosdaBudgetDet$,
      this.service.listUnitDesc$,
      this.service.paguBosda$
    );
    combine.subscribe(resp => {
      const [unitId, activityId, listBosdaBudgetDet, listUnitDesc, paguBosda] = resp;
      this.unitId = unitId;
      this.activityId = activityId;
      this.listBosdaBudgetDet = Object.assign(listBosdaBudgetDet);
      this.listUnitDesc = Object.assign(listUnitDesc);
      this.paguBosda = paguBosda;
    });
    this.service.getUnitDecs();
  }
  calculateTotal(x, y) {
    let total = 0;
    if (this.listBosdaBudgetDet) {
      // tslint:disable-next-line:prefer-const
      for (let data of this.listBosdaBudgetDet) {
        switch (x) {
          case data.chartOfAccountCode:
            if (data.chartOfAccountId === y && data.type === 2) {
              total += data.total;
            } break;
        }

      }
    }
    return total;
  }
  castSatuanUnit(a) {
    if (a) {
      const unitSatuan = _.find(this.listUnitDesc, (s) => {
        return s.id === +a;
      });
      return unitSatuan.name;
    }
  }
  btnEdit(e) {
    this.service.setDataEditRincian(e);
    this.service.getUnitDecs();
    this.service.showEditRincian(true);
  }
  btnAdd() {
    if (this.unitId) {
      if (this.paguBosda === 0) {
        swal({
          position: 'top-end',
          type: 'warning',
          title: 'Pagu Belum diset pada Plafon',
          showConfirmButton: false,
          timer: 3000
        });
      } else {
        this.service.showAddRincian(true);
        this.service.getUnitDecs();
        this.service.showAddRincian(true);
      }
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Sekolah Harus Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }

  }
  btnDelete() {
    if (this.bosdaBudgetDetSelected.length > 0) {
      swal({
        title: 'Hapus',
        text: 'Data Akan Dihapus',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal'
      }).then((result) => {
        if (result.value) {
          const listId = [];
          _.forEach(this.bosdaBudgetDetSelected, function (k) {
            listId.push(k.id);
          });
          const dataBody = {
            ids : listId
          };
          this.service.deleteBudgetDet(this.unitId, this.userInfo.stages, this.activityId, dataBody)
            .subscribe(resp => {
              this.bosdaBudgetDetSelected = [];
              this.service.getBudgetDet(this.unitId, this.userInfo.stages, this.activityId, 0);
              this.bosdaAnggaranKasService.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
              this.bosdaPreviewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
              swal({
                position: 'top-end',
                type: 'success',
                title: 'Data Berhasil Dihapus',
                showConfirmButton: false,
                timer: 3000
              });
            }, error => {
              swal({
                position: 'top-end',
                type: 'error',
                title: 'Data Gagal Dihapus [' + error.statusText + ']',
                showConfirmButton: false,
                timer: 3000
              });
            });
        } else if (result.dismiss === swal.DismissReason.cancel) {
          return false;
        }
      });
    } else {
      swal({
        position: 'top-end',
        type: 'warning',
        title: 'Data Belum Dipilih',
        showConfirmButton: false,
        timer: 3000
      });
    }
  }
}
