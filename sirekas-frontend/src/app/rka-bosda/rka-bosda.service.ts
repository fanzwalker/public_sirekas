import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../environments/environment';
import { IunitActivity } from '../interface/iunit-activity';

@Injectable()
export class RkaBosdaService {
  private unitId = new BehaviorSubject<number>(null);
  public unitId$ = this.unitId.asObservable();
  private activityId = new BehaviorSubject<number>(null);
  public activityId$ = this.activityId.asObservable();
  constructor(private http: HttpClient) { }
  setUnitAct(unitId, activityId) {
    this.unitId.next(unitId);
    return this.activityId.next(activityId);
  }
  getUnits(Stages, ExcludeType, UnitStructure, ActivityType, BudgetType) {
    const queryParam = new HttpParams()
    .set('Stages', Stages)
    .set('ExcludeType', ExcludeType)
    .set('UnitStructure', UnitStructure)
    .set('ActivityType', ActivityType)
    .set('BudgetType', BudgetType);
    return this.http.get(`${environment.url}units`, {params: queryParam});
  }
  getPrograms() {
    return this.http.get(`${environment.url}programs`);
  }
  getKegiatan(unitId, programId, stages, activityType?) {
    const params = new HttpParams()
    .set('activityType', activityType);
    if (activityType) {
      return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/programs/${programId}/activities`, {params: params})
      .map(resp => <IunitActivity[]>resp);
    } else {
      return this.http.get(`${environment.url}units/${unitId}/stages/${stages}/programs/${programId}/activities`)
      .map(resp => <IunitActivity[]>resp);
    }

  }
}
