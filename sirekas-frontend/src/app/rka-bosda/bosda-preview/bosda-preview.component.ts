import { UsersService } from './../../service/users.service';
import { BosdaPreviewService } from './bosda-preview.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IpreviewRkas } from '../../interface/ipreview-rkas';
import { IunitProgAct } from '../../interface/iunit-prog-act';
import { MenuItem } from 'primeng/primeng';
import { RkaBosdaService } from '../rka-bosda.service';
import { ReportService } from '../../service/report.service';
import { Observable } from 'rxjs/Observable';
import { IndikatorKenerjaGlobalService } from '../../shared/indikator-kenerja-global.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-bosda-preview',
  templateUrl: './bosda-preview.component.html',
  styleUrls: ['./bosda-preview.component.css']
})
export class BosdaPreviewComponent implements OnInit, OnDestroy {
  previews: IpreviewRkas[];
  unitProgAct: IunitProgAct;
  listIndikatorKinerja: any;
  itemPrints: MenuItem[];
  preProgram: any;
  preActivity: any;
  userInfo: any;
  total: number;
  constructor(private rksBosdaService: RkaBosdaService,
              private service: BosdaPreviewService,
              private reportService: ReportService,
              private userService: UsersService,
              private indikatorKinerjaService: IndikatorKenerjaGlobalService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.dataPreview$,
      this.service.unitProgAct$,
      this.indikatorKinerjaService.ListPerfBenchmarkFixed$
    );
    combine.subscribe(resp => {
      const [dataPreview, unitProgAct, listIndikatorKinerja] = resp;
      this.previews = Object.assign(dataPreview);
      this.unitProgAct = Object.assign(unitProgAct);
      this.listIndikatorKinerja = listIndikatorKinerja;
      if (this.unitProgAct.Programs) {
        this.service.getProgramById(this.unitProgAct.Programs)
        .subscribe(dataProgram => this.preProgram = Object.assign(dataProgram));
      }
      if (this.unitProgAct.Activity) {
        this.service.getActivityById(this.unitProgAct.Programs, this.unitProgAct.Activity)
        .subscribe(dataActivity => this.preActivity = Object.assign(dataActivity));
      }
      this.total = _.sum(_.filter(this.previews, e => e.type === 2)
        .map(e => e.total));
    });
  }
  convertIndikator(type) {
    switch (type) {
      case 1:
        return 'Capaian Program';
      case 2:
        return 'Masukan';
      case 3:
        return 'Keluaran';
      case 4:
        return 'Hasil';
    }
  }
  btnCetak(type) {
    const paramBody = {};
    paramBody['@unitId'] = this.unitProgAct.Units.id;
    paramBody['@activityId'] = this.unitProgAct.Activity;
    paramBody['@stageId'] = +this.userInfo.stages;
    this.reportService.execPrint('Rka221.rpt', type, paramBody)
    .subscribe(resp =>  {
      this.reportService.extractData(resp, type, 'RKA221');
    });
  }
  ngOnDestroy() {
    this.service.setPreview([]);
    this.service.setUnitProgAct([]);
  }
}
