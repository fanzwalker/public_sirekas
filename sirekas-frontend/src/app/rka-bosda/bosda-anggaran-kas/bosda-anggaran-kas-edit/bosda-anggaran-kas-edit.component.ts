import { BosdaPreviewService } from './../../bosda-preview/bosda-preview.service';
import { BosdaAnggaranKasService } from './../bosda-anggaran-kas.service';
import { RkaBosdaService } from './../../rka-bosda.service';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { IlistBudgetMonthly } from '../../../interface/ilist-budget-monthly';
import { FormGroup, FormBuilder } from '@angular/forms';
import { UsersService } from '../../../service/users.service';
import { Observable } from 'rxjs/Observable';
@Component({
  selector: 'app-bosda-anggaran-kas-edit',
  templateUrl: './bosda-anggaran-kas-edit.component.html',
  styleUrls: ['./bosda-anggaran-kas-edit.component.css']
})
export class BosdaAnggaranKasEditComponent implements OnInit {
  showThis: boolean;
  unitId: number;
  activityId: number;
  dataEdit: IlistBudgetMonthly;
  formEdit: FormGroup;
  userInfo: any;
  constructor(private fb: FormBuilder,
              private rkaBosdaService: RkaBosdaService,
              private service: BosdaAnggaranKasService,
              private bosdaPreviewService: BosdaPreviewService,
              private userService: UsersService) {
    this.userInfo = this.userService.getUserInfo();
    this.formEdit = this.fb.group({
      q1: [0],
      q2: [0],
      q3: [0],
      q4: [0]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayEditAnggaranKas$,
      this.rkaBosdaService.unitId$,
      this.rkaBosdaService.activityId$,
      this.service.dataEditAnggaranKas$
    );
    combine.subscribe(resp => {
      const [diplayEdit , unitId, activityId, dataEdit] = resp;
      this.showThis = diplayEdit;
      this.unitId = unitId;
      this.activityId = activityId;
      this.dataEdit = Object.assign(dataEdit);
      if (this.dataEdit.id) {
        this.formEdit.patchValue({
          q1: this.dataEdit.q1,
          q2: this.dataEdit.q2,
          q3: this.dataEdit.q3,
          q4: this.dataEdit.q4
        });
      }
    });
  }
  btnSimpan() {
    this.service.putBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages, this.dataEdit.id, this.formEdit.value)
      .subscribe(resp => {
        if (resp) {
          this.onHide();
          swal({
            position: 'top-end',
            type: 'success',
            title: 'Data Berhasil Diubah',
            showConfirmButton: false,
            timer: 3000
          });
        }
      }, error => {
        swal({
          position: 'top-end',
          type: 'error',
          title: 'Data Gagal Disimpan [' + error.error.error[0] + ']',
          showConfirmButton: false,
          timer: 3000
        });
      });

  }
  btnBatal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.service.setDataEditAnggaranKas([]);
    this.service.getBudgetMonthly(this.unitId, this.activityId, this.userInfo.stages);
    this.bosdaPreviewService.getPreview(this.unitId, this.activityId, this.userInfo.stages);
    this.service.showEdit(false);
  }
}
