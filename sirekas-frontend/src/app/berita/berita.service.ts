import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';

@Injectable()
export class BeritaService {
  private listBerita = new BehaviorSubject(<any>[]);
  public listBerita$ = this.listBerita.asObservable();

  private displayAdd = new BehaviorSubject<boolean>(false);
  public displayAdd$ = this.displayAdd.asObservable();

  private displayEdit = new BehaviorSubject<boolean>(false);
  public displayEdit$ = this.displayEdit.asObservable();

  private dataEdit = new BehaviorSubject(<any>[]);
  public dataEdit$ = this.dataEdit.asObservable();
  constructor(private http: HttpClient) { }

  getBerita() {
    return this.http.get(`${environment.url}messages`).map(resp => resp)
    .subscribe(data => this.setListBerita(data));
  }
  setListBerita(data) {
    return this.listBerita.next(data);
  }
  showAdd(action: boolean) {
    return this.displayAdd.next(action);
  }
  showEdit(action: boolean) {
    return this.displayEdit.next(action);
  }
  postBerita(postBody) {
    return this.http.post(`${environment.url}messages`, postBody).map(resp => <any>resp);
  }
  setDataEdit(data) {
    return this.dataEdit.next(data);
  }
  putBerita(id, postBody) {
    return this.http.put(`${environment.url}messages/${id}`, postBody).map(resp => <any>resp);
  }
  deleteBerita(id) {
    return this.http.request('DELETE', `${environment.url}messages/${id}`).map(resp => <any>resp);
  }
}
