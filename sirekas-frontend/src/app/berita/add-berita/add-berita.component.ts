import { Observable } from 'rxjs/Observable';
import { BeritaService } from './../berita.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { AllOption } from '../../shared/all-option';
import * as _ from 'lodash';
import swal from 'sweetalert2';
@Component({
  selector: 'app-add-berita',
  templateUrl: './add-berita.component.html',
  styleUrls: ['./add-berita.component.css']
})
export class AddBeritaComponent implements OnInit {
  showThis: boolean;
  formAdd: FormGroup;
  optJenisBerita: SelectItem[];
  constructor(private fb: FormBuilder,
              private service: BeritaService) {
    this.formAdd = this.fb.group({
      type: [0, Validators.required],
      title: ['', Validators.required],
      dateCreate: [''],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayAdd$
    );
    combine.subscribe(resp => {
      const [displayAdd] = resp;
      this.showThis = displayAdd;
    });
    this.setOptJenisBerita();
  }
  setOptJenisBerita() {
    const temp = [];
    _.forEach(AllOption.jenisBerita(), (x) => {
      temp.push({label: x.label, value: x.value});
    });
    this.optJenisBerita = temp;
  }
  Simpan() {
    this.formAdd.patchValue({dateCreate: AllOption.getDateNow()});
    if (this.formAdd.valid) {
      this.service.postBerita(this.formAdd.value)
      .subscribe(resp => {
        if (resp.id) {
          this.service.getBerita();
          this.onHide();
        }
      });
    }
    // swal({
    //   position: 'top-end',
    //   type: 'success',
    //   title: 'Data Berhasil Disimpan',
    //   showConfirmButton: false,
    //   timer: 3000
    // });
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formAdd.reset();
    this.service.showAdd(false);
  }

}
