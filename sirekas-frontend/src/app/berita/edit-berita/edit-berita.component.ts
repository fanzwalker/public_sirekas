import { Observable } from 'rxjs/Observable';
import { BeritaService } from './../berita.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import * as _ from 'lodash';
import swal from 'sweetalert2';
import { AllOption } from '../../shared/all-option';
@Component({
  selector: 'app-edit-berita',
  templateUrl: './edit-berita.component.html',
  styleUrls: ['./edit-berita.component.css']
})
export class EditBeritaComponent implements OnInit {
  showThis: boolean;
  formEdit: FormGroup;
  dataEdit: any;
  optJenisBerita: SelectItem[];
  constructor(private fb: FormBuilder,
              private service: BeritaService) {
    this.formEdit = this.fb.group({
      type: [0, Validators.required],
      title: ['', Validators.required],
      dateCreate: [''],
      description: ['', Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.displayEdit$,
      this.service.dataEdit$
    );
    combine.subscribe(resp => {
      const [displayEdit, dataEdit] = resp;
      this.showThis = displayEdit;
      this.dataEdit = Object.assign(dataEdit);
      this.setOptJenisBerita();
      if (this.dataEdit.id) {
        this.formEdit.patchValue({
          type: this.dataEdit.type,
          title: this.dataEdit.title,
          description: this.dataEdit.description
        });
      }
    });
  }
  setOptJenisBerita() {
    const temp = [];
    _.forEach(AllOption.jenisBerita(), (x) => {
      temp.push({label: x.label, value: x.value});
    });
    this.optJenisBerita = temp;
  }
  Simpan() {
    this.formEdit.patchValue({dateCreate: AllOption.getDateNow()});
    if (this.formEdit.valid) {
      this.service.putBerita(this.dataEdit.id, this.formEdit.value)
      .subscribe(resp => {
        if (resp.id) {
          this.service.getBerita();
          this.onHide();
        }
      });
    }
  }
  Batal() {
    this.onHide();
  }
  onHide() {
    this.formEdit.reset();
    this.service.showEdit(false);
  }
}
