import { animationPage } from './../shared/animation-page';
import { Observable } from 'rxjs/Observable';
import { BeritaService } from './berita.service';
import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';

@Component({
  selector: 'app-berita',
  templateUrl: './berita.component.html',
  styleUrls: ['./berita.component.css'],
  animations: [animationPage]
})
export class BeritaComponent implements OnInit {
  listBerita: any;
  constructor(private service: BeritaService) { }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.listBerita$
    );
    combine.subscribe(resp => {
      const [listBerita] = resp;
      this.listBerita = Object.assign(listBerita);
    });
    this.service.getBerita();
  }
  addBerita() {
    this.service.showAdd(true);
  }
  editBerita(e) {
    this.service.setDataEdit(e);
    this.service.showEdit(true);
  }
  hapusBerita(e) {
    swal({
      title: 'Hapus',
      text: 'Data Akan Dihapus ' + e.title,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal'
    }).then((result) => {
      if (result.value) {
        this.service.deleteBerita(e.id)
        .subscribe(resp => {
          this.service.getBerita();
            swal({
              position: 'top-end',
              type: 'success',
              title: e.title + ' Berhasil Dihapus',
              showConfirmButton: false,
              timer: 3000
            });
        }, error => {
          swal({
            position: 'top-end',
            type: 'error',
            title: e.title + ' Gagal Dihapus',
            showConfirmButton: false,
            timer: 3000
          });
          return false;
        });
      } else if (result.dismiss === swal.DismissReason.cancel) {
        return false;
      }
    });
  }
}
