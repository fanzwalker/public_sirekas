import { Component, OnInit } from '@angular/core';
import {TesService} from './tes.service';

@Component({
  selector: 'app-tes-page',
  templateUrl: './tes-page.component.html',
  styleUrls: ['./tes-page.component.css']
})
export class TesPageComponent implements OnInit {

  dataEmbem: any;
  constructor(private tesService: TesService) { }

  ngOnInit() {
    this.tesService.getData()
      .subscribe(resp => {
        this.dataEmbem = resp;
      });
  }

}
