import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TesService {

  constructor(private http: HttpClient) { }

  getData() {
    return this.http.get('assets/data/cars-small.json').map(
      resp => {
        return resp['data'];
      }
    );
  }
}
