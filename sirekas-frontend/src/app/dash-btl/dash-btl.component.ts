import { DashBtlService } from './dash-btl.service';
import { Observable } from 'rxjs/Observable';
import { animationPage } from './../shared/animation-page';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../service/users.service';
import { SelectItem, MenuItem } from 'primeng/api';
import swal from 'sweetalert2';
import * as _ from 'lodash';
import { IpreviewRkas } from '../interface/ipreview-rkas';
import { ReportService } from '../service/report.service';

@Component({
  selector: 'app-dash-btl',
  templateUrl: './dash-btl.component.html',
  styleUrls: ['./dash-btl.component.css'],
  animations: [animationPage]
})
export class DashBtlComponent implements OnInit, OnDestroy {
  formFilter: FormGroup;
  userInfo: any;
  listSekolah: any; optSekolah: SelectItem[];
  previews: IpreviewRkas[];
  Programs: any;
  unitSelected: any;
  itemPrints: MenuItem[];
  constructor(private fb: FormBuilder,
              private service: DashBtlService,
              private userService: UsersService,
              private reportService: ReportService) {
    this.userInfo = this.userService.getUserInfo();
    this.itemPrints = [
      {label: 'PDF', icon: 'fa fa-file-pdf-o', command: () => { this.btnCetak(1); }},
      {label: 'WORD', icon: 'fa fa-file-word-o', command: () => { this.btnCetak(2); }},
      {label: 'EXCEL', icon: 'fa fa-file-excel-o', command: () => { this.btnCetak(3); }}
    ];
    this.formFilter = this.fb.group({
      sekolah: [null, Validators.required],
      program: [null, Validators.required],
      kegiatan: [null, Validators.required]
    });
  }

  ngOnInit() {
    const combine = Observable.combineLatest(
      this.service.dataPreview$
    );
    combine.subscribe(resp => {
      const [dataPreview] = resp;
      this.previews = Object.assign(dataPreview);
    });
    if (!this.userInfo.IsAdmin) {
      this.formFilter.patchValue({
        sekolah: this.userInfo.unitId
      });
    } else {
      this.getSekolah();
    }
    this.getProgram();
  }
  getSekolah() {
    const temp = [];
    this.service.getUnits(this.userInfo.stages, 0, 4, 1, 3)
      .subscribe(resp => {
        this.listSekolah = resp;
        temp.push({label: 'Pilih Sekolah', value: null});
        _.forEach(this.listSekolah, function (e) {
          temp.push({label: e.code + ' - ' + e.name, value: e.id});
        });
        this.optSekolah = temp;
      });
  }
  getProgram() {
    const temp = [];
    this.service.getPrograms()
      .subscribe(resp => {
        if (resp.length > 0) {
          this.formFilter.patchValue({
            program: resp[0].id,
            kegiatan: 0
          });
          this.Programs = Object.assign(resp[0]);
        } else {
          this.Programs = [];
        }
        this.onChangeUnit();
      });
  }
  onChangeUnit() {
    const filterUnit = _.filter(this.listSekolah, (x) => {
      return x.id === this.formFilter.value.sekolah;
    });
    if (filterUnit.length > 0) {
      this.unitSelected = Object.assign(filterUnit[0]);
    } else {
      this.unitSelected = Object.assign([]);
    }
    this.service.getPreview(this.formFilter.value.sekolah, this.formFilter.value.kegiatan, this.userInfo.stages);
  }
  convertIndikator(type) {
    switch (type) {
      case 1:
        return 'Capaian Program';
      case 2:
        return 'Masukan';
      case 3:
        return 'Keluaran';
      case 4:
        return 'Hasil';
    }
  }
  btnCetak(type) {
    this.reportService.execPrint('units.rpt', type)
      .subscribe(resp =>  {
        this.reportService.extractData(resp, type, 'Tes');
      });
  }
  ngOnDestroy() {
    this.unitSelected = Object.assign([]);
    this.service.setPreview([]);
  }
}
